# Naming Conventions
#### JavaScript
```javascript
class DoSomeWork { }            // ตั้งชื่อ Class ต้องเป็น Upper Camel Case

const CONSTANT_NAME = 2;        // ตัวแปรประเภทค่าคงที่
```

#### HTML
```html
<div id="div-..."></div>            <!-- ถ้าเป็น Tag ที่มี ID จะต้อง Prefix ชื่อของ Tag นั้นด้วยชนิดของ Tag นั้น -->
<span id="span-login-name"></span>          
<h1 id="h1-importance"></h1>     

<div class="..."></div>             <!-- ถ้าเป็น Tag ที่มี Class ให้ระบุหน้าที่ของ Class นั้นให้ชัดเจน -->
<span class="topic-header"></span>   
<p class="amazing-text"></p>
```

# ข้อตกลงในการตั้งชื่อ Branch
**Branch Name** | **Explanation**
------------ | -------------
features-<branch-name\> | เป็น Branch สำหรับเป็น Feature ที่กำลังพัฒนาเพื่อใช้ใน Production
devs-<branch-name\>  | เป็น Branch สำหรับทดลอง Feature เพื่ออาจจะนำมาใช้ใน Production  
  
# การใช้งาน Git
#### ดึงข้อมูลล่าสุดจาก Remote มายัง Local โดยยังไม่มีการอัพเดทข้อมูลจริง
```sh
git fetch
```

#### เป็นการรวมข้อมูลระหว่าง Branch ที่กำลัง Checkout อยู่ และ Branch ที่ทำการระบุ (ถ้าข้อมูลมี Conflict จะต้องทำการแก้ไขก่อนแล้วจึงใช้คำสั่ง git merge --continue)
```sh
git merge <branch-name>
```

#### การแก้ไข Merge Conflict
เมื่อเกิด Merge Conflict แล้วระบบของ Git จะทำการแทรกข้อความเข้ามาในลักษณะนี้
```javascript
    ...
    
<<<<<<< HEAD
    const MAGIC_NUMBER = 1;
=======
    const MAGIC_NUMBER = 3;
>>>>>>> master

    ...
```
ซึ่งสามารถแก้ไขได้โดยการเลือกว่าจะทำการเก็บเวอร์ชั่นไหนเก็บไว้ เช่นถ้าต้องการเก็บ MAGIC_NUMBER = 3 ไว้
ก็สามารถทำได้โดยลบบรรทัดอื่นๆออก เหลือแค่ MAGIC_NUMBER = 3
```javascript
    ...

    const MAGIC_NUMBER = 3;

    ...
```
แล้วจึงทำการเรียกใช้งานคำสั่ง 
```sh
git merge --continue
```
  

#### คำสั่งสำหรับสร้าง Local Branch ใหม่
```sh
git branch <branch-name>
```

#### คำสั่งที่ใช้สำหรับสลับ Local Branch
```sh
git checkout <branch-name>
```

#### คำสั่งสำหรับสร้าง Local Branch ใหม่และสลับทันที
```sh
git checkout -b <branch-name>
```

#### การเพิ่ม Local Branch ที่สร้างใหม่ขึ้นไปเป็น Remote Branch
```sh
git push -u origin <new-local-branch-name>
```