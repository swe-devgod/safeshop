const fs = require('fs-extra');

fs.copy('./build', '../safeshop-backend/public', function(err) {
    if (err) {
        console.error('move-to-backend: ' + err);
    } else {
        console.log('move-to-backend: copied.');
    }
});