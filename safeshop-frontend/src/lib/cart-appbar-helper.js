export function fetchCartAppbarAmount(dispatcher) {
    fetch("/api/cart/")
        .then(res => {
            if (res.status === 200) {
                return res.json();
            } else {
                return Promise.reject();
            }
        })
        .then(json => {
            dispatcher.dispatchStatic('cart-button-appbar', { itemAmount: json.count });
        });
}