import { doAuthorization } from '../components/auth-service';

export class BaseSingleFileUploader {
    constructor(file, bufferSize = 16 * 1024) {
        this.file = file;
        this.bufferSize = bufferSize;
        this.isAbort = false;
        this.onUploadStart = null;
        this.onProgress = null;
        this.onUploadEnd = null;
    }

    upload(apiEndPoint) {
        throw new Error('NotImplementedException');
    }

    abort() {
        throw new Error('NotImplementedException');
    }
}

export class SafeShopSingleFileUploader extends BaseSingleFileUploader {
    constructor(file, bufferSize = 256 * 1024) {
        super(file, bufferSize);
    }
    
    abort() {
        this.isAbort = true;
    }

    upload(apiEndPoint, json) {
        return new Promise((resolve, reject) => {
            const readChucked = offset => {
                if (this.onProgress) {
                    this.onProgress(offset, this.file.size);
                }
                const fr = new FileReader();
                const readSize = Math.min(this.bufferSize, this.file.size - offset);
                fr.onloadend = e => {
                    if (offset < this.file.size) {
                        if (this.isAbort) {
                            fetch(apiEndPoint, {
                                method: 'DELETE',
                                headers: {
                                    'content-type': 'application/json',
                                    'accept': 'application/json'
                                },
                                body: JSON.stringify({
                                    fileName: this.file.name,
                                    fileSize: this.file.size
                                })
                            })
                            .then(() => {
                                resolve(false);
                            })
                            .catch(err => {
                                console.error('Problem occur while aborting with: ', err)
                                reject(err);
                            });
                        } else {
                            fetch(apiEndPoint, {
                                method: 'PUT',
                                headers: {
                                    'content-type': 'application/octet-stream',
                                    'accept': 'application/json'
                                },
                                body: fr.result
                            })
                            .then(() => {
                                readChucked(offset + readSize);
                            })
                            .catch(err => {
                                console.error('Problem occur while uploading file with: ', err);
                                reject(err);
                            });
                        }
                    } else {     
                        if (this.onUploadEnd) {
                            this.onUploadEnd();
                        }                       
                        resolve(true);
                    }
                }
                fr.readAsArrayBuffer(this.file.slice(offset, offset + readSize));
            }

            fetch(apiEndPoint, {
                method: 'POST',
                headers: {
                    'content-type': 'application/json',
                    'accept': 'application/json'
                },
                body: JSON.stringify({
                    fileName: this.file.name,
                    fileSize: this.file.size,
                    ...json
                })
            })
            .then(res =>{
                doAuthorization(res, {
                    200: true
                });
                if(res.status == 200){
                    res.json().then(json => {
                        if (this.onUploadStart) {
                            this.onUploadStart(json);
                        }
                        readChucked(0);
                    });
                }
                else {
                    console.log(res);
                }
               
            })
            .catch(err => { 
                console.error('Cannot upload file with error: ', err)
                reject(err);
            });
        });
    }
}