import { BaseSingleFileUploader } from "./safeshop-uploader";

export class SafeShopSingleFileUploader2 extends BaseSingleFileUploader {
    constructor(file, bufferSize = 256 * 1024) {
        super(file, bufferSize);
    }

    abort() {
        this.isUploading = false;
        this.isAbort = true;
    }

    async upload(apiEndPoint, json) {
        if (!!!this.isUploading) {
            try {
                const response = await fetch(apiEndPoint, {
                    method: 'POST',
                    headers: {
                        'content-type': 'application/json',
                        'accept': 'application/json'
                    },
                    body: JSON.stringify({
                        fileName: this.file.name,
                        fileSize: this.file.size,
                        ...json
                    })
                });
                const resJson = await response.json();
                console.log(resJson);
            } catch (err) {
                this.abort();
                console.log('SafeShopSingleFileUploader2 error ', err);
                return;
            }

            const totalUploadingSize = this.file.size;
            let curUploadingSize = 0;
            while (curUploadingSize < totalUploadingSize) {
                if (this.onProgress) {
                    this.onProgress(curUploadingSize, totalUploadingSize);
                }

                let result = null;
                try {
                    result = await new Promise(async (resolve, reject) => {
                        const readingSize = Math.min(
                            this.bufferSize, 
                            totalUploadingSize - curUploadingSize);

                        const fr = new FileReader();
                        fr.onerror = _ => reject(false);
                        fr.onabort = _ => reject(true);
                        fr.onloadend = e => {
                            if (this.isAbort) {
                                reject(true);
                            }
                            resolve({ buffer: fr.result, size: readingSize });
                        };

                        fr.readAsArrayBuffer(
                            this.file.slice(
                                curUploadingSize, 
                                curUploadingSize + readingSize));
                    });
                } catch (err) {
                    console.log('SafeShopSingleFileUploader2 error ', err);
                    this.abort();
                    if (typeof err === 'boolean') {
                        await fetch(apiEndPoint, {
                            method: 'DELETE',
                            headers: {
                                'content-type': 'application/json',
                                'accept': 'application/json'
                            },
                            body: JSON.stringify({
                                fileName: this.file.name,
                                fileSize: this.file.size
                            })
                        });
                    }
                    break;
                }

                let putResult = null;
                try {
                    putResult = await fetch(apiEndPoint, {
                        method: 'PUT',
                        headers: {
                            'content-type': 'application/octet-stream',
                            'accept': 'application/json'
                        },
                        body: result.buffer
                    });
                } catch (err) {
                    console.log('SafeShopSingleFileUploader2 error ', err);
                    this.abort();
                    break;
                }
                curUploadingSize += result.size;
            }

            if (this.onProgress) {
                this.onProgress(totalUploadingSize, totalUploadingSize);
            }

            if (this.onUploadEnd) {
                this.onUploadEnd();
            }

            this.isUploading = !!!this.isUploading;
        }
    }
}