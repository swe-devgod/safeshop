class PeriodSchduler {
    static schdule(func, period) {
        const task  = new PeriodSchdulerTask(func, period);
        task.execute();
        return task;
    }

    static cancel(task) {
        task.cancel();
    }
}

class PeriodSchdulerTask {
    constructor(func, period) {
        this.isRunning = true;
        this.func = func;
        this.period = period;
    }

    execute() {
        const innerFunc = () => {
            if (this.isRunning) {
                const startTime = new Date().getTime();
                this.func();
                const endTime = new Date().getTime();
                
                if (endTime - startTime >= this.period) {
                    var time = 0;
                } else {
                    var time = this.period - (endTime - startTime);
                }
                setTimeout(innerFunc, time);
            }
        };
        setTimeout(innerFunc, this.period);
    }
s
    cancel() {
        this.isRunning = false;
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

var task = PeriodSchduler.schdule(async() => {
    console.log('test');
}, 1000);
PeriodSchduler.cancel(task);