import React from 'react';
import { Link, Redirect, Router } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ProfileInformationView from './components/';
import Paper from '@material-ui/core/Paper';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import Divider from '@material-ui/core/Divider';

import ProgressView from '../../components/progress-view';

const styles = theme => ({
    paper: {
        padding: theme.spacing.unit * 4
    }
});


class ProfileView extends React.Component {
    state = {
        mobileOpen: false,
    };
    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    componentDidUpdate() {

        function SideBar() {
            return (
                <React.Fragment>
                    <Link to="/profile">
                        <ListItem button>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="User Profile" />
                        </ListItem>
                    </Link>
                    <Divider />
                    <Link to="/buyer-management">
                        <ListItem button>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText primary="Buyer Management" />
                        </ListItem>
                    </Link>
                    <Divider />
                    <Link to="/seller-management">
                        <ListItem button>
                        <ListItemIcon>
                            <SendIcon />
                        </ListItemIcon>
                        <ListItemText primary="Seller Management" />
                        </ListItem>
                    </Link>
                    <Divider />
                </React.Fragment>
            );
        }

        this.props.setSideBarItems(
            <Router history={this.props.browserHistory}>
                <SideBar />
            </Router>
        );
    }

    render() {
        if (!!!this.props.knownState) {
            return <ProgressView />
        }
        
        if (this.props.redirectToIndex) {
            return <Redirect to="/" />
        }

        const { classes } = this.props;

        return (
            <Paper className={classes.paper}>
                <ProfileInformationView 
                    onSubmitButtonClicked={this.props.onSubmitButtonClicked}
                    onSubmitPicture={this.props.onSubmitPicture}
                    chProfile={this.props.chProfile}

                    clearCh={this.props.clearCh}
                    redirectToProfile={this.props.redirectToProfile} 
                    onEditDisplayName={this.props.onEditDisplayName}
                    onEditFirstName={this.props.onEditFirstName}
                    onEditMiddleName={this.props.onEditMiddleName}
                    onEditLastName={this.props.onEditLastName}
                    onEditEmail={this.props.onEditEmail}
                    onEditTel={this.props.onEditTel}
                    onEditAddr={this.props.onEditAddr}
                    onEditPassword={this.props.onEditPassword}
                    
                    profileJson={this.props.profileJson}/>
            </Paper>
        );
    }
}

ProfileView.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,

    knownState: PropTypes.bool.isRequired,
    redirectToIndex: PropTypes.bool.isRequired,
    redirectToProfile: PropTypes.bool.isRequired,

    chProfile: PropTypes.array.isRequired,
    profileJson: PropTypes.array.isRequired,
    formData: PropTypes.array.isRequired,

    onSubmitButtonClicked: PropTypes.func.isRequired,
    clearCh: PropTypes.func.isRequired,
    onEditDisplayName: PropTypes.func.isRequired,
    onEditFirstName: PropTypes.func.isRequired,
    onEditMiddleName: PropTypes.func.isRequired,
    onEditLastName: PropTypes.func.isRequired,
    onEditEmail: PropTypes.func.isRequired,
    onEditTel: PropTypes.func.isRequired,
    onEditAddr: PropTypes.func.isRequired,
    onEditPassword: PropTypes.func.isRequired,
};

export default withStyles(styles, { withTheme: true })(ProfileView);