import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import classNames from 'classnames';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Card from '@material-ui/core/Card';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';

import UploadPicture from './upload-picture-view';
import { SafeShopSingleFileUploader } from '../../../lib/safeshop-uploader';
import PropTypes from 'prop-types';

const styles = theme =>({
    button: {
        margin: theme.spacing.unit,
        '&:hover' : {
            color: '#FF5733',
            background: '#f3f5f8'
        },
    },
    text:{
        fontSize:'75%'
    },
    griddatabox:{
        padding:'5px 20px',
    },
    bigAvatar: {
        width: 175,
        height: 175 ,
        marginTop:'10px',
    },
    text:{
        fontSize:'75%'
    },
    errorBox:{
        display: 'flex',
        flexDirection: 'column',
        background: '#d32f2f',
        alignItems: 'center',
        color: '#FFFFFF',
        padding: theme.spacing.unit * 1.5,
    },
    errorCard:{
        minWidth:'50%',
        maxWidth:'60%',
        display: 'flex',
        flexDirection: 'column',
        background: 'red',
        alignItems: 'center',
        color: '#FFFFFF',
        fontSize:'13px'
    },
});
function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class ProfileInformationView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadedInPercent: 0,
        };
        this.formData = {
            username: '',
            curPassword: '',
            newPassword: '',
            confirmPassword:'',
            displayName: '',
            firstName: '',
            middleName: '',
            lastName: '',
            email: '',
            tel: '',
            addr: '',
            picture:'',
            registerDate:'',
            file:'',            
        };
        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
    }
    
    init = () =>{
        this.formData.username = this.props.profileJson.usernameDB;
        this.formData.displayName = this.props.profileJson.displayNameDB;
        this.formData.firstName = this.props.profileJson.firstNameDB;
        this.formData.middleName = this.props.profileJson.middleNameDB;
        this.formData.lastName = this.props.profileJson.lastNameDB;
        this.formData.email = this.props.profileJson.emailDB;
        this.formData.tel = this.props.profileJson.telDB;
        this.formData.addr = this.props.profileJson.addressDB;
        this.formData.picture = this.props.profileJson.picture;
        this.formData.registerDate = this.props.profileJson.registerDate;
        this.formData.curPassword = '';
        this.formData.newPassword = '';
        this.formData.confirmPassword = '';
        
    }
    componentWillMount(){
        this.init();
    }
    
    handleCloseCancel = () => {
        this.props.onCloseCancel();
        this.init();
    };
    handleClickOpen = () => {
        if(this.formData.firstName !== '' &&  this.formData.lastName !== '' && this.formData.displayName !== '' && this.formData.email !== '' && this.formData.tel !== ''  && this.formData.addr !== '' 
        && ((this.formData.confirmPassword === this.formData.newPassword) || (this.formData.newPassword === '' && this.formData.confirmPassword === ''))){
            this.props.onClickOpen();
        }
    };
    onClickChangePassword = () => {
        this.props.onClickChangePassword();
    }
    
    handleSubmitdata = () =>{
        this.props.onSubmitButtonClicked(this.formData,this.props.profileJson.picture);
        this.props.onCloseCancel();
    }
    onSubmitButtonClicked(formData) {
        const body = new FormData();
        body.append('picture',formData.file);
        this.props.onSubmitPicture(body,formData.newImg,this.formData);
    };
    ch = () =>{
        //console.log(this.formData);
        //console.log(this.props.profileJson);
        if((this.formData.displayName != this.props.profileJson.displayNameDB)|| 
            (this.formData.firstName != this.props.profileJson.firstNameDB)|| 
            (this.formData.middleName != this.props.profileJson.middleNameDB) || 
            (this.formData.lastName != this.props.profileJson.lastNameDB) || 
            (this.formData.tel != this.props.profileJson.telDB) || 
            (this.formData.addr != this.props.profileJson.addressDB) || 
            (this.formData.email != this.props.profileJson.emailDB)){
            return true;
        }
        return false;
    }
    render() {
        const { classes } = this.props;
        const{
            picture,
            registerDate,
        } = this.props.profileJson;
        return (
            
            <React.Fragment>
                <Grid item xs={12} container direction='row-reverse'>
                    <Grid item xs = {12} md = {5}>
                        <center>
                            <Avatar
                                alt="Adelle Charles"
                                src={picture}
                                className={classNames(classes.avatar, classes.bigAvatar)}
                            />
                            <UploadPicture
                                uploadedInPercent={this.state.uploadedInPercent} 
                                onSubmitButtonClicked={this.onSubmitButtonClicked}/>
                            <text style={{fontSize:'80%'}}>Registration Date: {new Date(registerDate).toDateString()}</text>                                        
                        </center>
                    </Grid>

                    <Grid item xs = {12} md = {7}>
                        <Grid item xs={12} container direction='row'>
                            <Grid item xs={12} md={6} className={classes.griddatabox}>
                                <form noValidate autoComplete="off">
                                    <text>Username</text><br></br>
                                    <TextField
                                        disabled
                                        id="outlined-username"
                                        value = {this.formData.username}
                                        variant="outlined"
                                        fullWidth = "true"
                                        style={{
                                            background:'#EFEFF0',
                                        }}
                                    />
                                </form>
                            </Grid>
                            <Grid item xs={12} md={6} className={classes.griddatabox}>
                                <form noValidate autoComplete="off">
                                    <text>Display Name</text><br></br>
                                    <TextField
                                        required
                                        id="outlined-firstname"
                                        value = {this.formData.displayName}
                                        fullWidth = "true"
                                        variant="outlined"
                                        onChange={e => 
                                            {
                                                this.props.onEditDisplayName(e)
                                                this.formData.displayName = e.target.value
                                            }
                                    }/>
                                </form>
                                {
                                    ((!this.props.chProfile.displayName || this.formData.displayName === '')) && 
                                    <Paper className={classes.errorBox} >
                                            Display Name must be at least 5 characters long and only alphabet and number are allowed.
                                    </Paper>
                                }
                            </Grid>
                        </Grid>

                        <Grid item xs={12} container direction='row'>
                            <Grid item xs={12} md={4} className={classes.griddatabox}>
                                <form noValidate autoComplete="off">
                                    <text>First Name</text><br></br>
                                    <TextField
                                        id="outlined-firstname"
                                        value = {this.formData.firstName}
                                        fullWidth = "true"
                                        variant="outlined"
                                        onChange={e => 
                                            {
                                                this.props.onEditFirstName(e)
                                                this.formData.firstName = e.target.value
                                            }   
                                    }/>
                                    {
                                        (!this.props.chProfile.firstName || this.formData.firstName === '') &&
                                        <Paper className={classes.errorBox} >
                                            First Name mustn't be empty and only allow in alphabet.
                                        </Paper>
                                    }   
                                </form>
                            </Grid>
                            <Grid item xs={12} md={4} className={classes.griddatabox}>
                                <form noValidate autoComplete="off">
                                    <text>Middle Name</text><br></br>
                                    <TextField
                                        required
                                        id="outlined-middlename"
                                        value = {this.formData.middleName}
                                        fullWidth = "true"
                                        variant="outlined"
                                        onChange={e => 
                                            {
                                                this.props.onEditMiddleName(e)
                                                this.formData.middleName = e.target.value
                                            }   
                                    }/>  
                                    {
                                        (!this.props.chProfile.middleName) &&
                                        <Paper className={classes.errorBox} >
                                            Middle Name is only allow in alphabet.
                                        </Paper>
                                    } 
                                </form>
                            </Grid>
                            <Grid item xs={12} md={4} className={classes.griddatabox}>
                                <form noValidate autoComplete="off">
                                    <text>Last Name</text><br></br>
                                    <TextField
                                        required
                                        id="outlined-lastname"
                                        value = {this.formData.lastName}
                                        fullWidth = "true"
                                        variant="outlined"
                                        onChange={e => 
                                            {
                                                this.props.onEditLastName(e)
                                                this.formData.lastName = e.target.value
                                            }   
                                    }/> 
                                    {
                                        (!this.props.chProfile.lastName || this.formData.lastName === '') &&
                                        <Paper className={classes.errorBox} >
                                            Last Name mustn't be empty and only allow in alphabet.
                                        </Paper>
                                    } 
                                </form>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} className={classes.griddatabox}>
                            <form noValidate autoComplete="off">
                                <text>Address</text><br></br>
                                <TextField
                                    id="outlined-address"
                                    value = {this.formData.addr}
                                    fullWidth = "true"
                                    variant="outlined"
                                    multiline rows="4" 
                                    onChange={e => 
                                        {
                                            this.props.onEditAddr(e)
                                            this.formData.addr = e.target.value
                                        }   
                                }/>
                                {
                                        (this.formData.addr === '') &&
                                        <Paper className={classes.errorBox} >
                                            Address mustn't be empty.
                                        </Paper>
                                    }
                            </form>
                        </Grid>

                        <Grid item xs={12} container direction='row' >
                            <Grid item xs={12} md={6} className={classes.griddatabox}>
                                <form noValidate autoComplete="off">
                                    <text>Phone Number</text><br></br>
                                    <TextField
                                        id="outlined-phonnumber"
                                        type = "number"
                                        value = {this.formData.tel}
                                        fullWidth = "true"
                                        variant="outlined"
                                        onChange={e => 
                                            {
                                                this.props.onEditTel(e)
                                                this.formData.tel = e.target.value
                                            }   
                                    }/> 
                                    {
                                        (!this.props.chProfile.tel || this.formData.tel === '') &&
                                        <Paper className={classes.errorBox} >
                                            Phone Number mustn't be empty.
                                        </Paper>
                                    }
                                </form>
                            </Grid>
                            <Grid item xs={12} md={6} className={classes.griddatabox}>
                                <form noValidate autoComplete="off">
                                    <text>Email</text><br></br>
                                    <TextField
                                        id="outlined-email"
                                        value = {this.formData.email}
                                        type = "email"
                                        autoComplete="email"
                                        fullWidth = "true"
                                        variant="outlined"
                                        onChange={e => 
                                            {
                                                this.props.onEditEmail(e)
                                                this.formData.email = e.target.value     
                                            }   
                                    }/> 
                                </form>
                                {
                                        (!this.props.chProfile.email || this.formData.email === '') &&
                                        <Paper className={classes.errorBox} >
                                            Email incorrect syntax.
                                        </Paper>
                                }
                            </Grid>
                        </Grid>

                        <Grid item xs={12} className={classes.griddatabox}>
                            <form noValidate autoComplete="off">
                                {
                                    !this.props.isActive && 
                                    <div>
                                        <text>Password</text><br></br>
                                        <TextField
                                            disabled
                                            type="password"
                                            id="outlined-password"
                                            value="........"
                                            variant="outlined"
                                        />            
                                        <Button variant="outlined" color="secondary" aria-label="Edit" className={classes.button} size='small' onClick={this.onClickChangePassword}>
                                            <Icon>edit_icon</Icon>
                                            &nbsp;Change Password
                                        </Button>
                                    </div>
                                }
                                {
                                    this.props.isActive && 
                                    <div>
                                    <text>Current Password</text><br></br>
                                        <TextField
                                            type="password"
                                            id="outlined-password"
                                            variant="outlined"
                                            value={this.formData.curPassword}
                                            onChange={e => 
                                                {
                                                    this.props.onEditPassword(e)
                                                    this.formData.curPassword = e.target.value     
                                                }  
                                            }
                                        /><br></br>
                                        {
                                            //console.log(this.formData.curPassword)
                                        }
                                        <text>New Password</text><br></br>
                                        <TextField
                                            type="password"
                                            id="outlined-password"
                                            variant="outlined"
                                            value={this.formData.newPassword}
                                            onChange={e =>
                                                { 
                                                    this.props.onEditPassword(e)
                                                    this.formData.newPassword = e.target.value     
                                                }
                                            }
                                        /><br></br>
                                        <text>Confirm New Password</text><br></br>
                                        <TextField
                                            type="password"
                                            id="outlined-password"
                                            variant="outlined"
                                            value={this.formData.confirmPassword}
                                            onChange={e => 
                                                {
                                                    this.props.onEditPassword(e)
                                                    this.formData.confirmPassword = e.target.value     
                                                }  
                                            }
                                        /><br></br>
                                        {
                                            ((this.formData.confirmPassword !== this.formData.newPassword)&&this.formData.newPassword !== '')&&
                                            <Card className={classes.errorCard} >
                                                New Password doesn't match.
                                            </Card>
                                        }
                                        {
                                            (!this.props.chProfile.password && this.props.changePass)&&
                                            <Card className={classes.errorCard} >
                                                Password doesn't change.
                                            </Card>
                                        }
                                    </div>
                                }
                                {
                                    //console.log(this.formData)
                                }
                                {
                                    (
                                    this.props.isActive ||
                                    this.ch()
                                    )
                                    && 
                                    <div>
                                        <Button variant="contained" size="small" className={classes.button} onClick={this.handleClickOpen}>
                                            <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                                                &nbsp;Save Changed
                                        </Button>
                                        <Dialog
                                            open={this.props.open}
                                            TransitionComponent={Transition}
                                            keepMounted
                                            aria-labelledby="alert-dialog-slide-title"
                                            aria-describedby="alert-dialog-slide-description"
                                        >
                                            <DialogTitle id="alert-dialog-slide-title">
                                                {"Are you sure?"}
                                            </DialogTitle>
                                            <DialogContent>
                                                <DialogContentText id="alert-dialog-slide-description">
                                                    Are you sure to change profile?
                                                </DialogContentText>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={this.handleSubmitdata} color="primary">
                                                    Confirm
                                                </Button>
                                                <Button onClick={this.handleCloseCancel} color="primary">
                                                    Cancel
                                                </Button>
                                            </DialogActions>
                                        </Dialog>
                                        <Button variant="contained" color="secondary" className={classes.button} onClick={this.handleCloseCancel}>
                                            Cancel
                                        </Button>
                                    </div>
                                }
                            </form>
                        </Grid>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}
export default withStyles(styles)(ProfileInformationView);

ProfileInformationView.propTypes ={
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,

    chProfile: PropTypes.array.isRequired,
    profileJson: PropTypes.array.isRequired,

    onSubmitButtonClicked: PropTypes.func.isRequired,
    onEditDisplayName: PropTypes.func.isRequired,
    onEditFirstName: PropTypes.func.isRequired,
    onEditMiddleName: PropTypes.func.isRequired,
    onEditLastName: PropTypes.func.isRequired,
    onEditEmail: PropTypes.func.isRequired,
    onEditTel: PropTypes.func.isRequired,
    onEditAddr: PropTypes.func.isRequired,
    onEditPassword: PropTypes.func.isRequired,


    isActive: PropTypes.bool.isRequired,
    open: PropTypes.bool.isRequired,
    changePass: PropTypes.bool.isRequired,
    
    onCloseCancel: PropTypes.func.isRequired,
    onClickOpen: PropTypes.func.isRequired,
    onClickChangePassword: PropTypes.func.isRequired,
};