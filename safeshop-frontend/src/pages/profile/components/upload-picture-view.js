import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import classNames from 'classnames';
import './upload-picture-style.css';

import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';


const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
        '&:hover' : {
            color: '#FF5733',
            background: '#F3F5F8'
        },
    },
    text:{
        fontSize:'75%'
    },    
    bigAvatar: {
        width: 175,
        height: 175 ,
        marginTop:'10px',
    },
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class UploadPicture extends Component {     
    constructor(props) {
        super(props);
        this.state = {
            file: null,
            open:false,
            isActive:false,
            inputKey: Date.now()
        };

        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
    }
    onSubmitButtonClicked() {
        this.props.onSubmitButtonClicked({
            file: this.state.file,
            newImg: this.state.imageData
        });
        this.setState({
            open:false,
        })
        this.handleClose()
    }
    handleClose = () => {
        this.setState({
            open:false,
            isActive:false,
            inputKey: Date.now()
        })
        if (this.props.onChange) this.props.onChange(null);
        
    };
    handleClickOpen = () => {
        this.setState({ open: true });
    };
    
    render() {
        const { classes } = this.props; 
        return (
            <React.Fragment>
                <form onSubmit={e => e.preventDefault()}>
                    <div class="upload-btn-wrapper" xs={12}>
                        <IconButton color="primary" className={classes.button} component="span">
                            <PhotoCamera /><text className={classes.text}>&nbsp;Choose Picture</text>
                        </IconButton>
                        <input id="inputPic" accept="image/*" type="file" key={this.state.inputKey}
                            onChange={e => {
                                this.setState({ 
                                    file: e.target.files[0],
                                    isActive: true
                                });
                                if (e.target.files && e.target.files[0]) {
                                    let reader = new FileReader;
                                    reader.onload = (e) => {
                                        this.setState({
                                            imageData: e.target.result
                                        })
                                    }
                                    reader.readAsDataURL(e.target.files[0]);
                                }
                            }}
                        />
                    </div>
                </form>
                <div xs={12}>
                    {
                        (this.state.isActive) &&
                        <div>
                            <Avatar
                                    src = {this.state.imageData}
                                    className={classNames(classes.avatar, classes.bigAvatar)}
                            />
                            <Button variant="contained" size="small" className={classes.button} onClick={this.handleClickOpen}>
                                <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                                    &nbsp;Save 
                            </Button>
                            <Dialog
                                open={this.state.open}
                                TransitionComponent={Transition}
                                keepMounted
                                onClose={this.handleClose}
                                aria-labelledby="alert-dialog-slide-title"
                                aria-describedby="alert-dialog-slide-description"
                            >
                                <DialogTitle id="alert-dialog-slide-title">
                                    {"Are you sure?"}
                                </DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-slide-description">
                                        Are you sure to Save?
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.onSubmitButtonClicked} color="primary"  >
                                        Confirm
                                    </Button>
                                    <Button onClick={this.handleClose} color="primary">
                                        Cancle
                                    </Button>
                                </DialogActions>
                            </Dialog>
                            <Button variant="contained" color="secondary" className={classes.button} onClick={this.handleClose}>
                                Cancle
                            </Button>
                        </div>
                    }
                </div>
            </React.Fragment>
        );
    }
}
  
export default withStyles(styles)(UploadPicture);