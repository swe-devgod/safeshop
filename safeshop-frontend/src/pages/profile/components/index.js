import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import ProfileInformationView from './profile-information-view';
import PropTypes from 'prop-types';

export default class ProfileInformation extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isActive: false,
            open: false,
            changePass:false,
        };
        this.handleCloseCancel = this.handleCloseCancel.bind(this);
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.onClickChangePassword = this.onClickChangePassword.bind(this);

    }
    handleCloseCancel = () => {
        this.setState({
            isActive: false,
            changePass:false,
            open:false,
        });
        this.props.clearCh();
    };
    handleClickOpen = () => {
        this.setState({ open: true });
    };
    onClickChangePassword = () => {
        this.setState({
            isActive:true,
            changePass:false
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <Grid item xs = {12}>
                    <ProfileInformationView 
                        onSubmitButtonClicked={this.props.onSubmitButtonClicked}
                        onSubmitPicture={this.props.onSubmitPicture}

                        chProfile={this.props.chProfile}
                        
                        onEditDisplayName={this.props.onEditDisplayName}
                        onEditFirstName={this.props.onEditFirstName}
                        onEditMiddleName={this.props.onEditMiddleName}
                        onEditLastName={this.props.onEditLastName}
                        onEditEmail={this.props.onEditEmail}
                        onEditTel={this.props.onEditTel}
                        onEditAddr={this.props.onEditAddr}
                        onEditPassword={this.props.onEditPassword}

                        isActive={this.state.isActive}
                        open={this.state.open}
                        changePass={this.state.changePass}
                        
                        onCloseCancel={this.handleCloseCancel}
                        onClickOpen={this.handleClickOpen}
                        onClickChangePassword={this.onClickChangePassword}
                        profileJson={this.props.profileJson}/>
                </Grid>
            </React.Fragment>
        );
    }
}
ProfileInformation.propTypes ={
    clearCh: PropTypes.func.isRequired,
    redirectToProfile: PropTypes.bool.isRequired,
    
    
    chProfile: PropTypes.array.isRequired,
    profileJson: PropTypes.array.isRequired,

    onSubmitButtonClicked: PropTypes.func.isRequired,
    onEditDisplayName: PropTypes.func.isRequired,
    onEditFirstName: PropTypes.func.isRequired,
    onEditMiddleName: PropTypes.func.isRequired,
    onEditLastName: PropTypes.func.isRequired,
    onEditEmail: PropTypes.func.isRequired,
    onEditTel: PropTypes.func.isRequired,
    onEditAddr: PropTypes.func.isRequired,
    onEditPassword: PropTypes.func.isRequired,
    
    handleCloseCancel: PropTypes.func.isRequired,
    handleClickOpen: PropTypes.func.isRequired,
    onClickChangePassword: PropTypes.func.isRequired,
};