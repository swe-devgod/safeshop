import React, { Component } from 'react';
import ProfileView from './profile-view';
import { doAuthorization } from '../../components/auth-service';
import PropTypes from 'prop-types';

export default class ProfilePage extends Component {
    constructor(props) {
        super(props);
        this.state={
            redirectToProfile:false,
            profileJson: {
                usernameDB:'',
                displayNameDB:'',
                firstNameDB:'',
                middleNameDB:'',
                lastNameDB:'',
                addressDB:'',
                telDB:'',
                emailDB:'',
                picture:'',
                registerDate:'',
            },
            chProfile:{
                displayName:true,
                firstName:true,
                password:true,
                middleName:true,
                lastName:true,
                tel:true,
                email:true,
            },
            knownState: false,
            redirectToIndex: false
        }
        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
        this.onEditDisplayName = this.onEditDisplayName.bind(this);
        this.onEditFirstName = this.onEditFirstName.bind(this);
        this.onEditMiddleName = this.onEditMiddleName.bind(this);
        this.onEditLastName = this.onEditLastName.bind(this);
        this.onEditEmail = this.onEditEmail.bind(this);
        this.onEditTel = this.onEditTel.bind(this);
        this.onEditAddr = this.onEditAddr.bind(this);
        this.onEditPassword = this.onEditPassword.bind(this);
        this.clearCh = this.clearCh.bind(this);
    }

    onEditDisplayName(e){
        this.setState({
            profile:{displayName:e.target.value}
        })
    }
    onEditFirstName(e){
        this.setState({
            profile:{firstName:e.target.value}
        })
    }
    onEditMiddleName(e){
        this.setState({
            profile:{middleName:e.target.value}
        })
    }
    onEditLastName(e){
        this.setState({
            profile:{lastName:e.target.value}
        })
    }
    onEditEmail(e){
        this.setState({
            profile:{email:e.target.value}
        })
    }
    onEditTel(e){
        this.setState({
            profile:{tel:e.target.value}
        })
    }
    onEditAddr(e){
        this.setState({
            profile:{addr:e.target.value}
        })
    }
    onEditPassword(e){
        this.setState({
            profile:{password:e.target.value}
        })
    }
    clearCh(){
        this.setState({chProfile :{
            displayName:true,
            firstName:true,
            password:true,
            middleName:true,
            lastName:true,
            tel:true,
            email:true,
        } });
    }
    componentDidMount() {
        document.title = "Profile - SafeShop";
        //this.props.dySec.setSideBarItems(PROFILE_DRAWER_ITEMS);
        
        fetch("/api/user/profile")
            .then(res => { 
                doAuthorization(res, {
                    200: true
                });
                if (res.status === 200) {   
                    res.json().then(json => {

                        this.setState({
                            knownState: true,
                            profileJson :{
                                usernameDB:json.username,
                                displayNameDB:json.displayName,
                                firstNameDB:json.firstName,
                                middleNameDB:json.middleName,
                                lastNameDB:json.lastName,
                                addressDB:json.address,
                                telDB:json.tel,
                                emailDB:json.email,
                                picture:json.picture,
                                registerDate:json.registerDate
                            }
                        });
                    });
                } else {
                    this.setState({
                        knownState: true,
                        redirectToIndex: true
                    })
                }
            })
            .catch(err => console.error(err));
    }
    onSubmitPicture = (formData,newImg,nowData) =>{
        fetch('/api/user/profile/image', {
            method: 'POST',
            body: formData
        }).then(res => {
            if (res.status == 200) {
                this.setState({profileJson :{
                    usernameDB:nowData.username,
                    displayNameDB:nowData.displayName,
                    firstNameDB:nowData.firstName,
                    middleNameDB:nowData.middleName,
                    lastNameDB:nowData.lastName,
                    addressDB:nowData.addr,
                    telDB:nowData.tel,
                    emailDB:nowData.email,
                    picture:newImg,
                    registerDate:nowData.registerDate
                } });
                //console.log(res);
            } 
            else {
                //console.log(res)
            }
        });
    }

    onSubmitButtonClicked(formData,nowPic) {
        fetch('/api/user/profile/', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
            }).then(res => {
            if (res.status == 200) {
                //console.log(res);
                this.setState({profileJson :{
                    usernameDB:formData.username,
                    displayNameDB:formData.displayName,
                    firstNameDB:formData.firstName,
                    middleNameDB:formData.middleName,
                    lastNameDB:formData.lastName,
                    addressDB:formData.addr,
                    telDB:formData.tel,
                    emailDB:formData.email,
                    picture:nowPic,
                    registerDate:formData.registerDate
                } });
                this.setState({chProfile :{
                    displayName:true,
                    firstName:true,
                    password:true,
                    middleName:true,
                    lastName:true,
                    tel:true,
                    email:true,
                } });
            } 
            else {
                //console.log(res)
                res.json().then(json => {
                    this.setState({
                        knownState: true,
                        chProfile :json ,
                    });
                });
            }
        });
    }
    render() {
        return <ProfileView 
                    knownState={this.state.knownState} 
                    redirectToIndex={this.state.redirectToIndex} 
                    onSubmitButtonClicked={this.onSubmitButtonClicked}
                    onSubmitPicture={this.onSubmitPicture}
                    redirectToProfile={this.state.redirectToProfile}  
                    chProfile={this.state.chProfile}
                    clearCh={this.clearCh}
                    
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    browserHistory={this.props.history}

                    onEditDisplayName={this.onEditDisplayName}
                    onEditFirstName={this.onEditFirstName}
                    onEditMiddleName={this.onEditMiddleName}
                    onEditLastName={this.onEditLastName}
                    onEditEmail={this.onEditEmail}
                    onEditTel={this.onEditTel}
                    onEditAddr={this.onEditAddr}
                    onEditPassword={this.onEditPassword}

                    profileJson={this.state.profileJson}/>
    }
}

ProfilePage.propTypes = {
    dySec : PropTypes.any.isRequired        
};