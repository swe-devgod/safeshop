import React from 'react';
import { PropTypes } from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import { withStyles, Typography } from '@material-ui/core';

const styles = theme => ({
    imgProduct: {
        width: '100%',
        display: 'block',
    },
    imgWraper: {
        maxWidth: 125,
        margin: '0 auto',
        overflow: 'hidden',
    },
    select: {
        minWidth: 200,
    },
    priceListItem: {
        marginBottom: 8,
    }
});

class ProductListView extends React.Component {

    render() {
        const { classes, productList } = this.props;

        return (
            <Grid container direction="column">
                { productList.map( (item,index) =>
                    <React.Fragment key={item.id}>
                        <Grid item container spacing={16} style={{padding: 16}}>
                            <Grid item>
                                <Paper className={classes.imgWraper}>
                                <img className={classes.imgProduct} src={item.topic.thumbnail} />
                                </Paper>
                            </Grid>
                            <Grid item xs>
                                <Typography variant="body1">{item.topic.topicName}</Typography>
                                <Typography variant="body1" color="textSecondary">จำนวน {item.amount} ชิ้น</Typography>
                            </Grid>
                            <Grid item container direction="column" xs={12}>
                                <Grid item container className={classes.priceListItem}> 
                                    <Grid item>
                                        <Typography variant="body1">วิธีการขนส่ง</Typography>
                                    </Grid>
                                    <Grid item xs>
                                        <Typography variant="body1" align="right" color="textSecondary">{item.shipping.name}</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item container className={classes.priceListItem}>
                                    <Grid item xs>
                                        <Typography color="textSecondary">ราคาสินค้า:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="textSecondary">{item.topic.topicPrice * item.amount} บาท</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item container className={classes.priceListItem}>
                                    <Grid item xs>
                                        <Typography color="textSecondary">ค่าขนส่ง:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="textSecondary">{item.shipping.price * item.amount} บาท</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item container>
                                    <Grid item xs>
                                        <Typography>ทั้งหมด:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="textSecondary">{(item.shipping.price + item.topic.topicPrice) * item.amount} บาท</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        { index !== productList.length - 1 &&
                            <Grid item>
                                <Divider />
                            </Grid>
                        }
                    </React.Fragment>
                )}
            </Grid>
        );
    }
}

ProductListView.propTypes = {
    productList: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default withStyles(styles)(ProductListView);