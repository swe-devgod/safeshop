import React from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import green from '@material-ui/core/colors/green';
import { withStyles, Typography, Divider } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import ProductList from './components/product-list-view';

const styles = theme => ({
    bill: {
        padding: 8,
    },
    greenText: {
        color:  green[700],
    },
    headerText: {
        margin: 24,
        fontWeight: 'bold',
    },
    greenCenter: {
        color:  green[700],
        margin: '0 0 -8px 0',
        width: 64,
        height: 64,
    }
});

class BillView extends React.Component {
    constructor(props) {
        super(props);
        this.calculatePriceAllProduct = this.calculatePriceAllProduct.bind(this);
        this.calculatePriceAllShipping = this.calculatePriceAllShipping.bind(this);
    }

    calculatePriceAllProduct() {
        const itemList = this.props.billJson.checkout.checkoutItems;

        let total = 0;
        for (let i = 0, size=itemList.length; i < size; i++) {
            total += itemList[i].amount * itemList[i].topic.topicPrice;
        }
        return total;
    }

    calculatePriceAllShipping() {
        const itemList = this.props.billJson.checkout.checkoutItems;

        let total = 0;
        for (let i = 0, size=itemList.length; i < size; i++) {
                total += itemList[i].amount * itemList[i].shipping.price;
        }
        return total;
    }

    render() {
        const { classes } = this.props;
        const { user, checkout, address, paymentMethods } = this.props.billJson;

        const priceAllProduct = this.calculatePriceAllProduct();
        const priceAllShipping = this.calculatePriceAllShipping();

        return (
            <Grid container direction="column" alignItems="center" style={{marginBottom: 16}}>
                <Grid item sm={10} container justify="center">
                    <Grid item xs={12} sm={8} md={6}>
                        <Paper square className={classes.bill}>
                            <Grid container direction="column" spacing={8}>
                                <Grid item>
                                    <Typography
                                        variant="h4"
                                        align="center"
                                        color="primary"
                                        className={classes.headerText}
                                    >
                                        SafeShop
                                    </Typography>
                                </Grid>
                                <Divider />
                                <Grid item style={{padding: 8}} direction="column">
                                    <Typography align="center">
                                        <CheckCircleIcon className={classes.greenCenter} />
                                    </Typography>
                                    <Typography align="center" variant="h6" className={classes.greenText}>
                                        ทำรายการสำเร็จ
                                    </Typography>
                                    <Typography align="center" color="textSecondary">
                                        {new Date(checkout.checkoutDate).toLocaleString( "th-TH", {weekday: "long", day: "numeric", month: "long", year: "numeric", hour: "numeric", minute: "numeric", hour12: false})}
                                    </Typography>
                                    { /*
                                    <Typography align="center" color="textSecondary">
                                        {`เลขกำกับตะกร้า: ${checkout.id}`}
                                    </Typography>
                                    */ }                              
                                </Grid>
                                <Divider />
                                <Grid item>
                                    <ProductList productList={checkout.checkoutItems} />
                                </Grid>
                                <Divider />
                                <Grid item container direction="column" spacing={8} style={{padding: 16}}>
                                    <Grid item container>
                                        <Grid item xs>
                                            <Typography variant="body1">ชื่อ:</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body1" color="textSecondary">{`${user.firstName} ${user.middleName} ${user.lastName}`}</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item container>
                                        <Grid item xs>
                                            <Typography variant="body1">ที่อยู่:</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body1" color="textSecondary">{address.address}</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item container>
                                        <Grid item xs>
                                            <Typography variant="body1">เบอร์ติดต่อ:</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body1" color="textSecondary">{user.tel}</Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Divider />
                                <Grid item container direction="column" spacing={8} style={{padding: 16}}>
                                    <Grid item container>
                                        <Grid item>
                                            <Typography variant="body1">วิธีการชำระเงิน:</Typography>
                                        </Grid>
                                        <Grid item xs>
                                        <Typography variant="body1" color="textSecondary" align="right">{paymentMethods.name}</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item container>
                                        <Grid item xs>
                                            <Typography color="textSecondary">ราคาสินค้า:</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography color="textSecondary">{priceAllProduct} บาท</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item container>
                                        <Grid item xs>
                                            <Typography color="textSecondary">ค่าขนส่ง:</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography color="textSecondary">{priceAllShipping} บาท</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item container>
                                        <Grid item xs>
                                            <Typography variant="body1">ทั้งหมด:</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="body1" color="primary">{priceAllProduct + priceAllShipping} บาท</Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

BillView.propTypes = {
    classes: PropTypes.object.isRequired,
    billJson: PropTypes.object.isRequired,
};

export default withStyles(styles)(BillView);