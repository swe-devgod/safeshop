import React from 'react'

import ProgressView from '../../components/progress-view';
import BillView from './bill-view'

export default class BillPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isKnownState: false,
            isPageError: false,
            billJson: {},
        }
    }

    componentDidMount() {
        fetch(`/api/checkout/${this.props.match.params.id}/billing`)
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    this.setState({
                        isKnownState: true,
                        isPageError: true,
                    });
                    return Promise.reject();
                }
            })
            .then(json => {
                this.setState({
                    isKnownState: true,
                    billJson: json,
                })
            })
    }

    render() {
        if (!this.state.isKnownState) {
            return <ProgressView />
        }

        if (this.state.isPageError) {
            return null;
        }

        return (
            <BillView
                billJson={this.state.billJson}
            />
        );
    }
}