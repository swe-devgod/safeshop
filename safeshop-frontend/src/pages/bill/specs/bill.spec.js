import React from 'react';
import { shallow } from 'enzyme';

import BillPage from '../index';

describe('Testing BillPage', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    it('User go to valid bill ID' , async () => {
        // Mockup response
        const billJson_mock = {"user":{"id":2,"firstName":"check","middleName":"check","lastName":"check","displayName":"Rawit","email":"check@safeshop.com","tel":"0000000000","registerDate":1540387757284,"rating":4,"ratingVoteCount":1},"address":{"id":2,"address":"Kasetsart"},"paymentMethods":{"id":1,"name":"PromptPay"},"checkout":{"id":14,"checkoutDate":1543687593266,"statusId":2,"checkoutItems":[{"id":15,"pricePerItem":399,"amount":1,"addedDate":1543687590381,"topic":{"id":1,"userId":1,"topicCreatedDate":1540387757293,"topicPrice":399,"topicRemainingItems":47,"topicName":"Photo Set BNK48 รวมรูปภาพชุดใหม่ สำหรับปี 2018","topicDescription":"รูปภาพ BNK48 ขายแบบทั้ง set ของแท้ มีทั้งมิวนิค จูเน่และอีกหลายๆคน","topicVoteCount":0,"topicRating":0,"thumbnail":"http://localhost:3001/static/topics/1/m-1-img_bnk01.jpg"},"shipping":{"id":1,"price":45,"name":"ThaiPost EMS"}}]}};
        fetch.mockResponseOnce(JSON.stringify(billJson_mock));

        const wrapper = shallow(<BillPage match={{params: {id: 14}}}/>);
        expect(wrapper.state('isKnownState')).toEqual(false);

        await setTimeout(() => {
            expect(wrapper.state('isKnownState')).toEqual(true);
            expect(wrapper.state('billJson')).toEqual(billJson_mock);
        }, 500);
    });

    it('User go to non-valid bill ID' , async () => {
        // Mockup response
        fetch.mockResponseOnce({status: 403});

        const wrapper = shallow(<BillPage match={{params: {id: 1}}}/>);
        expect(wrapper.state('isKnownState')).toEqual(false);
        expect(wrapper.state('isPageError')).toEqual(false);

        await setTimeout(() => {
            expect(wrapper.state('isKnownState')).toEqual(true);
            expect(wrapper.state('isPageError')).toEqual(true);
        }, 500);
    });
});