import React from 'react';
import { shallow } from 'enzyme';

import CheckoutPage from '../index';

describe('Testing CheckoutPage', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    it('User go to valid checkout', async () => {
        const checkoutJson_mock = {"user":{"id":2,"firstName":"check","middleName":"check","lastName":"check","displayName":"Rawit","email":"check@safeshop.com","tel":"0000000000","registerDate":1540387757284,"rating":4,"ratingVoteCount":1,"picture":"/static/users/2.png"},"address":{"defaultId":2,"addresses":[{"id":2,"address":"Kasetsart"}]},"checkout":{"checkoutDate":1543759897885,"checkoutItems":[{"id":15,"amount":1,"addedDate":1543759894540,"topic":{"id":1,"topicCreatedDate":1540387757293,"topicPrice":399,"topicRemainingItems":47,"topicName":"Photo Set BNK48 รวมรูปภาพชุดใหม่ สำหรับปี 2018","topicDescription":"รูปภาพ BNK48 ขายแบบทั้ง set ของแท้ มีทั้งมิวนิค จูเน่และอีกหลายๆคน","topicVoteCount":0,"topicRating":0,"thumbnail":"/static/topics/1/m-1-img_bnk01.jpg","shippings":[{"id":1,"price":45,"shipping":{"id":1,"name":"ThaiPost EMS"}},{"id":2,"price":30,"shipping":{"id":2,"name":"ThaiPost Register"}},{"id":3,"price":45,"shipping":{"id":4,"name":"Kerrry"}}]}}]},"paymentMethods":[{"id":1,"name":"PromptPay"}]};
        fetch.mockResponseOnce(checkoutJson_mock);

        const wrapper = shallow(<CheckoutPage match={{params: {id: 14}}}/>);
        expect(wrapper.state('checkoutJson')).toEqual(undefined);
        expect(wrapper.state('isKnownState')).toEqual(false);

        // Wait for async functions to complete
        await setTimeout(() => {
            expect(wrapper.state('checkoutJson')).toEqual(checkoutJson_mock);
            expect(wrapper.state('isKnownState')).toEqual(true);
        }, 100);
    });

    it('User go to not valid checkout', async () => {
        fetch.mockResponseOnce({status: 403});

        const wrapper = shallow(<CheckoutPage match={{params: {id: 14}}}/>);
        expect(wrapper.state('checkoutJson')).toEqual(undefined);
        expect(wrapper.state('isKnownState')).toEqual(false);

        // Wait for async functions to complete
        await setTimeout(() => {
            expect(wrapper.state('checkoutJson')).toEqual(undefined);
            expect(wrapper.state('isKnownState')).toEqual(true);
        }, 100);
    });

    it('User submited checkout successfully', async () => {
        fetch.mockResponseOnce({status: 200});

        const wrapper = shallow(<CheckoutPage match={{params: {id: 14}}}/>);
        expect(wrapper.state('redirectToBill')).toEqual(false);

        // User submited
        fetch.mockResponseOnce({status: 200});
        wrapper.instance().handleSubmit({});

        // Wait for async functions to complete
        await setTimeout(() => {
            expect(wrapper.state('redirectToBill')).toEqual(true);
        }, 100);
    });

    it('User submited checkout unsuccessfully', async () => {
        fetch.mockResponseOnce({status: 200});

        const wrapper = shallow(<CheckoutPage match={{params: {id: 14}}}/>);
        expect(wrapper.state('redirectToBill')).toEqual(false);

        // User submited
        fetch.mockResponseOnce({status: 403});
        wrapper.instance().handleSubmit({});

        // Wait for async functions to complete
        await setTimeout(() => {
            expect(wrapper.state('redirectToBill')).toEqual(false);
        }, 100);
    });
});