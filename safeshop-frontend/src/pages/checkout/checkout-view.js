import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Paper, Typography, withStyles, Button, CircularProgress } from '@material-ui/core';

import AddressSelector from './components/address-selector-view';
import PaymentSelector from './components/payment-selector-view';
import ProductList from './components/product-list-view';

const styles = theme => ({
    paper: {
        padding: 16,
    },
    paperAddress: {
        padding: 16,
    },
    typoLeftIndent: {
        marginLeft: 8,
    },
    imgProduct: {
        width: '100%',
        display: 'block',
    },
    divSubmit: {
        position: 'relative',
    },
    loginProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    marginBottom: {
        marginBottom: 16,
    }
});

class CheckoutView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            productShippingList: {},
            addressId: props.checkoutJson.address.defaultId,
            paymentId: undefined,
        }
    }

    handlePaymentChange = val => {
        this.setState({paymentId: val});
    }

    handleProductShippingListChange = (itemId, shippingId) => {
        let newProductShippingList = this.state.productShippingList;
        newProductShippingList[itemId] = shippingId;
        this.setState({productShippingList: newProductShippingList});
    };
    
    handleAddressIdChange = addressId => {
        this.setState({
            addressId: addressId,
        });
    }

    handleSubmit = () => {
        let isShippingFormComplete = true;
        const checkoutShippings = this.props.checkoutJson.checkout.checkoutItems.map(item => {
            if (isShippingFormComplete) {
                const shippingId = this.state.productShippingList[item.id];
                if (shippingId) {
                    return {
                        itemId: item.id,
                        topicShippingId: shippingId,
                    }
                } else {
                    isShippingFormComplete = false;
                    return;
                }
            } else {
                return;
            }
        })
        if (!isShippingFormComplete) {
            console.error("Shipping Form is not complete.")
            return;
        }
        if (!this.state.paymentId) {
            console.error("Payment Form is not complete.");
            return;
        }
        this.props.handleSubmit({
            selectedAddressId: this.state.addressId,
            selectedPaymentId: this.state.paymentId,
            checkoutShippings: checkoutShippings,
        });
    }

    calculatePriceAllProduct = () => {
        const itemList = this.props.checkoutJson.checkout.checkoutItems;

        let total = 0;
        for (let i = 0, size=itemList.length; i < size; i++) {
            total += itemList[i].amount * itemList[i].topic.topicPrice;
        }
        return total;
    }

    findShippingFromProduct(product, shippingId) {
        return product.topic.shippings.find(shipping => shipping.id === shippingId);
    }

    calculatePriceAllShipping = () => {
        const itemList = this.props.checkoutJson.checkout.checkoutItems;
        const { productShippingList } = this.state;

        let total = 0;
        for (let i = 0, size=itemList.length; i < size; i++) {
            if (productShippingList[itemList[i].id]) {
                total += itemList[i].amount * this.findShippingFromProduct(itemList[i],productShippingList[itemList[i].id]).price;
            } else {
                return null;
            }
        }
        return total;
    }

    render() {
        const { classes } = this.props;
        const { user, address, checkout, paymentMethods} = this.props.checkoutJson;
        const { paymentId, productShippingList, addressId } = this.state;

        const priceAllProduct = this.calculatePriceAllProduct();
        const priceAllShipping = this.calculatePriceAllShipping();
        const priceAll = priceAllShipping ? priceAllProduct + priceAllShipping : null;

        return (
            <Grid container justify="center" className={classes.marginBottom}>
                <Grid item container sm={10} spacing={16} justify="center">
                    <Grid item xs={12}>
                    <Typography
                        variant="h4"
                        gutterBottom
                        className={classes.typoLeftIndent}>รายละเอียดการสั่งซื้อ</Typography>
                        <Paper square>
                            <ProductList
                                productList={checkout.checkoutItems}
                                productShippingList={productShippingList}
                                handleShippingChange={this.handleProductShippingListChange}
                            />
                        </Paper>
                    </Grid>
                    <Grid item container direction="column" xs={12} md={6}>
                        <Grid item>
                            <Typography variant="h6" gutterBottom className={classes.typoLeftIndent}>ข้อมูลที่อยู่จัดส่ง</Typography>
                        </Grid>
                        <Grid item xs>
                            <AddressSelector
                                value={addressId}
                                name={user.firstName + ' ' + user.middleName + ' ' + user.lastName} 
                                addressList={address.addresses}
                                tel={user.tel}
                                handleAddressChange={this.handleAddressIdChange}
                            />
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant="h6" gutterBottom className={classes.typoLeftIndent}>การชำระเงิน</Typography>
                        <Paper className={classes.paper} square>
                            <Grid container direction="column" spacing={8} justify="space-around">
                                <Grid item container alignItems="center">
                                    <Grid item xs>
                                        <Typography variant="body1">วิธีการชำระเงิน:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <PaymentSelector
                                            value={paymentId}
                                            paymentList={paymentMethods}
                                            onChange={this.handlePaymentChange}     
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item container>
                                    <Grid item xs>
                                        <Typography color="textSecondary">ราคาสินค้า:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="textSecondary">{priceAllProduct} บาท</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item container>
                                    <Grid item xs>
                                        <Typography color="textSecondary">ค่าขนส่ง:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="textSecondary">{priceAllShipping} บาท</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item container>
                                    <Grid item xs>
                                        <Typography variant="body1">ทั้งหมด:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body1" color="primary">{priceAll} บาท</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                <div className={classes.divSubmit}>
                                    <Button
                                        variant="contained"
                                        fullWidth
                                        color="primary"
                                        type="submit"
                                        disabled={this.props.isSubmitLoading}
                                        onClick={this.handleSubmit}
                                    >
                                        ยืนยันการชำระเงิน
                                    </Button>
                                    { this.props.isSubmitLoading && 
                                        <CircularProgress size={24} className={classes.loginProgress} />
                                    }
                                </div>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

CheckoutView.propTypes = {
    classes: PropTypes.object.isRequired,
    checkoutJson: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitLoading: PropTypes.bool
};

export default withStyles(styles)(CheckoutView);