import React from 'react';
import { Select, MenuItem, withStyles, FormHelperText, FormControl } from '@material-ui/core';

const styles = theme => ({
    select: {
        minWidth: 120,
    }
});

class PaymentSelectorView extends React.Component {
    
    handleChange = e => {
        this.props.onChange(e.target.value);
    }   
    
    render() {
        const { classes, theme } = this.props;

        return (
            <FormControl error={this.props.error}>
                <Select
                    value={this.props.value}
                    onChange={this.handleChange}
                    name="paymentId"
                    className={classes.select}
                    style={{color: this.props.error ? theme.palette.error.main : theme.palette.common.black}}
                >
                    { this.props.paymentList && this.props.paymentList.map( item => 
                        <MenuItem key={item.id} value={item.id}>
                            {item.name}
                        </MenuItem>
                    )}
                </Select>
                <FormHelperText>* ระบุ</FormHelperText>
            </FormControl>
        );
    }
}

export default withStyles(styles, {withTheme: true})(PaymentSelectorView);