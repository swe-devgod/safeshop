import React from 'react';

import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles, FormControl, FormHelperText, Typography } from '@material-ui/core';

const styles = theme => ({
    imgProduct: {
        width: '100%',
        display: 'block',
    },
    imgWraper: {
        maxWidth: 125,
        margin: '0 auto',
        overflow: 'hidden',
    },
    shippingMethodContainer: {
        [theme.breakpoints.down('sm')]: {
            alignItems: 'flex-end',
        }
    },
    select: {
        minWidth: 200,
    }
});

const product_list_mock = [
    {
        name: 'รองเท้า Adidas',
        price: 2990,
        img: 'http://www.central.co.th/e-shopping/wp-content/uploads/2017/01/%E0%B8%A3%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%97%E0%B9%89%E0%B8%B2-Adidas-NMD-R1-Tri-Color.jpg',
    },
    {
        name: 'แก้วชา',
        price: 129,
        img: 'https://png.pngtree.com/element_origin_min_pic/20/16/03/0356d8079142033.jpg'
    },
];

class ProductListView extends React.Component {

    findShippingFromProduct(product, shippingId) {
        return product.topic.shippings.find(shipping => shipping.id === shippingId);
    }

    handleShippingChange = event => {
        this.props.handleShippingChange(event.target.name, event.target.value);
    };

    render() {
        const { classes, productList, productShippingList } = this.props;

        return (
            <Grid container direction="column">
                { productList.map(item =>
                    <React.Fragment key={item.id}>
                        <Grid item container spacing={16} style={{padding: 16}}>
                            <Grid item xs={3} sm={2}>
                                <Paper className={classes.imgWraper}>
                                <img className={classes.imgProduct} src={item.topic.thumbnail} />
                                </Paper>
                            </Grid>
                            <Grid item xs>
                                <Typography variant="body1">{item.topic.topicName}</Typography>
                                <Typography variant="body1" color="textSecondary">จำนวน {item.amount} ชิ้น</Typography>
                            </Grid>
                            <Grid item container xs direction="column" className={classes.shippingMethodContainer} spacing={8}>
                                <Grid item>
                                    <Typography variant="body1">วิธีการขนส่ง</Typography>
                                </Grid>
                                <Grid item>
                                    <FormControl>
                                        <Select
                                            autoWidth
                                            value={productShippingList[item.id]}
                                            onChange={this.handleShippingChange}
                                            className={classes.select}
                                            inputProps={{
                                                name: item.id,
                                            }}
                                        > 
                                            { item.topic.shippings.map(shipping => 
                                                <MenuItem key={shipping.id} value={shipping.id}>
                                                    {shipping.shipping.name}
                                                </MenuItem>
                                            )}
                                        </Select>
                                        <FormHelperText>* ระบุ</FormHelperText>
                                    </FormControl>
                                </Grid>
                            </Grid>
                            <Grid item container direction="column" xs={12} md={true} spacing={8}>
                                <Grid item container>
                                    <Grid item xs>
                                        <Typography color="textSecondary">ราคาสินค้า:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="textSecondary">{item.topic.topicPrice * item.amount} บาท</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item container>
                                    <Grid item xs>
                                        <Typography color="textSecondary">ค่าขนส่ง:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="textSecondary">{productShippingList[item.id] ? this.findShippingFromProduct(item, productShippingList[item.id]).price * item.amount : ""} บาท</Typography>
                                    </Grid>
                                </Grid>
                                <Grid item container>
                                    <Grid item xs>
                                        <Typography>ทั้งหมด:</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography color="secondary">{productShippingList[item.id] ? (this.findShippingFromProduct(item, productShippingList[item.id]).price + item.topic.topicPrice) * item.amount: ""} บาท</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Divider />
                        </Grid>
                    </React.Fragment>
                )}
            </Grid>
        );
    }
}

export default withStyles(styles)(ProductListView);