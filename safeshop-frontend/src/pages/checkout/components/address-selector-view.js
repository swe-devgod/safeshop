import React from 'react';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import SwipeableViews from 'react-swipeable-views';
import Button from '@material-ui/core/Button';
import MobileStepper from '@material-ui/core/MobileStepper';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';

import PersonIcon from '@material-ui/icons/Person';
import PlaceIcon from '@material-ui/icons/Place';
import LocalPhoneIcon from '@material-ui/icons/LocalPhone'

const styles = theme => ({
    divAddress: {
        padding: 16,
    },
    iconBig: {
        width: 24,
        height: 24,
    }
})

class AddressSelector extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            activeStep: props.addressList.findIndex(address => address.id === props.value),
        }
    }

    handleNext = () => {
        this.setState(prevState => {
            const index = prevState.activeStep + 1;
            this.props.handleAddressChange(this.props.addressList[index].id);
            return { activeStep: index};
        });
    }

    handleBack = () => {
        this.setState(prevState => {
            const index = prevState.activeStep - 1;
            this.props.handleAddressChange(this.props.addressList[index].id);
            return { activeStep: index};
        });
    }

    handleChangeIndex = activeStep => {
        this.props.handleAddressChange(this.props.addressList[activeStep].id);
        this.setState({ activeStep });
    }

    render() {
        const { name, tel, addressList, classes } = this.props;
        const { activeStep } = this.state;

        return (
            <Paper square>
                <Grid container direction="column" style={{height: '100%'}}>
                    <Grid item xs>
                        <SwipeableViews
                            index={activeStep}
                            onChangeIndex={this.handleChangeIndex}
                            enableMouseEvents
                        >
                        {addressList.map((address, index) => {
                            const gridItems = [
                                { icon: <PersonIcon className={classes.iconBig} />, iconLabel: 'ผู้รับ', value: name },
                                { icon: <PlaceIcon className={classes.iconBig} />,  iconLabel: 'ที่อยู่',value: address.address },
                                { icon: <LocalPhoneIcon className={classes.iconBig} />, iconLabel: 'เบอร์ติดต่อ',value: tel },
                            ];
                            return (
                                <Grid
                                    container
                                    direction="column"
                                    key={index}
                                    className={classes.divAddress}
                                >
                                { gridItems.map(item => 
                                    <Grid item container spacing={8} alignItems="center">
                                        <Grid item>
                                            {item.icon}
                                        </Grid>
                                        <Grid item xs>
                                            <Typography
                                                variant="body1"
                                            >
                                                {item.value}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                )}
                                </Grid>
                            )
                        })}
                        </SwipeableViews>
                    </Grid>
                { addressList.length > 1 &&
                    <Grid item>
                        <MobileStepper
                            steps={addressList.length}
                            activeStep={activeStep}
                            position="static" 
                            nextButton={
                                <Button
                                    size="small"
                                    onClick={this.handleNext}
                                    disabled={activeStep === addressList.length - 1}
                                >
                                    <KeyboardArrowRight />
                                </Button>
                            }
                            backButton={
                                <Button
                                    size="small"
                                    onClick={this.handleBack}
                                    disabled={activeStep === 0}
                                >
                                    <KeyboardArrowLeft />
                                </Button>
                            }
                        />
                    </Grid>
                }
                </Grid>
            </Paper>
        );
    }
}

export default withStyles(styles)(AddressSelector);