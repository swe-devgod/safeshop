import React from 'react';
import { Redirect } from 'react-router-dom';

import CheckoutView from './checkout-view';
import ProgressView from '../../components/progress-view';

export default class CheckoutPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checkoutJson: undefined,
            isKnownState: false,
            isSubmitLoading: false,
            redirectToBill: false,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {       
        fetch(`/api/checkout/${this.props.match.params.id}`)
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    return Promise.reject();
                }
            }).then(json => {
                this.setState({
                    checkoutJson: json,
                    isKnownState: true,
                });
            });
        
    }

    handleSubmit(formData) {
        this.setState({isSubmitLoading: true});
        fetch(`/api/checkout/${this.props.match.params.id}`, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                this.setState({isSubmitLoading: false});
                if (res.status === 200) {
                    this.setState({redirectToBill: true});
                } else {
                    console.error("Failed To confirm");
                }
            })
    }

    render() {
        if (!this.state.isKnownState) {
            return <ProgressView />;
        }

        if (this.state.redirectToBill) {
            return <Redirect
                        to={`/checkout/bill/${this.props.match.params.id}`}
                    />
        }

        return (
            <CheckoutView
                checkoutJson={this.state.checkoutJson}
                handleSubmit={this.handleSubmit}
                isSubmitLoading={this.state.isSubmitLoading}
            />
        );
    }
}