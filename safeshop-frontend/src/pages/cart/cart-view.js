import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CartItem from './components/index'
import Redirect from 'react-router/Redirect';
import ProgressView from '../../components/progress-view';

const styles = theme => ({
      
});

class CartView extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setSideBarItems(null);
    }

    render() {
        const { classes } = this.props;

        if (!this.props.isKnownState) {
            return <ProgressView />
        }
        
        if (this.props.redirectToLogin) {
            return <Redirect to="/login" />;
        }

        if(this.props.redirectToCheckout){
            return <Redirect to={`/checkout/${this.props.redirectToCheckout}`} />;
        }

        return(
            <React.Fragment>
                <Grid container justify="center" alignItems="stretch" style={{marginTop: -16}}>
                    <Grid item md={10} sm={10} xs={12}>
                        <h1 style={{marginLeft: 8}}>Your Cart</h1>
                            <CartItem
                                itemList={this.props.cartListJson}
                                isDeletable={true}
                                setProductList={this.props.setProductList}
                                onRemoveCartItem={this.props.onRemoveCartItem}
                                onChangeCartItemAmount={this.props.onChangeCartItemAmount}
                                onCheckoutCartItem={this.props.onCheckoutCartItem}
                            />
                    </Grid>
                </Grid>
            </React.Fragment>
        )
    }
}

CartView.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,

    isKnownState : PropTypes.bool.isRequired,
    redirectToLogin : PropTypes.bool.isRequired,
    redirectToCheckout : PropTypes.number.isRequired,

    cartListJson : PropTypes.array.isRequired,

    setProductList : PropTypes.func.isRequired,
    isDeletable : PropTypes.bool.isRequired,
    onRemoveCartItem : PropTypes.func.isRequired,
    onChangeCartItemAmount : PropTypes.func.isRequired,
    onCheckoutCartItem : PropTypes.func.isRequired,
    
};

export default withStyles(styles)(CartView);
