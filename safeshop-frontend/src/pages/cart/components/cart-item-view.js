import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import BuyIcon from '@material-ui/icons/ShoppingCart';
import Divider from '@material-ui/core/Divider';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import { SelectItemAmount } from './../../showcase/components/select-item-amount/index';
import { ProductRating } from './../../showcase/components/product-rating/index';


const styles = theme => ({
    img: {
        display: 'block',
        width: '70%',
        height: '60%',
      },

    
    paper: {
        paddingTop:"20px"    , 
        paddingBottom: "20px",
  },
});

class CartItemView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkedItems: {},
            cartItemIds: []
        }
    }

    onClickedCheckout = () => {
        let cardItemIds = [];
        const { checkedItems } = this.state;
        for (let itemId in checkedItems) {
            if (checkedItems[itemId]) {
                cardItemIds.push(parseInt(itemId, 10));
            }
        }
        const formData = {
            cardItemIds: cardItemIds
        };
        if(cardItemIds.length > 0){
             this.props.onClickedCheckout(formData);
        }
    }

    handleChange = name => event => {
        let checkedItems = this.state.checkedItems;
        checkedItems[name] = event.target.checked
        this.setState({ checkedItems: checkedItems });
    };

    render() {
        const { classes } = this.props;

        return(
            <React.Fragment>
                <Dialog
                    fullWidth={true}
                    maxWidth = {'md'}
                    open={this.props.isDeleteDialogOpen}
                >
                    <DialogTitle>
                        <p>ยืนยันที่จะนำสินค้านี้ออกจากตะกร้าหรือไม่?</p>
                    </DialogTitle>
                    <DialogContent>
                        {
                            this.props.itemList && 
                            <div>
                                <span>ชื่อกระทู้สินค้า: <h4>{this.props.wantToDeleteName}</h4></span>
                            </div>
                        }
                    </DialogContent>
                    <DialogActions>
                        <Grid container xs={12} spacing={16}>
                            <Grid item>
                                <Button
                                    variant='contained' size='small' 
                                    onClick={this.props.onDialogSubmit}>ยืนยัน
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    color="primary" 
                                    onClick={this.props.onDialogCancel}>ยกเลิก
                                </Button>
                            </Grid>
                        </Grid>
                    </DialogActions>
                </Dialog>   
                <Grid container spacing={16} style={{width: '100%', margin: '0 0'}}> {/* Important !!! */}
                    {
                        this.props.itemList &&
                        this.props.itemList.map((item, index) => (
                            <Grid item key={item.id}>
                                <Paper className={classes.paper}>
                                    <Grid container alignItems={"center"} spacing={16}>
                                        <Grid item xs={1} style={{padding:'0 0 0 10px'}}>
                                            <Checkbox value="checked"
                                                style={{ width: 20, height: 20 }}
                                                icon={<CheckBoxOutlineBlankIcon     tyle={{ fontSize: 30}}/>}
                                                checkedIcon={<CheckBoxIcon style={{ fontSize: 30 }}/>}
                                                color="primary"
                                                checked={this.state.checkedItems[item.id]}
                                                onChange={this.handleChange(item.id)}
                                                value="checked"
                                                />
                                        </Grid>
                                        <Grid container item md={3} sm={3} xs={8} justify="center">
                                            <img className={classes.img} src={item.topic.thumbnail}/>
                                        </Grid>
                                        <Grid item container direction="column" md={7} sm={7} xs={10} style={{padding:'0 0 0 30px'}}>
                                            <Grid item>
                                                <Typography variant="h6">{item.topic.topicName}</Typography>
                                            </Grid>
                                            <Grid item>
                                                <ProductRating topicRating={item.topic.topicRating}
                                                    topicRatingCount={item.topic.topicRatingCount}
                                                />
                                            </Grid>
                                            <Grid>
                                                <Typography gutterBottom color="textSecondary">
                                                    ผู้ขาย: {item.topic.ownerName}
                                                </Typography>
                                            </Grid>
                                            <Divider/>
                                            <SelectItemAmount
                                                value={item.amount}
                                                remainingItems={item.topic.remainingItems}
                                                setValue={(value) => this.props.onChangeCartItemAmount(item.topic.topicId, value)}
                                            />
                                            <Grid item>
                                                <Typography gutterBottom variant="title" style={{color:'orange', paddingTop: '10px'}}>ราคา {item.topic.topicPrice} บาท</Typography>
                                            </Grid>
                                            <Grid item>
                                                <Button
                                                    variant={"outlined"} color={"primary"}
                                                    onClick={() => this.props.onClickRemove(index, item.id)}>
                                                    Remove
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))
                    }
                    <Grid item md={12}>
                        <Button color="primary" style={{ background: '#00acc1' ,margin: '16px 0 16px 8px'}} variant="raised" onClick={this.onClickedCheckout}>
                            <BuyIcon/>
                            Buy
                        </Button> 
                    </Grid>
                </Grid>   
            </React.Fragment>
        )
    }
}

CartItemView.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,

    itemList : PropTypes.array.isRequired,

    isDeleteDialogOpen : PropTypes.bool.isRequired, 
    wantToDeleteIndex : PropTypes.number.isRequired,

    onClickRemove : PropTypes.func.isRequired, 
    onDialogSubmit : PropTypes.func.isRequired,
    onDialogCancel : PropTypes.func.isRequired,
    onChangeCartItemAmount : PropTypes.func.isRequired,
    onClickedCheckout : PropTypes.func.isRequired,
    handleChange : PropTypes.func.isRequired,

};

export default withStyles(styles)(CartItemView);
