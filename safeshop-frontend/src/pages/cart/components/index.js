import React, { Component } from 'react';
import CartItemView from './cart-item-view';

export default class CartItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemList: this.props.itemList,
            wantToDeleteIndex: 0,
            wantToDeleteId: null,
            isDeleteDialogOpen: false,
            wantToDeleteName: '',
        };
        this.handleRemoveItemFromList = this.handleRemoveItemFromList.bind(this);
        this.handleDialogClickRemove = this.handleDialogClickRemove.bind(this);
        this.handleDialogClickCancel = this.handleDialogClickCancel.bind(this);
        this.handleDialogClickSubmit = this.handleDialogClickSubmit.bind(this);
        this.handleRemoveItemFromSellingList=this.handleRemoveItemFromSellingList.bind(this);
    }

    handleRemoveItemFromList(){
        const newItemList = this.state.itemList.slice();
        newItemList.splice(this.state.wantToDeleteIndex,1);
        this.setState({
            itemList : newItemList
        })
        return newItemList;
    }

    handleDialogClickRemove(index, id){
        this.setState({
            isDeleteDialogOpen : true,
            wantToDeleteIndex : index,
            wantToDeleteId: id,
            wantToDeleteName: this.props.itemList[index].topic.topicName
        });
    }

    handleDialogClickCancel(){
        this.setState({
            isDeleteDialogOpen : false
        });
    }

    handleDialogClickSubmit(){
        this.handleRemoveItemFromSellingList();
        this.setState({
            isDeleteDialogOpen : false
        });
    }

    handleRemoveItemFromSellingList(){
        this.props.setProductList(this.handleRemoveItemFromList());
        this.props.onRemoveCartItem({id: this.state.wantToDeleteId});
    }

    onChangeCartItemAmount = (topicId, amount) => {
        this.props.onChangeCartItemAmount({
            topicId: topicId,
            amount: amount,
        });
    }

    render() {
        return <CartItemView
                    itemList={this.props.itemList}
                    isDeletable={this.props.isDeletable}
                    isDeleteDialogOpen={this.state.isDeleteDialogOpen}
                    wantToDeleteIndex={this.state.wantToDeleteIndex}
                    wantToDeleteName={this.state.wantToDeleteName}
                    onClickRemove={this.handleDialogClickRemove}
                    onChangeCartItemAmount={this.onChangeCartItemAmount}
                    onClickedCheckout={this.props.onCheckoutCartItem}
                    onDialogSubmit={this.handleDialogClickSubmit}
                    onDialogCancel={this.handleDialogClickCancel}
                />
    }
}