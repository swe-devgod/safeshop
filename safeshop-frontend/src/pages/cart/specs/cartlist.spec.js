import React from 'react';
import { shallow } from 'enzyme';

import CartPage from '../index';

describe('Testing CartPage', () => {

    //beforeEach(() => {
    //    fetch.resetMocks();
    //});

    it('Show cart-list when User login', () => {
        const cartList = {"id": 1, "amount":2, "addedDate": 1540387757284, "topic":{"topicId": 1, "topicName":"Photo Set BNK48 รวมรูปภาพชุดใหม่ สำหรับปี 2018", "topicPrice":399, "createdDate":1540387757293, "topicRating":4.5, "topicRatingCount": 49, "owerUserId": 1, "ownerName": "Pharanyu", "remainingItems":47, "owerPicture": "http://localhost:3001/static/users/1.png", "ownerRating":4.2, "ownerRatingCount": 36, "thumbnail":"http://localhost:3001/static/topics/1/m-1-img_bnk01.jpg"}}; 
        fetch.mockResponseOnce(JSON.stringify(cartList));
        const wrapper = shallow(<CartPage />);

        expect(wrapper.state('isKnownState')).toEqual(false);

        setTimeout(() => {
            expect(wrapper.state('isKnownState')).toEqual(true);
            expect(wrapper.state('cartListJson')).toEqual(cartList);
        }, 500);
    });
});