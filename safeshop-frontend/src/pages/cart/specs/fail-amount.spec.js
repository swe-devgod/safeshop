import React from 'react';
import { shallow } from 'enzyme';

import CartPage from '../index';

describe('Testing CartPage', () => {

    //beforeEach(() => {
    //    fetch.resetMocks();
    //});

    it('item not enough amount', () => {
        fetch.mockResponseOnce({status: 403});
        const itemAmount = 25;
        const itemId = 1;
        const wrapper = shallow(<CartPage />);


        expect(wrapper.state('isEnoughItem')).toEqual(false);

        wrapper.instance().onChangeCartItemAmount({
            topicId: JSON.stringify(itemId),
            amount: JSON.stringify(itemAmount)
        });

        setTimeout(() => {
            expect(wrapper.state('isEnoughItem').toEqual(false));
        }, 500);
    });
});