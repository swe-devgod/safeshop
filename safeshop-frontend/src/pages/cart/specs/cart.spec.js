import React from 'react';
import { shallow } from 'enzyme';

import CartPage from '../index';

describe('Testing CartPage', () => {

    //beforeEach(() => {
    //    fetch.resetMocks();
    //});

    it('User submited checkout', () => {
        //fetch.mockResponseOnce(JSON.stringify({formdata: [1,2]}));
        fetch.mockResponseOnce(JSON.stringify({ checkoutId: 2 }));
        const wrapper = shallow(<CartPage />);

        expect(wrapper.state('redirectToCheckout')).toEqual(false);

        wrapper.instance().onCheckoutCartItem({
            formdata: [1, 2]
        });

        setTimeout(() => {
            expect(wrapper.state('redirectToCheckout')).toEqual(true);
        }, 500);
    });
});