import React from 'react';
import { shallow } from 'enzyme';

import CartPage from '../index';

describe('Testing CartPage', () => {

    // beforeEach(() => {
    //     fetch.resetMocks();
    // });

    it('User submited checkout without cart-list', () => {
        //fetch.mockResponseOnce(JSON.stringify({formdata: [1,2]}));
        fetch.mockResponseOnce(JSON.stringify({ checkoutId: 2 }));
        const wrapper = shallow(<CartPage />);

        //expect(wrapper.state('redirectToCheckout')).toBeFalsy();

        wrapper.instance().onCheckoutCartItem({
            formdata: []
        });

        setTimeout(() => {
            expect(wrapper.state('redirectToCheckout')).toEqual(false);
        }, 500);
    });
});