import React, { Component } from 'react';
import CartView from './cart-view';
import { AuthService } from '../../components/auth-service';
import { fetchCartAppbarAmount } from '../../lib/cart-appbar-helper';

const mock_product = [
];

export default class CartPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,                                 
            amount: 0,                     
            addedDate: 0,
            cartListJson: [],
            productList : mock_product.slice(),
            isKnownState: false,
            redirectToLogin: false,
            redirectToCheckout: false,
            isEnoughItem: false
        };
        this.setProductList = this.setProductList.bind(this);
        this.onCheckoutCartItem = this.onCheckoutCartItem.bind(this);
    }
    
    componentDidMount() {
        document.title = "Cart List - SafeShop";
        this.fetchCartList();
    }

    fetchCartList() {
        fetch("/api/cart/list", {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            this.setState({isKnownState: true});
            if (res.status === 200 && AuthService.isAuthenticated()) {
                return res.json();
            } else {
                this.setState({redirectToLogin: true});
                return Promise.reject();
            }
        })
        .then(json => this.setState({
            cartListJson : json,
        }));
    }

    onRemoveCartItem = (formData) => {
        fetch('/api/cart/', {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        })
        .then(res => {
            if (res.status === 200) {
                fetchCartAppbarAmount(this.props.dispatcher)
                this.fetchCartList();
            }
        });
    }

    onChangeCartItemAmount = (formData) => {
        fetch('/api/cart/', {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        })
        .then(res => {
            if (res.status === 200) {
                this.setState({isEnoughItem: true});
                fetchCartAppbarAmount(this.props.dispatcher)
                this.fetchCartList();
            }
        });
    }

    setProductList(data){
        this.setState({
            productList : data
        });
    }

    onCheckoutCartItem = (formData) => {
        fetch('/api/checkout/', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        })
        .then(res => {
            if (res.status === 200) {
                return res.json();
            } else {
                return Promise.reject();
            }
        })
        .then(json => {
            fetchCartAppbarAmount(this.props.dispatcher)
            this.setState({
                redirectToCheckout: json.checkoutId
            });
        });
    }

    render() {
        return <CartView
                    productList={this.state.productList}
                    cartListJson={this.state.cartListJson}
                    setProductList={data => this.setProductList(data)}
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    onRemoveCartItem={this.onRemoveCartItem}
                    onChangeCartItemAmount={this.onChangeCartItemAmount}
                    onCheckoutCartItem={this.onCheckoutCartItem}
                    redirectToLogin={this.state.redirectToLogin}
                    redirectToCheckout={this.state.redirectToCheckout}
                    isKnownState={this.state.isKnownState}
                    isEnoughItem={this.state.isEnoughItem}
                />
    }
}