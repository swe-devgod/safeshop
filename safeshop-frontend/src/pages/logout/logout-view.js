import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import ProgressView from '../../components/progress-view';

import { styles } from './logout-view.styles';

class LogoutView extends Component {

    render() {
        const { redirectToLogin } = this.props;

        if (redirectToLogin) {
            return <Redirect to="/login" />
        }

        return (
            <React.Fragment>
                <ProgressView />
            </React.Fragment>
        );
    }
}

LogoutView.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(LogoutView);