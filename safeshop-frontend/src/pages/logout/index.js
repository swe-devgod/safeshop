import React, { Component } from 'react';
import LogoutView from './logout-view';
import { doAuthorization, AuthService } from '../../components/auth-service';

export default class LogoutPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToLogin: false
        };
    }

    componentDidMount() {
        document.title = "Logout - SafeShop";
        this.props.dySec.setSideBarItems(null);

        fetch('/api/user/logout', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: '{}'
        })
        .then(res => {
            AuthService.onUserLoggedOut(res, success => {
                if (success) {
                    
                } else {
                    console.error('fetch: error');
                }
                this.props.dispatcher.removeProps('cart-button-appbar');
                this.setState({ redirectToLogin: true });
            });
        });
    }

    render() {
        return <LogoutView redirectToLogin={this.state.redirectToLogin}/>
    }
}