import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { withStyles, Typography } from '@material-ui/core';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ProductRating } from './../../../showcase/components/product-rating/';
import Rating from '../../../../components/rating/'
import PropTypes from 'prop-types';

const styles = theme => ({

    img: {
        marginTop: '35px',
        marginLeft: '20px',
        marginBottom: '35px',
        display: 'block',
        width: '70%',
        height: '60%',
      },

    button:{
        height: '56px',
        marginLeft: '10px'
    },

});

class TopicItemListView extends Component{
    constructor(props){
        super(props);
    }
    onStarClickItem(nextValue) {
        this.props.onStarClickItem(nextValue)
    }
    onStarClickSeller(nextValue) {
        this.props.onStarClickSeller(nextValue)
    }
    render(){
        const { classes } = this.props;
        if(this.props.redirectToCart){
            return <Redirect to={`/checkout/${this.props.redirectToid}`} />;
        }
        if(this.props.isWatchTopic){
            return <Redirect to={`/topic/${this.props.isWatchTopicid}`} />;
        }
        return(
            <React.Fragment>
                <Dialog
                    fullWidth={true}
                    maxWidth = {'md'}
                    open={this.props.isDeleteDialogOpen}
                >
                    <DialogTitle>
                    {
                            this.props.isBuying &&
                            <p>ยืนยันที่จะยกเลิกการซื้อสินค้านี้หรือไม่?</p>
                        }
                        {
                            this.props.isOnDelivery &&
                            <p>ยืนยันได้รับของเรียบร้อย?</p>
                        }
                    </DialogTitle>
                    <DialogContent>
                        {
                            this.props.itemList && 
                            <Grid item>
                                {
                                    //ยังแสดงชื่อเจ้าของกระทู้ผิดเพราะ db ส่งมาผิด อีกเหตุผลคือเพราะ เราย้ายมาจาก delivery user ในที่นี้หมายถึง ตัวเรา เพราะงั้น พังแน่นอนจ้า
                                }
                                <Typography variant="h4">ชื่อกระทู้สินค้า: {this.props.wantToDeleteName}</Typography>
                            </Grid>
                        }
                    </DialogContent>
                    <DialogActions>
                        <Grid container xs={12} spacing={16}>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    onClick={this.props.onDialogSubmit}>ยืนยัน
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    color="primary"
                                    onClick={this.props.onDialogCancel}>ยกเลิก
                                </Button>
                            </Grid>
                        </Grid>
                    </DialogActions>
                </Dialog>
                {
                    //success dialog
                    this.props.isVoteDialogOpen &&
                    <Rating
                        setDialogVote={this.props.setDialogVote}
                        isVoteItem={true}
                        isVoteSeller={true}
                        itemName={this.props.wantToVoteItemName}
                        itemIndex={this.props.wantToVoteItemindex}
                    />
                }
                
                <Grid container xs={12} spacing={16} style={{width: '100%', margin: '0 0'}}> {/* Important !!! */}
                {
                        (this.props.itemList && !!!this.props.isBuying) &&
                        this.props.itemList.map((item, index) => (
                            <Grid item xs={12} md={6}>
                                <Paper style={{padding: 12}}>
                                    <Grid container xs={12} alignItems={"center"}>
                                        <Grid item xs={6} md={3}>
                                        {
                                            <img className={classes.img} src={item.topic.thumbnail}/>
                                        }
                                        </Grid>
                                        {
                                            (!!!this.props.isBuying)?
                                            <Grid item container direction="column" xs={6} md={6}>
                                            <Grid item>
                                                <Typography variant="h6">{item.topic.topicName}</Typography>
                                            </Grid>
                                            <Grid item>
                                                <ProductRating topicRating={item.topic.topicRating}
                                                    topicRatingCount={item.topic.topicVoteCount}
                                                />
                                            </Grid>
                                            <Grid item>
                                            {
                                                <Typography>ชื่อเจ้าของกระทู้: {item.user.firstName} </Typography>
                                            }
                                            </Grid>
                                            <Grid item>
                                                <Typography>ราคา {item.pricePerItem} บาท</Typography>
                                            </Grid>

                                            <Grid item>
                                                <Typography>จำนวน {item.amount} ชิ้น</Typography>
                                            </Grid>

                                            <Grid item>
                                                <Typography>ชื่อผู้รับสินค้า: {item.user.firstName} {item.user.middleName} {item.user.lastName} </Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography>ที่อยู่สำหรับการจัดส่ง: {item.address.address}</Typography>
                                            </Grid> 
                                            <Grid item>
                                                {
                                                    (this.props.isOnDelivery||this.props.isSuccess) &&
                                                    <Typography>วิธีการขนส่ง: {item.shipping.name}</Typography>
                                                }
                                            </Grid> 
                                            
                                            {
                                                //หน้า is awaiting
                                                (this.props.isAwaitingDelivery)?
                                                    <Grid item>
                                                        <Typography variant="h7">อยู่ในระหว่างรอการจัดส่งสินค้า</Typography>
                                                    </Grid>
                                                :
                                                    <span/>
                                            }
                                            
                                            {
                                                //หน้า is ondelivery
                                                (this.props.isOnDelivery)?
                                                    <Grid xs={12}>
                                                        <Grid item>
                                                            <Typography>หมายเลขพัสดุ: {item.shipping.shippingNo}</Typography>
                                                        </Grid>
                                                        <Grid item>
                                                            <Typography variant="h7">สามารถตรวจสอบการขนส่งสินค้าได้ที่เว็บไซต์ขนส่ง</Typography>
                                                        </Grid>
                                                    </Grid>
                                                    
                                                :
                                                    <span/>
                                            }
                                        </Grid>
                                        :
                                        <span/>
                                        }
                                        <Grid item xs={12} md={3}>
                                            {
                                                //หน้า Delivery
                                                (this.props.isOnDelivery)?
                                                <Grid container alignItems={"center"} spacing={8}>
                                                    <Grid item xs={6} md={12}>
                                                        <Button
                                                            fullWidth
                                                            variant={"outlined"}
                                                            onClick={() => this.props.onClickRemove(index)}>
                                                            Got it
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                                :
                                                    <span/>
                                            }
                                            {
                                                //หน้า is success
                                                (this.props.isSuccess)?
                                                <Grid container alignItems={"center"} spacing={8}>
                                                    <Grid item xs={6} md={12}>
                                                        <Button
                                                            fullWidth
                                                            variant={"outlined"}
                                                            onClick={() => this.props.onClickViewTopic(index)}>
                                                            ดูสินค้า
                                                        </Button>
                                                    </Grid>
                                                    <Grid item xs={6} md={12}>
                                                        <Button
                                                            fullWidth
                                                            variant={"outlined"}
                                                            onClick={() => this.props.onClickVote(index)}>
                                                            ให้คะแนนสินค้า
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                                :
                                                    <span/>
                                            }
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))
                    }
                    {
                        (this.props.itemList && this.props.isBuying) &&
                        this.props.itemList.map((item, index) => (
                            <Grid item xs={12} md={12}>
                                <Paper style={{padding: 12}}>
                                    <Grid container xs={12} alignItems={"center"}>
                                    {
                                        item.checkoutItems.map((checkout,it) => (
                                            <Grid item container direction="row" xs={12} md={6}>
                                                <Grid xs={4} md={4}>
                                                    <img className={classes.img} src={checkout.topic.thumbnail}/>
                                                </Grid>
                                                <Grid xs={8} md={8}>
                                                    <Grid item>
                                                        <Typography variant="h6">{checkout.topic.topicName}</Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <ProductRating topicRating={checkout.topic.topicRating}
                                                            topicRatingCount={checkout.topic.topicVoteCount}
                                                        />
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography>ชื่อเจ้าของกระทู้: {checkout.user.firstName} </Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography>ราคาต่อชิ้น {checkout.topic.topicPrice} บาท</Typography>
                                                    </Grid>

                                                    <Grid item>
                                                        <Typography>จำนวน {checkout.amount} ชิ้น</Typography>
                                                    </Grid>
                                                </Grid>
                                                
                                            </Grid>
                                        ))
                                    }
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Grid container alignItems={"center"} spacing={8}>
                                            <Grid item xs={6}>
                                                <Button
                                                    fullWidth
                                                    variant={"outlined"}
                                                    onClick={() => this.props.onClickCartBuying(item.id)}>
                                                    Checkout
                                                </Button>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Button
                                                    fullWidth
                                                    variant={"outlined"} color={"primary"}
                                                    onClick={() => this.props.onClickRemove(index)}>
                                                    Remove
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))
                    }
                </Grid>
            </React.Fragment>
        )
    }
}
TopicItemListView.propTypes = {
    itemList: PropTypes.array.isRequired,

    //Buying
    isDeleteDialogOpen: PropTypes.bool.isRequired,
    wantToDeleteName: PropTypes.string.isRequired,
    wantToDeleteIndex: PropTypes.number.isRequired,
    redirectToCart: PropTypes.bool.isRequired,
    redirectToid: PropTypes.string.isRequired,

    onClickCartBuying: PropTypes.func.isRequired,
    onClickRemove: PropTypes.func.isRequired,
    onDialogSubmit: PropTypes.func.isRequired,
    onDialogCancel: PropTypes.func.isRequired,
    //Await
    isAwaitingDelivery: PropTypes.bool.isRequired,

    //OnDelivery
    Gotitem: PropTypes.func.isRequired,

    //check page
    isBuying: PropTypes.bool.isRequired,
    isOnDelivery: PropTypes.bool.isRequired,
    isSuccess: PropTypes.bool.isRequired,

    //success
    isVoteDialogOpen: PropTypes.bool.isRequired,
    wantToVoteItemName: PropTypes.string.isRequired,
    wantToVoteSellerName: PropTypes.string.isRequired,
    wantToVoteItemindex: PropTypes.number.isRequired,
    wantToVoteSellerindex: PropTypes.number.isRequired,
    isWatchTopic: PropTypes.bool.isRequired,
    isWatchTopicid: PropTypes.bool.isRequired,
    onClickViewTopic: PropTypes.func.isRequired,
    onClickVote: PropTypes.func.isRequired,
    setDialogVote: PropTypes.func.isRequired,
};

export default withStyles(styles)(TopicItemListView);