import React, { Component } from 'react';
import TopicItemListView from './topic-item-list-view'
import PropTypes from 'prop-types';

export default class TopicItemList extends Component {
    constructor(props) {
        super(props);
        this.state={
            itemList: this.props.itemList,

            wantToDeleteIndex: 0,
            wantToVoteIndex: 0,
            //Buying
            isDeleteDialogOpen: false,
            wantToDeleteName: '',
            redirectToid:'',
            redirectToCart:false,
            //Delivry
            confirmItem: {
                itemId: 0,
            },
            //success
            isVoteDialogOpen: false,
            wantToVoteItemName: '',
            wantToVoteSellerName: '',
            wantToVoteItemindex: 0,
            wantToVoteSellerindex: 0,
            isWatchTopic: false,
            isWatchTopicid: '',

        };
        this.handleRemoveItemFromList = this.handleRemoveItemFromList.bind(this);
        //Buyng
        this.handleDialogClickRemove = this.handleDialogClickRemove.bind(this);
        this.handleDialogClickCancel = this.handleDialogClickCancel.bind(this);
        this.handleDialogClickSubmit = this.handleDialogClickSubmit.bind(this);
        this.handleRemoveItemFromBuyingList=this.handleRemoveItemFromBuyingList.bind(this);
        //Delivery
        this.Gotitem=this.Gotitem.bind(this);
        this.handleAddItemToSuccessList=this.handleAddItemToSuccessList.bind(this);
        this.handleInsertItem=this.handleInsertItem.bind(this);
        //Success
        this.handleClickViewTopic = this.handleClickViewTopic.bind(this);
        this.handleDialogClickVote = this.handleDialogClickVote.bind(this);
        this.setDialogVote = this.setDialogVote.bind(this);
        
    }
    
    handleRemoveItemFromList(){
        const newItemList = this.props.itemList.slice();
        newItemList.splice(this.state.wantToDeleteIndex,1);
        this.setState({
            itemList : newItemList
        })
        return newItemList;
    }

    //Buying
    handleClickCartTopic = (index) => {
        this.setState({
            redirectToid: index,
            redirectToCart:true
        });
    }
    handleDialogClickRemove(index){
        this.setState({
            isDeleteDialogOpen : true,
            wantToDeleteIndex : index,
            gotitemindex: index,
            
        });
        if(this.props.isBuying){
            this.setState({
                wantToDeleteName: this.props.itemList[index].checkoutItems[0].topic.topicName
            });
        }
        else{
           this.setState({
                wantToDeleteName: this.props.itemList[index].topic.topicName,
                confirmItem: {
                    itemId: this.props.itemList[index].id,
                } 
            });
        }
    }

    handleDialogClickCancel(){
        this.setState({
            isDeleteDialogOpen : false
        });
    }

    handleDialogClickSubmit(){
        if(this.props.isBuying){
            this.handleRemoveItemFromBuyingList();
        }
        if(this.props.isOnDelivery){
            this.Gotitem();
        }
        this.setState({
            isDeleteDialogOpen : false
        });
    }

    handleRemoveItemFromBuyingList(){
        this.props.setBuyingList(this.handleRemoveItemFromList());
    }
    
    //Delivery
    handleInsertItem(){
        const newItemList = this.props.itemList[this.state.gotitemindex];
        //console.log(this.props.itemList[this.state.wantToDeleteIndex]);
        return newItemList;
    }
    
    Gotitem(){
        this.handleAddItemToSuccessList()
    }

    handleAddItemToSuccessList = () =>{
        this.props.setOnSuccessList(this.handleInsertItem())
        //console.log(this.state.confirmItem);
        this.props.setOnDeliveryList(this.handleRemoveItemFromList(),this.state.confirmItem)
    }
    
    //Success
    setDialogVote(ch){
        this.setState({isVoteDialogOpen:ch});
    }
    handleClickViewTopic(index){
        this.setState({
            isWatchTopic:true,
            isWatchTopicid:this.props.itemList[index].topic.id,
        })
    }
    handleDialogClickVote(index){
        //console.log(this.props.itemList[index])
        this.setState({
            isVoteDialogOpen : true,
            wantToVoteIndex : index,
            wantToVoteItemName: this.props.itemList[index].topic.topicName,
            wantToVoteSellerName: this.props.itemList[index].user.firstName,
            wantToVoteItemindex: this.props.itemList[index].id,
        });
    }
    render() {
        return (
            
            <TopicItemListView
                //Data
                itemList={this.props.itemList}

                //Buying
                isDeleteDialogOpen={this.state.isDeleteDialogOpen}
                wantToDeleteIndex={this.state.wantToDeleteIndex}
                wantToDeleteName={this.state.wantToDeleteName}
                redirectToCart={this.state.redirectToCart}
                redirectToid={this.state.redirectToid}

                onClickCartBuying={this.handleClickCartTopic}
                onClickRemove={this.handleDialogClickRemove}
                onDialogSubmit={this.handleDialogClickSubmit}
                onDialogCancel={this.handleDialogClickCancel}

                //Await
                isAwaitingDelivery={this.props.isAwaitingDelivery}

                //OnDelivery
                Gotitem={this.Gotitem}

                //check page
                isBuying={this.props.isBuying}
                isOnDelivery={this.props.isOnDelivery}
                isSuccess={this.props.isSuccess}

                //success
                isVoteDialogOpen={this.state.isVoteDialogOpen}
                wantToVoteItemName={this.state.wantToVoteItemName}
                wantToVoteSellerName={this.state.wantToVoteSellerName}
                wantToVoteItemindex={this.state.wantToVoteItemindex}
                wantToVoteSellerindex={this.state.wantToVoteSellerindex}
                isWatchTopic={this.state.isWatchTopic}
                isWatchTopicid={this.state.isWatchTopicid}
                onClickViewTopic={this.handleClickViewTopic}
                onClickVote={this.handleDialogClickVote}
                setDialogVote={ch => this.setDialogVote(ch)}
            />
        )
    }
}
TopicItemList.propTypes = {
    //Event
    setBuyingList: PropTypes.func,
    setAwaitingList: PropTypes.func,
    setOnDeliveryList: PropTypes.func,
    setOnSuccessList: PropTypes.func,

    itemList: PropTypes.array.isRequired,

    //check page
    isBuying: PropTypes.bool,
    isOnDelivery: PropTypes.bool,
    isAwaitingDelivery: PropTypes.bool,
    isSuccess: PropTypes.bool,

};