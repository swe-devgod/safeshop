import React, { Component } from 'react';
import { withStyles, InputAdornment, Typography } from '@material-ui/core';
import { Redirect, Link, Router } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import Grid from '@material-ui/core/Grid';
import TopicItemList from './components/topic-item-list'
import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({

});

class BuyerManagementView extends Component{
    constructor(props){
        super(props);
    }

    componentDidUpdate() {
        function SideBar() {
            return (
                <React.Fragment>
                    <Link to="/profile">
                        <ListItem button>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="User Profile" />
                        </ListItem>
                    </Link>
                    <Divider />
                    <Link to="/buyer-management">
                        <ListItem button>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText primary="Buyer Management" />
                        </ListItem>
                    </Link>
                    <Divider />
                    <Link to="/seller-management">
                        <ListItem button>
                        <ListItemIcon>
                            <SendIcon />
                        </ListItemIcon>
                        <ListItemText primary="Seller Management" />
                        </ListItem>
                    </Link>
                    <Divider />
                </React.Fragment>
            );
        }

        this.props.setSideBarItems(
            <Router history={this.props.browserHistory}>
                <SideBar />
            </Router>
        );
    }

    render(){
        const { classes } = this.props;

        if (!!!this.props.isAuthenticated) {
            return <Redirect to={`/login`} />
        }
        return(
            <Grid container justify="center">
                <Grid item xs={12} md={10} direction="column">
                    <Grid item>
                        <h1>Buyer Management</h1>
                    </Grid>
                    <Grid item>
                        <Paper
                            style={{overflow: 'hidden'}}
                        >
                            <Tabs
                                fullWidth
                                value={this.props.menuIndex}
                                onChange={this.props.onMenuChange}
                            >
                                <Tab label="Buying"/>
                                <Tab label="Awaiting Delivery"/>
                                <Tab label="On delivery"/>
                                <Tab label="Success"/>
                            </Tabs>
                        </Paper>
                    </Grid>
                    <Grid item>
                    
                        <SwipeableViews
                            index={this.props.menuIndex}
                            onChangeIndex={this.props.onSwipeMenuChange}
                        >
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>Buying</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการสินค้าทั้งหมดที่รอดำเนินการชำระเงิน</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        isBuying={true}
                                        itemList={this.props.buyinglist}
                                        setBuyingList={this.props.setBuyingList}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>Waiting Delivery</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการคำสั่งซื้อที่รอการจัดส่ง</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        itemList={this.props.awaitingDeliveryList}
                                        isAwaitingDelivery={true}
                                        setAwaitingList={this.props.setAwaitingList}
                                        setOnDeliveryList={this.props.setOnDeliveryList}
                                        onDeliveryList={this.props.onDeliveryList}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>On Delivery</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการคำสั่งซื้อที่อยู่ระหว่างการจัดส่ง</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        itemList={this.props.onDeliveryList}
                                        isOnDelivery={true}
                                        setOnDeliveryList={this.props.setOnDeliveryList}
                                        setOnSuccessList={this.props.setOnSuccessList}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>Success</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการสินค้าที่สั่งซื้อทั้งหมด</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        itemList={this.props.successList}
                                        isSuccess={true}
                                    /> 
                                </Grid>
                            </Grid>
                        </SwipeableViews>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}
BuyerManagementView.propTypes ={
    //Init
    setSideBarItems: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.func.isRequired,
    //Data

    menuIndex: PropTypes.number.isRequired,
    buyinglist: PropTypes.array.isRequired,
    awaitingDeliveryList: PropTypes.array.isRequired,
    onDeliveryList: PropTypes.array.isRequired,
    successList: PropTypes.array.isRequired,

    //Event
    setBuyingList: PropTypes.func.isRequired,
    setAwaitingList: PropTypes.func.isRequired,
    setOnDeliveryList: PropTypes.func.isRequired,
    setOnSuccessList: PropTypes.func.isRequired,

    onMenuChange: PropTypes.func.isRequired,
    onSwipeMenuChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(BuyerManagementView);