import React, { Component } from 'react';
import BuyerManagementView from './buyer-management-view'
import { AuthService } from '../../components/auth-service';

import pic1 from '../index/images/shoes.jpg'
import pic2 from '../index/images/watches.jpg'
import pic3 from '../index/images/bag.jpg'
import PropTypes from 'prop-types';

const mockup_selling_list = [{id: 0, topicName: 'BNKPhoto',topicRating: 3.4, topicRatingCount: 35, topicPrice: 150, amount: 37, img: pic1, buyerName: 'สมชาย', deliveryMethod: 'ThaiPost', address: 'Manhattan, USA'},
                            {id: 1, topicName: 'Audii',topicRating: 4.7, topicRatingCount: 123, topicPrice: 3900, amount: 11, img: pic2,buyerName: 'สมศรี', deliveryMethod: 'EMS', address: 'Bangkok, Thailand'},
                            {id: 2, topicName: 'Kitkat',topicRating: 1.4, topicRatingCount: 7, topicPrice: 20, amount: 95, img: pic3,buyerName: 'สมหญิง', deliveryMethod: 'Kerry', address: 'Tokyo, Japan'}];
const mockup_awaiting_delivery_list = [{id: 2, topicName: 'Kitkat',topicRating: 1.4, topicRatingCount: 7, topicPrice: 20, amount: 5, img: pic3, buyerName: 'สมชาย', deliveryMethod: 'ThaiPost', address: 'Manhattan, USA',sellerName:'FewGod'},
                                        {id: 3, topicName: 'Audii',topicRating: 4.7, topicRatingCount: 123, topicPrice: 3900, amount: 3, img: pic2, buyerName: 'สมศรี', deliveryMethod: 'EMS', address: 'Bangkok, Thailand',sellerName:'CheckGod'},
                                        {id: 0, topicName: 'BNKPhoto',topicRating: 3.4, topicRatingCount: 35, topicPrice: 150, amount: 37, img: pic1, buyerName: 'สมหญิง', deliveryMethod: 'Kerry', address: 'Tokyo, Japan',sellerName:'PhanGod'}];
const mockup_on_delivery_list = [{id: 4, topicName: 'สายชาร์จมือถือ',topicRating: 2.7, topicRatingCount: 15, topicPrice: 79, amount: 1, img: pic2, buyerName: 'สมศักดิ์', deliveryMethod: 'ThaiPost', address: 'Kasetsart University, Thailand', trackingNumber: 'SAF0AB359',sellerName:'WaveGod'}];
const mockup_success_list = [{id: 5,pricePerItem: 58, amount: 2,
                                topic:{id: 5, topicName: 'วิตามินซี 1000mg 60เม็ด', thumbnail: pic1, topicVoteCount: 2, topicRating: 0.37},
                               address: 'Loey, Thailand', trackingNumber: 'SAF0AB359'},
                        {id: 6, pricePerItem: 49, amount: 10,
                            topic:{id: 6, topicName: 'ซองจดหมาย', thumbnail: pic2, topicVoteCount: 0, topicRating: 0},
                            address: 'Chiangmai, Thailand', trackingNumber: 'SAF15A36'},
                        {id: 7, pricePerItem: 50000, amount: 99,
                            topic:{id: 7, topicName: 'กิ่งไม้โง่ๆ', thumbnail: pic3, topicVoteCount: 11, topicRating: 2.1},
                            address: 'Chonburi, Thailand'}];
export default class BuyerManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuIndex : 0,
            buyinglist : [],
            awaitingDeliveryList : [],
            onDeliveryList : [],
            successList : [],
        };
        
        this.handleMenuIndexChange = this.handleMenuIndexChange.bind(this);
        this.handleSwipeMenuIndexChange = this.handleSwipeMenuIndexChange.bind(this);

        this.setBuyingList = this.setBuyingList.bind(this);
        this.setAwaitingList = this.setAwaitingList.bind(this);
        this.setOnDeliveryList = this.setOnDeliveryList.bind(this);
    }

    componentDidMount() {
        document.title = "Buyer Management - SafeShop";
        //fetch data here
        fetch('/api/management/buyer/checkout', {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
        .then(res1 => {
            fetch('/api/management/buyer/awaiting-shipment', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then(res2 => {
                fetch('/api/management/buyer/shipping', {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json'
                    }
                })
                .then(res3 => {
                    fetch('/api/management/buyer/success', {
                        method: 'GET',
                        headers: {
                            'Accept': 'application/json'
                        }
                    })
                    .then(res4 => {
                        if (res1.status === 200) {   
                            res1.json().then(json => {
                                this.setState({
                                    buyinglist: json.map(c => ({ ...c })),
                                });
                                //console.log(json)
                            });
                        }
                        
                        if (res2.status === 200) {   
                            res2.json().then(json => {
                                this.setState({
                                    awaitingDeliveryList: json.map(c => ({ ...c })),
                                });
                                //console.log(json)
                            });
                        }
                        
                        if (res3.status === 200) {   
                            res3.json().then(json => {
                                this.setState({
                                    onDeliveryList: json.map(c => ({ ...c })),
                                });
                                //console.log(json)
                            });
                        }
                        
                        if (res4.status === 200) {   
                            res4.json().then(json => {
                                this.setState({
                                    successList: json.map(c => ({ ...c })),
                                });
                                //console.log(json)
                            });
                        }

                    })
                })
            })
        }) 
        .catch(err => console.error(err));
    }

    handleMenuIndexChange = (event, menuIndex) =>{
        this.setState({
            menuIndex
        });
    }

    handleSwipeMenuIndexChange = index => {
        this.setState({ 
            menuIndex: index 
        });
    };
    

    setBuyingList(data){
        this.setState({
            buyinglist : data
        });
        //บอกกลับไปหาback
    }

    setAwaitingList(data){
        this.setState({
            awaitingDeliveryList : data
        });
    }
    setOnDeliveryList(data,itemID){
        this.setState({
            onDeliveryList : data
        });
        
        //console.log(itemID)
        fetch('/api/management/buyer/confirm', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(itemID)
        }).then(res => {
            if (res.status == 200) {
                //console.log(res);
            } 
            else {
                //console.log(res)
            }
        });
        //fetch data back to db that Item is gone
    }
    setOnSuccessList(data){
        this.state.successList.push(data)
    }

    render() {
        return <BuyerManagementView
                    //Init
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    browserHistory={this.props.history}
                    
                    isAuthenticated={AuthService.isAuthenticated()}

                    //Data
                    menuIndex={this.state.menuIndex}
                    buyinglist={this.state.buyinglist}
                    awaitingDeliveryList={this.state.awaitingDeliveryList}
                    onDeliveryList={this.state.onDeliveryList}
                    successList={this.state.successList}

                    //Event
                    setBuyingList={data => this.setBuyingList(data)}
                    setAwaitingList={data => this.setAwaitingList(data)}
                    setOnDeliveryList={this.setOnDeliveryList}
                    setOnSuccessList={data => this.setOnSuccessList(data)}

                    onMenuChange={this.handleMenuIndexChange}
                    onSwipeMenuChange={this.handleSwipeMenuIndexChange}

                />
    }
}
BuyerManagement.propTypes = {
    dySec : PropTypes.any.isRequired
};