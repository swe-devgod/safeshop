import React, { Component } from 'react';
import FileUploadingView from '../../../components/file-uploading/';
import { SafeShopSingleFileUploader } from '../../../lib/safeshop-uploader';

export default class FileUploadingPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            filesPercent: null,
            isUploading: false
        };
    }

    componentDidMount() {
        document.title = "FileUploading - SafeShop";
        this.props.dySec.setSideBarItems(null);
    }
    
    handleFileChanged(files) {
        this.setState({
            files: files.map(f => f.file),
            filesPercent: files.map(f => f.percent)
        });
    }

    async upload() {
        for (let i = 0; i < this.state.files.length && this.state.isUploading; i++) {
            const uploader = new SafeShopSingleFileUploader(this.state.files[i]);
            uploader.onProgress = (cur, end) => {
                const newFilesPercent = this.state.filesPercent.slice();
                newFilesPercent[i] = cur / end * 100;
                this.setState({
                    filesPercent: newFilesPercent
                });
            };
            try {
                await uploader.upload('/api/upload', { commentId: 1 });
            } catch (err) {
                console.log(err);
                break;
            }
        }
        // uploading is being done.
        this.setState(state => ({
            isUploading: !!!state.isUploading
        }));
    }

    // upload(index) {
    //     if (!!!this.state.isUploading) {
    //         return;
    //     }

    //     if (index >= this.state.files.length) {
    //         this.setState(state => ({
    //             isUploading: !!!state.isUploading
    //         }));
    //         return;
    //     }

    //     this.uploader = new SafeShopSingleFileUploader(this.state.files[index]);
    //     this.uploader.onProgress = (cur, end) => {
    //         const copy = this.state.filesPercent.slice();
    //         copy[index] = cur / end * 100;
    //         this.setState({
    //             filesPercent: copy
    //         });
    //     };
    //     this.uploader.onUploadEnd = () => {
    //         this.uploader = null;
    //         this.uploadFunc(index + 1);
    //     };
    //     this.uploader.upload('/api/upload', { commentId: 1 });
    // }

    handleUploadButtonClicked() {
        this.setState(state => {
            return { isUploading: !!!state.isUploading }
        }, () => {
            if (this.state.isUploading) {
                Promise.resolve(this.upload());
            } else {
                if (this.uploader != null) {
                    this.uploader.abort();
                }
            }
        });
    }

    render() {
        return <FileUploadingView 
                    mobileItemHeight={36}
                    desktopItemHeight={72}
                    accept='image/jpeg, image/png'
                    isUploading={this.state.isUploading}
                    fileUploadingPercent={this.state.filesPercent}
                    onUploadButtonClicked={this.handleUploadButtonClicked.bind(this)}
                    onFileChanged={this.handleFileChanged.bind(this)}
                    showItems={3} />
    }
}