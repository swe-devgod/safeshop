import React, { Component } from 'react';
import UploadDevView from './upload-view';
import { SafeShopSingleFileUploader2 } from '../../../lib/safeshop-uploader-2';

const imgs = [
    "https://static-cdn.jtvnw.net/ttv-boxart/Fortnite-85x113.jpg",
    "https://i.ytimg.com/vi/DZ9wk6aVjT0/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLAn1dUdepgHRWzp1poMKJEvdLfcPQ",
    "https://i.ytimg.com/vi/xSKFPDe3QvE/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLDYY07pQjgEWk5G_k5Wxi5ZbdQTxA",
    "https://static-cdn.jtvnw.net/ttv-boxart/Fortnite-85x113.jpg",
    "https://i.ytimg.com/vi/DZ9wk6aVjT0/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLAn1dUdepgHRWzp1poMKJEvdLfcPQ",
    "https://i.ytimg.com/vi/xSKFPDe3QvE/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLDYY07pQjgEWk5G_k5Wxi5ZbdQTxA",
    "https://static-cdn.jtvnw.net/ttv-boxart/Fortnite-85x113.jpg",
    "https://i.ytimg.com/vi/DZ9wk6aVjT0/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLAn1dUdepgHRWzp1poMKJEvdLfcPQ",
    "https://i.ytimg.com/vi/xSKFPDe3QvE/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLDYY07pQjgEWk5G_k5Wxi5ZbdQTxA",
    "https://static-cdn.jtvnw.net/ttv-boxart/Fortnite-85x113.jpg",
    "https://i.ytimg.com/vi/DZ9wk6aVjT0/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLAn1dUdepgHRWzp1poMKJEvdLfcPQ",
    "https://i.ytimg.com/vi/xSKFPDe3QvE/hqdefault.jpg?sqp=-oaymwEiCNIBEHZIWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLDYY07pQjgEWk5G_k5Wxi5ZbdQTxA",
];

export default class UploadDevPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imgSrc: '',
            filesPercent: [],
        };
        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
    }

    componentDidMount() {
        document.title = "Upload - SafeShop";
        this.props.dySec.setSideBarItems(null);
    }

    onSubmitButtonClicked(formData) {
        const uploader = new SafeShopSingleFileUploader2(formData.file);
        uploader.onProgress = (cur, length) => {
            this.setState({ uploadedInPercent: (cur / length) * 100 });
        };

        let serverUrl = '';
        uploader.onUploadStart = json => {
            serverUrl = json.url;
        };
        uploader.onUploadEnd = () => {
            this.setState({
                imgSrc: serverUrl
            })
        }
        uploader.upload('/api/upload', {
            commentId: 1
        });
    }

    handleFileChanged(file) {
        console.log(file);
        this.setState({
            filesPercent: Array(file.length).fill(0)
        });
    }

    render() {
        return <UploadDevView 
                    items={imgs}
                    dispatcher={this.props.dispatcher}
                    browserRouterHistory={this.props.history}
                    imgSrc={this.state.imgSrc}
                    setAppBarItems={this.props.dySec.setAppBarItems}
                    browserHistory={this.props.history}
                    uploadedInPercent={this.state.uploadedInPercent} 
                    onSubmitButtonClicked={this.onSubmitButtonClicked}
                    onFileChanged={this.handleFileChanged.bind(this)}
                    fileUploadingPercent={this.state.filesPercent}
                    />
    }
}