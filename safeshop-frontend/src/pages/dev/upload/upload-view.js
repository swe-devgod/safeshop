import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';
import { Link, Router } from 'react-router-dom';
import FileUploadingView from '../../../components/file-uploading';
import NewFileUploadingView from '../../../components/new-file-uploading';
import DeafultSelectedFileUploading from '../../../components/deafult-selected-file-uploading';

const styles = theme => ({
    form: {
        width: '100%', // Fix IE11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

class MyCustom extends Component {
    componentDidMount() {
        console.log('MyCustom: ', 'test');
    }

    render() {
        return <h3>{this.props.text}</h3>
    }
}

class UploadDevView extends Component {
    constructor(props) {
        super(props);
        console.log('UploadDevView ', this.props.browserRouterHistory);
        this.state = {
            file: null
        };

        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
    }

    onSubmitButtonClicked() {
        this.props.onSubmitButtonClicked({
            file: this.state.file
        });
    }

    handleClose = () => {
        this.setState({ open: false });
        this.setState((state, props) => ({
            isActive: !state.isActive
        }));
    };
    
    componentDidMount() {
        console.log('upload-view: componentDidMount');
    }

    componentDidUpdate() {
        console.log('upload-view: componentDidUpdate');
    }

    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <DeafultSelectedFileUploading 
                    itemWidth={150}
                    itemHeight={150}
                    items={this.props.items}
                    onDelete={index => console.log(index)}
                />
                {/* <FileUploadingView 
                    onFileChanged={this.props.onFileChanged}
                    fileUploadingPercent={this.props.fileUploadingPercent}
                    accept='image/jpeg, image/png' 
                    showItems={3} /> */}
            </React.Fragment>
        );
    }
}

UploadDevView.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(UploadDevView);