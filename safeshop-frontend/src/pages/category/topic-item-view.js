import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { StarRating } from '../../components/star-rating';

const styles = {
    card: {
        //maxWidth: 345,
        width: 345,
        cursor: 'pointer'
    },
    cardAction: {
        maxWidth: 'inherit'
    },
    media: {
      // ⚠️ object-fit is not supported by IE11.
      objectFit: 'cover',
    },
    ratings: {
        marginLeft: 'auto'
    },
    header: {
        minWidth: 0,
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
    },
    content: {
        //minWidth: 0,
        //whiteSpace: 'nowrap',
        //textOverflow: 'ellipsis',
        overflow: 'hidden',
        lineHeight: '1.8em',
        maxHeight: '3.6em'
    }
};

class TopicItemView extends Component {
    constructor(props) {
        super(props);
    }

    
    render() {
        const { 
            classes, 
            header,
            cover,
            price,
            ratings,
            ratingCount
        } = this.props;

        return (
            <Card className={classes.card}>
                <CardMedia
                    component="img"
                    alt={header}
                    className={classes.media}
                    height="140"
                    image={cover}
                    title={header}/>
                <CardContent>
                    <Typography className={classes.header} gutterBottom component="h4">
                        {header}
                    </Typography>
                    { /*<Typography className={classes.content} component="p">
                        รูปภาพBNK48 ขายแบบทั้งset ของแท้ มีทั้งมิวนิค จูเน่และอีกหลายๆคน
                        </Typography>
                      */ 
                    }
                </CardContent>
                
                <CardActions>
                    <Typography gutterBottom component="h3">
                        {this.numberFormat(price)}฿
                    </Typography>
                    <StarRating 
                        className={classes.ratings}
                        ratings={ratings} 
                        ratingCount={ratingCount} />
                </CardActions>
            </Card>
        );
    }

    numberFormat(number) {
        const dotIndex = number.lastIndexOf('.');
        let curIndex = dotIndex === -1 ? number.length - 1 : dotIndex - 1;
        let counter = 0;
        let returnNumber = dotIndex === -1 ? '' : number.substr(dotIndex - number.length);
        while (curIndex > 0) {
            returnNumber = number[curIndex] + returnNumber;
            counter++;
            if (counter === 3) {
                returnNumber = ',' + returnNumber;
                counter = 0;
            }
            curIndex--;
        }
        returnNumber = number[curIndex] + returnNumber;
        return returnNumber;
    }
}

TopicItemView.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(TopicItemView);