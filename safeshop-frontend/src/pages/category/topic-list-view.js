import React, { Component } from 'react';
import { Link, Redirect, Router } from 'react-router-dom';

import PropTypes, { string } from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './topic-list-view.styles';
import Grid from '@material-ui/core/Grid';
import TopicItemView from './topic-item-view';

import ArrowForwardIcon from '@material-ui/icons/ArrowForwardIos';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import Divider from '@material-ui/core/Divider';
import ProgressView from '../../components/progress-view';

const drawerStyles = theme => ({
    nested: {
        paddingLeft: theme.spacing.unit * 6,
    }
});

const TopicDrawerItemList = withStyles(drawerStyles)(
    class extends Component {
        render() {
            const { classes, categories } = this.props;
            return (
                <React.Fragment>
                    {
                        categories && categories.map(category => (
                            <React.Fragment key={category.id}>
                                <Router history={this.props.browserHistory}>
                                    <React.Fragment>
                                        <Link to={`/categories/${category.id}`}>
                                            <ListItem button>
                                                <ListItemIcon>
                                                    <ArrowForwardIcon />
                                                </ListItemIcon>
                                                <ListItemText primary={category.categoryName} />
                                            </ListItem>
                                            <Divider />
                                        </Link>
                                        {
                                            category.childCategories != null && category.childCategories.map(childCategory => (
                                                <Link key={childCategory.id} to={`/categories/${childCategory.id}`}>
                                                    <ListItem className={classes.nested} button>
                                                        <ListItemIcon>
                                                            <InboxIcon />
                                                        </ListItemIcon>
                                                        <ListItemText primary={childCategory.categoryName} />
                                                    </ListItem>
                                                    <Divider />
                                                </Link>
                                            ))
                                        }
                                    </React.Fragment>
                                </Router>
                            </React.Fragment>
                        ))
                    }
                </React.Fragment>
            );
        }
    }
);

class TopicListViewImpl extends Component {

    componentDidMount() {
        
    }

    componentDidUpdate() {
        if (this.props.categories != null) {
            this.props.setSideBarItems(<TopicDrawerItemList browserHistory={this.props.browserHistory} categories={this.props.categories} />);
        }
    }

    render() {
        if (!!!this.props.isKnownState) {
            return <ProgressView />;
        }

        const { topics } = this.props;

        return (
            <React.Fragment>
                <Grid container spacing={16} justify="center">
                    { 
                        topics.map(topic => 
                            <Grid item key={topic.id}>
                                <Link to={`/topic/${topic.id}`}>
                                    <TopicItemView 
                                        header={topic.name}
                                        cover={topic.thumbnail}
                                        price={topic.price.toString()}
                                        ratings={topic.rating}
                                        ratingCount={topic.ratingCount} />
                                </Link>
                            </Grid>
                        )
                    }
                </Grid>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(TopicListViewImpl);