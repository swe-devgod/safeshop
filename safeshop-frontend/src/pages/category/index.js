import React, { Component } from 'react';
import TopicListView from './topic-list-view';

export default class TopicListPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isKnownState: false,
            curCategoryId: 1,
            categories: [],
            topics: []
        }
    }

    componentDidMount() {
        document.title = "ProductList - SafeShop";
        const categoryId = +this.props.match.params.id || 1;
        console.log(categoryId);
        fetch('/api/category/list', {
            headers: {
                Accept: 'application/json'
            }
        })
        .then(res => res.json())
        .then(categoriesJson => {
            fetch(`/api/category/${categoryId}/`, {
                headers: {
                    Accept: 'application/json'
                }
            })
            .then(res => res.json())
            .then(topicJson => {
                this.setState({ 
                    isKnownState: true,
                    curCategoryId: categoryId,
                    categories: categoriesJson,
                    topics: topicJson
                });
            })
            .catch(err => console.error(err));
        })
        .catch(err => console.error(err));
    }

    componentDidUpdate(prevProps) {
        console.log('Update: ', prevProps);
        console.log(this.props);
        console.log(this.state);
        const nextCategoryId = +this.props.match.params.id || 1;
        if (nextCategoryId !== this.state.curCategoryId) {
            this.setState({ 
                isKnownState: false,
                curCategoryId: nextCategoryId,
            });
        } else {
            if (!!!this.state.isKnownState) {
                console.log('fetch');
                fetch(`/api/category/${this.state.curCategoryId}/`, {
                    headers: {
                        Accept: 'application/json'
                    }
                })
                .then(res => res.json())
                .then(topicJson => {
                    this.setState({ 
                        isKnownState: true,
                        topics: topicJson
                    });
                })
                .catch(err => console.error(err));
            } else {
                console.log('same');
            }
        }
    }

    render() {
        return <TopicListView 
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    categories={this.state.categories}
                    topics={this.state.topics} 
                    browserHistory={this.props.history}
                    isKnownState={this.state.isKnownState} />
    }
}