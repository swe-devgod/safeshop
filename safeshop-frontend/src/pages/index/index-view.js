import React, { Component } from 'react';

import './style.css';
import { Banner } from './components/banner.js';
import ItemsGrid from './components/items-grid';

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core';

import MensImg from './images/mens.jpg'
import WomensImg from './images/womens.jpg'
import BoysImg from './images/boy.jpg'
import GirlsImg from './images/girl.jpg'

import ComImg from './images/comGadgets.jpg'
import GardenImg from './images/gardenGadgets.jpg'
import KitchenImg from './images/kitchenGadgets.jpg'
import PhotoImg from './images/photography.jpg'

import ShoesImg from './images/shoes.jpg'
import WatchImg from './images/watches.jpg'
import BagImg from './images/bag.jpg'
import RingImg from './images/ring.jpg'

import FurnitureImg from './images/furnitures.jpg'
import ElectronicImg from './images/electronics.jpg'
import PlantImg from './images/plant.jpg'

import MusicImg from './images/music.jpg'
import CollectImg from './images/collection.jpg'

const gridList = [
    {
        title: "Clothes",
        itemList: [
            { imgPath: MensImg , label: "MENS"},
            { imgPath: WomensImg , label: "WOMENS"},
            { imgPath: BoysImg , label: "BOYS"},
            { imgPath: GirlsImg , label: "GIRLS"},
        ]
    },

    {
        title: "Gadgets",
        itemList: [
            { imgPath: ComImg , label: "COMPUTER GADGETS"},
            { imgPath: KitchenImg , label: "KITCHEN GADGETS"},
            { imgPath: GardenImg , label: "GARDEN GADGETS"},
            { imgPath: PhotoImg , label: "PHOTOGRAPHY"},
        ]
    },

    {
        title: "Accessories",
        itemList: [
            { imgPath: ShoesImg , label: "SHOES"},
            { imgPath: WatchImg , label: "WATCHES"},
            { imgPath: BagImg , label: "BAGS"},
            { imgPath: RingImg , label: "DECORATION"}
        ]
    },

    {
        title: "Home",
        itemList: [
            { imgPath: FurnitureImg , label: "FURNITURE"},
            { imgPath: ElectronicImg , label: "ELECTRONICS"},
            { imgPath: PlantImg , label: "PLANT"},
            
        ]
    },

    {
        title: "Hobbies",
        itemList: [
            { imgPath: MusicImg , label: "MUSIC"},
            { imgPath: CollectImg , label: "COLLECTION"},      
        ]
    },
];

const styles = theme => ({
    viewLayout: {
        marginTop: -theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
    },
});

class IndexView extends Component {
    render() {
        const { classes } = this.props;

        return (
            <Grid container justify="center" className={classes.viewLayout}>
                <Grid item md={10} container spacing={16}>
                    <Grid item xs={12}>
                        <Banner />
                    </Grid>
                    { gridList.map(grid =>
                        <Grid item xs={12}> 
                            <ItemsGrid title={grid.title} itemList={grid.itemList} />
                        </Grid>
                    )}
                </Grid>
            </Grid>
        );
    }
}

  
export default withStyles(styles)(IndexView);
