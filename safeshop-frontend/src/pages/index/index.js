import React, { Component } from 'react';
import IndexView from './index-view';

export default class IndexPage extends Component {

    componentDidMount() {
        document.title = "Index - SafeShop";
        this.props.dySec.setSideBarItems(null);
    }

    render() {
        return <IndexView />;
    }
}