import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    mainPaper: {
        padding: theme.spacing.unit * 2,
    },
    buttonImage: {
        width: "100%"
    },
    buttonLayout: {
        overflow: "hidden",
        borderRadius: 4,
    },
    itemOverlay: {
        position: "absolute",
        height: "100%",
        width: "100%",
        top: 0,
        left: 0,
        textAlign: 'center',
        opacity: 0,
        backgroundColor: 'rgba(0,0,0,0.5)',
        transition: '.5s ease',
        '&:hover' : {
            opacity: 1,
            filter: 'alpha(opacity=100)',
        }
    },
    itemLabelOverlay: {
        color: 'white',
        position: 'absolute',
        width: '100%',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        textAlign: 'center',
    },
    typoItem: {
        marginTop: theme.spacing.unit * 1,
    },
    img: {
        display: 'block',
        position: 'relative',
        width: '100%',
        transition: 'all .4s linear',
        '&:hover': {
            transform: 'scale(1.2)',
        }
    }
});

class ItemsGrid extends Component{
    render() {
        const { title, itemList, classes } = this.props;
        
        return(
            <Paper square className={classes.mainPaper}>
                <Typography gutterBottom variant="display2">{title}</Typography>
                <Grid container spacing={16}>
                    { itemList.map( item =>
                        <Grid item xs={6} sm={3} md={3}>
                            <ButtonBase className={classes.buttonLayout}>
                                <img src={item.imgPath} className={classes.img} alt="item.label"/>
                                <div className={classes.itemOverlay}>
                                    <Typography variant="h6" className={classes.itemLabelOverlay}>{item.label}</Typography>
                                </div>
                            </ButtonBase>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                                noWrap
                                className={classes.typoItem}
                            >
                                {item.label}
                            </Typography>
                        </Grid> 
                    )}
                </Grid>
            </Paper>
        )
    }
}

ItemsGrid.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles)(ItemsGrid);
