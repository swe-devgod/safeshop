import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import MobileStepper from '@material-ui/core/MobileStepper'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

import banner01 from '../images/banner01.jpg';
import banner02 from '../images/banner02.jpg';
import banner03 from '../images/banner03.jpg';
import banner01_m from '../images/Mobileban01.jpg';
import banner02_m from '../images/Mobileban02.jpg';
import banner03_m from '../images/Mobileban03.jpg';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const photos = [
    { imgPath: banner01, imgPathMobile: banner01_m },
    { imgPath: banner02, imgPathMobile: banner02_m },
    { imgPath: banner03, imgPathMobile: banner03_m }
];

const styles = theme => ({
    paper: {
        width: '100%',
    },
    divImg: {
        display: 'block',
        width: '100%',
    }
});

class BannerStepper extends Component{
    constructor(props){
        super(props)
        this.state = {
            activeStep: 0,
            // time: 0,
        };
        /* 
        setInterval(() =>{
            this.setState({
                time: this.state.time+1
            })
        }, 1000)
        */
    }
    handleNext = () => {
        this.setState(prevState => ({
            activeStep: (prevState.activeStep + 1)%photos.length,
            //time: this.state.time*0  
        }))
    };  
    
    handleBack = () => {
        this.setState(prevState => {
            let newActiveStep = prevState.activeStep - 1;
            if (newActiveStep < 0) {
                newActiveStep = photos.length - 1;
            }
            
            return { activeStep: newActiveStep, /* time: this.state.time*0 */ };
        }); 
    };

    handleStepChange = activeStep => {
        this.setState({ activeStep });
      };

    render() {
        const { classes, theme  } = this.props;
        const { activeStep } = this.state;
        // const { time } = this.state;
        const maxSteps = photos.length;

        /*
        if(time === 4){
            this.handleNext();
        }
        */

        return(
                <Paper className={classes.paper}>
                    <div id='HeaderBanner'>
                        <AutoPlaySwipeableViews
                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                            index={activeStep}
                            onChangeIndex={this.handleStepChange}
                            enableMouseEvents
                        >
                            {photos.map((step, index) => (
                                <picture>
                                    <source srcSet={step.imgPathMobile} media="(max-width: 599px)" />
                                    <img className={classes.divImg} src={step.imgPath} alt="banner" />
                                </picture>
                            ))}
                        </AutoPlaySwipeableViews>
                        <MobileStepper
                            steps={maxSteps}
                            position="static"
                            activeStep={activeStep}
                            className={classes.mobileStepper}
                            nextButton={
                                <Button size="small" onClick={this.handleNext}>
                                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                                </Button>
                            }
                            backButton={
                            <Button size="small" onClick={this.handleBack}>
                                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                                </Button>
                            }
                        />
                    </div>
                </Paper>
        )
    }
}

BannerStepper.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export const Banner = withStyles(styles,{ withTheme: true })(BannerStepper);
