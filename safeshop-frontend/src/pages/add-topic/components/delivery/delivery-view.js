import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MenuItem from '@material-ui/core/MenuItem';
import { TextField, withStyles, InputAdornment } from '@material-ui/core';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import MinusIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import Menu from '@material-ui/core/Menu';

const styles = theme => ({
    deliveryTextField: {
        display: 'inline-block',
        width: '100%'
    },
    fullWidth:{
        width: '100%',
    },
    button:{
        height: '56px',
        marginLeft: '10px'
    },
    gridDeliveryMarginTop:{
        marginTop: '5px'
    },
    gridBtn:{
        marginTop: '5px',
        marginLeft: '10px'
    },
    spanMarginLeft:{
        marginLeft: '15px'
    },
});

class DeliveryView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            achorElement: null,
            isDeliveryMenuOpen: false
        };
    }

    render() {
        const { classes } = this.props;
        
        return(
            <React.Fragment>
                <Grid item container alignItems="center" spacing={0}>
                    {
                        this.props.selectedDeliveryList &&
                        this.props.selectedDeliveryList.map((delivery, index) => (
                            <React.Fragment>
                                <Grid item xs={4} className={classes.gridDeliveryMarginTop}>
                                    {
                                        (delivery.isDefault == undefined || delivery.isDefault) ? //ถ้าเกิดรับมาจากEditหรือเป็นdefault
                                        <span className={classes.spanMarginLeft}>{delivery.name}</span>
                                        :
                                        <TextField variant='outlined'
                                            required label="ชื่อการขนส่ง"
                                            className={classes.deliveryTextField}
                                            value={delivery.name}
                                            onChange={e => this.props.onNameTextFieldChanged(e.target.value, index)}
                                            InputProps={{disableUnderline: true}}
                                            fullWidth
                                        />
                                    }
                                </Grid>
                                <Grid item xs={4} className={classes.gridDeliveryMarginTop}>
                                    <TextField required label="ราคา" type="number"
                                        variant='outlined'
                                        value={delivery.price}
                                        onKeyPress={e => { if ("0123456789".indexOf(e.key) === -1) { e.preventDefault(); }}}
                                        onChange={e => this.props.onPriceTextFieldChanged(e.target.value, index)}
                                        InputProps={{disableUnderline: true,
                                            endAdornment:<InputAdornment position="end">บาท</InputAdornment>}}
                                        className={classes.deliveryTextField}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={1} className={classes.gridBtn}>
                                    <Button variant="outlined" 
                                        onClick={e => this.props.onRemoveDeliveryClicked(e, index, delivery.isDefault)}
                                        className={classes.button}
                                        fullWidth
                                    >
                                        <MinusIcon/>
                                    </Button>
                                </Grid>
                            </React.Fragment>
                        ))
                    }
                    <Grid item xs={12}>
                        <Button 
                            variant='extendedFab'
                            onClick={e => this.setState({ achorElement: e.currentTarget, isDeliveryMenuOpen: true })}>
                            <AddIcon />
                            เพิ่มวิธีการจัดส่งพัสดุ
                        </Button>
                        <Menu 
                            anchorEl={this.state.achorElement}
                            open={Boolean(this.state.achorElement)}
                            onClose={() => this.setState({ achorElement: null })}
                            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                            transformOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                            {
                                    this.props.deliveryCarrierList &&
                                    this.props.deliveryCarrierList.map((delivery, index) => (
                                        <MenuItem 
                                            id={delivery.id} key={index} 
                                            index={index} 
                                            value={ delivery.name }
                                            onClick={e => {
                                                this.props.onSelectChanged(e, {
                                                    key: index,
                                                    props: {
                                                        id: delivery.id,
                                                        value: delivery.name,
                                                    }
                                                });
                                                this.setState({ achorElement: null });
                                            }}>
                                            { delivery.name }
                                        </MenuItem>
                                    ))
                            }
                        </Menu>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

DeliveryView.propTypes = {
    //เป็นการดูว่าpropsที่ส่งมานั้นถูกต้องไหม
    //Data
    selectedDeliveryList : PropTypes.array, //ไม่จำเป็นตอนแรกสามารถเว้นว่างไว้ได้ แล้วถ้าเลือกค่อยส่งมา
    deliveryCarrierList : PropTypes.array.isRequired,

    //Event
    onSelectChanged : PropTypes.func.isRequired,
    onPriceTextFieldChanged : PropTypes.func.isRequired,
    onNameTextFieldChanged : PropTypes.func.isRequired,
    onRemoveDeliveryClicked : PropTypes.func.isRequired,
    setDeliveryList : PropTypes.func.isRequired
};

export default withStyles(styles)(DeliveryView);