import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DeliveryView from './delivery-view';

export default class Delivery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showingCarrierList: this.props.deliveryCarrierList,
            selectedData: [],
        };
    }

    //bug: กดeditTopicไม่ว่าจะsubmitหรือยกเลิก ที่delivery ค่าที่แก้ไว้มันยังอยู่ (มันไม่ควรจะอยู่) ใช้deep coy แก้ได้เฉย

    componentDidMount(){
        if(this.props.isEditTopic){ //ถ้าเกิดเป็นการแก้ไขกระทู้ก็ให้ดึงข้อมูลเก่ามา
            this.setSelectedFromEditTopic(this.props.currentSelectedDeliveryList);
            this.setShowingCarrierListFromEditTopic(this.props.currentSelectedDeliveryList);
        }
    }

    setSelectedFromEditTopic(list){
        this.setState({
            selectedData : list,
        });
        this.props.setDeliveryList(list);
    }

    setShowingCarrierListFromEditTopic(list){
        const newShowingCarrierList = this.state.showingCarrierList.slice();
        for(let i = 0; i < list.length; i++){
            const indexToDelete = newShowingCarrierList.findIndex(e => e.id === list[i].id); //เอาelement eที่ได้ ไปดูa.id ว่าตรงกับเงื่อนไขมั้ยถ้าใช่ก็return index
            newShowingCarrierList.splice(indexToDelete, 1);
            this.setState({
                showingCarrierList: newShowingCarrierList
            });
        }
    }

    handleSelectChanged(e, child) {
        if (child.props.value != undefined) {
            const newShowingCarrierList = this.state.showingCarrierList.slice();
            const newSelectedData = this.state.selectedData.slice();

            if(child.props.value === '') {
                newSelectedData.push({name: '', price: '' , isDefault: false});
            } else {
                newShowingCarrierList.splice(+child.key, 1);
                newSelectedData.push({id:child.props.id, name: child.props.value, price: '' , isDefault: true});
            }
            this.props.setDeliveryList(newSelectedData);
            this.setState({
                showingCarrierList: newShowingCarrierList,
                selectedData: newSelectedData
            });
        }
    }

    handlePriceTextFieldChanged(value, index) {
        const newSelectedData = this.state.selectedData.slice();
        newSelectedData[index].price = value;
        this.props.setDeliveryList(newSelectedData);
        this.setState(() => {
            return{
                selectedData: newSelectedData
            }
        });
    }

    handleNameTextFieldChanged(value, index) {
        const newSelectedData = this.state.selectedData.slice();
        newSelectedData[index].name = value;
        this.props.setDeliveryList(newSelectedData);
        this.setState(() => {
            return{
                selectedData: newSelectedData
            }
        })
    }

    handleRemoveDeliveryClicked(e, index, isDefault) {
        const newSelectedData = this.state.selectedData.slice();
        const removedData = newSelectedData.splice(index, 1);
        const newShowingCarrierList = this.state.showingCarrierList.slice();

        if(isDefault || isDefault == undefined){ //undefined for editTopic
            newShowingCarrierList.push({id: removedData[0].id, name: removedData[0].name});
        }
        this.props.setDeliveryList(newSelectedData);
        this.setState(() => {
            return{
                showingCarrierList: newShowingCarrierList,
                selectedData: newSelectedData
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                <DeliveryView   
                    // DATA
                    selectedDeliveryList={this.state.selectedData}
                    deliveryCarrierList={this.state.showingCarrierList}

                    // FOR EVENT
                    onSelectChanged={(e, child) => this.handleSelectChanged(e, child)}
                    onPriceTextFieldChanged={(e, index) => this.handlePriceTextFieldChanged(e, index)}
                    onNameTextFieldChanged={(e, index) => this.handleNameTextFieldChanged(e, index)}
                    onRemoveDeliveryClicked={(e, index, isDefault) => this.handleRemoveDeliveryClicked(e, index, isDefault)}
                    setDeliveryList={this.props.setDeliveryList}
                />
            </React.Fragment>
        );        
    }
}

Delivery.propTypes = {
    //Data
    deliveryCarrierList : PropTypes.array.isRequired,
    //Event
    handleDeliveryInfoChanged : PropTypes.func.isRequired,
    setDeliveryList : PropTypes.func.isRequired,
    //For Edit
    isEditTopic : PropTypes.bool.isRequired,//จำเป็นเพราะdeliveryจะได้รู้ว่านี่เป็นการแก้ไขกระทู้หรือสร้างกระทู้ใหม่
    currentSelectedDeliveryList : PropTypes.array, //ไม่จำเป็นไว้เฉพาะตอนแก้ไขกระทู้
}