import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CategoryView from './category-view'

export default class Category extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return <CategoryView
                    //Data
                    categoryList ={this.props.categoryList}
                    selectedCategory={this.props.selectedCategory}
                    isDialogOpen={this.props.isDialogOpen}
                    dialogScroll={this.props.dialogScroll}
                    //Event
                    onDialogOpen={this.props.onDialogOpen}
                    onDialogSubmit={this.props.onDialogSubmit}
                    onDialogCancel={this.props.onDialogCancel}
                    onCategorySelected={this.props.onCategorySelected}
                    onChipDelete={this.props.onChipDelete}
                />
    }
}

Category.propTypes = {
    //Data
    categoryList : PropTypes.array.isRequired,
    selectedCategory : PropTypes.array.isRequired,
    isDialogOpen : PropTypes.bool.isRequired,
    dialogScroll : PropTypes.string.isRequired,
    //Event
    onDialogOpen : PropTypes.func.isRequired,
    onDialogSubmit : PropTypes.func.isRequired,
    onDialogCancel : PropTypes.func.isRequired,
    onCategorySelected : PropTypes.func.isRequired,
    onChipDelete : PropTypes.func.isRequired,
}