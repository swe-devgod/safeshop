import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import { Divider, DialogActions } from '@material-ui/core';
import { withStyles, InputAdornment, Typography } from '@material-ui/core';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';

import IconPlus from '@material-ui/icons/AddSharp';

const styles = theme => ({
    dialog:{
        margin: '8px 10px 8px 10px'
    },

    divider: {
        margin: '8px 10px 0 10px'
    },
    chipAddItem:{
        margin: 2 * 2,
        backgroundColor: 'rgb(255, 221, 184)',
        '&:hover' : {
            opacity: '1'
        },
    },
});

class CategoryView extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const { classes } = this.props;
        return(
            <React.Fragment>
                {
                    /*this.props.selectedCategoryLength === 0 ?
                        <Button 
                            variant='outlined' onClick={this.props.onDialogOpen}>
                            Select Category
                        </Button>
                    :
                        <Button
                            variant='outlined' onClick={this.props.onDialogOpen}>
                            Edit Category
                        </Button>*/
                }
                <Chip
                    onClick={this.props.onDialogOpen}
                    icon={<IconPlus/>}
                    label="Add category"
                    style={{margin: 2 * 2, backgroundColor: 'rgb(255, 221, 184)'}}
                    //className={classes.chipAddItem}
                />
                <Dialog 
                    fullWidth={true}
                    maxWidth = {'md'}
                    open={this.props.isDialogOpen}
                    scroll={this.props.dialogScroll}
                >
                    <DialogTitle>
                        <div>
                            {
                                this.props.selectedCategory.length === 0 ?
                                <span>เลือกประเภทของสินค้า</span>
                            :
                                <span>ประเภทสินค้าที่เลือก</span>
                            }
                        </div>
                        <div>
                            {
                                this.props.selectedCategory &&
                                this.props.selectedCategory.map((data, index) => (
                                    <Chip
                                        label={data.categoryName}
                                        onDelete={() => this.props.onChipDelete(data, index)}
                                    />
                                ))
                                
                            }
                        </div>
                        <Divider className={classes.divider}/>
                    </DialogTitle>
                    <DialogContent>
                        <div className={classes.dialog}>
                            <div>
                                <Grid container alignItems="center">
                                {
                                    this.props.categoryList &&
                                    this.props.categoryList.map((category, index) => (
                                            <Grid item xs={12} sm={6} md={4}>
                                                {
                                                    !!!category.isSelected ?
                                                    <FormControlLabel
                                                    control={
                                                        <Checkbox 
                                                            className={classes.checkBox} checked={false} 
                                                            onChange={() => this.props.onCategorySelected(category, index)} 
                                                            value={category.categoryName}
                                                        />
                                                    }
                                                    label={category.categoryName}
                                                    />
                                                    :
                                                    <FormControlLabel
                                                    control={
                                                        <Checkbox 
                                                            checked={true} 
                                                            onChange={() => this.props.onCategorySelected(category, index)} 
                                                            value={category.categoryName} 
                                                        />
                                                    }
                                                    label={category.categoryName}
                                                    />
                                                }
                                            </Grid>
                                    ))
                                }
                                </Grid>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Grid container spacing={16}>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    onClick={this.props.onDialogSubmit}>Submit
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    color="primary"
                                    onClick={this.props.onDialogCancel}>Cancel
                                </Button>
                            </Grid>
                        </Grid>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        )
    }
}
export default withStyles(styles)(CategoryView);

CategoryView.propTypes = {
    //Data
    categoryList : PropTypes.array.isRequired,
    selectedCategory : PropTypes.array.isRequired,
    isDialogOpen : PropTypes.bool.isRequired,
    dialogScroll : PropTypes.string.isRequired,
    //Event
    onDialogOpen : PropTypes.func.isRequired,
    onDialogSubmit : PropTypes.func.isRequired,
    onDialogCancel : PropTypes.func.isRequired,
    onCategorySelected : PropTypes.func.isRequired,
    onChipDelete : PropTypes.func.isRequired,
}