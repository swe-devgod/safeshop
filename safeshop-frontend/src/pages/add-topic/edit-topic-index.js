import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AddTopicView from './add-topic-view';
import { SafeShopSingleFileUploader } from '../../lib/safeshop-uploader';
import { AuthService } from '../../components/auth-service';

export default class EditTopic extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isKnownState : false,
            pageTitle: 'แก้ไขกระทู้สินค้า',
            topicIdToRedirect: 0,
            isRedirectAfterSubmit: false,
            redirectToIndex: false,

            deliveryCarrierList: [],
            selectedDeliveryList: [],

            dialogScroll: 'paper',
            isDialogOpen : false,
            showingCategoryList : [],
            selectedCategory: [],
            tempShowingCategoryList: [],
            tempSelectedCategory: [],
            wantToDeleteId: 0,

            files: [],
            filesPercent: [],
            isUploading: false,

            uploadingImages: [],
            uploadingImageFiles: [],

            topicThumbnail: 0,
            appendTopicPicture: [],
            removeTopicPicture: [],

            topicJson: [],

            //Tooltip
            isTopicNameTooltipOpen: false,
            isTopicPriceTooltipOpen: false,
            isTopicDesctriptionTooltipOpen: false,
            isTopicRemainingItemTooltipOpen: false,
            isCategoryTooltipOpen: false,
            isDeliveryTooltipOpen: false,
            deliveryTooltipMsg: '',
            isTopicPictureTooltipOpen: false,

            //before edit (receive topic info)
            prevTopicName: '',
            prevTopicDescription: '',
            prevTopicPrice: 0,
            prevTopicRemainingItem: 0,
            prevSelectedCategory: [],
            prevSelectedDelivery: [],
            prevImgs: [],

            //For send Edited data to server
            addCategories : [],
            removeCategories : [],

            addShippings : [],
            updateShippings : [],
            removeShippings : [],

            addPictures : [],
            removePictures : [],
            isEditThumbnail : false,
            isThumbnailFromPrev : true,
        };
        this.handleSumbit = this.handleSumbit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);

        this.handleDialogOpen = this.handleDialogOpen.bind(this);
        this.handleDialogSubmit = this.handleDialogSubmit.bind(this);
        this.handleDialogCancel = this.handleDialogCancel.bind(this);

        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleDialogChipDelete = this.handleDialogChipDelete.bind(this);
        this.handleChipQuickDelete = this.handleChipQuickDelete.bind(this);

        this.setDeliveryList = this.setDeliveryList.bind(this);

        //For compare before & after edit
        //this.compareTopicSelectedCategory = this.compareTopicSelectedCategory.bind(this);
        //this.compareTopicSelectedDelivery = this.compareTopicSelectedDelivery.bind(this);

        this.checkFormValidation = this.checkFormValidation.bind(this);
        this.handleCloseTooltip = this.handleCloseTooltip.bind(this);
    }

    async fetchDataAsync() {
        const apiGetDeliveryCarrierList = "/api/topic/shipping/list";
        const apiGetAvaiableCategoryList = "/api/category/list";
        const apiGetTopicURL = `/api/topic/${this.props.match.params.id}`

        function getJson(endpoint) {
            return new Promise((resolve, reject) => {
                fetch(endpoint)
                .then(res => res.json())
                .then(json => resolve(json))
                .catch(err => reject(err));
            });
        }

        const json1 = await getJson(apiGetDeliveryCarrierList);
        const json2 = await getJson(apiGetAvaiableCategoryList);
        const topicInfo = await getJson(apiGetTopicURL);
        const newUploadingImages = [];
        //const newTopicThumbnail = topicInfo.imgs[0].id; //ต้องการthumbnailส่งมาเปนimgId
        for(let i = 0; i < topicInfo.imgs.length; i++){
            newUploadingImages.push(topicInfo.imgs[i].url);
        }
        this.setState(() => {// real fetch data
            return {    
                showingCategoryList : json2.map(c => ({ ...c })),
                tempShowingCategoryList : json2.map(c => ({ ...c })),
                deliveryCarrierList : json1,
                selectedDeliveryList : topicInfo.shippings.map(c => ({ ...c })),
                selectedCategory : topicInfo.categories.map(c => ({ ...c })),
                topicJson : topicInfo,
                uploadingImages : newUploadingImages,
                //topicThumbnail : newTopicThumbnail,

                //for compare
                prevSelectedDelivery : topicInfo.shippings.map(c => ({ ...c })),
                prevSelectedCategory : topicInfo.categories.map(c => ({ ...c })),
                prevImgs : topicInfo.imgs,
                prevTopicName : topicInfo.productName,
                prevTopicDescription : topicInfo.productDescription,
                prevTopicPrice : topicInfo.productPrice,
                prevTopicRemainingItem : topicInfo.remainingItems,

                isKnownState: true,
            };
        }, () => {
             this.setShowingCategoryListFromEdit(this.state.topicJson.categories.map(c => ({ ...c })));
        });
    }

    componentDidMount() {
        document.title = "Edit Topic - SafeShop";
        Promise.resolve(this.fetchDataAsync());
    }

    setShowingCategoryListFromEdit(list){
        const newShowingCategoryList = this.state.tempShowingCategoryList.map(c => ({ ...c })); //undefined เพราะยังไม่ได้setState??
        for(let i = 0; i < list.length; i++){
            const indexToSet = newShowingCategoryList.findIndex(e => e.id === list[i].id); //เอาelement eที่ได้ ไปดูa.id ว่าตรงกับเงื่อนไขมั้ยถ้าใช่ก็return index
            newShowingCategoryList[indexToSet].isSelected = true;
            this.setState({
                tempShowingCategoryList: newShowingCategoryList,
                showingCategoryList: newShowingCategoryList,
                tempSelectedCategory: this.state.selectedCategory //setให้เริ่มมาโชว์เลย
            });
        }
    }

    handleCloseTooltip(){
        this.setState({
            isTopicNameTooltipOpen: false,
            isTopicPriceTooltipOpen: false,
            isTopicDesctriptionTooltipOpen: false,
            isTopicRemainingItemTooltipOpen: false,
            isCategoryTooltipOpen: false,
            isDeliveryTooltipOpen: false,
            isTopicPictureTooltipOpen: false,
        });
    }

    checkFormValidation(formData){
        let invalidInput = false;
        if(formData.topicName == undefined || formData.topicName.length === 0){
            this.setState({
                isTopicNameTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(formData.topicDescription == undefined || formData.topicDescription.length === 0){
            this.setState({
                isTopicDesctriptionTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(formData.topicPrice == undefined || formData.topicPrice.length === 0){
            this.setState({
                isTopicPriceTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(formData.topicRemainingItem == undefined || formData.topicRemainingItem.length === 0){
            this.setState({
                isTopicRemainingItemTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(this.state.selectedCategory.length === 0){
            this.setState({
                isCategoryTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(this.state.selectedDeliveryList.length === 0){
            this.setState({
                isDeliveryTooltipOpen: true,
                deliveryTooltipMsg: 'กรุณาเลือกวิธีจัดส่งสินค้า'
            })
            invalidInput = true;
        }
        if(this.state.uploadingImages.length === 0){
            this.setState({
                isTopicPictureTooltipOpen: true,
            })
            invalidInput = true;
        }
        const tempDeliveryList = this.state.selectedDeliveryList.slice();
        for(let i = 0; i <  tempDeliveryList.length; i++){
            if(tempDeliveryList[i].price == undefined || tempDeliveryList[i].price.length === 0){
                this.setState({
                    isDeliveryTooltipOpen: true,
                    deliveryTooltipMsg: 'กรุณาระบุราคาในการจัดส่งสินค้า'
                })
                invalidInput = true;
                break;
            }
        }
        if(invalidInput === false){
            return true;
        }else{
            return false;
        }
    }

    handleUploadPicture(formData){
        const uploader = new SafeShopSingleFileUploader(formData.file);
        uploader.upload('/api/upload');
    }

    //============Edit Topic===========

    /*compareTopicSelectedCategory(){
        const tempPrevSelectedCat = this.state.prevSelectedCategory.map(c => ({ ...c }));
        const tempSelectedCat = this.state.selectedCategory.map(c => ({ ...c }));

        //Checkที่ถูกลบ
        let newRemovedCategory = [];
        for(let i = 0; i < tempPrevSelectedCat.length; i++){
            if(tempSelectedCat.findIndex(e => e.id === tempPrevSelectedCat[i].id) === -1){
                newRemovedCategory.push(tempPrevSelectedCat[i].id);
            }
        }

        //Checkที่เพิ่มเข้ามา
        let newAddedCategory = [];
        for(let i = 0; i < tempSelectedCat.length; i++){
            if(tempPrevSelectedCat.findIndex(e => e.id === tempSelectedCat[i].id) === -1){
                newAddedCategory.push(tempSelectedCat[i].id);
            }
        }
        this.setState(() => {
            return { 
                removeCategories : newRemovedCategory,
                addCategories : newAddedCategory
            };
        });
    }

    compareTopicSelectedDelivery(){
        //findIndex(e => e.id === category.id) ทำแบบนี้กับincludes,indexOfไม่ได้
        const tempPrevSelectedDev = this.state.prevSelectedDelivery.map(c => ({ ...c }));
        const tempSelectedDev = this.state.selectedDeliveryList.map(c => ({ ...c }));
        console.log('tempPrevSelectedDev', tempPrevSelectedDev);
        console.log('tempSelectedDev', tempSelectedDev);
        //Checkที่ถูกลบ
        let newRemovedDev = [];
        for(let i = 0; i < tempPrevSelectedDev.length; i++){
            if(tempSelectedDev.findIndex(e => e.id === tempPrevSelectedDev[i].id) === -1){
                newRemovedDev.push(tempPrevSelectedDev[i].id);
            }
        }

        //Checkที่เพิ่มเข้ามา & ที่แก้ราคา
        let newAddedDev = [];
        let newUpdatedDev = [];
        for(let i = 0; i < tempSelectedDev.length; i++){
            const indexOfEditDelivery = tempPrevSelectedDev.findIndex(e => e.id === tempSelectedDev[i].id);
            if(tempPrevSelectedDev.findIndex(e => e.id === tempSelectedDev[i].id) === -1){//ถ้าเกิดว่าไม่เจอแสดงว่าโดนเพิ่มเข้ามาใหม่
                newAddedDev.push({id: tempSelectedDev[i].id, price: tempSelectedDev[i].price});
            }else if(tempSelectedDev[i].price !== tempPrevSelectedDev[indexOfEditDelivery].price){// ถ้าเจอแต่ราคาไม่เท่าเดิม
                newUpdatedDev.push({id: tempSelectedDev[i].id, price: tempSelectedDev[i].price});
            }
        }

        this.setState(() => {
            return { 
                removeShippings : newRemovedDev,
                addShippings : newAddedDev,
                updateShippings : newUpdatedDev
            };
        });
    }*/

    /*compareTopicInfoBeforeAndAfterEdit(formData){
        this.compareTopicSelectedCategory();
        this.compareTopicSelectedDelivery();
    }*/

    handleSumbit(formData){//เหลือpicture
        let newFormData = { ...formData };
        if(this.checkFormValidation(newFormData)){ //checkว่ากรอกครบไหม
            //this.compareTopicInfoBeforeAndAfterEdit();

            //===========COMPARE BEFORE & AFTER EDIT===============
            //=========compare Cat======
            const tempPrevSelectedCat = this.state.prevSelectedCategory.map(c => ({ ...c }));
            const tempSelectedCat = this.state.selectedCategory.map(c => ({ ...c }));

            //Checkที่ถูกลบ
            let newRemovedCategory = [];
            for(let i = 0; i < tempPrevSelectedCat.length; i++){
                if(tempSelectedCat.findIndex(e => e.id === tempPrevSelectedCat[i].id) === -1){
                    newRemovedCategory.push(tempPrevSelectedCat[i].id);
                }
            }

            //Checkที่เพิ่มเข้ามา
            let newAddedCategory = [];
            for(let i = 0; i < tempSelectedCat.length; i++){
                if(tempPrevSelectedCat.findIndex(e => e.id === tempSelectedCat[i].id) === -1){
                    newAddedCategory.push(tempSelectedCat[i].id);
                }
            }
            this.setState(() => {
                return { 
                    removeCategories : newRemovedCategory,
                    addCategories : newAddedCategory
                };
            });
            //===========compare shipping===
            const tempPrevSelectedDev = this.state.prevSelectedDelivery.map(c => ({ ...c }));
            const tempSelectedDev = this.state.selectedDeliveryList.map(c => ({ ...c }));
            //Checkที่ถูกลบ
            let newRemovedDev = [];
            for(let i = 0; i < tempPrevSelectedDev.length; i++){
                if(tempSelectedDev.findIndex(e => e.id === tempPrevSelectedDev[i].id) === -1){
                    newRemovedDev.push(tempPrevSelectedDev[i].id);
                }
            }

            //Checkที่เพิ่มเข้ามา & ที่แก้ราคา
            let newAddedDev = [];
            let newUpdatedDev = [];
            for(let i = 0; i < tempSelectedDev.length; i++){
                const indexOfEditDelivery = tempPrevSelectedDev.findIndex(e => e.id === tempSelectedDev[i].id);
                if(tempPrevSelectedDev.findIndex(e => e.id === tempSelectedDev[i].id) === -1){//ถ้าเกิดว่าไม่เจอแสดงว่าโดนเพิ่มเข้ามาใหม่
                    newAddedDev.push({id: tempSelectedDev[i].id, price: tempSelectedDev[i].price});
                }else if(tempSelectedDev[i].price !== tempPrevSelectedDev[indexOfEditDelivery].price){// ถ้าเจอแต่ราคาไม่เท่าเดิม
                    newUpdatedDev.push({id: tempSelectedDev[i].id, price: tempSelectedDev[i].price});
                }
            }

            this.setState(() => {
                return { 
                    removeShippings : newRemovedDev,
                    addShippings : newAddedDev,
                    updateShippings : newUpdatedDev
                };
            });
            //==========================================
            const tempPrevTopicName = this.state.prevTopicName;
            const tempPrevTopicDescription = this.state.prevTopicDescription;
            const tempPrevTopicPrice = this.state.prevTopicPrice;
            const tempPrevTopicRemainingItem = this.state.prevTopicRemainingItem;

            console.log('test remainingItem',newFormData.topicRemainingItem);
            newFormData.addShippings = newAddedDev;
            newFormData.updateShippings = newUpdatedDev;
            newFormData.removeShippings = newRemovedDev;

            newFormData.addCategories = newAddedCategory;
            newFormData.removeCategories = newRemovedCategory;

            const reqFormData = new FormData();
            if(tempPrevTopicName !== newFormData.topicName){
                reqFormData.append('topicName', newFormData.topicName);
            }
            if(tempPrevTopicDescription !== newFormData.topicDescription){
                reqFormData.append('topicDescription', newFormData.topicDescription);
            }
            if(tempPrevTopicPrice !== newFormData.topicPrice){
                reqFormData.append('topicPrice', newFormData.topicPrice);
            }
            if(tempPrevTopicRemainingItem !== newFormData.topicRemainingItem){
                reqFormData.append('topicRemainingItems', newFormData.topicRemainingItem);
                console.log('test remainingItem change',newFormData.topicRemainingItem);
            }
            if(newFormData.removeCategories.length !== 0){
                reqFormData.append('removeCategories', JSON.stringify(newFormData.removeCategories));
            }
            if(newFormData.addCategories.length !== 0){
                reqFormData.append('addCategories', JSON.stringify(newFormData.addCategories));
            }
            if(newFormData.addShippings.length !== 0){
                reqFormData.append('addShippings', JSON.stringify(newFormData.addShippings));
            }
            if(newFormData.updateShippings.length !== 0){
                reqFormData.append('updateShippings', JSON.stringify(newFormData.updateShippings));
            }
            if(newFormData.removeShippings.length !== 0){
                reqFormData.append('removeShippings', JSON.stringify(newFormData.removeShippings));
            }
            
            const curUploadingImageFiles = this.state.uploadingImageFiles.slice();
            if(curUploadingImageFiles.length !== 0){
                curUploadingImageFiles.forEach((file, index) => {
                    reqFormData.append('addPictures', file, file.name);
                });
            }
            const tempRemovedPictures = this.state.removePictures.slice();
            if(tempRemovedPictures.length !== 0){
                reqFormData.append('removePictures', JSON.stringify(tempRemovedPictures))
            }

            //Thumbnail
            const newThumbnail = this.state.topicThumbnail;
            if(this.state.isEditThumbnail === true){//ถ้ามีการแก้ไขthumbnail ค่อยส่งถ้าไม่มีการแก้ไขก็ไม่ต้องทำอะไร
                reqFormData.append('topicThumbnail', JSON.stringify(newThumbnail));
            }

            const apiGetTopic = `/api/topic/${this.props.match.params.id}`;
            fetch(apiGetTopic, {
                method: 'PATCH',
                headers: {
                    Accept: 'application/json'
                },
                body: reqFormData
            })
            .then(res => console.log('res',res))
            .then(() => this.setState({
                topicIdToRedirect : this.props.match.params.id,
                isRedirectAfterSubmit: true
            }))
            .catch(err => console.error(err));
        }
    }

    handleCancel(){
        this.setState({ 
            redirectToIndex: true 
        });
    }

    //=============Category==========================
    handleDialogOpen(){
        this.setState(state => {
            return { 
                tempSelectedCategory: state.selectedCategory,
                tempShowingCategoryList: state.showingCategoryList
            };
        }, () => {
            this.setState({
                isDialogOpen : true
            });
        });
    }

    handleDialogSubmit(){
        
        this.setState(state => {
            return {    
                selectedCategory: state.tempSelectedCategory,
                showingCategoryList: state.tempShowingCategoryList,
            };
        }, () => {
            this.setState(state => {
                return {    
                    selectedCategoryList: state.selectedCategory,
                    isDialogOpen : false
                };
            })
        });
    }

    handleDialogCancel(){

        this.setState(state => {
            return {    
                tempSelectedCategory: state.selectedCategory,
                tempShowingCategoryList: state.showingCategoryList
            };
        }, () => {
            this.setState({
                isDialogOpen : false
            });
        });
    }

    handleSelectChange(category, index){
        const newShowingDataList = this.state.tempShowingCategoryList.map(c => ({ ...c }));
        const newSelectedCategory = this.state.tempSelectedCategory.map(c => ({ ...c }));

        if(category.isSelected == undefined || category.isSelected === false){
            newShowingDataList[index].isSelected = true;
            newSelectedCategory.push({ ...category });
            this.setState({
                tempShowingCategoryList: newShowingDataList,
                tempSelectedCategory: newSelectedCategory
            });
        }else{
            newShowingDataList[index].isSelected = false;
            newSelectedCategory.splice(newSelectedCategory.findIndex(e => e.id === category.id), 1);
            this.setState({
                tempSelectedCategory: newSelectedCategory,
                tempShowingCategoryList: newShowingDataList,
            });
        }
    }

    handleDialogChipDelete(data, index){
        const newShowingDataList = this.state.tempShowingCategoryList.map(c => ({ ...c }));
        const newSelectedCategory = this.state.tempSelectedCategory.map(c => ({ ...c }));
        const showingDataIndex = newShowingDataList.findIndex(e => e.id === data.id);
        newShowingDataList[showingDataIndex].isSelected = false;
        newSelectedCategory.splice(index, 1);
        this.setState({
            tempSelectedCategory: newSelectedCategory,
            tempShowingCategoryList: newShowingDataList
        });
    }

    handleChipQuickDelete(data, index){
        const newShowingDataList = this.state.tempShowingCategoryList.map(c => ({ ...c }));
        const newSelectedCategory = this.state.tempSelectedCategory.map(c => ({ ...c }));
        const showingDataIndex = newShowingDataList.findIndex(e => e.id === data.id);
        newShowingDataList[showingDataIndex].isSelected = false;
        newSelectedCategory.splice(index, 1);
        this.setState({
            tempSelectedCategory: newSelectedCategory,
            tempShowingCategoryList: newShowingDataList,
            selectedCategory: newSelectedCategory,
            showingCategoryList: newShowingDataList,
        });
    }

    //==================UPLOAD PICTURE=======================

    handleFileChanged(files) {
        console.log(files.map(f => f.file));
        this.setState({
            files: files.map(f => f.file),
            filesPercent: files.map(f => f.percent)
        });
    }

    handleUploadButtonClicked() {
        this.setState(state => {
            return { isUploading: !!!state.isUploading }
        }, () => {
            if (this.state.isUploading) {
                Promise.resolve(this.upload());
            } else {
                if (this.uploader != null) {
                    this.uploader.abort();
                }
            }
        });
    }

    handleUploadImageChanged(files) {
        const newImgsUrl = this.state.uploadingImages.slice();//โหลดรูปเดิมๆที่มีแต่urlมา
        const imgsUrl = newImgsUrl;
        const imgFiles = [];
        for (let i = 0; i < files.length; i++) {
            imgsUrl.push(window.URL.createObjectURL(files[i]));
            imgFiles.push(files[i]);

        }

        this.setState({
            uploadingImages: imgsUrl,
            uploadingImageFiles: imgFiles
        });
    }

    handleUploadImageClicked(index) {
        const uploadingImageFiles = this.state.uploadingImageFiles.slice();
        const newImgsUrl = this.state.uploadingImages.slice();
        const tempPrevImgs = this.state.prevImgs.slice();
        let newTopicThumbnailNumber = 0;
        if(tempPrevImgs.findIndex(e => e.url === newImgsUrl[index]) !== -1){ //ถ้ารูปที่กดเป็นรูปที่มาจากของเก่า ก็ให้เก็บidเพื่อไปบอกserver
            const changeIndex = tempPrevImgs.findIndex(e => e.url === newImgsUrl[index]);
            newTopicThumbnailNumber = tempPrevImgs[changeIndex].id;
            this.setState({
                topicThumbnail : newTopicThumbnailNumber,
                isThumbnailFromPrev : true,
                isEditThumbnail : true
            });
        }
        else{ //ถ้ารูปใหม่ที่พึ่งอัพโดนเลือกเป็นthumbnail
            const realNewUploadIndex = index - (this.state.prevImgs.length - this.state.removePictures.length);
            //จะเอาindexจริงๆของรูปที่พึ่งถูกupload ไม่นับรูปที่ได้มาก่อนอยู่แล้ว
            this.setState({
                topicThumbnail: {
                    name: uploadingImageFiles[realNewUploadIndex].name,
                    type: uploadingImageFiles[realNewUploadIndex].type,
                    size: uploadingImageFiles[realNewUploadIndex].size,
                },
                isThumbnailFromPrev : false,
                isEditThumbnail : true
            });
        }
    }

    handleUploadImageDeleting(index) {
        const newImgsUrl = this.state.uploadingImages.slice();
        const tempPrevImgs = this.state.prevImgs.slice();
        const newRemovedPicture = this.state.removePictures.slice();

        if(tempPrevImgs.findIndex(e => e.url === newImgsUrl[index]) !== -1){ //ถ้ารูปที่กดลบเป็นรูปที่มาจากของเก่า ก็ให้เก็บidเพื่อไปบอกserver
            const removedIndex = tempPrevImgs.findIndex(e => e.url === newImgsUrl[index]);
            newRemovedPicture.push(tempPrevImgs[removedIndex].id);
        }
        newImgsUrl.splice(index, 1);

        this.setState({
            uploadingImages : newImgsUrl,
            removePictures : newRemovedPicture
        });
    }
    //========Delivery============
    setDeliveryList(arrayData){
        this.setState({
            selectedDeliveryList : arrayData
        })
    }

    render() {

        return <AddTopicView
                    //Init

                    pageTitle={this.state.pageTitle}
                    redirectToIndex={this.state.redirectToIndex}
                    isAuthenticated={AuthService.isAuthenticated()}
                    isKnownState={this.state.isKnownState}
                    topicIdToRedirect={this.state.topicIdToRedirect}
                    isRedirectAfterSubmit={this.state.isRedirectAfterSubmit}

                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}

                    //========Tooltip=================
                    isTopicNameTooltipOpen={this.state.isTopicNameTooltipOpen}
                    isTopicPriceTooltipOpen={this.state.isTopicPriceTooltipOpen}
                    isTopicDesctriptionTooltipOpen={this.state.isTopicDesctriptionTooltipOpen}
                    isTopicRemainingItemTooltipOpen={this.state.isTopicRemainingItemTooltipOpen}
                    isCategoryTooltipOpen={this.state.isCategoryTooltipOpen}
                    isDeliveryTooltipOpen={this.state.isDeliveryTooltipOpen}
                    deliveryTooltipMsg={this.state.deliveryTooltipMsg}
                    isTopicPictureTooltipOpen={this.state.isTopicPictureTooltipOpen}

                    onInputData={this.handleCloseTooltip}
                    //======For Edit or just Add=======================
                    topicJson={this.state.topicJson}
                    topicName={this.state.topicJson.productName}
                    topicPrice={this.state.topicJson.productPrice}
                    topicRemainingItem={this.state.topicJson.remainingItems}
                    topicDescription={this.state.topicJson.productDescription}
                    currentSelectedDeliveryList={this.state.selectedDeliveryList.map(c => ({ ...c }))}//ดึงจากdbละพัง
                    isEditTopic={true}
                    

                    //===============Delivery=================
                    //Data
                    deliveryCarrierList={this.state.deliveryCarrierList}
                    setDeliveryList={data => this.setDeliveryList(data)}
                    //Event
                    setDeliveryList={this.setDeliveryList}
                    handleDeliveryInfoChanged={e => this.handleDeliveryInfoChanged(e)}
                    onSubmitButtonClicked={this.handleSumbit}
                    onUploadPictureButtonClicked={this.handleUploadPicture}
                    onCancelButtonClicked={this.handleCancel}
                    
                    //================Category===============
                    //Data
                    categoryList={this.state.tempShowingCategoryList}
                    selectedCategory={this.state.tempSelectedCategory}
                    isCategoryTooltipOpen={this.state.isCategoryTooltipOpen}
                    isDialogOpen={this.state.isDialogOpen}
                    dialogScroll={this.state.dialogScroll}

                    //Event
                    onDialogOpen={this.handleDialogOpen}
                    onDialogSubmit={this.handleDialogSubmit}
                    onDialogCancel={this.handleDialogCancel}
                    onCategorySelected={this.handleSelectChange}
                    onChipDelete={this.handleDialogChipDelete}
                    onChipQuickDelete={this.handleChipQuickDelete}

                    //===========Upload Picture===========
                    //Data
                    isUploading={this.state.isUploading}
                    fileUploadingPercent={this.state.filesPercent}
                    //Event
                    onUploadButtonClicked={this.handleUploadButtonClicked.bind(this)}
                    onFileChanged={this.handleFileChanged.bind(this)}

                    //======== NEW UPLOAD ============
                    uploadingImages={this.state.uploadingImages}
                    onUploadImageClicked={this.handleUploadImageClicked.bind(this)}
                    onUploadImageDelete={this.handleUploadImageDeleting.bind(this)}
                    onUploadImageChanged={this.handleUploadImageChanged.bind(this)}
                    />
    }
}

EditTopic.propTypes = {
    //Data
    location : PropTypes.object.isRequired, //เพื่อนำข้อมูลที่ได้มาedit
    dySec : PropTypes.any.isRequired
}