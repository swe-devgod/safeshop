export const styles = theme => ({
    paper: {
        padding: '16px' ,
    },

    fullWidth: {
        width: '100%',
    },
    divider: {
        margin: '8px 0 8px 0'
    },

    inputAdornmentStyle: {
        fontSize: '2px'
    },
    tooltipResize:{
        fontSize: '13px'
    },

    arrowPopper: {
        '&[x-placement*="bottom"] $arrowArrow': {
          top: 0,
          left: 0,
          marginTop: '-0.9em',
          width: '3em',
          height: '1em',
          '&::before': {
            borderWidth: '0 1em 1em 1em',
            borderColor: `transparent transparent ${theme.palette.grey[700]} transparent`,
          },
        },
        '&[x-placement*="top"] $arrowArrow': {
          bottom: 0,
          left: 0,
          marginBottom: '-0.9em',
          width: '3em',
          height: '1em',
          '&::before': {
            borderWidth: '1em 1em 0 1em',
            borderColor: `${theme.palette.grey[700]} transparent transparent transparent`,
          },
        },
    },
    arrowArrow: {
        position: 'absolute',
        fontSize: '13px',
        width: '3em',
        height: '3em',
        '&::before': {
            content: '""',
            margin: 'auto',
            display: 'block',
            width: 0,
            height: 0,
            borderStyle: 'solid',
        },
    }
});