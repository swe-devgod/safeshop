import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AddTopicView from './add-topic-view';
import { SafeShopSingleFileUploader } from '../../lib/safeshop-uploader';
import { AuthService } from '../../components/auth-service';

export default class AddTopic extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isKnownState : false,
            pageTitle: 'สร้างกระทู้สินค้า',
            topicIdToRedirect: 0,
            isRedirectAfterSubmit: false,
            redirectToIndex: false,

            deliveryCarrierList: [],//mock_delivery_list,//[],
            selectedDeliveryList: [],

            dialogScroll: 'paper',
            isDialogOpen : false,
            showingCategoryList : [],//mock_category_list.map(c => ({ ...c })),
            selectedCategory: [],
            tempShowingCategoryList: [],//mock_category_list.map(c => ({ ...c })),
            tempSelectedCategory: [],
            wantToDeleteId: 0,

            files: [],
            filesPercent: [],
            isUploading: false,

            uploadingImages: [],
            uploadingImageFiles: [],
            topicThumbnail: {},
            isSelectedThumbnail: false,

            //Tooltip
            isTopicNameTooltipOpen: false,
            isTopicPriceTooltipOpen: false,
            isTopicDesctriptionTooltipOpen: false,
            isTopicRemainingItemTooltipOpen: false,
            isCategoryTooltipOpen: false,
            isDeliveryTooltipOpen: false,
            deliveryTooltipMsg: '',
            isTopicPictureTooltipOpen: false,

        };
        this.handleSumbit = this.handleSumbit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);

        this.handleDialogOpen = this.handleDialogOpen.bind(this);
        this.handleDialogSubmit = this.handleDialogSubmit.bind(this);
        this.handleDialogCancel = this.handleDialogCancel.bind(this);

        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleDialogChipDelete = this.handleDialogChipDelete.bind(this);
        this.handleChipQuickDelete = this.handleChipQuickDelete.bind(this);

        this.setDeliveryList = this.setDeliveryList.bind(this);
        this.checkFormValidation = this.checkFormValidation.bind(this);
        this.handleCloseTooltip = this.handleCloseTooltip.bind(this);
    }

    async fetchDataAsync() {
        const apiGetDeliveryCarrierList = "/api/topic/shipping/list";
        const apiGetAvaiableCategoryList = "/api/category/list";
        
        function getJson(endpoint) {
            return new Promise((resolve, reject) => {
                fetch(endpoint)
                .then(res => res.json())
                .then(json => resolve(json))
                .catch(err => reject(err));
            });
        }

        const json1 = await getJson(apiGetDeliveryCarrierList);
        const json2 = await getJson(apiGetAvaiableCategoryList);

        this.setState({
            showingCategoryList : json2.map(c => ({ ...c })),
            tempShowingCategoryList : json2.map(c => ({ ...c })),
            deliveryCarrierList : json1,
            isKnownState: true,
        })   
    }


    componentDidMount() {
        document.title = "Add Topic - SafeShop";
        Promise.resolve(this.fetchDataAsync());
    }

    handleUploadPicture(formData){
        const uploader = new SafeShopSingleFileUploader(formData.file);
        uploader.upload('/api/upload');
    }

    handleCloseTooltip(){
        this.setState({
            isTopicNameTooltipOpen: false,
            isTopicPriceTooltipOpen: false,
            isTopicDesctriptionTooltipOpen: false,
            isTopicRemainingItemTooltipOpen: false,
            isCategoryTooltipOpen: false,
            isDeliveryTooltipOpen: false,
            isTopicPictureTooltipOpen: false,
        });
    }

    checkFormValidation(formData){
        let invalidInput = false;
        if(formData.topicName == undefined || formData.topicName.length === 0){
            this.setState({
                isTopicNameTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(formData.topicDescription == undefined || formData.topicDescription.length === 0){
            this.setState({
                isTopicDesctriptionTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(formData.topicPrice == undefined || formData.topicPrice.length === 0){
            this.setState({
                isTopicPriceTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(formData.topicRemainingItem == undefined || formData.topicRemainingItem.length === 0){
            this.setState({
                isTopicRemainingItemTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(this.state.selectedCategory.length === 0){
            this.setState({
                isCategoryTooltipOpen: true,
            })
            invalidInput = true;
        }
        if(this.state.selectedDeliveryList.length === 0){
            this.setState({
                isDeliveryTooltipOpen: true,
                deliveryTooltipMsg: 'กรุณาเลือกวิธีจัดส่งสินค้า'
            })
            invalidInput = true;
        }
        if(this.state.uploadingImages.length === 0){
            this.setState({
                isTopicPictureTooltipOpen: true,
            })
            invalidInput = true;
        }
        const tempDeliveryList = this.state.selectedDeliveryList.slice();
        for(let i = 0; i <  tempDeliveryList.length; i++){
            if(tempDeliveryList[i].price == undefined || tempDeliveryList[i].price.length === 0){
                this.setState({
                    isDeliveryTooltipOpen: true,
                    deliveryTooltipMsg: 'กรุณาระบุราคาในการจัดส่งสินค้า'
                })
                invalidInput = true;
                break;
            }
        }
        if(invalidInput === false){
            return true;
        }else{
            return false;
        }
    }

    //============Add Topic===========

    handleSumbit(formData) {//เหลือ array picture
        let newFormData = { ...formData };
        if(this.checkFormValidation(newFormData)){
            newFormData.topicShipping = [];
            newFormData.topicShipping = this.state.selectedDeliveryList.slice();
            newFormData.topicCategory = this.state.selectedCategory.map(s=>({id: s.id ,name: s.name}));
            newFormData.pictures = this.state.files.map(file => ({
                name: file.name,
                size: file.size
            }));
    
            const reqFormData = new FormData();
            reqFormData.append('topicName', newFormData.topicName);
            reqFormData.append('topicDescription', newFormData.topicDescription);
            reqFormData.append('topicPrice', newFormData.topicPrice);
            reqFormData.append('topicRemainingItem', newFormData.topicRemainingItem);
            reqFormData.append('topicShipping', JSON.stringify(newFormData.topicShipping));
            reqFormData.append('topicCategory', JSON.stringify(newFormData.topicCategory));
            const curUploadingImageFiles = this.state.uploadingImageFiles.slice();
            if(this.state.isSelectedThumbnail === false){ //ถ้าไม่ได้เลือกthumbnailให้เอารูปแรกสุดเป็นthumbnail
                let newThumbnail = {};
                newThumbnail = {name: '0-' + curUploadingImageFiles[0].name,
                    type: curUploadingImageFiles[0].type,
                    size: curUploadingImageFiles[0].size};
                reqFormData.append('topicThumbnail', JSON.stringify(newThumbnail));
            }else{
                reqFormData.append('topicThumbnail', JSON.stringify(this.state.topicThumbnail));
            }
            curUploadingImageFiles.forEach((file, index) => {
                reqFormData.append('topicPictures', file, index + '-' + file.name); //รอ
            });
    
            const apiGetTopic = "/api/topic/";
            fetch(apiGetTopic, {
                method: 'POST',
                headers: {
                    Accept: 'application/json'
                },
                body: reqFormData
            })
            .then(res => res.json())
            .then(json => this.setState({
                topicIdToRedirect : json.topicId,
                isRedirectAfterSubmit: true
            }))
            .catch(err => console.error(err));
        }
    }


    handleCancel(){
        this.setState({ 
            redirectToIndex: true 
        });
    }

    //=============Category==========================
    handleDialogOpen(){
        this.setState(state => {
            return { 
                tempSelectedCategory: state.selectedCategory,
                tempShowingCategoryList: state.showingCategoryList
            };
        }, () => {
            this.setState({
                isDialogOpen : true
            });
        });
    }

    handleDialogSubmit(){
        this.handleCloseTooltip();
        this.setState(state => {
            return {    
                selectedCategory: state.tempSelectedCategory,
                showingCategoryList: state.tempShowingCategoryList,
            };
        }, () => {
            this.setState(state => {
                return {    
                    selectedCategoryList: state.selectedCategory,
                    isDialogOpen : false
                };
            })
        });
    }

    handleDialogCancel(){

        this.setState(state => {
            return {    
                tempSelectedCategory: state.selectedCategory,
                tempShowingCategoryList: state.showingCategoryList
            };
        }, () => {
            this.setState({
                isDialogOpen : false
            });
        });
    }

    handleSelectChange(category, index){
        const newShowingDataList = this.state.tempShowingCategoryList.map(c => ({ ...c }));
        const newSelectedCategory = this.state.tempSelectedCategory.map(c => ({ ...c }));

        if(category.isSelected == undefined || category.isSelected === false){
            newShowingDataList[index].isSelected = true;
            newSelectedCategory.push({ ...category });
            console.log(newSelectedCategory);
            this.setState({
                tempShowingCategoryList: newShowingDataList,
                tempSelectedCategory: newSelectedCategory
            });
        }else{
            newShowingDataList[index].isSelected = false;
            newSelectedCategory.splice(newSelectedCategory.findIndex(e => e.id === category.id), 1);
            this.setState({
                tempSelectedCategory: newSelectedCategory,
                tempShowingCategoryList: newShowingDataList,
            });
        }
    }

    handleDialogChipDelete(data, index){
        const newShowingDataList = this.state.tempShowingCategoryList.map(c => ({ ...c }));
        const newSelectedCategory = this.state.tempSelectedCategory.map(c => ({ ...c }));
        const showingDataIndex = newShowingDataList.findIndex(e => e.id === data.id);
        newShowingDataList[showingDataIndex].isSelected = false;
        newSelectedCategory.splice(index, 1);
        this.setState({
            tempSelectedCategory: newSelectedCategory,
            tempShowingCategoryList: newShowingDataList
        });
    }

    handleChipQuickDelete(data, index){
        const newShowingDataList = this.state.tempShowingCategoryList.map(c => ({ ...c }));
        const newSelectedCategory = this.state.tempSelectedCategory.map(c => ({ ...c }));
        const showingDataIndex = newShowingDataList.findIndex(e => e.id === data.id);
        newShowingDataList[showingDataIndex].isSelected = false;
        newSelectedCategory.splice(index, 1);
        this.setState({
            tempSelectedCategory: newSelectedCategory,
            tempShowingCategoryList: newShowingDataList,
            selectedCategory: newSelectedCategory,
            showingCategoryList: newShowingDataList,
        });
    }

    //==================UPLOAD PICTURE=======================
    handleFileChanged(files) {
        this.handleCloseTooltip();
        console.log(files.map(f => f.file));
        this.setState({
            files: files.map(f => f.file),
            filesPercent: files.map(f => f.percent)
        });
    }

    handleUploadButtonClicked() {
        this.setState(state => {
            return { isUploading: !!!state.isUploading }
        }, () => {
            if (this.state.isUploading) {
                Promise.resolve(this.upload());
            } else {
                if (this.uploader != null) {
                    this.uploader.abort();
                }
            }
        });
    }
    //========Delivery============
    setDeliveryList(arrayData){
        this.handleCloseTooltip();
        this.setState({
            selectedDeliveryList : arrayData
        })
    }

    /**
     * NEW UPLOADING
     */

    handleUploadImageChanged(files) {
        this.handleCloseTooltip();
        const imgsUrl = [];
        const imgFiles = [];
        for (let i = 0; i < files.length; i++) {
            imgsUrl.push(window.URL.createObjectURL(files[i]));
            imgFiles.push(files[i]);
        }

        this.setState({
            uploadingImages: imgsUrl,
            uploadingImageFiles: imgFiles
        });
    }

    handleUploadImageClicked(index) {
        const uploadingImageFiles = this.state.uploadingImageFiles.slice();
        this.setState({
            topicThumbnail: {
                name: index + '-' + uploadingImageFiles[index].name,
                type: uploadingImageFiles[index].type,
                size: uploadingImageFiles[index].size
            },
            isSelectedThumbnail : true
        });
    }

    handleUploadImageDeleting(index) {
        const newImgsUrl = this.state.uploadingImages.slice();
        newImgsUrl.splice(index, 1);

        this.setState({
            uploadingImages: newImgsUrl
        });
    }

    render() {

        return <AddTopicView
                    //Init
                    pageTitle={this.state.pageTitle}
                    redirectToIndex={this.state.redirectToIndex}
                    isAuthenticated={AuthService.isAuthenticated()}
                    isKnownState={this.state.isKnownState}
                    topicIdToRedirect={this.state.topicIdToRedirect}
                    isRedirectAfterSubmit={this.state.isRedirectAfterSubmit}

                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    

                    //========Tooltip=================
                    isTopicNameTooltipOpen={this.state.isTopicNameTooltipOpen}
                    isTopicPriceTooltipOpen={this.state.isTopicPriceTooltipOpen}
                    isTopicDesctriptionTooltipOpen={this.state.isTopicDesctriptionTooltipOpen}
                    isTopicRemainingItemTooltipOpen={this.state.isTopicRemainingItemTooltipOpen}
                    isCategoryTooltipOpen={this.state.isCategoryTooltipOpen}
                    isDeliveryTooltipOpen={this.state.isDeliveryTooltipOpen}
                    deliveryTooltipMsg={this.state.deliveryTooltipMsg}
                    isTopicPictureTooltipOpen={this.state.isTopicPictureTooltipOpen}

                    onInputData={this.handleCloseTooltip}
                    //======For Edit or just Add=======================
                    isEditTopic={false}


                    //===============Delivery=================
                    //Data
                    deliveryCarrierList={this.state.deliveryCarrierList}
                    setDeliveryList={data => this.setDeliveryList(data)}
                    //Event
                    setDeliveryList={this.setDeliveryList}
                    handleDeliveryInfoChanged={e => this.handleDeliveryInfoChanged(e)}
                    onSubmitButtonClicked={this.handleSumbit}
                    onUploadPictureButtonClicked={this.handleUploadPicture}
                    onCancelButtonClicked={this.handleCancel}
                    
                    //================Category===============
                    //Data
                    categoryList={this.state.tempShowingCategoryList}
                    selectedCategory={this.state.tempSelectedCategory}
                    isDialogOpen={this.state.isDialogOpen}
                    dialogScroll={this.state.dialogScroll}

                    //Event
                    onDialogOpen={this.handleDialogOpen}
                    onDialogSubmit={this.handleDialogSubmit}
                    onDialogCancel={this.handleDialogCancel}
                    onCategorySelected={this.handleSelectChange}
                    onChipDelete={this.handleDialogChipDelete}
                    onChipQuickDelete={this.handleChipQuickDelete}

                    //===========Upload Picture===========
                    //Data
                    isUploading={this.state.isUploading}
                    fileUploadingPercent={this.state.filesPercent}
                    //Event
                    onUploadButtonClicked={this.handleUploadButtonClicked.bind(this)}
                    onFileChanged={this.handleFileChanged.bind(this)}

                    //======== NEW UPLOAD ============
                    uploadingImages={this.state.uploadingImages}
                    onUploadImageClicked={this.handleUploadImageClicked.bind(this)}
                    onUploadImageDelete={this.handleUploadImageDeleting.bind(this)}
                    onUploadImageChanged={this.handleUploadImageChanged.bind(this)}
                    />
    }
}

AddTopic.propTypes = {
    dySec : PropTypes.any.isRequired
}