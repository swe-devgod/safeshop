import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider'
import { withStyles, InputAdornment, Typography } from '@material-ui/core';
import Delivery from './components/delivery/index'
import Category from './components/category/index'
import Chip from '@material-ui/core/Chip';

import Tooltip from '@material-ui/core/Tooltip';
import ProgressView from '../../components/progress-view';

import DefaultSelectedFileUploadingView from '../../components/deafult-selected-file-uploading';

import { styles } from './add-topic-view.styles';

class AddTopicView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            formData: {}
        };
        
        this.formData = {
            topicName: '',
            topicDescription: '',
            topicPrice: '',
            topicRemainingItem: ''
        };
    }

    componentDidMount() {
        this.props.setSideBarItems(null);
    }

    componentDidUpdate(prevProps, prevState) {
        if (!!!prevProps.isKnownState) {
            const propsCopy = { ...this.props.topicJson }
            this.setState({
                formData: Object.assign({}, {
                    topicName: propsCopy.productName,
                    topicDescription: propsCopy.productDescription,
                    topicPrice: propsCopy.productPrice,
                    topicRemainingItem: propsCopy.remainingItems
                })
            });

            this.formData = Object.assign({}, {
                topicName: propsCopy.productName,
                topicDescription: propsCopy.productDescription,
                topicPrice: propsCopy.productPrice,
                topicRemainingItem: propsCopy.remainingItems
            });
        }
    }
    
    handleFormInput(e) {
        this.props.onInputData();
        this.formData[e.target.name] = e.target.value;
        this.forceUpdate();
    }

    render() {
        const { classes } = this.props;

        if (!!!this.props.isAuthenticated) {
            return <Redirect to={`/login`} />
        } else if (this.props.redirectToIndex) {
            if(this.props.isEditTopic){
                return <Redirect to="/seller-management" />;
            }else{
                return <Redirect to="/" />;
            }
        }
        if(this.props.isRedirectAfterSubmit){
            return <Redirect to={`/topic/${this.props.topicIdToRedirect}`} />;
        }

        if(!!!this.props.isKnownState){
            return <ProgressView/>
        }

        return (
            <React.Fragment>
                <Grid container justify="center">
                    <Grid item container sm={10}>
                        <Grid item xs={12}>
                            <Typography variant="h6" gutterBottom>{this.props.pageTitle}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                            <form>
                                <Grid container spacing={8}>
                                    <Grid item xs={12}>
                                        <Tooltip
                                            PopperProps={{
                                                disablePortal: true,
                                            }}
                                            open={this.props.isTopicNameTooltipOpen}
                                            disableFocusListener
                                            disableHoverListener
                                            disableTouchListener
                                            title={
                                                <React.Fragment>
                                                    <span className={classes.tooltipResize}>กรุณาระบุชื่อกระทู้</span>
                                                    <span className={classes.arrowArrow} ref={this.handleArrowRef} />
                                                </React.Fragment>
                                            }
                                            classes={{ popper: classes.arrowPopper }} 
                                        >
                                            <TextField required label="ชื่อกระทู้สินค้า" variant="outlined" fullWidth
                                                value={this.formData.topicName}
                                                name="topicName"
                                                onChange={this.handleFormInput.bind(this)}
                                            />
                                        </Tooltip>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Tooltip
                                            PopperProps={{
                                                disablePortal: true,
                                            }}
                                            open={this.props.isTopicDesctriptionTooltipOpen}
                                            disableFocusListener
                                            disableHoverListener
                                            disableTouchListener
                                            title={
                                                <React.Fragment>
                                                    <span className={classes.tooltipResize}>กรุณาระบุรายละเอียดสินค้า</span>
                                                    <span className={classes.arrowArrow} ref={this.handleArrowRef} />
                                                </React.Fragment>
                                            }
                                            classes={{ popper: classes.arrowPopper }} 
                                        >
                                            <TextField required variant="outlined" label="รายละเอียดสินค้า" multiline rows="5" fullWidth
                                                value={this.formData.topicDescription}
                                                onChange={e => this.formData.topicDescription = e.target.value}
                                                name="topicDescription"
                                                onChange={this.handleFormInput.bind(this)}
                                            />
                                        </Tooltip>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Tooltip
                                            PopperProps={{
                                                disablePortal: true,
                                            }}
                                            open={this.props.isTopicPriceTooltipOpen}
                                            disableFocusListener
                                            disableHoverListener
                                            disableTouchListener
                                            title={
                                                <React.Fragment>
                                                    <span className={classes.tooltipResize}>กรุณาระบุราคาสินค้า</span>
                                                    <span className={classes.arrowArrow} ref={this.handleArrowRef} />
                                                </React.Fragment>
                                            }
                                            classes={{ popper: classes.arrowPopper }} 
                                        >
                                            <TextField required variant="outlined" label="ราคาสินค้า" type="number"
                                                value={this.formData.topicPrice}
                                                onKeyPress={e => { if ("0123456789".indexOf(e.key) === -1) { e.preventDefault(); }}}                                    
                                                onChange={e => this.formData.topicPrice = e.target.value}
                                                InputProps={{endAdornment:<InputAdornment position="end">บาท</InputAdornment>}}
                                                className={classes.fullWidth}
                                                name="topicPrice"
                                                onChange={this.handleFormInput.bind(this)}
                                            />
                                        </Tooltip>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Tooltip
                                            PopperProps={{
                                                disablePortal: true,
                                            }}
                                            open={this.props.isTopicRemainingItemTooltipOpen}
                                            disableFocusListener
                                            disableHoverListener
                                            disableTouchListener
                                            title={
                                                <React.Fragment>
                                                    <span className={classes.tooltipResize}>กรุณาระบุจำนวนที่ต้องการขาย</span>
                                                    <span className={classes.arrowArrow} ref={this.handleArrowRef} />
                                                </React.Fragment>
                                            }
                                            classes={{ popper: classes.arrowPopper }} 
                                        >
                                            <TextField required variant="outlined" type="number" label="จำนวน"
                                                value={this.formData.topicRemainingItem}
                                                onKeyPress={e => { if ("0123456789".indexOf(e.key) === -1) { e.preventDefault(); }}}
                                                onChange={e => this.formData.topicRemainingItem = e.target.value}
                                                name="topicRemainingItem"
                                                onChange={this.handleFormInput.bind(this)}
                                                InputProps={{
                                                    endAdornment: <InputAdornment classes={{ root: classes.inputAdornmentStyle }} position="end">ชิ้น</InputAdornment>
                                                }}
                                                className={classes.fullWidth}
                                            />
                                        </Tooltip>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Paper elevation={1} style={{padding: 4 * 2, marginBottom: 8}}>
                                        <Tooltip
                                                PopperProps={{
                                                    disablePortal: true,
                                                }}
                                                open={this.props.isCategoryTooltipOpen}
                                                disableFocusListener
                                                disableHoverListener
                                                disableTouchListener
                                                //classes={{
                                                    //tooltip: classes.tooltipResize //ข้างในTooltipมันมี tooltip เป็นwrapperอยู่
                                                    //พอจะปรับfontSizeเลยต้องปรับที่tooltipข้างในให้มีpropตามtooltipResize
                                                //}}
                                                title={
                                                    <React.Fragment>
                                                        <span className={classes.tooltipResize}>กรุณาเลือกหมวดหมู่สินค้า</span>
                                                        <span className={classes.arrowArrow} ref={this.handleArrowRef} />
                                                    </React.Fragment>
                                                }
                                                classes={{ popper: classes.arrowPopper }}
                                            >
                                                <Category 
                                                    categoryList ={this.props.categoryList}
                                                    selectedCategory={this.props.selectedCategory}
                                                    isDialogOpen={this.props.isDialogOpen}

                                                    dialogScroll={this.props.dialogScroll}
                                                    onDialogOpen={this.props.onDialogOpen}
                                                    onDialogSubmit={this.props.onDialogSubmit}
                                                    onDialogCancel={this.props.onDialogCancel}
                                                    onCategorySelected={this.props.onCategorySelected}
                                                    onChipDelete={this.props.onChipDelete}
                                                />
                                            </Tooltip>

                                            {
                                                this.props.selectedCategory &&
                                                this.props.selectedCategory.map((data, index) => (
                                                    <Chip
                                                        onDelete={() => this.props.onChipQuickDelete(data, index)}
                                                        label={data.categoryName}
                                                        style={{margin: 2 * 2, backgroundColor: 'rgb(255, 221, 184)'}}
                                                    />
                                                ))
                                            }
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Tooltip
                                            PopperProps={{
                                                disablePortal: true,
                                            }}
                                            open={this.props.isTopicPictureTooltipOpen}
                                            disableFocusListener
                                            disableHoverListener
                                            disableTouchListener
                                            title={
                                                <React.Fragment>
                                                    <span className={classes.tooltipResize}>กรุณาเลือกรูปภาพสินค้า</span>
                                                </React.Fragment>
                                            }
                                            classes={{ popper: classes.arrowPopper }}
                                        >
                                            <DefaultSelectedFileUploadingView
                                                items={this.props.uploadingImages}
                                                itemWidth={150}
                                                itemHeight={150}
                                                onClick={this.props.onUploadImageClicked}
                                                onDelete={this.props.onUploadImageDelete}
                                                onFileChanged={this.props.onUploadImageChanged}
                                            />
                                        </Tooltip>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Divider className={classes.divider}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Tooltip
                                            PopperProps={{
                                                disablePortal: true,
                                            }}
                                            open={this.props.isDeliveryTooltipOpen}
                                            disableFocusListener
                                            disableHoverListener
                                            disableTouchListener
                                            title={
                                                <React.Fragment>
                                                    <span className={classes.tooltipResize}>{this.props.deliveryTooltipMsg}</span>
                                                </React.Fragment>
                                            }
                                            classes={{ popper: classes.arrowPopper }}
                                        >
                                            <p>เลือกวิธีการจัดส่งสินค้าของคุณ</p>
                                        </Tooltip>
                                        <Delivery   
                                            deliveryCarrierList={this.props.deliveryCarrierList}

                                            handleDeliveryInfoChanged={this.props.handleDeliveryInfoChanged}
                                            setDeliveryList={this.props.setDeliveryList}
                                            //For Edit
                                            isEditTopic={this.props.isEditTopic}
                                            currentSelectedDeliveryList={this.props.currentSelectedDeliveryList}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Divider className={classes.divider}/>
                                    </Grid>
                                    <Grid item container xs={12} spacing={8}>
                                        <Grid item>
                                            <Button variant="contained"
                                                onClick={() => this.props.onSubmitButtonClicked(this.formData)}>Submit</Button>
                                        </Grid>
                                        <Grid item>
                                            <Button color="primary" variant="contained"
                                                onClick={this.props.onCancelButtonClicked}>Cancel</Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </form>
                        </Paper>
                    </Grid>
                    </Grid>
                </Grid>
            </React.Fragment>   
        );
    }
}

export default withStyles(styles)(AddTopicView);

AddTopicView.propTypes = {
    //Init
    pageTitle : PropTypes.string.isRequired,
    redirectToIndex : PropTypes.bool.isRequired,
    isAuthenticated : PropTypes.bool.isRequired,
    isRedirectAfterSubmit : PropTypes.bool.isRequired,
    isKnownState : PropTypes.bool.isRequired,
    topicIdToRedirect : PropTypes.number, //ไม่จำเป็น แต่ตอนสร้างกระทู้เสร็จต้องมี

    //=============Tooltip================
    isTopicNameTooltipOpen : PropTypes.bool.isRequired,
    isTopicPriceTooltipOpen : PropTypes.bool.isRequired,
    isTopicDesctriptionTooltipOpen : PropTypes.bool.isRequired,
    isTopicRemainingItemTooltipOpen : PropTypes.bool.isRequired,
    isCategoryTooltipOpen : PropTypes.bool.isRequired,
    isDeliveryTooltipOpen : PropTypes.bool.isRequired,
    deliveryTooltipMsg : PropTypes.string.isRequired,
    isTopicPictureTooltipOpen : PropTypes.bool.isRequired,

    onInputData : PropTypes.func.isRequired,


    //======For Edit or just Add=======================
    topicName : PropTypes.string, //ไม่จำเป็น ไว้สำหรับแก้ไขกระทู้เท่านั้น
    topicPrice : PropTypes.number, //ไม่จำเป็น ไว้สำหรับแก้ไขกระทู้เท่านั้น
    topicRemainingItem : PropTypes.number, //ไม่จำเป็น ไว้สำหรับแก้ไขกระทู้เท่านั้น
    topicDescription : PropTypes.string, //ไม่จำเป็น ไว้สำหรับแก้ไขกระทู้เท่านั้น
    currentSelectedDeliveryList : PropTypes.array, //ไม่จำเป็น ไว้สำหรับแก้ไขกระทู้เท่านั้น
    isEditTopic : PropTypes.bool.isRequired, //จำเป็น จะได้รู้และบอกต่อchildว่านี่คือการแก้ไขกระทู้หรือสร้างกระทู้ใหม่
    

    //===============Delivery=================
    //Data
    deliveryCarrierList : PropTypes.array.isRequired,
    setDeliveryList : PropTypes.func.isRequired,
    //Event
    setDeliveryList : PropTypes.func.isRequired,
    setSideBarItems : PropTypes.func.isRequired,
    handleDeliveryInfoChanged : PropTypes.func.isRequired,
    onSubmitButtonClicked : PropTypes.func.isRequired,
    onUploadPictureButtonClicked : PropTypes.func.isRequired,
    onCancelButtonClicked : PropTypes.func.isRequired,
    
    //================Category===============
    //Data
    categoryList : PropTypes.array.isRequired,
    selectedCategory : PropTypes.array.isRequired,
    isDialogOpen : PropTypes.bool.isRequired,
    dialogScroll : PropTypes.string.isRequired,
    //Event
    onDialogOpen : PropTypes.func.isRequired,
    onDialogSubmit : PropTypes.func.isRequired,
    onDialogCancel : PropTypes.func.isRequired,
    onCategorySelected : PropTypes.func.isRequired,
    onChipDelete : PropTypes.func.isRequired,
    onChipQuickDelete : PropTypes.func.isRequired,

    //===========Upload Picture===========
    //Data
    isUploading : PropTypes.bool.isRequired,
    fileUploadingPercent : PropTypes.array.isRequired,
    //Event
    onUploadButtonClicked : PropTypes.func.isRequired,
    onFileChanged : PropTypes.func.isRequired,
}