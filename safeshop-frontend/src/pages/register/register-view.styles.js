export const styles = theme => ({
    btnSubmit: {
        color: 'white',
        //background: '#00acc1',
        background: theme.palette.secondary.main,
        '&:hover' : {
            color: 'white',
            //background: '#33bccd'
            background: theme.palette.secondary.main
        }
    },
    layout: {
        width: 'auto',
        display: 'block', // Fix IE11 issue.
        marginLeft: 'auto',
        marginRight: 'auto',
        [theme.breakpoints.up('md')]: {
            width: '864px',
        }
    },
    paper: {
        //marginTop: theme.spacing.unit * 4,
        width: 'auto',
        padding: theme.spacing.unit * 4
    },
    avatar: {
        margin: theme.spacing.unit,
        background: '#ffa726',
        width: 60,
        height: 60
        //backgroundColor: theme.palette.secondary.main,
    },
    errorBox:{
        display: 'flex',
        flexDirection: 'column',
        background: '#d32f2f',
        alignItems: 'center',
        color: '#FFFFFF',
        padding: theme.spacing.unit * 1.5,
    },
});