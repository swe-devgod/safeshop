import React, { Component } from 'react';
import RegisterView from './register-view';
import PropTypes from 'prop-types';

export default class RegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToLogin: false,
            result:{username: true,
                password: true,
                displayName: true,
                firstName: true,
                middleName: true,
                lastName: true,
                email: true,
                tel: true,
                addr: true
            },
            username:'',
            password: '',
            displayName: '',
            firstName: '',
            middleName: '',
            lastName: '',
            email: '',
            tel: '',
            addr: ''

        };
        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
        this.onEditUserName = this.onEditUserName.bind(this);
        this.onEditPassword = this.onEditPassword.bind(this);
        this.onEditConfirmPassword = this.onEditConfirmPassword.bind(this);
        this.onEditDisplayName = this.onEditDisplayName.bind(this);
        this.onEditFirstName = this.onEditFirstName.bind(this);
        this.onEditMiddleName = this.onEditMiddleName.bind(this);
        this.onEditLastName = this.onEditLastName.bind(this);
        this.onEditEmail = this.onEditEmail.bind(this);
        this.onEditTel = this.onEditTel.bind(this);
        this.onEditAddr = this.onEditAddr.bind(this);
    }
    onEditUserName(e){
        this.setState({
            username:e.target.value
        })
    }
    onEditPassword(e){
        this.setState({
            password:e.target.value
        })
    }
    onEditConfirmPassword(e){
        this.setState({
            confirmPassword:e.target.value
        })
    }
    onEditDisplayName(e){
        this.setState({
            displayName:e.target.value
        })
    }
    onEditFirstName(e){
        this.setState({
            firstName:e.target.value
        })
    }
    onEditMiddleName(e){
        this.setState({
            middleName:e.target.value
        })
    }
    onEditLastName(e){
        this.setState({
            lastName:e.target.value
        })
    }
    onEditEmail(e){
        this.setState({
            email:e.target.value
        })
    }
    onEditTel(e){
        this.setState({
            tel:e.target.value
        })
    }
    onEditAddr(e){
        this.setState({
            addr:e.target.value
        })
    }
    componentDidMount() {
        document.title = "Register - SafeShop";
        this.props.dySec.setSideBarItems(null);
    }

    onSubmitButtonClicked(formData) {
        fetch('/api/user/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        }).then(res => {
            if (res.status == 200 && formData.password == formData.confirmPassword) {
                //console.log(res);
                this.setState({ redirectToLogin: true });
            } else {
                if(formData.password !== formData.confirmPassword){
                    this.setState({confirmPassword:''});
                }
                res.json()
                    .then(json => {
                        //console.log(json.result);
                        this.setState({result: json.result});
                        if(!json.result["username"]){
                            this.setState({username:''});
                        }
                        if(!json.result["password"]){
                            this.setState({password:''});
                        }
                        if(formData.password !== formData.confirmPassword){
                            this.setState({confirmPassword:''});
                        }
                        if(!json.result["displayName"]){
                            this.setState({displayName:''});
                        }
                        if(!json.result["firstName"]){
                            this.setState({firstName:''});
                        }
                        if(!json.result["middleName"]){
                            this.setState({middleName:''});
                        }
                        if(!json.result["lastName"]){
                            this.setState({lastName:''});
                        }
                        if(!json.result["email"]){
                            this.setState({email:''});
                        }
                        if(!json.result["tel"]){
                            this.setState({tel:''});
                        }
                        if(!json.result["addr"]){
                            this.setState({addr:''});
                        }
                        
                    });
                
                
            }
        });
    }

    render() {
        return <RegisterView
                    redirectToLogin={this.state.redirectToLogin}
                    onSubmitButtonClicked={this.onSubmitButtonClicked}
                    resultError={this.state.result}
                    onEditUserName={this.onEditUserName}
                    onEditPassword={this.onEditPassword}
                    onEditConfirmPassword={this.onEditConfirmPassword}
                    onEditDisplayName={this.onEditDisplayName}
                    onEditFirstName={this.onEditFirstName}
                    onEditMiddleName={this.onEditMiddleName}
                    onEditLastName={this.onEditLastName}
                    onEditEmail={this.onEditEmail}
                    onEditTel={this.onEditTel}
                    onEditAddr={this.onEditAddr}

                    username={this.state.username} 
                    password={this.state.password}
                    confirmPassword={this.state.confirmPassword}
                    displayName={this.state.displayName}
                    firstName={this.state.firstName}
                    middleName={this.state.middleName}
                    lastName={this.state.lastName}
                    email={this.state.email}
                    tel={this.state.tel}
                    addr={this.state.addr}
                    />
    }
}
RegisterPage.propTypes = {
    dySec : PropTypes.any.isRequired    
};