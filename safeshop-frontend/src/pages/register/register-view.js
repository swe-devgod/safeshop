import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import NavigationIcon from '@material-ui/icons/Navigation';
import InputIcon from '@material-ui/icons/Input';
import Avatar from '@material-ui/core/Avatar';

import { styles } from './register-view.styles';

class TextFieldEx extends TextField {
    render() {
        const { id, label, styles, ...other } = this.props;
        const extendStyles = styles || {};
        extendStyles.width = extendStyles.width || '100%';
        return (
            <TextField
                {...other}
                id={id}
                label={label}
                margin="normal"
                variant="outlined"
                style={extendStyles}
            />
        );
    }
}

function configTextField(config) {
    return class extends Component {
        render() {
            return <TextFieldEx {...config} {...this.props} />
        }
    }
}

const PasswordFieldEx = configTextField({ type: "password", autoComplete: "current-password" });
const EmailFieldEx = configTextField({ type: "email", autoComplete: "email "});

class RegisterView extends Component {

    constructor(props) {
        super(props);

        this.formData = {
            username: '',
            password: '',
            confirmPassword: '',
            displayName: '',
            firstName: '',
            middleName: '',
            lastName: '',
            email: '',
            tel: '',
            addr: ''
        };
        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
    }

    onSubmitButtonClicked() {
        this.props.onSubmitButtonClicked(this.formData);
    }

    render() {
        const { classes, redirectToLogin,resultError, username,password,confirmPassword,displayName,firstName,middleName,lastName,email,tel,addr } = this.props;

        if (redirectToLogin) {
            return <Redirect to="/login" />;
        }
        return (
            <React.Fragment>
                <div className={classes.layout}>
                    <Paper className={classes.paper}>
                        <center>
                            <Avatar className={classes.avatar}>
                                <InputIcon/>
                            </Avatar>
                            <Typography variant="display2">
                                Registration
                            </Typography>
                        </center>
                        <form onSubmit={e => e.preventDefault()}>
                            <Grid container spacing = {16}>
                                <Grid item xs={12} md={6}>
                                    <TextFieldEx required id="outlined-username" label="Username" value={username} autoFocus 
                                        onChange={e =>
                                            { 
                                                this.props.onEditUserName(e)
                                                this.formData.username = e.target.value
                                            }
                                        }/>
                                    {
                                        (!resultError["username"]) &&
                                        <Paper className={classes.errorBox} >
                                            Username must be at least 5 characters long and only alphabet and number are allowed.
                                        </Paper>
                                    }   
                                </Grid>
                                
                                <Grid item xs={12} md={6}>
                                    <TextFieldEx id="outlined-display-name" label="Display Name" value={displayName} 
                                    onChange={e => 
                                        {
                                            this.props.onEditDisplayName(e)
                                            this.formData.displayName = e.target.value
                                        }
                                    }/>
                                    {
                                        (!resultError["displayName"]) &&
                                        <Paper className={classes.errorBox} >
                                            Display Name must be at least 5 characters long and only alphabet and number are allowed.
                                        </Paper>
                                    }   
                                </Grid>
                                
                            </Grid>
                            <Grid container spacing = {16}>
                                <Grid item xs={12} md={6}>
                                    <PasswordFieldEx required id="outlined-password" label="Password" value={password}
                                    onChange={e => 
                                        {
                                            this.props.onEditPassword(e)
                                            this.formData.password = e.target.value
                                        }
                                    }/>
                                    {
                                        (!resultError["password"]) &&
                                        <Paper className={classes.errorBox} >
                                            Password must be at least 8 characters long and only alphabet and number are allowed.
                                        </Paper>
                                    }   
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <PasswordFieldEx required id="outlined-confirm-password" label="Confirm Password" value={confirmPassword} 
                                    onChange={e => 
                                        {
                                            this.props.onEditConfirmPassword(e)
                                            this.formData.confirmPassword = e.target.value
                                        }
                                    }/>
                                    {
                                        (confirmPassword !== password && password !== '') &&
                                        <Paper className={classes.errorBox} >
                                            Password doesn't match.
                                        </Paper>
                                    }
                                </Grid>                    
                            </Grid>
                            <Grid container spacing = {16}>
                                <Grid item xs={12} md={4}>
                                    <TextFieldEx required id="outlined-firstname" label="FirstName" value={firstName} 
                                    onChange={e => 
                                        {
                                            this.props.onEditFirstName(e)
                                            this.formData.firstName = e.target.value
                                        }
                                    }/>
                                    {
                                        (!resultError["firstName"]) &&
                                        <Paper className={classes.errorBox} >
                                            First Name is only allow in alphabet.
                                        </Paper>
                                    }   
                                </Grid>
                                <Grid item xs={12} md={4}>
                                    <TextFieldEx id="outlined-middlename" label="MiddleName" value={middleName}
                                    onChange={e => 
                                        {
                                            this.props.onEditMiddleName(e)
                                            this.formData.middleName = e.target.value
                                        }
                                    }/>
                                    {
                                        (!resultError["middleName"]) &&
                                        <Paper className={classes.errorBox} >
                                            Middle Name is only allow in alphabet.
                                        </Paper>
                                    } 
                                </Grid>
                                <Grid item xs={12} md={4}>
                                    <TextFieldEx required id="outlined-lastname" label="LastName" value={lastName}
                                    onChange={e => 
                                        {
                                            this.props.onEditLastName(e)
                                            this.formData.lastName = e.target.value
                                        }
                                    }/>
                                    {
                                        (!resultError["lastName"]) &&
                                        <Paper className={classes.errorBox} >
                                            Last Name is only allow in alphabet.
                                        </Paper>
                                    } 
                                </Grid>
                            </Grid>
                            <Grid container spacing = {16}>
                                <Grid item xs={12} md={6}>
                                    <EmailFieldEx required id="outlined-email-input" label="Email" value={email}
                                    onChange={e => 
                                        {
                                            this.props.onEditEmail(e)
                                            this.formData.email = e.target.value
                                        }
                                    }/>
                                    {
                                        (!resultError["email"]) &&
                                        <Paper className={classes.errorBox} >
                                            Email incorrect syntax.
                                        </Paper>
                                    }
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <TextFieldEx required id="outlined-phone" label="Phone Number" value={tel}
                                    onChange={e => 
                                        {
                                            this.props.onEditTel(e)
                                            this.formData.tel = e.target.value
                                        }
                                    }/>
                                    {
                                        (!resultError["tel"]) &&
                                        <Paper className={classes.errorBox} >
                                            Telphone is only allow in number.
                                        </Paper>
                                    }
                                </Grid>                    
                            </Grid>
                            <Grid container spacing = {16}>
                                <Grid item xs={12}>
                                    <TextFieldEx required id="outlined-address" label="Address" value={addr} multiline rows="3" 
                                    onChange={e => 
                                        {
                                            this.props.onEditAddr(e)
                                            this.formData.addr = e.target.value
                                        }
                                    }/>    
                                    {
                                        (!resultError["addr"]) &&
                                        <Paper className={classes.errorBox} >
                                            Please fill address.
                                        </Paper>
                                    }
                                </Grid>                 
                            </Grid>
                            <Grid item xs={4}>
                                <Button 
                                    type="submit"
                                    variant="extendedFab" 
                                    className={classes.btnSubmit} 
                                    aria-label="Delete"
                                    onClick={this.onSubmitButtonClicked}>
                                    <NavigationIcon/>
                                    Submit
                                </Button>
                            </Grid>
                        </form>
                    </Paper>
                </div>
            </React.Fragment>
        );
    }
}

RegisterView.propTypes = {
    classes: PropTypes.object.isRequired,
    formData: PropTypes.array.isRequired,
    redirectToLogin: PropTypes.bool.isRequired,
    resultError: PropTypes.array.isRequired, 
    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    confirmPassword: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    middleName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    tel: PropTypes.string.isRequired,
    addr: PropTypes.string.isRequired,
    //func
    onSubmitButtonClicked: PropTypes.func.isRequired,
    onEditUserName: PropTypes.func.isRequired,
    onEditPassword: PropTypes.func.isRequired,
    onEditConfirmPassword: PropTypes.func.isRequired,
    onEditDisplayName: PropTypes.func.isRequired,
    onEditFirstName: PropTypes.func.isRequired,
    onEditMiddleName: PropTypes.func.isRequired,
    onEditLastName: PropTypes.func.isRequired,
    onEditEmail: PropTypes.func.isRequired,
    onEditTel: PropTypes.func.isRequired,
    onEditAddr: PropTypes.func.isRequired,

};

export default withStyles(styles)(RegisterView);