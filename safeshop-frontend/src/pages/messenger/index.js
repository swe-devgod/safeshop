import React, { Component } from 'react';
import MessengerView from './messenger-view';
import { AuthService } from '../../components/auth-service';

export default class MessengerPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            curRoom: 0,
            messages: [],
            isKnownState: false,
            roomData: null,
            lastestMessageTimestamp: 0,
            messageData: [],
            isRedirectToRoom: false,
            redirectToRoom: 0,
            isRedirectToLogin: !!!AuthService.isAuthenticated()
        }

        this.handleSendButtonClicked = this.handleSendButtonClicked.bind(this);
    }

    componentDidMount() {
        document.title = "Messenger - SafeShop";

        this.roomMap = new Map();
        if (!this.fetchMessageHandle) {
            this.fetchMsgRoomHandle = setInterval(() => { this.fetchMessengerRoom() }, 3000);
        }
    }

    componentDidUpdate() {
        //console.log('componentDidUpdate', this.props.match.params.id, this.state);
        console.log('didUpdate ', this.props.match.params.id);
        if (this.props.match.params.id) {
            if (!this.fetchMessageHandle) {
                console.log('create new');
                this.fetchMessageHandle = setInterval(() => { this.fetchMessages() }, 1000);
            }

            if (this.state.curRoom !== +this.props.match.params.id) {
                //console.log('setState curRoom');
                if (this.fetchMessageHandle) {
                    console.log('clear');
                    clearInterval(this.fetchMessageHandle);
                    delete this.fetchMessageHandle;
                }

                this.setState({
                    curRoom: +this.props.match.params.id,
                    messages: [],
                    isKnownState: false,
                    lastestMessageTimestamp: 0,
                    messageData: [],
                });
            }
        } else {
            if (!!!this.state.isRedirectToRoom) {
                this.setState((prevState) => ({
                    isRedirectToRoom: true,
                    redirectToRoom: prevState.curRoom
                }));
            } else {
                this.setState({
                    isRedirectToRoom: false,
                    redirectToRoom: 0
                });
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.fetchMsgRoomHandle);
        clearInterval(this.fetchMessageHandle);
    }

    fetchMessengerRoom() {
        fetch('/api/messenger/lists', {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
        .then(res => {
            AuthService.doAuthorization(res, {
                200: true
            });
            if (!!!AuthService.isAuthenticated()) {
                this.setState({
                    isRedirectToLogin: true
                });
                return [];
            }
            return res.json();
        })
        .then(json => {
            let shouldUpdateRoomList = false;
            json.forEach(room => {
                if (!this.roomMap.has(room.roomId)) {
                    this.roomMap.set(room.roomId, room.timestamp);
                    shouldUpdateRoomList = true;
                } else if (this.roomMap.get(room.roomId) !== room.timestamp) {
                    this.roomMap.set(room.roomId, room.timestamp);
                    shouldUpdateRoomList = true;
                }
                if (shouldUpdateRoomList) {
                    //console.log(shouldUpdateRoomList);
                }
            });

            if (shouldUpdateRoomList) {
                if (!this.props.match.params.id) {
                    console.log('setState redirectToRoom');
                    this.setState({
                        isRedirectToRoom: true,
                        redirectToRoom: +json[0].roomId,
                        roomData: json
                    });
                } else {
                    this.setState({
                        roomData: json
                    })
                }
            }
        })
        .catch(err => console.log('error', err));
    }

    fetchMessages() {
        fetch(`/api/messenger/${this.props.match.params.id}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                timestamp: this.state.lastestMessageTimestamp
            })
        })
        .then(res => {
            AuthService.doAuthorization(res, {
                200: true
            });
            if (!!!AuthService.isAuthenticated()) {
                this.setState({
                    isRedirectToLogin: true
                });
                return [];
            }
            return res.json()
        })
        .then(json => {
            if (json.length > 0) {
                json = json.reverse();
                json.forEach(msg => {
                    msg.isOwner = msg.userId === AuthService.getLoggedInUserId();
                });
                const newMessageData = this.state.messageData.concat(json);
                this.setState({
                    isKnownState: true,
                    lastestMessageTimestamp: json[json.length - 1].timestamp,
                    messageData: newMessageData,
                })
            } else if (!!!this.state.isKnownState) {
                this.setState({
                    isKnownState: true
                });
            }
        })
        .catch(err => console.log(err));
    }

    handleSendButtonClicked(msg) {
        fetch(`/api/messenger/${this.props.match.params.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                roomId: +this.props.match.params.id,
                message: msg
            })
        })
        .then(res => res.json())
        .then(json => {
            console.log(json);
        });
    }

    render() {
        return <MessengerView 
                    browserHistory={this.props.history}
                    isRedirectToLogin={this.state.isRedirectToLogin}
                    isRedirectToRoom={this.state.isRedirectToRoom}
                    redirectToRoom={this.state.redirectToRoom}
                    roomData={this.state.roomData}
                    messageData={this.state.messageData}
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    isKnownState={this.state.isKnownState}
                    messages={this.state.messages} 
                    onSendButtonClicked={this.handleSendButtonClicked} />
    }
}