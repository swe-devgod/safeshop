import React, { Component } from 'react';
import { Link, Redirect, Router } from 'react-router-dom';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';

import Avatar from '@material-ui/core/Avatar';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ProgressView from '../../components/progress-view';
import TextField from '@material-ui/core/TextField';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon';

import MessageView from './message-view';
import MessengerRoomView from './messenger-room-view';

const drawerStyles = theme => ({
    nested: {
        paddingLeft: theme.spacing.unit * 6,
    },
    listItemButton: {
        padding: 8
    },
    container: {
        height: '85vh',
        //overflowX: 'hidden',
        overflowY: 'scroll'
    },
    primaryText: {
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
    },
    secondaryText: {
        lineHeight: '1.5em',
        maxHeight: '3em',
        overflow: 'hidden',
    }
});

const MessengerDrawerItemList = withStyles(drawerStyles)(
    class extends Component {
        render() {
            const { classes } = this.props;
            return (
                <React.Fragment>
                    <div className={classes.container}>
                        {
                            this.props.roomData && this.props.roomData.map((room, index) => (
                                <Link key={index} to={`/messenger/${room.roomId}/`}>
                                    <MessengerRoomView 
                                        userPicture={room.userPicture}
                                        displayName={room.displayName} 
                                        message={room.message} />
                                </Link>
                            ))
                        }
                    </div>
                </React.Fragment>
            );
        }
    }
);

const styles = theme => ({
    msgContainer: {
        height: 'calc(85vh - 16px)',
        display: 'flex',
        flexDirection: 'column'
    },
    msgContentContainer: {
        flex: 1,
        height: 'auto',
        overflowX: 'hidden',
        overflowY: 'scroll',
    },
    paper: {
        paddingTop: 4,
        height: '100%'
        //overflow: 'hidden'
    },
    msgBoxContainer: {
        marginTop: 8,
        padding: 4
    },
    button: {
        margin: theme.spacing.unit,
        width: `calc(100% - ${theme.spacing.unit}px)`,
        height: `calc(100% - ${theme.spacing.unit}px)`
    },
});

class MessengerViewImpl extends Component {

    constructor(props) {
        super(props);
        this.onSendButtonClicked = this.onSendButtonClicked.bind(this);

        this.state = {
            msgText: ''
        };
    }

    componentDidMount() {
        this.props.setSideBarItems(<ProgressView />);
    }

    componentDidUpdate() {
        this.props.setSideBarItems(
            <Router history={this.props.browserHistory}>
                <MessengerDrawerItemList roomData={this.props.roomData} />
            </Router>
        );
    }

    onSendButtonClicked() {
        this.props.onSendButtonClicked(this.state.msgText);
        this.setState({
            msgText: ''
        });
    }

    render() {

        if (this.props.isRedirectToLogin) {
            return <Redirect to={`/login`} />
        }

        if (this.props.isRedirectToRoom) {
            //console.log('redirect');
            return <Redirect to={`/messenger/${this.props.redirectToRoom}`} />
        }

        if (!!!this.props.isKnownState) {
            return <ProgressView />;
        }
        const { classes } = this.props;
        return (
            <React.Fragment>
                <div className={classes.msgContainer}>
                    <div className={classes.msgContentContainer}>
                        <Paper className={classes.paper}>
                            {
                                this.props.messageData && this.props.messageData.map((message, index) => (
                                    <React.Fragment key={index}>
                                        { 
                                            message.isOwner ?
                                            <MessageView isLeft={false} message={message.message} /> 
                                            :
                                            <MessageView isLeft={true} message={message.message} />
                                        }
                                    </React.Fragment>
                                ))
                            }   
                        </Paper>
                    </div>
                    <Grid container>
                        <Grid item xs={11}>
                            <Paper className={classes.msgBoxContainer}>
                                <TextField 
                                    autoFocus
                                    ref={this.refMsgText}
                                    fullWidth
                                    variant="outlined"
                                    rows={3}
                                    rowsMax={3}
                                    multiline 
                                    value={this.state.msgText}
                                    placeholder="Write message here."
                                    onChange={e => this.setState({ msgText: e.target.value })} />
                            </Paper>
                        </Grid>
                        <Grid item xs={1}>
                            <Button variant="contained" color="primary" className={classes.button} onClick={this.onSendButtonClicked}>
                                Send
                                <Icon className={classes.rightIcon}>send</Icon>
                            </Button>
                        </Grid>
                    </Grid>
                </div>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(MessengerViewImpl);