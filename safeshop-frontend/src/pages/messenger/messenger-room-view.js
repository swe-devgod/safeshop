import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Avatar from '@material-ui/core/Avatar';
import classnames from 'classnames';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ProgressView from '../../components/progress-view';
import TextField from '@material-ui/core/TextField';

const styles = ({
    listItemButton: {
        padding: 8
    },
    secondaryText: {

    }
});

export default withStyles(styles)(class extends Component {
    render() {
        const {
            classes,
            displayName,
            userPicture,
            message
        } = this.props;

        return (
            <ListItem button className={classes.listItemButton}>
                <Avatar 
                    alt={displayName} 
                    src={userPicture}
                    className={classes.avatar} />
                <ListItemText 
                    classes={{ primary: classes.primaryText, secondary: classes.secondaryText }}
                    primary={displayName}
                    secondary={message} />
            </ListItem>
        );
    }
});