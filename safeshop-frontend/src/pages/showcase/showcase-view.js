import React, { Component } from 'react';
import PropsType from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider'
import { withStyles } from '@material-ui/core';
import AddToCartIcon from '@material-ui/icons/Add';
import ChatIcon from '@material-ui/icons/Chat';

import { ProductCat } from './components/product-cat';
import { SelectItemAmount } from './components/select-item-amount/index';
import ProductSellerNameAndRating from './components/product-seller-name-rating';
import { ProductRating } from './components/product-rating/index';

import PhotoGallery from './components/photo-gallery';
import ProgressView from '../../components/progress-view';

import { Comment } from './components/comment';
import CommentForm from './components/comment-form';
import CommentBox from './components/comment-box';

import './style.css';

const styles = theme => ({
    badgeCart: {
        color: "white",
    }
});

class TopicView extends Component {
    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
        this.state = {
            selectItemAmount: 1,
        }
    }

    componentDidMount() {
        this.props.setSideBarItems(null);
    }

    setItemAmount = (amount) => {
        this.setState({selectItemAmount: amount});
    }

    onClickedAddToCart = () => {
        const { selectItemAmount } = this.state;

        const formData = {
            amount: selectItemAmount,
        }
        this.props.onSubmitAddToCart(formData);
    }

    onClickedMessegeToSeller = () => {
        const { ownerUserId } = this.props.topicJson;
        
        this.props.onMessageToSeller(ownerUserId);
    }

    render() {
        const {
            productName,
            productDescription,
            productPrice,
            topicRating,
            topicRatingCount,
            ownerName,
            ownerRating,
            ownerPicture,
            ownerRatingCount,
            remainingItems,
            shippings,
            categories,
        } = this.props.topicJson;
        const { selectItemAmount } = this.state; 

        if (!!!this.props.isKnownState) {
            return <ProgressView />;
        }

        let commentItems = null;
        if (this.props.commentsJson) {
            commentItems = this.props.commentsJson.map(comment => 
                <Grid key={comment.commentId} item xs={12}>
                    <Comment
                        commentId={comment.commentId}
                        userId={comment.userId}
                        displayName={comment.displayName}
                        userPicture={comment.userPicture}
                        date={comment.createdDate}
                        imgs={comment.imgs}
                        subComments={comment.subComments}
                        onSubmit={this.props.onSubmitComment}
                        replyable={this.props.userLoggedInId ? true : false}
                        userLoggedInId={this.props.userLoggedInId}
                    >
                        {comment.content}
                    </Comment>
                </Grid>
            );
        }

        return (    
            <React.Fragment>
                <Grid container justify="center">
                    <Grid container item
                        sm={12} md={10} spacing={16}
                        alignItems="stretch" justify="center"
                    >
                        <Grid item xs={12}>
                            <ProductCat productName={productName} categoryList={categories} />
                        </Grid>
                        <Grid item container sm={12} spacing={0}>
                            <Grid item xs={12} sm={5}>
                                <PhotoGallery photos={this.props.topicJson.imgs} />
                            </Grid>
                            <Grid item xs={12} sm={7}>
                                <Paper square id='paper-product-detail' elevation={1}>
                                    <Grid container direction="column">
                                        <Grid item id='grid-product-name-price'>
                                            <Typography variant="headline" style={{ paddingTop: "5px"}}>
                                                {productName}
                                            </Typography>
                                            <Typography component="p">
                                                ราคา <span className="color-orange" style={{ fontSize: "24px"}}>฿{productPrice}</span>
                                            </Typography>
                                            <ProductRating topicRating={topicRating}
                                                            topicRatingCount={topicRatingCount}/>
                                            <Divider style={{ margin: '8px 0 8px 0' }}/>
                                            <Grid container spacing={16} alignItems="center">
                                                <Grid item>
                                                    <ProductSellerNameAndRating 
                                                        ownerName={ownerName}
                                                        ownerPicture={ownerPicture}
                                                        ownerRating={ownerRating}
                                                        ownerRatingCount={ownerRatingCount}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm='auto'>
                                                    <Button 
                                                        id='button-chat'
                                                        variant="outlined"
                                                        fullWidth
                                                        onClick={this.onClickedMessegeToSeller}
                                                    >
                                                        <ChatIcon/>ติดต่อผู้ขาย
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid id='grid-product-description'>
                                            <div style={{fontSize: '20px'}}>รายละเอียดสินค้า</div>
                                            <Divider style={{ margin: '2px 0 2px 0' }}/>
                                            <p style={{ fontSize: "15px"}}>
                                                {productDescription}
                                            </p>
                                            <div style={{fontSize: '20px'}}>การจัดส่ง</div>
                                            <Divider style={{ margin: '2px 0 2px 0' }}/>
                                            <ul>
                                                {
                                                    shippings &&
                                                    shippings.map((data) => (
                                                        <li>{data.name} - {data.price} บาท</li>
                                                    ))
                                                }
                                            </ul>
                                            <Divider style={{ margin: '8px 0 8px 0' }}/>
                                            <SelectItemAmount
                                                value={selectItemAmount}
                                                remainingItems={remainingItems}
                                                setValue={this.setItemAmount}
                                            />
                                            <Grid
                                                container
                                                spacing = {8}
                                                direction='row'
                                                style={{ paddingBottom: "10px", paddingTop: "5px"}}
                                            >
                                                <Grid item>
                                                    <Button 
                                                        onClick={() => this.onClickedAddToCart()}
                                                        color="primary"
                                                        variant="raised"
                                                    >
                                                        <AddToCartIcon/>
                                                        Add to Cart
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        </Grid>
                        <Grid item container xs={12} spacing={8} style={{marginBottom: 16}}>
                            {commentItems}
                            { this.props.userLoggedInId &&
                                <Grid item xs={12}>
                                    <CommentBox>
                                        <CommentForm
                                            parentId={null}
                                            onSubmit={this.props.onSubmitComment}
                                        />
                                    </CommentBox>
                                </Grid>
                            }
                        </Grid>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(TopicView);

TopicView.propTypes = {
    isKnownState : PropsType.bool.isRequired,
    topicJson : PropsType.object.isRequired,
    topicCategory : PropsType.array,
    commentsJson : PropsType.array.isRequired,
    userLoggedInId : PropsType.number.isRequired,
    onSubmitComment : PropsType.func.isRequired,
    onSubmitAddToCart : PropsType.func.isRequired,
    onMessageToSeller : PropsType.func.isRequired,
    dispatcher : PropsType.object.isRequired,
    setSideBarItems : PropsType.func,
    setAppBarItems : PropsType.func,
}