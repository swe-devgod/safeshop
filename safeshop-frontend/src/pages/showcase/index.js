import React, { Component } from 'react';
import PropsType from 'prop-types';

import Redirect from 'react-router-dom/Redirect';

import { AuthService } from '../../components/auth-service'
import { fetchCartAppbarAmount } from '../../lib/cart-appbar-helper';

import ShowcaseView from './showcase-view';

export default class ShowcasePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            topicJson: {
                productName: '',
                productDescription: '',
                productPrice: 0,
                createdDate: 0,
                topicRating: 0.0,
                topicRatingCount: 0,
                ownerName: '',
                ownerRating: 0.0,
                ownerRatingCount: 0,
                remainingItems: 0,
                imgs: [],
                shippings: [],
            },
            isKnownState: false,
            commentsJson: [],
            redirectLocation: null,
        }

        this.onSubmitComment = this.onSubmitComment.bind(this);
    }

    fetchTopicComment() {
        const apiGetTopicCommentURL = `/api/topic/${this.props.match.params.id}/comments`;
        fetch(apiGetTopicCommentURL)
            .then(res => res.json())
            .then(json => this.setState({
                commentsJson: json
            }))
            .catch(err => console.error(err));
    }

    fetchTopic() {
        const apiGetTopicURL = "/api/topic/" + this.props.match.params.id;
        fetch(apiGetTopicURL)
            .then(res => res.json())
            .then(json => this.setState({
                topicJson : json,
                isKnownState: true
            }))
            .catch(err => console.error(err));
    }

    componentDidMount() {
        document.title = "Showcase - SafeShop";

        this.fetchTopic();
        this.fetchTopicComment();
    }

    onSubmitComment(formData) {
        return new Promise( (resolve, reject) => {
            switch(formData.method) {
                case "POST": {
                    const body = new FormData();
                    body.append('content', formData.content);
                    formData.imgs.slice().forEach((file, index) => {
                        body.append('imgs', file, index + '-' + file.name);
                    });
                    if (formData.parentId) {
                        body.append('parentId', formData.parentId);
                    }
                    fetch(`/api/topic/${parseInt(this.props.match.params.id, 10)}/comments`, {
                        method: "POST",
                        body: body,
                    })
                    .then(res => {
                        resolve(res.status === 200 ? true : false);
                        this.fetchTopicComment();
                    });
                    break;
                }
                case "DELETE": {
                    fetch(`/api/topic/comments/${formData.commentId}`, {
                        method: "DELETE",
                    })
                    .then(res => {
                        resolve(res.status === 200 ? true : false);
                        this.fetchTopicComment();
                    })
                    break;
                }
                case "PUT": {
                    const body = new FormData();
                    body.append("content", formData.content);
                    formData.appendImgs.slice().forEach((file, index) => {
                        body.append('appendImgs', file, index + '-' + file.name);
                    });
                    body.append('deletedImgs', JSON.stringify(formData.deletedImgs));
                    fetch(`/api/topic/comments/${formData.commentId}`, {
                        method: "PUT",
                        body: body,
                    })
                    .then(res => {
                        resolve(res.status === 200 ? true : false);
                        this.fetchTopicComment();
                    });
                    break;
                }
                default: 
                    console.error("onSubmitComment(): Invalid method");
            }
        });
    }

    onSubmitAddToCart = (formData) => {
        const { dispatcher } = this.props;
        /* Set CartButton if success */

        const body = {
            topicId: parseInt(this.props.match.params.id, 10),
            ...formData,
        };
        fetch("/api/cart/", {
            method: "PUT",
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    fetchCartAppbarAmount(dispatcher);
                    this.fetchTopicComment();
                }
            });      
    }


    onMessageToSeller = (sellerId) => {
        const body = { destUserId: sellerId };
        fetch("/api/messenger/new", {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200 && AuthService.isAuthenticated()) {
                    return res.json();
                } else {
                    return new Promise((resolve, reject) => {
                        return reject('Response is not return JSON');
                    })
                }
            })
            .then(json => this.setState({redirectLocation: `/messenger/${json.roomId}`}));
    }

    render() {
        const { redirectLocation } = this.state;

        if (redirectLocation) {
            return <Redirect to={redirectLocation} />
        }

        return <ShowcaseView
                    isKnownState={this.state.isKnownState} 
                    topicJson={this.state.topicJson}
                    commentsJson={this.state.commentsJson}
                    userLoggedInId={AuthService.isAuthenticated() ? AuthService.getLoggedInUserId() : null}
                    onSubmitComment={this.onSubmitComment}
                    onSubmitAddToCart={this.onSubmitAddToCart}
                    onMessageToSeller={this.onMessageToSeller}
                    dispatcher={this.props.dispatcher}
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    setAppBarItems={items => this.props.dySec.setAppBarItems(items)} />
    }
}

ShowcasePage.propTypes = {
    dispatcher : PropsType.object.isRequired,
    setSideBarItems : PropsType.func,
    setAppBarItems : PropsType.func,
}