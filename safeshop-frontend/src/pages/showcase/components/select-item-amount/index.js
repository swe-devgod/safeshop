import React from 'react';
import PropsType from 'prop-types';

import { TextField } from '@material-ui/core';
import { SelectItemAmountView } from './select-item-amount-view';

export class SelectItemAmount extends TextField {
    constructor(props) {
        super(props);
        this.state = {
            amountToBuy: props.value || 1,
            textFieldValue: props.value || 1,
        }
        this.incrementItem = this.incrementItem.bind(this); //บอกว่าthisของ IncrementItem หมายถึงthis ในcontext ของconstruct
        this.decrementItem = this.decrementItem.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }

    onChange = (amount) => {
        const { setValue } = this.props;

        if (setValue) {
            setValue(amount);
        }
    }

    handleChange(e){
        const { remainingItems } = this.props;
        const value = +e.target.value;

        if (value !== NaN && value !==  0) {
            if(value > remainingItems) {
                this.setState({
                    textFieldValue : remainingItems,
                    amountToBuy : remainingItems
                });
                this.onChange(remainingItems);
            } else {
                this.setState({
                    textFieldValue : value,
                    amountToBuy : value
                });
                this.onChange(value)
            }
        } else {
            if (e.target.value !== '') {
                this.setState(state => {
                    this.onChange(state.amountToBuy);
                    return { ...state };
                });
            } else {
                this.setState({
                    textFieldValue: '',
                    amountToBuy: 0 
                });
                this.onChange(0);
            }
        }
    }

    handleBlur(e){
        if(this.state.textFieldValue === ''){
            this.setState({
                textFieldValue : 1,
                amountToBuy : 1
            })
            this.onChange(1);
        }
    }

    incrementItem() {
        const { amountToBuy } = this.state;
        const { remainingItems } = this.props;
        if(this.state.amountToBuy + 1 > remainingItems){ //เหมือนประมาณว่าsetStateแล้วไม่renderใหม่ทันที ถ้าให้> itemเฉยๆ ตอนกดครั้งที่จาก amount = remainไปเป็นamout > remain เลยต้องกันไว้
            this.setState({
                textFieldValue : remainingItems,
                amountToBuy : remainingItems
            });
            this.onChange(remainingItems);
        }else{
            this.setState({ amountToBuy: amountToBuy + 1,
                textFieldValue: amountToBuy + 1
            });
            this.onChange(amountToBuy + 1);
        }
    }

    decrementItem() {
        const { amountToBuy } = this.state;
        if(amountToBuy >=2){
            this.setState({ amountToBuy: amountToBuy - 1,
                textFieldValue: amountToBuy - 1
            });
            this.onChange(amountToBuy - 1);
        }else{
            this.setState({ amountToBuy: 1,
                textFieldValue: 1
            });
            this.onChange(1);
        }
    }

    render() {
        return(
            <SelectItemAmountView   
                textFieldValue={this.state.textFieldValue}
                remainingItems={this.props.remainingItems}
                decrementItemClicked={this.decrementItem}
                incrementItemClicked={this.incrementItem}
                handleChange={this.handleChange}
                onBlur={this.handleBlur}
            />
        );
    }
}

SelectItemAmount.propTypes = {
    defaultValue : PropsType.number.isRequired,
    remainingItems : PropsType.number.isRequired
}