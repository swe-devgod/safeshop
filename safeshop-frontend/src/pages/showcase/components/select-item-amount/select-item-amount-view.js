import React from 'react';
import PropsType from 'prop-types';

import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import PlusIcon from '@material-ui/icons/AddSharp';
import MinusIcon from '@material-ui/icons/Remove';
import { TextField } from '@material-ui/core';

export class SelectItemAmountView extends TextField {

    render() {
        return(
            <React.Fragment>
                <Grid container spacing={0} direction='row'>
                    <Grid item xs={12}>
                        จำนวนที่ต้องการจะซื้อ
                    </Grid>
                    <Grid item style={{ outlineStyle: 'auto' , outlineColor:'grey'}}>
                        <div className='color-white' style={{display: 'inline-block'}}>
                            <Button className="buy-button-element" onClick={this.props.decrementItemClicked}>
                                <MinusIcon/>
                            </Button>
                            <TextField
                                onKeyPress={e => { if ("0123456789".indexOf(e.key) === -1) { e.preventDefault(); }}}
                                type="number" 
                                InputProps={{disableUnderline: true}}
                                id='textfield-buyAmount' 
                                onChange={this.props.handleChange}
                                onBlur={this.props.onBlur} 
                                value={this.props.textFieldValue}/>
                            <Button className="buy-button-element" onClick={this.props.incrementItemClicked}>
                                <PlusIcon/>
                            </Button>
                        </div>
                    </Grid>
                    <Grid item style={{ paddingLeft: '6px' ,height: '48px', marginTop: '-5px'}}>
                        <p className='color-black' style={{ fontSize: '17px'}}>ชิ้น 
                            <span className="color-grey" style={{ marginLeft: '4px' }}>(มีสินค้าทั้งหมด {this.props.remainingItems} ชิ้น)</span>
                        </p>
                    </Grid>
                </Grid>
            </React.Fragment> 
        );
    }
}

SelectItemAmountView.propTypes = {
    textFieldValue : PropsType.number.isRequired,
    remainingItems : PropsType.number.isRequired,

    decrementItemClicked : PropsType.func.isRequired,
    incrementItemClicked : PropsType.func.isRequired,
    handleChange : PropsType.func.isRequired,
    onBlur : PropsType.func.isRequired,
}