import React from 'react';

import TextField from '@material-ui/core/TextField'
import IconButton from "@material-ui/core/IconButton";
import SendIcon from "@material-ui/icons/Send"
import InputAdornment from '@material-ui/core/InputAdornment';
import CircularProgress from '@material-ui/core/CircularProgress';
import ErrorIcon from '@material-ui/icons/Error';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';

import NewFileUploading from '../../../components/new-file-uploading';

export default class CommentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            content: props.content ? props.content : "",
            prevImgs: props.prevImgs ? props.prevImgs : [],
            uploadImages: props.prevImgs ? props.prevImgs.map(img => img.url) : [],
            uploadImageFiles: [],
            deletedImgs: [],
            isSending: false,
            isError: false,
            isShowUpload: props.prevImgs ? true : false,
        };
        this.onSendButtonClicked = this.onSendButtonClicked.bind(this);
        this.onShowUploadClicked = this.onShowUploadClicked.bind(this);
        this.handleUploadImageChanged = this.handleUploadImageChanged.bind(this);
        this.handleUploadImageDeleted = this.handleUploadImageDeleted.bind(this);
    }

    onShowUploadClicked() {
        this.setState(prevState => ({
            isShowUpload: !prevState.isShowUpload,
        }));
    }

    handleUploadImageChanged(files) {
        let imgsUrl = this.state.uploadImages.slice();
        let imgFiles = this.state.uploadImageFiles.slice();
        for (let i = 0; i < files.length; i++) {
            imgsUrl.push(window.URL.createObjectURL(files[i]));
            imgFiles.push(files[i]);
        }

        this.setState({
            uploadImages: imgsUrl,
            uploadImageFiles: imgFiles,
        })
    }

    handleUploadImageDeleted(index) {
        if (index < this.state.prevImgs.length) {
            const newPrevImgs = this.state.prevImgs.slice();
            const newdeletedImgs = this.state.deletedImgs.slice();
            const deletedImgsId = this.state.prevImgs[index].id;
            newPrevImgs.splice(index, 1);
            newdeletedImgs.push(deletedImgsId);

            const newImgsUrl = this.state.uploadImages.slice();
            newImgsUrl.splice(index, 1);

            this.setState({
                prevImgs: newPrevImgs,
                uploadImages: newImgsUrl,
                deletedImgs: newdeletedImgs,
            })
        } else {
            index = index - this.state.prevImgs.length;

            const newImgsUrl = this.state.uploadImages.slice();
            const newImgsFile = this.state.uploadImageFiles.slice();
            newImgsUrl.splice(index, 1);
            newImgsFile.splice(index, 1);

            this.setState({
                uploadImages: newImgsUrl,
                uploadImageFiles: newImgsFile,
            });
        }
    }

    onSendButtonClicked() {
        const { content, uploadImageFiles, deletedImgs } = this.state;

        this.setState({isSending: true});
        switch (this.props.type) {
            case "edit":
                this.props.onSubmit({
                    method: 'PUT',
                    commentId: this.props.parentId,
                    content: content,
                    appendImgs: uploadImageFiles,
                    deletedImgs: deletedImgs,
                })
                .then(isSuccess => {
                    this.setState({isSending: false});
                    if (isSuccess) {
                        this.setState({
                            isError: false,
                        });
                        if (this.props.handleSuccess) {
                            this.props.handleSuccess();
                        }
                    } else {
                        this.setState({isError: true})
                    }
                });
                break;
            default:
                this.props.onSubmit({
                    method: 'POST',
                    content: content,
                    imgs: uploadImageFiles,
                    parentId: this.props.parentId,
                })
                .then(isSuccess => {
                    this.setState({isSending: false});
                    if (isSuccess) {
                        this.setState({
                            content: '',
                            uploadImages: [],
                            uploadImageFiles: [],
                            isShowUpload: false,
                            isError: false
                        });
                    } else {
                        this.setState({isError: true})
                    }
                });
                break;
        }
    }
    
    render() {
        const { content, isSending, isError, isShowUpload, uploadImages } = this.state;
        const sendButton = isSending ? <CircularProgress /> : <IconButton onClick={this.onSendButtonClicked}><SendIcon /></IconButton>
        const showUploadButton = <IconButton color={isShowUpload ? "primary" : "default"} onClick={this.onShowUploadClicked}><AddPhotoAlternateIcon /></IconButton>
        const placeholderMessage = "แสดงความคิดเห็น...";

        return (
            <React.Fragment>
                <TextField
                    placeholder={placeholderMessage}
                    fullWidth
                    multiline
                    rows={1}
                    rowsMax={10}
                    InputProps={{
                        endAdornment: <InputAdornment position="end" >{isError && <ErrorIcon color="error"/>}{showUploadButton}{sendButton}</InputAdornment>,
                        disableUnderline: true,
                    }}
                    value={content}
                    onChange={e => this.setState({
                        content: e.target.value,
                    })}
                />
                { isShowUpload &&
                    <NewFileUploading
                        items={uploadImages}
                        itemWidth={150}
                        itemHeight={150}
                        onFileChanged={this.handleUploadImageChanged}
                        onDelete={this.handleUploadImageDeleted}
                    />
                }
            </React.Fragment>
        );
    }
}