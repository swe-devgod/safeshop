import React from 'react';
import PropsType from 'prop-types';
import Grid from '@material-ui/core/Grid'

export class ProductRatingView extends React.Component {

    render() {
        return (
            <React.Fragment>
                <div>
                    <Grid container spacing = {0} direction='row'>
                        <span className="color-grey">{this.props.productNoRatingMsg}</span>
                        { this.props.productNoRatingMsg === '' &&
                            <React.Fragment>
                                <Grid item>
                                    <img
                                        src={this.props.imgSrcFirstStar}
                                        style={{marginTop: '3px', width: '15px', display: 'block'}}
                                        alt='firstStar'
                                    />
                                </Grid>
                                <Grid item>
                                    <img
                                        src={this.props.imgSrcSecondStar}
                                        style={{marginTop: '3px', width: '15px', display: 'block'}}
                                        alt='secondStar'
                                    />
                                </Grid>
                                <Grid item>
                                    <img
                                        src={this.props.imgSrcThirdStar}
                                        style={{marginTop: '3px', width: '15px', display: 'block'}}
                                        alt='thirdStar'
                                    />
                                </Grid>
                                <Grid item>
                                    <img
                                        src={this.props.imgSrcFourthStar}
                                        style={{marginTop: '3px', width: '15px', display: 'block'}}
                                        alt='fourthStar'
                                    />
                                </Grid>
                                <Grid item>
                                    <img
                                        src={this.props.imgSrcFifthStar}
                                        style={{marginTop: '3px', width: '15px', display: 'block'}}
                                        alt='fifthStar'
                                    />
                                </Grid>
                                <Grid item>
                                    <span className="color-grey" style={{width: '13px'}}>({this.props.topicRatingCount} ratings)</span>
                                </Grid>
                            </React.Fragment>
                        }
                        
                    </Grid>
                </div>
            </React.Fragment>
        );
    }
}

ProductRatingView.propTypes = {
    topicRatingCount : PropsType.number, //อาจมาเป็น0หรือundefinedได้
    imgSrcFirstStar : PropsType.string.isRequired,
    imgSrcSecondStar : PropsType.string.isRequired,
    imgSrcThirdStar : PropsType.string.isRequired,
    imgSrcFourthStar : PropsType.string.isRequired,
    imgSrcFifthStar : PropsType.string.isRequired,
    productNoRatingMsg : PropsType.string.isRequired,
}