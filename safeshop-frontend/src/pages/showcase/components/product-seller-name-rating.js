import React from 'react';
import PropsType from 'prop-types';

import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar';

import { SellerRating } from './seller-rating/index';
import { Typography, withStyles } from '@material-ui/core';

const styles = theme => ({
    avatarBig: {
        width: 48,
        height: 48,
        backgroundColor: 'white',
        color: theme.palette.secondary.main,
    },
})

class ProductSellerNameAndRating extends React.Component {

    render() {
        const { classes } = this.props;

        return (
                <Grid container spacing={8} alignItems="center">
                    <Grid item>
                        <Avatar 
                            src={this.props.ownerPicture}
                            className={classes.avatarBig}
                        />
                    </Grid>
                    <Grid item container direction="column" xs justify="center">
                        <Grid item>
                            <Typography variant="title" color="primary">
                                {this.props.ownerName}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <SellerRating ownerRating={this.props.ownerRating} ownerRatingCount={this.props.ownerRatingCount}/> 
                        </Grid>
                    </Grid>
                </Grid>     
        );
    }
}

export default withStyles(styles)(ProductSellerNameAndRating);

ProductSellerNameAndRating.propTypes = {
    ownerName : PropsType.string.isRequired,
    ownerPicture : PropsType.string, // Optional
    ownerRating : PropsType.number,
    ownerRatingCount : PropsType.number,
}