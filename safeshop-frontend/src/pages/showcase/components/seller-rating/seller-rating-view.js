import React from 'react';
import PropsType from 'prop-types';

import Grid from '@material-ui/core/Grid'

export class SellerRatingView extends React.Component {

    render() {
        return (
                <Grid container spacing = {0} direction='row'>
                    <span style={{color:'grey'}}>{this.props.sellerNoRatingMsg}</span>
                    { this.props.sellerNoRatingMsg === '' &&
                        <React.Fragment>
                            <Grid item>
                                <img
                                    src={this.props.imgSrcFirstStar}
                                    style={{marginTop: '-3px', width: '13px', display: 'block'}}
                                    alt='firstStar'
                                />
                            </Grid>
                            <Grid item>
                                <img
                                    src={this.props.imgSrcSecondStar}
                                    style={{marginTop: '-3px', width: '13px', display: 'block'}}
                                    alt='secondStar'
                                />
                            </Grid>
                            <Grid item>
                                <img
                                    src={this.props.imgSrcThirdStar}
                                    style={{marginTop: '-3px', width: '13px', display: 'block'}}
                                    alt='thirdStar'
                                />
                            </Grid>
                            <Grid item>
                                <img
                                    src={this.props.imgSrcFourthStar}
                                    style={{marginTop: '-3px', width: '13px', display: 'block'}}
                                    alt='fourthStar'
                                />
                            </Grid>
                            <Grid item>
                                <img
                                    src={this.props.imgSrcFifthStar}
                                    style={{marginTop: '-3px', width: '13px', display: 'block'}}
                                    alt='fifthStar'
                                />
                            </Grid>
                            <Grid item>
                                <span 
                                    style={{color:'grey' , marginTop: '-4px', display: 'block', fontSize: '11px'}}>
                                    ({this.props.ownerRatingCount})
                                </span>
                            </Grid>
                        </React.Fragment>
                    }
                </Grid>
        );
    }
}

SellerRatingView.propTypes = {
    ownderRatingCount : PropsType.number, //อาจมาเป็น0หรือundefinedได้
    imgSrcFirstStar : PropsType.string.isRequired,
    imgSrcSecondStar : PropsType.string.isRequired,
    imgSrcThirdStar : PropsType.string.isRequired,
    imgSrcFourthStar : PropsType.string.isRequired,
    imgSrcFifthStar : PropsType.string.isRequired,
    sellerNoRatingMsg : PropsType.string.isRequired,
}