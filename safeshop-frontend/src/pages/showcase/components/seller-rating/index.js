import React from 'react';
import PropsType from 'prop-types';

import imgStarFull from '../../raw/image/img_star_full.png';
import imgStarQuarterFull from '../../raw/image/img_star_quarter_full.png';
import imgStarHalf from '../../raw/image/img_star_half.png';
import imgStarQuarterEmpty from '../../raw/image/img_star_quarter_empty.png';
import imgStarEmpty from '../../raw/image/img_star_empty.png';
import { SellerRatingView } from './seller-rating-view';

const star = [
    { imgPath: imgStarFull },//full
    { imgPath: imgStarQuarterFull },//3/4
    { imgPath: imgStarHalf },//half
    { imgPath: imgStarQuarterEmpty },//1/4
    { imgPath: imgStarEmpty },//empty
];

export class SellerRating extends React.Component {
    constructor(props) {
        super(props);
        this.viewData = {
            imgSrcFirstStar: '',
            imgSrcSecondStar: '',
            imgSrcThirdStar: '',
            imgSrcFourthStar: '',
            imgSrcFifthStar: '',
            sellerNoRatingMsg: ''
        };
    }

    getStarSrcPic(imgSrcObj, values) {
        if(this.props.ownerRating < values[0]){
            imgSrcObj.imgSrc = star[4].imgPath;
        }else if(this.props.ownerRating < values[1]){
            imgSrcObj.imgSrc = star[3].imgPath;
        }else if(this.props.ownerRating < values[2]){
            imgSrcObj.imgSrc = star[2].imgPath;
        }else if(this.props.ownerRating <= values[3]){
            imgSrcObj.imgSrc = star[1].imgPath;
        }else{
            imgSrcObj.imgSrc = star[0].imgPath;
        }
    }

    getRatingAsStar(){
        if(this.props.ownerRatingCount === 0 || this.props.ownerRatingCount === undefined){
            this.viewData.sellerNoRatingMsg = "ยังไม่มีการให้คะแนนคนขายนี้";
        }else{
            this.viewData.sellerNoRatingMsg = "";
            this.getFirstStarSrcPic();
            this.getSecondStarSrcPic();
            this.getThirdStarSrcPic();
            this.getFourthStarSrcPic();
            this.getFifthStarSrcPic();
        }
    }

    getFirstStarSrcPic(){
        const passed = { imgSrc: '' }
        this.getStarSrcPic(passed, [ 0.06, 0.4, 0.7, 0.99 ]);
        this.viewData.imgSrcFirstStar = passed.imgSrc;
    }

    getSecondStarSrcPic(){
        const passed = { imgSrc: '' }
        this.getStarSrcPic(passed, [ 1.06, 1.4, 1.7, 1.99 ]);
        this.viewData.imgSrcSecondStar = passed.imgSrc;
    }

    getThirdStarSrcPic(){
        const passed = { imgSrc: '' }
        this.getStarSrcPic(passed, [ 2.06, 2.4, 2.7, 2.99 ]);
        this.viewData.imgSrcThirdStar = passed.imgSrc;
    }

    getFourthStarSrcPic(){
        const passed = { imgSrc: '' }
        this.getStarSrcPic(passed, [ 3.06, 3.4, 3.7, 3.99 ]);
        this.viewData.imgSrcFourthStar = passed.imgSrc;
    }

    getFifthStarSrcPic(){
        const passed = { imgSrc: '' }
        this.getStarSrcPic(passed, [ 4.06, 4.4, 4.7, 4.99 ]);
        this.viewData.imgSrcFifthStar = passed.imgSrc;
    }

    render() {
        this.getRatingAsStar();
        return (
            <SellerRatingView   ownerRatingCount={this.props.ownerRatingCount}
                                imgSrcFirstStar={this.viewData.imgSrcFirstStar}
                                imgSrcSecondStar={this.viewData.imgSrcSecondStar}
                                imgSrcThirdStar={this.viewData.imgSrcThirdStar}
                                imgSrcFourthStar={this.viewData.imgSrcFourthStar}
                                imgSrcFifthStar={this.viewData.imgSrcFifthStar}
                                sellerNoRatingMsg={this.viewData.sellerNoRatingMsg}
            />
        );
    }
}

SellerRating.propTypes = {
    ownerRating : PropsType.number,
    ownerRatingCount : PropsType.number,
}