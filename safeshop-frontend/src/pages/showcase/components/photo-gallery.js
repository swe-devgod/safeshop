import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import MobileStepper from '@material-ui/core/MobileStepper'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

const styles = theme => ({
    paperGallery: {
        height: '100%',
        overflow: 'hidden',
    },
    gridImg: {
        whiteSpace: 'nowrap',
        textAlign: 'center'
    },
    divImg: {
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        minHeight: '360px',
        cursor: "pointer",
    },
    dialogContentImage: {
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
    }
});

class PhotoGallery extends React.Component {
    state = {
        activeStep: 0,
        open: false,
    };

    isEmptyPhotos = () => {
        if (this.props.photos !== undefined && this.props.photos.length !== 0) {
            return false;
        } 
        return true;
    }

    handleNext = () => {
        this.setState((state, props) => {
            let newActiveStep = state.activeStep + 1;
            if (newActiveStep === props.photos.length) {
                newActiveStep = 0;
            }
            
            return { activeStep: newActiveStep };
        }); 
    };
    
    handleBack = () => {
        this.setState((state, props) => {
            let newActiveStep = state.activeStep - 1;
            if (newActiveStep < 0) {
                newActiveStep = props.photos.length - 1;
            }
            
            return { activeStep: newActiveStep };
        }); 
    };

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    }

    render() {
        const { classes, photos } = this.props;

        /* Return empty Paper for undefined Photos */
        if (this.isEmptyPhotos()) {
            return <Paper className={classes.paperGallery} />
        }

        const { activeStep } = this.state;
        const maxSteps = photos.length;

        return (
            <Paper square className={classes.paperGallery} elevation={1}>
                <Grid container direction="column" alignItems="stretch" style={{height:'100%'}}>
                    <Grid item xs style={{backgroundImage: `url(${photos[activeStep].url}`}} className={classes.divImg} onClick={this.handleClickOpen} />
                    <Grid item>
                        <Dialog
                            open={this.state.open}
                            onClose={this.handleClose}
                            fullScreen
                            PaperProps={{
                                style: {backgroundColor: "transparent"}
                            }}
                        >
                            <DialogContent
                                className={classes.dialogContentImage}
                                style={{backgroundImage: `url(${photos[activeStep].url}`}}
                                onClick={this.handleClose}
                            />
                        </Dialog>
                        <MobileStepper
                            steps={maxSteps}
                            position="static"
                            activeStep={activeStep}
                            className={classes.mobileStepper}
                            nextButton={
                                <Button size="small" onClick={this.handleNext}>
                                    <KeyboardArrowRight />
                                </Button>
                            }
                            backButton={
                                <Button size="small" onClick={this.handleBack}>
                                    <KeyboardArrowLeft />
                                </Button>
                            }
                        />
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

PhotoGallery.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(PhotoGallery);