import React from 'react';
import PropTypes from 'prop-types';

import CommentBox from './comment-box';
import CommentForm from './comment-form';
import ImgDialog from './img-dialog';

import { Grid, withStyles, Typography, Avatar, Divider } from '@material-ui/core';
import ReplyIcon from '@material-ui/icons/Reply';
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import MenuList from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
    avatarBig: {
        width: 48,
        height: 48,
        backgroundColor: 'white',
        color: theme.palette.secondary.main,
    },
    headerCommentBox: {
        marginBottom: 4,
        overflow: "hidden",
        flexWrap: 'nowrap',
    },
    iconSmall: {
        cursor: "pointer",
        '&:hover': {
            color: theme.palette.primary.main,
        }
    },
    gridImages: {
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        height: 100
    },
    divider: {
        margin: '8px 0px',
    },
    typoInlineBlock: {
        display: 'inline-block',
        margin: '0 8px 0 0',
    }
});

class CommentImpl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showCommentForm: false,
            showEditForm: false,
            anchorEl: null,
        }
        this.toggleCommentForm = this.toggleCommentForm.bind(this);
        this.handleEditSuccess = this.handleEditSuccess.bind(this);
    }

    toggleCommentForm() {
        const prevShowCommentForm = this.state.showCommentForm;
        this.setState({showCommentForm: !prevShowCommentForm});
    }

    handleMenuClicked = e => {
        this.setState({anchorEl: e.currentTarget});
    }

    onDeleteButtonClicked = e => {
        this.setState({anchorEl: null});
        this.props.onSubmit({
            method: 'DELETE',
            commentId: this.props.commentId
        });
    }

    onEditButtonClicked = e => {
        const prevShowEditForm = this.state.showEditForm;
        this.setState({anchorEl: null, showEditForm: !prevShowEditForm});
    }

    handleEditSuccess() {
        this.setState({showEditForm: false});
    }

    render() {
        const { classes } = this.props;
        const { showCommentForm, showEditForm, anchorEl } = this.state;

        let subCommentItems = null;
        if (this.props.subComments) {
            subCommentItems = this.props.subComments.map( subComment => 
                <Grid key={subComment.commentId} item xs>
                    <Comment
                        commentId={subComment.commentId}
                        userId={subComment.userId}
                        displayName={subComment.displayName}
                        userPicture={subComment.userPicture}
                        date={subComment.createdDate}
                        imgs={subComment.imgs}
                        subComments={subComment.subComments}
                        onSubmit={this.props.onSubmit}
                        replyable={false}
                        userLoggedInId={this.props.userLoggedInId} 
                    >
                        {subComment.content}
                    </Comment>
                </Grid>
            );
        }

        return (
            <Grid container spacing={8} >
                <Grid item>
                    <Avatar
                        src={this.props.userPicture}
                        className={classes.avatarBig}
                    >
                        {this.props.displayName[0]}
                    </Avatar>
                </Grid>
                <Grid container item direction="column" xs spacing={8}>
                    <Grid item>
                        <CommentBox>
                            <Grid container>
                                <Grid item container xs={12} className={classes.headerCommentBox}>
                                    <Grid item xs>
                                        <Typography
                                            component="span"
                                            variant="title"
                                            color="primary"
                                            className={classes.typoInlineBlock}
                                        >
                                            {this.props.displayName}
                                        </Typography>
                                        <Typography
                                            component="span"
                                            variant="caption"
                                            color="textSecondary"
                                            className={classes.typoInlineBlock}
                                        >
                                            {new Date(this.props.date).toLocaleString( "th-TH", {weekday: "long", day: "numeric", month: "long", year: "numeric", hour: "numeric", minute: "numeric", hour12: false})}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        { this.props.replyable &&
                                            <ReplyIcon color="action" onClick={this.toggleCommentForm} className={classes.iconSmall}/>
                                        } 
                                        { this.props.userLoggedInId === this.props.userId &&
                                            <React.Fragment>
                                                <MoreVertIcon color="action" className={classes.iconSmall} onClick={this.handleMenuClicked} />
                                                <MenuList
                                                    anchorEl={anchorEl}
                                                    open={Boolean(anchorEl)}
                                                    onClose={() => this.setState({anchorEl: null})}
                                                >
                                                    <MenuItem onClick={this.onEditButtonClicked}>
                                                        <ListItemIcon>
                                                            <EditIcon />
                                                        </ListItemIcon>
                                                        <ListItemText inset primary="แก้ไข"/>
                                                    </MenuItem>
                                                    <MenuItem onClick={this.onDeleteButtonClicked}>
                                                        <ListItemIcon>
                                                            <DeleteIcon />
                                                        </ListItemIcon>
                                                        <ListItemText inset primary="ลบ"/>
                                                    </MenuItem>
                                                </MenuList>
                                            </React.Fragment>
                                        }
                                    </Grid>
                                </Grid>
                                <Grid item xs={12}>
                                    { showEditForm ?
                                        <CommentForm
                                            type="edit"
                                            parentId={this.props.commentId}
                                            onSubmit={this.props.onSubmit}
                                            content={this.props.children}
                                            prevImgs={this.props.imgs}
                                            handleSuccess={this.handleEditSuccess}
                                        /> 
                                        : 
                                        <Typography>{this.props.children}</Typography>
                                    }
                                </Grid>
                                { this.props.imgs.length > 0 && !showEditForm &&
                                    <Grid item xs={12}>
                                        <Divider light className={classes.divider}/>
                                        <Grid container spacing={8}>
                                            {this.props.imgs.map(img => (
                                                <Grid item xs={4} md={2} key={img}>
                                                    <ImgDialog imgSrc={img.url} />
                                                </Grid>
                                            ))}
                                        </Grid>
                                    </Grid>
                                }
                            </Grid>
                        </CommentBox>
                    </Grid>
                    {subCommentItems}
                    { this.props.userLoggedInId && showCommentForm &&
                        <Grid item xs>
                            <CommentBox>
                                <CommentForm parentId={this.props.commentId} onSubmit={this.props.onSubmit} />
                            </CommentBox>
                        </Grid>
                    }
                </Grid>
            </Grid>
        );
    }
}

CommentImpl.propTypes = {
    classes: PropTypes.object.isRequired,
};

export const Comment = withStyles(styles)(CommentImpl);