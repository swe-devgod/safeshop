import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core';

const styles = theme => ({
    "commentBox": {
        padding: 12,
        borderRadius: 18,
    }
});

class CommentBox extends React.Component {
    render() {
        return (
            <Paper elevation={1} className={this.props.classes.commentBox} style={this.props.style}>
                {this.props.children}
            </Paper>
        );
    }
}

CommentBox.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(CommentBox);

