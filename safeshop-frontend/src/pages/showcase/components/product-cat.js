import React from 'react';

export class ProductCat extends React.Component {
    render() {
        const { productName, categoryList } = this.props;

        return (
            <p style={{"margin": "0 5px 5px 5px","color": "grey"}}>
                { categoryList &&
                    categoryList.map(category => `${category.categoryName} > `)
                }
                {productName}
            </p>
        );
    }
}