import React from 'react';

import { withStyles, Paper, Dialog, DialogContent } from '@material-ui/core';

const styles = theme => ({
    gridImage: {
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        height: 100,
        cursor: "pointer",
        backgroundColor: "#fafafa",
    },
    dialogContentImage: {
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
    }
});

class ImgDialog extends React.Component {
    state = {
        open: false,
    };

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    }

    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <Paper
                    elevation={0}
                    square style={{backgroundImage: `url(${this.props.imgSrc}`}}
                    className={classes.gridImage}
                    onClick={this.handleClickOpen}
                />
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    fullScreen
                    PaperProps={{
                        style: {backgroundColor: "transparent"}
                    }}
                >
                    <DialogContent
                        className={classes.dialogContentImage}
                        style={{backgroundImage: `url(${this.props.imgSrc}`}}
                        onClick={this.handleClose}
                    />
                </Dialog>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(ImgDialog);