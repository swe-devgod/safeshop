import React from 'react';
import { shallow } from 'enzyme';

import LoginPage from '../index';

describe('Testing LoginPage', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    it('User submited login with correct username and password', async () => {
        fetch.mockResponseOnce(JSON.stringify({userId: 3}));

        const wrapper = shallow(<LoginPage />);
        expect(wrapper.state('redirectToIndex')).toEqual(false);

        // User submited login
        wrapper.instance().onSubmitButtonClicked({
            username: 'Valid username',
            password: 'Valid password',
        });

        // Set timer for continuous asynchronous function 
        await setTimeout(() => {
            expect(wrapper.state('redirectToIndex')).toEqual(true)
        }, 500);
    });

    it('User submited login with wrong username or password', async () => {
        // Setup failed response
        fetch.mockResponseOnce({status: 401});

        const wrapper = shallow(<LoginPage />);
        expect(wrapper.state('redirectToIndex')).toEqual(false);

        // User enter wrong username and password 
        wrapper.instance().onEditUsername({
            target: {
                value: 'wrong username',
            }
        });
        wrapper.instance().onEditPassword({
            target: {
                value: 'wrong password',
            }
        });
        expect(wrapper.state('username')).toEqual('wrong username');
        expect(wrapper.state('password')).toEqual('wrong password');

        // User submited login
        wrapper.instance().onSubmitButtonClicked({
            username: 'wrong username',
            password: 'wrong password',
        });

        // Set timer for continuous asynchronous function
        await setTimeout(() => {
            expect(wrapper.state('redirectToIndex')).toEqual(false);
            expect(wrapper.state('username')).toEqual('');
            expect(wrapper.state('password')).toEqual('');
        }, 500);
    });
});