import React, { Component } from 'react';
import LoginView from './login-view';
import { doAuthorization, AuthService } from '../../components/auth-service';
import { fetchCartAppbarAmount } from '../../lib/cart-appbar-helper';

export default class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToIndex: false,
            showError: 0,
            username:'',
            password:'',
            loading: false,
        };
        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
        this.onEditUsername = this.onEditUsername.bind(this);
        this.onEditPassword = this.onEditPassword.bind(this);
    }

    componentDidMount() {
        document.title = "Login - SafeShop";
    }
    handleError(){
        if(this.state.error === 401 || this.state.error === 400){
            this.setState({
                username:'',
                password:'',
            })
        }
    }
    render() {
        this.handleError();
        return <LoginView 
                    redirectToIndex={this.state.redirectToIndex} 
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    onSubmitButtonClicked={this.onSubmitButtonClicked}
                    showError={this.state.showError} 
                    username={this.state.username}
                    password={this.state.password}
                    loading={this.state.loading}
                    onEditUsername={this.onEditUsername}
                    onEditPassword={this.onEditPassword} />;
    }

    onEditUsername(e) {
        this.setState({
            username:e.target.value
        });
    }
    
    onEditPassword(e){
        this.setState({
            password:e.target.value
        });
    }

    onSubmitButtonClicked(formData) {
        const data = new URLSearchParams();
        data.append('username', formData.username);
        data.append('password', formData.password);
        
        if (formData.username !== '' && formData.password !== '') {
            this.setState({ loading: true });
            fetch('/api/user/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                body: data
            }).then(res => {
                AuthService.onUserLogin(res, success => {
                    if (success) {
                        this.setState({ redirectToIndex: true });
                        fetchCartAppbarAmount(this.props.dispatcher);
                    } else {
                        this.setState({ showError: res.status });
                        this.setState({ username: '', password: '' });
                        console.error('[LoginPage]/[onSubmitButtonClicked]: Fetch Error');
                    }
                    this.setState({ loading: false });
                });
            });
        }
    }
}