import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import CircularProgress from '@material-ui/core/CircularProgress';

import { styles } from './login-view.styles';

class LoginView extends Component {
    constructor(props) {
        super(props);

        this.formData = {
            username: '',
            password: ''
        };

        this.onSubmitButtonClicked = this.onSubmitButtonClicked.bind(this);
    }

    componentDidMount() {
        this.props.setSideBarItems(null);
    }

    onSubmitButtonClicked() {
        this.props.onSubmitButtonClicked(this.formData);
    }
    render() {
        const { classes, redirectToIndex, showError, username, password } = this.props;

        if (redirectToIndex) {
            return <Redirect to="/" />;
        }

        return (
            <React.Fragment>
                <CssBaseline />
                <div className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockIcon />
                        </Avatar>
                        <Typography variant="h5">Log in</Typography>
                        <form className={classes.form} onSubmit={e => e.preventDefault()}>
                            {
                                showError === 401 &&
                                <Paper className={classes.errorBox} >
                                    Username or Password incorrect.
                                </Paper>
                            }
                            {
                                showError === 400 &&
                                <Paper className={classes.errorBox} >
                                    Username or Password incorrect input format.
                                </Paper>
                            }
                            <FormControl margin="normal" required fullWidth id="username">
                                <InputLabel htmlFor="username">Username</InputLabel>
                                <Input 
                                    id="inp-username" 
                                    name="username" 
                                    value={username}
                                    autoFocus 
                                    onChange={e => 
                                        {
                                            this.props.onEditUsername(e)
                                            this.formData.username = e.target.value   
                                        }
                                    } />
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Password</InputLabel>
                                <Input 
                                    name="password" 
                                    type="password" 
                                    id="inp-password"
                                    autoComplete="current-password"
                                    value={password} 
                                    onChange={e => 
                                        {
                                            this.props.onEditPassword(e)
                                            this.formData.password = e.target.value   
                                        }
                                    } />
                            </FormControl>
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary" />}
                                label="Remember me" />
                            <div className={classes.divSubmit}>
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    disabled={this.props.loading}
                                    className={classes.submit}
                                    onClick={this.onSubmitButtonClicked}>Sign in</Button>
                                { this.props.loading && 
                                    <CircularProgress size={24} className={classes.loginProgress} />
                                }
                            </div>
                        </form>
                    </Paper>
                </div>
          </React.Fragment>
        );
    }
}

LoginView.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(LoginView);