import React, { Component } from 'react';
import PropsType from 'prop-types';
import SellerManagementView from './seller-management-view'
import { AuthService } from '../../components/auth-service';

export default class SellerManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isKnownState : false,
            menuIndex : 0,
            isRefreshCurrentItemList: false,
            sellingList : [],
            awaitingDeliveryList :[],
            onDeliveryList : [],
            successList : [],
        };
        this.handleMenuIndexChange = this.handleMenuIndexChange.bind(this);
        this.handleSwipeMenuIndexChange = this.handleSwipeMenuIndexChange.bind(this);
        this.refreshCurrentItemList = this.refreshCurrentItemList.bind(this);
    }

    componentDidMount() {
        document.title = "Seller Management - SafeShop";
        //fetch data here
        Promise.resolve(this.fetchDataAsync());
    }

    async fetchDataAsync() {
        const apiGetSellingItemList = "/api/management/seller/selling";
        const apiGetAwaitingDeliveryItemList = "/api/management/seller/awaiting-shipment";
        const apiGetOnDeliveryItemList = "/api/management/seller/shipping";
        const apiGetSuccessItemList = "/api/management/seller/success";
        
        function getJson(endpoint) {
            return new Promise((resolve, reject) => {
                fetch(endpoint)
                .then(res => res.json())
                .then(json => resolve(json))
                .catch(err => reject(err));
            });
        }

        const json1 = await getJson(apiGetSellingItemList);
        const json2 = await getJson(apiGetAwaitingDeliveryItemList);
        const json3 = await getJson(apiGetOnDeliveryItemList);
        const json4 = await getJson(apiGetSuccessItemList);

        this.setState({
            sellingList: json1.map(c => ({ ...c })),
            awaitingDeliveryList: json2.map(c => ({ ...c })),
            onDeliveryList: json3.map(c => ({ ...c })),
            successList: json4.map(c => ({ ...c })),
            isKnownState: true,
            isRefreshCurrentItemList: false
        });
    }

    async fetchDataAsyncByMenuIndex() {
        const apiGetSellingItemList = "/api/management/seller/selling";
        const apiGetAwaitingDeliveryItemList = "/api/management/seller/awaiting-shipment";
        const apiGetOnDeliveryItemList = "/api/management/seller/shipping";
        const apiGetSuccessItemList = "/api/management/seller/success";
        
        function getJson(endpoint) {
            return new Promise((resolve, reject) => {
                fetch(endpoint)
                .then(res => res.json())
                .then(json => resolve(json))
                .catch(err => reject(err));
            });
        }

        if(this.state.menuIndex === 0){
            const json1 = await getJson(apiGetSellingItemList);
            this.setState({
                sellingList: json1.map(c => ({ ...c })),
                isKnownState: true,
                isRefreshCurrentItemList: false
            });
        }

        else if(this.state.menuIndex === 1){
            const json2 = await getJson(apiGetAwaitingDeliveryItemList);
            this.setState({
                awaitingDeliveryList: json2.map(c => ({ ...c })),
                isKnownState: true,
                isRefreshCurrentItemList: false
            });
        }

        else if(this.state.menuIndex === 2){
            const json3 = await getJson(apiGetOnDeliveryItemList);
            this.setState({
                onDeliveryList: json3.map(c => ({ ...c })),
                isKnownState: true,
                isRefreshCurrentItemList: false
            });
        }

        else if(this.state.menuIndex === 3){
            const json4 = await getJson(apiGetSuccessItemList);
            this.setState({
                successList: json4.map(c => ({ ...c })),
                isKnownState: true,
                isRefreshCurrentItemList: false
            });
        }
    }

    componentDidUpdate(){
        //ถ้าเปลี่ยนหมวดitemListหรือกดหมวดหมู่เดิมซ้ำให้ดึงข้อมูลจากdbมาใหม่ หรือ ทำให้ข้อมูลเปลี่ยน
        if(this.state.isRefreshCurrentItemList) {
            Promise.resolve(this.fetchDataAsyncByMenuIndex());
        }
    }

    refreshCurrentItemList(){
        this.setState({
            isKnownState: false,
            isRefreshCurrentItemList: true
        });
    }

    handleMenuIndexChange = (event, index) =>{
        this.setState({
            menuIndex: index,
            isKnownState: false,
            isRefreshCurrentItemList: true
        });
    }

    handleSwipeMenuIndexChange = index => {
        this.setState({ 
            menuIndex: index,
            isKnownState: false,
            isRefreshCurrentItemList: true
        });
    };

    render() {
        return <SellerManagementView
                    //Init
                    setSideBarItems={items => this.props.dySec.setSideBarItems(items)}
                    browserHistory={this.props.history}
                    
                    isAuthenticated={AuthService.isAuthenticated()}
                    isKnownState={this.state.isKnownState}

                    //Data
                    menuIndex={this.state.menuIndex}
                    sellingList={this.state.sellingList}
                    awaitingDeliveryList={this.state.awaitingDeliveryList}
                    onDeliveryList={this.state.onDeliveryList}
                    successList={this.state.successList}

                    //Event
                    refreshCurrentItemList={this.refreshCurrentItemList}
                    onMenuChange={this.handleMenuIndexChange}
                    onSwipeMenuChange={this.handleSwipeMenuIndexChange}

                />
    }
}

SellerManagement.propTypes = {
    dySec : PropsType.any.isRequired
}