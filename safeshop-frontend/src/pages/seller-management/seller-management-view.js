import React, { Component } from 'react';
import PropsType from 'prop-types';

import { withStyles } from '@material-ui/core';
import { Redirect, Link, Router } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import SwipeableViews from 'react-swipeable-views';
import TopicItemList from './components/topic-item-list'

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({

});

class SellerManagementView extends Component{
    constructor(props){
        super(props);
    }

    componentDidUpdate() {
        function SideBar() {
            return (
                <React.Fragment>
                    <Link to="/profile">
                        <ListItem button>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="User Profile" />
                        </ListItem>
                    </Link>
                    <Divider />
                    <Link to="/buyer-management">
                        <ListItem button>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText primary="Buyer Management" />
                        </ListItem>
                    </Link>
                    <Divider />
                    <Link to="/seller-management">
                        <ListItem button>
                        <ListItemIcon>
                            <SendIcon />
                        </ListItemIcon>
                        <ListItemText primary="Seller Management" />
                        </ListItem>
                    </Link>
                    <Divider />
                </React.Fragment>
            );
        }

        this.props.setSideBarItems(
            <Router history={this.props.browserHistory}>
                <SideBar />
            </Router>
        );
    }

    render(){
        const { classes } = this.props;

        if (!!!this.props.isAuthenticated) {
            return <Redirect to={`/login`} />
        }
        return(
            <Grid container justify="center">
                <Grid item xs={12} md={10}>
                    <Grid item>
                        <h1>Seller Management</h1>
                    </Grid>
                    <Grid item>
                        <Paper
                            style={{overflow: 'hidden'}}
                        >
                            <Tabs
                                fullWidth
                                value={this.props.menuIndex}
                                onChange={this.props.onMenuChange}
                            >
                                <Tab label="Selling"/>
                                <Tab label="Awaiting Delivery"/>
                                <Tab label="On delivery"/>
                                <Tab label="Success"/>
                            </Tabs>
                        </Paper>
                    </Grid>
                    <Grid item>
                        <SwipeableViews
                            index={this.props.menuIndex}
                            onChangeIndex={this.props.onSwipeMenuChange}
                        >
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>Selling</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการสินค้าทั้งหมดที่คุณกำลังขาย</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        itemList={this.props.sellingList}
                                        isSelling={true}
                                        isKnownState={this.props.isKnownState}

                                        refreshCurrentItemList={this.props.refreshCurrentItemList}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>Waiting Delivery</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการคำสั่งซื้อที่รอการจัดส่ง</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        itemList={this.props.awaitingDeliveryList}
                                        onDeliveryList={this.props.onDeliveryList}
                                        isAwaitingDelivery={true}
                                        isKnownState={this.props.isKnownState}

                                        refreshCurrentItemList={this.props.refreshCurrentItemList}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>On Delivery</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการคำสั่งซื้อที่อยู่ระหว่างการจัดส่ง</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        itemList={this.props.onDeliveryList}
                                        isOnDelivery={true}
                                        isKnownState={this.props.isKnownState}

                                        refreshCurrentItemList={this.props.refreshCurrentItemList}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container direction="column">
                                <Grid item>
                                    <h1>Success</h1>
                                </Grid>
                                <Grid item>
                                    <p>รายการคำสั่งซื้อทั้งหมดที่ได้ขายไป</p>
                                </Grid>
                                <Grid item>
                                    <TopicItemList
                                        itemList={this.props.successList}
                                        isSuccess={true}
                                        isKnownState={this.props.isKnownState}
                                    /> 
                                </Grid>
                            </Grid>
                        </SwipeableViews>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(SellerManagementView);

SellerManagementView.propTypes = {
    //Init
    setSideBarItems : PropsType.func.isRequired,
    isAuthenticated : PropsType.bool.isRequired,
    isKnownState : PropsType.bool.isRequired,

    //Data
    menuIndex : PropsType.number.isRequired,
    sellingList : PropsType.array.isRequired,
    awaitingDeliveryList : PropsType.array.isRequired,
    onDeliveryList : PropsType.array.isRequired,
    successList : PropsType.array.isRequired,

    //Event
    refreshCurrentItemList : PropsType.func.isRequired,
    onMenuChange : PropsType.func.isRequired,
    onSwipeMenuChange : PropsType.func.isRequired,
}