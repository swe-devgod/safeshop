import React from 'react';
import { shallow } from 'enzyme';

import SellerManagementPage from '../index';

describe('Testing SellerManagementPage', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    it('test get ItemList', async () => {
        const sellingListJson_mock = [{"id": 2, "shippings":[{"id": 4, "name": "ThaiPost EMS"},{"id": 4, "name": "ThaiPost Register"}],
                                    "thumbnail": "http://localhost:3001/static/topics/2/t-1.jpg", "topicCreatedDate": 1540387757294, 
                                    "topicDescription": "หูฟัง One Odio สภาพใหม่ ใช้ไป 2 ชั่วโมง ของแท้ Pre-Order มาจากจีน",
                                    "topicName": "หูฟัง One Odio สภาพใหม่", "topicPrice": 599, "topicRating": 4, "topicRemainingItems": 20,
                                    "topicVoteCount": 2, "userId": 2}];
        fetch.mockResponseOnce(JSON.stringify(sellingListJson_mock));

        const wrapper = shallow(<SellerManagementPage/>);
        expect(wrapper.state('isKnownState')).toEqual(false);

        await setTimeout(() => {
            expect(wrapper.state('sellingList')).toEqual(sellingListJson_mock);
        }, 100);
    });
    
    it('test func fetchDataAsync',async() => {
        const sellingListJson_mock = [{"id": 2, "shippings":[{"id": 4, "name": "ThaiPost EMS"},{"id": 4, "name": "ThaiPost Register"}],
                                    "thumbnail": "http://localhost:3001/static/topics/2/t-1.jpg", "topicCreatedDate": 1540387757294, 
                                    "topicDescription": "หูฟัง One Odio สภาพใหม่ ใช้ไป 2 ชั่วโมง ของแท้ Pre-Order มาจากจีน",
                                    "topicName": "หูฟัง One Odio สภาพใหม่", "topicPrice": 599, "topicRating": 4, "topicRemainingItems": 20,
                                    "topicVoteCount": 2, "userId": 2}];
        const wrapper = shallow(<SellerManagementPage/>);
        expect(wrapper.state('isKnownState')).toEqual(false);
        wrapper.instance().fetchDataAsync({
            sellingList: JSON.stringify(sellingListJson_mock),
        });

        await setTimeout(() => {
            expect(wrapper.state('sellingList')).toEqual(sellingListJson_mock);
        }, 100);
    });

});