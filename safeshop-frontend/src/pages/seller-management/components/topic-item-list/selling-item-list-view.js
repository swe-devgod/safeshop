import React, { Component } from 'react';
import PropsType from 'prop-types'
import { withStyles, Typography } from '@material-ui/core';
import ProgressView from './../../../../components/progress-view'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

//Selling
import { Link, Redirect} from 'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Hidden from '@material-ui/core/Hidden';
import { DialogActions } from '@material-ui/core';
import { ProductRating } from './../../../showcase/components/product-rating/';

const styles = theme => ({

    img: {
        marginTop: '35px',
        marginLeft: '20px',
        marginBottom: '35px',
        display: 'block',
        width: '70%',
        height: '60%',
      },

    button:{
        marginBottom: '10px'
    },

    delivery:{
        marginTop: '34px'
    },
});

class SellingListView extends Component{
    constructor(props){
        super(props);

    }

    render(){
        const { classes } = this.props;

        if(!!!this.props.isKnownState){
            return <ProgressView/>
        }

        if(this.props.isRedirectToEditTopic){
            return <Redirect to={{
                                pathname: `/topic/${this.props.wantToEditTopic.id}/edit`,
                                state: { 
                                    //topic : this.props.wantToEditTopic,
                                    id : this.props.wantToEditTopic.id
                                }
                            }}
                    />
        }
        return(
            <React.Fragment>
                <Dialog
                    fullWidth={true}
                    maxWidth = {'md'}
                    open={this.props.isDeleteDialogOpen}
                >
                    <DialogTitle>
                        <p>ยืนยันที่จะยกเลิกการขายสินค้านี้หรือไม่?</p>
                    </DialogTitle>
                    <DialogContent>
                        {
                            this.props.itemList && 
                            <div>
                                <span>ชื่อกระทู้สินค้า: <h4>{this.props.wantToDeleteTopic.topicName}</h4></span>
                            </div>
                        }
                    </DialogContent>
                    <DialogActions>
                        <Grid container spacing={16}>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    onClick={this.props.onDialogSubmit}>ยืนยัน
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    color="primary"
                                    onClick={this.props.onDialogCancel}>ยกเลิก
                                </Button>
                            </Grid>
                        </Grid>
                    </DialogActions>
                </Dialog>
                <Hidden smDown>
                    <Grid container spacing={16} style={{width: '100%', margin: '0 0'}}> {/* Important !!! */}
                        {//For Desktop
                            this.props.itemList &&
                            this.props.itemList.map((item, index) => (
                                <Grid item xs={12}>
                                    <Paper>
                                        <Grid container alignItems={"center"}>
                                            <Grid item xs={3}>
                                                <img className={classes.img} src={item.thumbnail}/>
                                            </Grid>
                                            <Grid item container direction="column" xs={6}>
                                                <Grid item container direction='row'>
                                                    <Grid item xs={6}>
                                                        <Grid item>
                                                            <Typography variant="h6">{item.topicName}</Typography>
                                                        </Grid>
                                                        <Grid item>
                                                            <ProductRating topicRating={item.topicRating}
                                                                topicRatingCount={item.topicVoteCount}
                                                            />
                                                        </Grid>
                                                        <Grid item>
                                                            <Typography>วันที่เริ่มขาย
                                                                <span>: </span>
                                                                {new Date(item.topicCreatedDate).toLocaleString
                                                                    ( "th-TH", {day: "numeric", month: "numeric", year: "numeric"})
                                                                }
                                                            </Typography>
                                                        </Grid>
                                                        <Grid item>
                                                            <Typography>ราคา {item.topicPrice} บาท</Typography>
                                                        </Grid>
                                                        <Grid item>
                                                            <Typography>จำนวน {item.topicRemainingItems} ชิ้น</Typography>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Grid item>
                                                            <Typography className={classes.delivery}>วิธีจัดส่งพัสดุ</Typography>
                                                        </Grid>
                                                    {
                                                        item.shippings.map((delivery) => (
                                                            <Typography>{delivery.name}</Typography>
                                                        ))
                                                    }   
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <Grid container alignItems={"center"}>
                                                    <Grid item xs={12}>
                                                        <Link to={`/topic/${item.id}/edit`}>
                                                            <Button
                                                                variant={"outlined"}
                                                                className={classes.button}
                                                                onClick={() => this.props.onClickEditSelling(index)}>
                                                                Edit
                                                            </Button>
                                                        </Link>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Hidden>
                <Hidden mdUp>
                    <Grid container spacing={16} style={{width: '100%', margin: '0 0'}}> {/* Important !!! */}
                        {//For Mobile
                            this.props.itemList &&
                            this.props.itemList.map((item, index) => (
                                <Grid item xs={12}>
                                    <Paper>
                                        <Grid container alignItems={"center"}>
                                            <Grid item xs={3}>
                                                <img className={classes.img} src={item.thumbnail}/>
                                            </Grid>
                                            <Grid item container direction="column" xs={6}>
                                                <Grid item>
                                                    <Typography variant="h6">{item.topicName}</Typography>
                                                </Grid>
                                                <Grid item>
                                                    <ProductRating topicRating={item.topicRating}
                                                        topicRatingCount={item.topicVoteCount}
                                                    />
                                                </Grid>
                                                <Grid item>
                                                    <Typography>วันที่เริ่มขาย
                                                        <span>: </span>
                                                        {new Date(item.topicCreatedDate).toLocaleString
                                                            ( "th-TH", {day: "numeric", month: "numeric", year: "numeric"})
                                                        }
                                                    </Typography>
                                                </Grid>
                                                <Grid item>
                                                    <Typography>ราคา {item.topicPrice} บาท</Typography>
                                                </Grid>
                                                <Grid item>
                                                    <Typography>จำนวน {item.amount} ชิ้น</Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <Grid container alignItems={"center"}>
                                                    <Grid item xs={12}>
                                                        <Button
                                                            variant={"outlined"}
                                                            className={classes.button}
                                                            onClick={() => this.props.onClickEditSelling(index)}>
                                                            Edit
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Hidden>
            </React.Fragment>
        );
    }
}


export default withStyles(styles)(SellingListView);

SellingListView.propTypes = {
    itemList : PropsType.array.isRequired, //จำเป็นต้องมี
    wantToEditTopic : PropsType.array.isRequired,
    wantToDeleteTopic : PropsType.array.isRequired,
    wantToEditTopic : PropsType.array.isRequired,
    isRedirectToEditTopic : PropsType.bool.isRequired,
    isDeleteDialogOpen : PropsType.bool.isRequired, ///จำเป็นต้องมีไม่งั้นจะเปิดdialog ลบกระทู้ไม่ได้
    wantToDeleteIndex : PropsType.number.isRequired,
    isKnownState : PropsType.bool.isRequired,

    onClickEditSelling : PropsType.func.isRequired, //จำเป็นต้องมีไม่งั้นจะไปหน้าแก้ไขกระทู้ไม่ได้
    onClickRemove : PropsType.func.isRequired, //จำเป็นต้องมีไม่งั้นจะลบกระทู้ที่เลือกออกไม่ได้
    onDialogSubmit : PropsType.func.isRequired,
    onDialogCancel : PropsType.func.isRequired,
}