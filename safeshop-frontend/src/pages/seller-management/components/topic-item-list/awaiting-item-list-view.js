import React, { Component } from 'react';
import PropsType from 'prop-types';
import { withStyles, TextField, Typography } from '@material-ui/core';
import ProgressView from './../../../../components/progress-view'

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { ProductRating } from './../../../showcase/components/product-rating/';

//Awaiting
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({

    paper: {
        paddingBottom : '10px'
    },

    img: {
        marginTop: '35px',
        marginLeft: '20px',
        marginBottom: '35px',
        display: 'block',
        width: '70%',
        height: '60%',
      },

    button:{
        height: '56px',
        marginLeft: '10px',
        marginTop: '10px'
    },

    devliveryMarginTop:{
        marginTop: '10px'
    },

    tooltipResize:{
        fontSize: '13px'
    },

    arrowPopper: {
        '&[x-placement*="bottom"] $arrowArrow': {
          top: 0,
          left: 0,
          marginTop: '-0.9em',
          width: '3em',
          height: '1em',
          '&::before': {
            borderWidth: '0 1em 1em 1em',
            borderColor: `transparent transparent ${theme.palette.grey[700]} transparent`,
          },
        },
        '&[x-placement*="top"] $arrowArrow': {
          bottom: 0,
          left: 0,
          marginBottom: '-0.9em',
          width: '3em',
          height: '1em',
          '&::before': {
            borderWidth: '1em 1em 0 1em',
            borderColor: `${theme.palette.grey[700]} transparent transparent transparent`,
          },
        },
    },
    arrowArrow: {
        position: 'absolute',
        fontSize: '13px',
        width: '3em',
        height: '3em',
        '&::before': {
            content: '""',
            margin: 'auto',
            display: 'block',
            width: 0,
            height: 0,
            borderStyle: 'solid',
        },
    }
});

class AwaitingListView extends Component{
    constructor(props){
        super(props);
        this.state = {
            arrowRef: null,
        };
    }

    handleArrowRef = node => {
        this.setState({
          arrowRef: node,
        });
    };

    render(){
        const { classes } = this.props;

        if(!!!this.props.isKnownState){
            return <ProgressView/>
        }

        return(
            <React.Fragment>
                <Grid container spacing={16} style={{width: '100%', margin: '0 0'}}>
                    {
                        this.props.itemList &&
                        this.props.itemList.map((item, index) => (
                            <Grid item xs={12} md={6}>
                                <Paper className={classes.paper}>
                                    <Grid container alignItems={"center"}>
                                        <Grid item xs={3}>
                                            <img className={classes.img} src={item.topic.thumbnail}/>
                                        </Grid>
                                        <Grid item xs={9}>
                                            <Typography variant="h6">{item.topic.topicName}</Typography>
                                            <ProductRating topicRating={item.topic.topicRating}
                                                            topicRatingCount={item.topic.topicVoteCount}/>
                                            <Typography>ราคา {item.pricePerItem} บาท</Typography>
                                            <Typography>จำนวน {item.amount} ชิ้น</Typography>
                                            <Typography>จัดส่งให้คุณ: {item.user.firstName} {item.user.middleName} {item.user.lastName} ({item.user.displayName})</Typography>
                                            <Typography>วิธีการขนส่ง: {item.shipping.name}</Typography>
                                            <Typography>ที่อยู่สำหรับการจัดส่ง: {item.address.address}</Typography>
                                            <Tooltip
                                                PopperProps={{
                                                    disablePortal: true,
                                                }}
                                                open={this.props.isTooptipOpen}
                                                disableFocusListener
                                                disableHoverListener
                                                disableTouchListener
                                                //classes={{
                                                    //tooltip: classes.tooltipResize //ข้างในTooltipมันมี tooltip เป็นwrapperอยู่
                                                    //พอจะปรับfontSizeเลยต้องปรับที่tooltipข้างในให้มีpropตามtooltipResize
                                                //}}
                                                title={
                                                    <React.Fragment>
                                                        <span className={classes.tooltipResize}>{this.props.errorMessage}</span>
                                                        <span className={classes.arrowArrow} ref={this.handleArrowRef} />
                                                    </React.Fragment>
                                                }
                                                classes={{ popper: classes.arrowPopper }}
                                            >
                                                <TextField required variant="outlined" label="เลขพัสดุ"
                                                    value={this.props.trackingNumberList[index]}                                  
                                                    onChange={e => this.props.onTrackingNumberChange(e.target.value, index)}
                                                    className={classes.devliveryMarginTop}
                                                />
                                            </Tooltip>
                                            <Button 
                                                variant={"outlined"}
                                                onClick={() => this.props.onClickDelivery(index)}
                                                className={classes.button}
                                            >
                                                ฉันได้จัดส่งสินค้าแล้ว
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))
                    }
                </Grid>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(AwaitingListView);

AwaitingListView.propTypes = {
    itemList : PropsType.array.isRequired,
    trackingNumberList : PropsType.array.isRequired,
    errorMessage : PropsType.string.isRequired, //จำเป็น เอาไว้โชว์error
    isTooptipOpen : PropsType.bool.isRequired, //จำเป็นไม่งั้นโชว์tooltipไม่ได้
    isKnownState : PropsType.bool.isRequired,
    
    onTrackingNumberChange : PropsType.func.isRequired,
    onClickDelivery : PropsType.func.isRequired
}