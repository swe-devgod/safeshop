import React, { Component } from 'react';
import PropsType from 'prop-types';
import { withStyles, Typography } from '@material-ui/core';
import ProgressView from './../../../../components/progress-view'

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

import { ProductRating } from './../../../showcase/components/product-rating/';
import Rating from '../../../../components/rating/'

const styles = theme => ({

    paper: {
        paddingBottom : '10px'
    },

    img: {
        marginTop: '35px',
        marginLeft: '20px',
        marginBottom: '35px',
        display: 'block',
        width: '70%',
        height: '60%',
      },

    button:{
        height: '56px',
        marginLeft: '10px'
    },

});

class SuccessListView extends Component{
    constructor(props){
        super(props);
        this.state = {
            arrowRef: null,
        };
    }

    handleArrowRef = node => {
        this.setState({
          arrowRef: node,
        });
    };

    render(){
        const { classes } = this.props;

        if(!!!this.props.isKnownState){
            return <ProgressView/>
        }

        return(
            <React.Fragment>
                {
                    //success dialog
                    this.props.isVoteDialogOpen &&
                    <Rating
                        setDialogVote={this.props.setDialogVote}
                        isVoteBuyer={true}
                        buyerName={this.props.wantToVoteBuyerName}
                        itemIndex={this.props.wantToVoteBuyerIndex}
                    />
                }
                <Grid container spacing={16} style={{width: '100%', margin: '0 0'}}>
                    {
                        this.props.itemList &&
                        this.props.itemList.map((item, index) => (
                            <Grid item xs={12} md={6}>
                                <Paper className={classes.paper}>
                                    <Grid container alignItems={"center"}>
                                        <Grid item xs={3}>
                                            <img className={classes.img} src={item.topic.thumbnail}/>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Typography variant="h6" className={classes.topicName}>{item.topic.topicName}</Typography>
                                            <ProductRating topicRating={item.topic.topicRating}
                                                            topicRatingCount={item.topic.topicVoteCount}/>
                                            <Typography>ราคา {item.pricePerItem} บาท</Typography>
                                            <Typography>จำนวน {item.amount} ชิ้น</Typography>
                                            <Typography>จัดส่งให้คุณ: {item.user.firstName} {item.user.middleName} {item.user.lastName} ({item.user.displayName})</Typography>
                                            <Typography>วิธีการขนส่ง: {item.shipping.name}</Typography>
                                            <Typography>ที่อยู่สำหรับการจัดส่ง: {item.address.address}</Typography>
                                            <Typography>หมายเลขพัสดุ: {item.shipping.shippingNo}</Typography>
                                        </Grid>
                                        <Grid item xs={3}>
                                            <Button
                                                variant={"outlined"}
                                                onClick={() => this.props.onClickRatingCustomer(index)}>
                                                ให้คะแนนผู้ซื้อ
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))
                    }
                </Grid>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(SuccessListView);

SuccessListView.propTypes = {
    itemList : PropsType.array.isRequired,
    isKnownState : PropsType.bool.isRequired,
    isVoteDialogOpen : PropsType.bool.isRequired,
    wantToVoteBuyerName : PropsType.string.isRequired,
    wantToVoteBuyerIndex : PropsType.string.isRequired,
    
    setDialogVote : PropsType.func.isRequired,
    onClickRatingCustomer : PropsType.func.isRequired,
}