import React, { Component } from 'react';
import PropsType from 'prop-types';
import { withStyles, TextField, Typography } from '@material-ui/core';
import ProgressView from './../../../../components/progress-view'

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { ProductRating } from './../../../showcase/components/product-rating/';
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { DialogActions } from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({

    paper: {
        paddingBottom : '10px'
    },

    img: {
        marginTop: '35px',
        marginLeft: '20px',
        marginBottom: '35px',
        display: 'block',
        width: '70%',
        height: '60%',
      },

    list: {
        padding: '0px'
    },
    
    button:{
        marginLeft: '10px'
    },
});

class OnDeliveryListView extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const { classes } = this.props;

        if(!!!this.props.isKnownState){
            return <ProgressView/>
        }

        return(
            <React.Fragment>
                <Dialog
                    fullWidth={true}
                    maxWidth = {'md'}
                    open={this.props.isEditDialogOpen}
                >
                    <DialogTitle>
                        <p>แก้ไขหมายเลขพัสดุ</p>
                    </DialogTitle>
                    <DialogContent>
                        {
                            this.props.itemList && 
                            <div>
                                <Tooltip
                                    PopperProps={{
                                        disablePortal: true,
                                    }}
                                    open={this.props.isTooptipOpen}
                                    disableFocusListener
                                    disableHoverListener
                                    disableTouchListener
                                    title={
                                        <React.Fragment>
                                            <span className={classes.tooltipResize}>{this.props.errorMessage}</span>
                                            <span className={classes.arrowArrow} ref={this.handleArrowRef} />
                                        </React.Fragment>
                                    }
                                    classes={{ popper: classes.arrowPopper }}
                                >
                                    <TextField required variant="outlined"
                                        value={this.props.editTrackingNumberTextField}                                  
                                        onChange={e => this.props.onEditTrackingNumberChange(e.target.value)}
                                    />
                                </Tooltip>
                            </div>
                        }
                    </DialogContent>
                    <DialogActions>
                        <Grid container spacing={16}>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    onClick={this.props.onDialogSubmit}>ยืนยัน
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    color="primary"
                                    onClick={this.props.onDialogCancel}>ยกเลิก
                                </Button>
                            </Grid>
                        </Grid>
                    </DialogActions>
                </Dialog>
                <Grid container spacing={16} style={{width: '100%', margin: '0 0'}}>
                    {
                        this.props.itemList &&
                        this.props.itemList.map((item, index) => (
                            <Grid item xs={12} md={6}>
                                <Paper className={classes.paper}>
                                    <Grid container alignItems={"center"}>
                                        <Grid item xs={3}>
                                            <img className={classes.img} src={item.topic.thumbnail}/>
                                        </Grid>
                                        <Grid item xs={9}>
                                            <Typography variant="h6" className={classes.topicName}>{item.topic.topicName}</Typography>
                                            <ProductRating topicRating={item.topic.topicRating}
                                                            topicRatingCount={item.topic.topicVoteCount}/>
                                            <Typography>ราคา {item.pricePerItem} บาท</Typography>
                                            <Typography>จำนวน {item.amount} ชิ้น</Typography>
                                            <Typography>จัดส่งให้คุณ: {item.user.firstName} {item.user.middleName} {item.user.lastName} ({item.user.displayName})</Typography>
                                            <Typography>วิธีการขนส่ง: {item.shipping.name}</Typography>
                                            <Typography>ที่อยู่สำหรับการจัดส่ง: {item.address.address}</Typography>
                                            <List dense className={classes.list}>
                                                <ListItem className={classes.list}>
                                                    <Typography>หมายเลขพัสดุ: {item.shipping.shippingNo}</Typography>
                                                    <Button
                                                        variant={"outlined"}
                                                        className={classes.button}
                                                        onClick={() => this.props.onClickEditTrackingNumber(index)}>
                                                        แก้ไข
                                                    </Button>
                                                </ListItem>
                                            </List>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))
                    }
                </Grid>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(OnDeliveryListView);

OnDeliveryListView.propTypes = {
    itemList : PropsType.array.isRequired,
    isEditDialogOpen : PropsType.bool.isRequired,
    isTooptipOpen : PropsType.bool.isRequired,
    errorMessage : PropsType.string.isRequired,
    editTrackingNumberTextField : PropsType.string.isRequired,
    wantToEditIndex : PropsType.number.isRequired,
    isKnownState : PropsType.bool.isRequired,

    onEditTrackingNumberChange : PropsType.func.isRequired,
    onClickEditTrackingNumber : PropsType.func.isRequired,
    onDialogSubmit : PropsType.func.isRequired,
    onDialogCancel : PropsType.func.isRequired,
}