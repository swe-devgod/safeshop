import React, { Component } from 'react';
import PropsType from 'prop-types';
import SellingListView from './selling-item-list-view'
import AwaitingListView from './awaiting-item-list-view'
import OnDeliveryListView from './on-delivery-item-list-view'
import SuccessListView from './success-item-list-view'

export default class TopicItemList extends Component {
    constructor(props) {
        super(props);
        this.state={
            itemList: this.props.itemList,

            wantToDeleteIndex: 0,
            wantToVoteIndex: 0,
            //selling
            isDeleteDialogOpen: false,
            wantToEditTopic: [],
            wantToDeleteTopic: [],
            //awaiting
            trackingNumber : [],
            errorMessage: '',
            isTooptipOpen: false,
            //on Delivery
            isEditDialogOpen: false,
            editTrackingNumberTextField: '',
            wantToEditIndex: 0,
            isRedirectToEditTopic: false,
            //success
            wantToVoteBuyerName: '',
            wantToVoteBuyerIndex: 0,

        };
        this.handleRemoveItemFromList = this.handleRemoveItemFromList.bind(this);
        //Selling
        this.handleClickEditTopic = this.handleClickEditTopic.bind(this);
        this.handleDialogClickRemove = this.handleDialogClickRemove.bind(this);
        this.handleDialogRemoveClickSubmit = this.handleDialogRemoveClickSubmit.bind(this);
        this.handleDialogRemoveClickCancel = this.handleDialogRemoveClickCancel.bind(this);
        this.handleRemoveItemFromSellingList=this.handleRemoveItemFromSellingList.bind(this);
        //Awaiting
        this.handleRemoveItemFromAwaitingList = this.handleRemoveItemFromAwaitingList.bind(this);
        this.handleTrackingTextFieldChange = this.handleTrackingTextFieldChange.bind(this);
        this.handleClickDelivery = this.handleClickDelivery.bind(this);
        //On Delivery
        this.handleEditTrackingNumberChange = this.handleEditTrackingNumberChange.bind(this);
        this.handleButtonClickEditTrackingNumber = this.handleButtonClickEditTrackingNumber.bind(this);
        this.handleDialogEditClickSubmit = this.handleDialogEditClickSubmit.bind(this);
        this.handleDialogEditClickCancel = this.handleDialogEditClickCancel.bind(this);
    }

    componentWillReceiveProps(){ //ทุกครั้งที่propsเปลี่ยน จะset itemList ตามpropsที่ได้รับ
        this.setState({
            itemList: this.props.itemList
        })
    }

    handleRemoveItemFromList(){
        const newItemList = this.state.itemList.slice();
        newItemList.splice(this.state.wantToDeleteIndex,1);
        this.setState({
            itemList : newItemList
        })
        return newItemList;
    }

    //Selling
    handleClickEditTopic(index){
        const newItemList = this.props.itemList.slice();
        this.setState({
            wantToEditTopic: newItemList[index],
            isRedirectToEditTopic: true
        });
        console.log('wantToEditTopic', newItemList[index]);
    }

    handleDialogClickRemove(index){
        this.setState({
            isDeleteDialogOpen : true,
            wantToDeleteIndex : index,
            wantToDeleteTopic: this.state.itemList[index]
        });
    }

    handleDialogRemoveClickCancel(){
        this.setState({
            isDeleteDialogOpen : false
        });
    }

    handleDialogRemoveClickSubmit(){
        this.handleRemoveItemFromSellingList();
        this.props.refreshCurrentItemList();
        this.setState({
            isDeleteDialogOpen : false
        });
    }

    handleRemoveItemFromSellingList(){
        this.props.setSellingList(this.handleRemoveItemFromList());
    }

    //Await Delivery
    handleTrackingTextFieldChange(value, index){
        const newErrorMessage = ''
        this.setState({
            errorMessage : newErrorMessage,
            isTooptipOpen : false
        });

        const newTrackingNumber = this.state.trackingNumber.slice();
        newTrackingNumber[index] = value;
        this.setState({
            trackingNumber : newTrackingNumber
        });
    }

    handleRemoveTrackingNumberTextField(index){
        const newTrackingNumber = this.state.trackingNumber.slice();
        newTrackingNumber[index] = '';
        this.setState(() => {
            return {  trackingNumber : newTrackingNumber }
        }, () => {
            newTrackingNumber.splice(index, 1);
            this.setState({
                trackingNumber : newTrackingNumber
            });
        });
    }

    handleRemoveItemFromAwaitingList(){

        this.props.setAwaitingList(this.handleRemoveItemFromList());
    }

    handleClickDelivery(index){
        if(this.state.trackingNumber[index] === '' || this.state.trackingNumber[index] == undefined){//ถ้าไม่ใส่เลขอะไรมาเลยให้show error
            const newErrorMessage = 'กรุณาระบุหมายเลขพัสดุ'
            this.setState({
                errorMessage : newErrorMessage,
                isTooptipOpen : true
            });
        }else{
            this.setState(() => {
                return { 
                    wantToDeleteIndex : index,
                }
            }, () => {
                const apiSendTrackingNumber = `/api/checkout/items/${this.state.itemList[index].id}`;
                fetch(apiSendTrackingNumber, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({shippingNo : this.state.trackingNumber[index]})
                }).catch(err => console.error(err));
                this.handleRemoveTrackingNumberTextField(index);
                this.props.refreshCurrentItemList();
            });
        }
    }

    //On Delivery
    handleEditTrackingNumberChange(value){
        const newErrorMessage = ''
        this.setState({
            errorMessage : newErrorMessage,
            isTooptipOpen : false
        });

        let newEditTrackingNumberTextField = this.state.editTrackingNumberTextField.slice();
        newEditTrackingNumberTextField = value;
        this.setState({
            editTrackingNumberTextField : newEditTrackingNumberTextField
        });

        
    }

    handleButtonClickEditTrackingNumber(index){
        const newTrackingNumber = this.state.itemList.slice();
        const currentTrackingNumber = newTrackingNumber[index].shipping.shippingNo; //อันที่addมาจากawaiting เป็นundefine??
        this.setState({
            wantToEditIndex: index,
            editTrackingNumberTextField: currentTrackingNumber,
            isEditDialogOpen: true,
        }); 
    }

    handleDialogEditClickSubmit(){
        if(this.state.editTrackingNumberTextField === '' || this.state.editTrackingNumberTextField == undefined){//ถ้าไม่ใส่เลขอะไรมาเลยให้show error
            const newErrorMessage = 'กรุณาระบุหมายเลขพัสดุ'
            this.setState({
                errorMessage : newErrorMessage,
                isTooptipOpen : true
            });
        }else{
            const apiSendTrackingNumber = `/api/checkout/items/${this.state.itemList[this.state.wantToEditIndex].id}`;
            fetch(apiSendTrackingNumber, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'content-type': 'application/json'
                },
                body: JSON.stringify({shippingNo : this.state.editTrackingNumberTextField})
            }).catch(err => console.error(err));
            this.setState({
                isEditDialogOpen : false,
                isTooptipOpen : false,
                editTrackingNumberTextField: ''
            });
            this.props.refreshCurrentItemList();
        }
    }

    handleDialogEditClickCancel(){
        const newErrorMessage = ''
        this.setState({
            isEditDialogOpen : false,
            isTooptipOpen : false,
            errorMessage : newErrorMessage,
            editTrackingNumberTextField: ''
        });
    }

    //Success
    handleClickRatingCustomer = (index) =>{
        console.log('Click give customer a rating');
        console.log(this.props.itemList)
        this.setState({
            isVoteDialogOpen : true,
            wantToVoteIndex : index,
            wantToVoteBuyerName: this.props.itemList[index].user.firstName,
            wantToVoteBuyerIndex: this.props.itemList[index].id
        })
    }
    setDialogVote = (ch) =>{
        this.setState({isVoteDialogOpen:ch});
    }
    render() {
        if(this.props.isSelling){
            return (
                <SellingListView
                    itemList={this.props.itemList}
                    isKnownState={this.props.isKnownState}
                    isRedirectToEditTopic={this.state.isRedirectToEditTopic}
                    isDeleteDialogOpen={this.state.isDeleteDialogOpen}
                    wantToDeleteIndex={this.state.wantToDeleteIndex}
                    wantToDeleteTopic={this.state.wantToDeleteTopic}
                    wantToEditTopic={this.state.wantToEditTopic}

                    onClickEditSelling={this.handleClickEditTopic}
                    onClickRemove={this.handleDialogClickRemove}
                    onDialogSubmit={this.handleDialogRemoveClickSubmit}
                    onDialogCancel={this.handleDialogRemoveClickCancel}
                />
            );
        }
        if(this.props.isAwaitingDelivery){
            return(
                <AwaitingListView
                    itemList={this.props.itemList}
                    isKnownState={this.props.isKnownState}
                    trackingNumberList={this.state.trackingNumber}
                    errorMessage={this.state.errorMessage}
                    isTooptipOpen={this.state.isTooptipOpen}

                    onTrackingNumberChange={this.handleTrackingTextFieldChange}
                    onClickDelivery={this.handleClickDelivery}
                />
            );
        }
        if(this.props.isOnDelivery){
            return (
                <OnDeliveryListView
                    itemList={this.props.itemList}
                    isKnownState={this.props.isKnownState}
                    isEditDialogOpen={this.state.isEditDialogOpen}
                    isTooptipOpen={this.state.isTooptipOpen}
                    errorMessage={this.state.errorMessage}
                    editTrackingNumberTextField={this.state.editTrackingNumberTextField}
                    wantToEditIndex={this.state.wantToEditIndex}

                    onEditTrackingNumberChange={this.handleEditTrackingNumberChange}
                    onClickEditTrackingNumber={this.handleButtonClickEditTrackingNumber}
                    onDialogSubmit={this.handleDialogEditClickSubmit}
                    onDialogCancel={this.handleDialogEditClickCancel}
                />
            );
        }
        if(this.props.isSuccess){
            return (
                <SuccessListView
                    itemList={this.props.itemList}
                    isKnownState={this.props.isKnownState}
                    isVoteDialogOpen={this.state.isVoteDialogOpen}
                    wantToVoteBuyerName={this.state.wantToVoteBuyerName}
                    wantToVoteBuyerIndex={this.state.wantToVoteBuyerIndex}
                    onClickRatingCustomer={this.handleClickRatingCustomer}
                    setDialogVote={ch => this.setDialogVote(ch)}

                />
            );
        }
    }
}

TopicItemList.propTypes = {
    itemList : PropsType.array.isRequired,
    isSelling : PropsType.bool,
    isAwaitingDelivery : PropsType.bool,
    isOnDelivery : PropsType.bool,
    isSuccess : PropsType.bool,
    isKnownState : PropsType.bool.isRequired,

    onDeliveryList : PropsType.array, //สำหรับส่งให้awaitไปก้orderนั้นเป็นอยู่ระหว่างขนส่ง
    
    refreshCurrentItemList : PropsType.func
}