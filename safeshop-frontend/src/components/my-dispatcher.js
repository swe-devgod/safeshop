import React, { Component } from 'react';

import { createMemoryHistory } from 'history';
import { Route, Router } from 'react-router-dom';

let browserHistory = null;

export function setBrowserHistory(bh) {
    browserHistory = bh;
}

export function WithBrowserHistory(props) {
    return (
        <Router history={browserHistory}>
            {props.children}
        </Router>
    );
}

export class DisplayDispatcher {
    constructor() {
        this.history = {};
        this.props = {};
    }

    addEndpoint(endpoint) {
        if (this.history[endpoint] == undefined) {
            this.history[endpoint] = createMemoryHistory({ 
                initialEntries: ['/'],
                initialIndex: 0
            });
        } else {
            //console.error('[DisplayDispatcher] duplicate endpoint.');
        }
    }
    
    removeEndpoint(endpoint) {
        delete this.history[endpoint];
        delete this.props[endpoint];
    }

    getHistory(endpoint) {
        return this.history[endpoint];
    }

    removeProps(endpoint) {
        delete this.props[endpoint];
    }

    getProps(endpoint) {
        return this.props[endpoint];
    }

    dispatchStatic(endpoint, props) {
        this.props[endpoint] = { ...props };
        this.history[endpoint].replace('/', props);
    }

    dispatch(endpoint, component, props) {
        this.props[endpoint] = { ...props };
        this.history[endpoint].replace('/', { view: component, props });
    }
}

export function Displayer(displayerProps) {
    const { dispatcher, endpoint } = displayerProps;
    dispatcher.addEndpoint(endpoint);
    return (
        <Router history={dispatcher.getHistory(endpoint)}>
            <Route exact path='/' render={renderProps => {
                if (renderProps.location.state != undefined) {
                    const { view, props } = renderProps.location.state;
                    const View = view;
                    return <View {...props} />
                } else {
                    return <React.Fragment />;
                }
            }} />
        </Router>
    );
}

export function StaticDisplayer(displayerProps) {
    const { dispatcher, endpoint, initProps, render } = displayerProps;
    dispatcher.addEndpoint(endpoint);
    if (!!!dispatcher.getProps(endpoint) && initProps) {
        dispatcher.dispatchStatic(endpoint, initProps);
    }
    return (
        <Router history={dispatcher.getHistory(endpoint)}>
            <Route exact path='/' render={renderProps => {
                return (
                    <React.Fragment>
                        { render(renderProps.location.state || {}) }
                    </React.Fragment>
                );
            }} />
        </Router>
    );
}