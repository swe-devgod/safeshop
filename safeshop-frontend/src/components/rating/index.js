import React, { Component } from 'react';
import RatingStar from './rating-view'
import PropTypes from 'prop-types';

export default class Rating extends Component {
    constructor(props) {
        super(props);
        this.state ={
            ratingBuyer:{
                rating: 1,
                itemId: 1,
            },
            ratingItem:{
                rating: 1,
                itemId: 1,
            },
            ratingSeller:{
                rating: 1,
                itemId: 1,
            },
            isVoteDialogOpen: true,
            wantToVoteSellerName:'',
            wantToVoteItemName:'',
            wantToVoteBuyerName:'',
            ratingBuyerDone: false,
            ratingSellerDone: false,
            ratingItemDone: false,
        };
        this.handleDialogClickCancelVote = this.handleDialogClickCancelVote.bind(this);
        this.handleDialogClickSubmitVote = this.handleDialogClickSubmitVote.bind(this);
        this.onStarClickItem = this.onStarClickItem.bind(this);
        this.onStarClickSeller = this.onStarClickSeller.bind(this);
        this.onStarClickBuyer = this.onStarClickBuyer.bind(this);
    }

    componentDidMount() {
        this.setState({
            ratingBuyer:{
                rating: 1,
                itemId: this.props.itemIndex,
            },
            ratingItem:{
                rating: 1,
                itemId: this.props.itemIndex,
            },
            ratingSeller:{
                rating: 1,
                itemId: this.props.itemIndex
            }
        });
    }
    clearRate = () =>{
        this.setState({
            ratingBuyer:{
                rating: 1,
                itemId: this.props.itemIndex,
            },
            ratingItem:{
                rating: 1,
                itemId: this.props.itemIndex,
            },
            ratingSeller:{
                rating: 1,
                itemId: this.props.itemIndex
            },
            isVoteDialogOpen : false,
        });
        this.props.setDialogVote(false)
    }
    onStarClickItem(newRate) {
        this.setState({
            ratingItem:{
                rating: newRate,
                itemId: this.props.itemIndex
            }
        });
    }
    onStarClickSeller(newRate) {
        this.setState({
            ratingSeller:{
                rating: newRate,
                itemId: this.props.itemIndex
            }
        });
    }
    onStarClickBuyer(newRate) {
        this.setState({
            ratingBuyer:{
                rating: newRate,
                itemId: this.props.itemIndex
            }
        });
    }

    handleDialogClickCancelVote(){
        this.clearRate();
    }

    handleDialogClickSubmitVote(){
        this.handleVoteItemFromBuyingList();
        this.clearRate();
    }
    handleVoteItemFromBuyingList(){
        /*
        console.log("===== from page to db ======");
        console.log(this.state.ratingItem);
        console.log(this.state.ratingSeller);
        console.log(this.state.ratingBuyer)
        */
        if(this.props.isVoteItem){
            fetch('/api/rating/topic/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state.ratingItem)
            }).then(res => {
                if (res.status === 200) {
                    res.json().then(json => {
                        console.log(json)
                    });
                    this.setState({ratingItemDone: true});
                } 
                else {
                    //console.log(res)
                }
            });
        }
        if(this.props.isVoteSeller){
            fetch('/api/rating/seller/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state.ratingSeller)
            }).then(res => {
                if (res.status === 200) {
                    res.json().then(json => {
                        console.log(json)
                    });
                    this.setState({ratingSellerDone: true});

                } 
                else {
                    //console.log(res)
                }
            });
        }
        if(this.props.isVoteBuyer){
            fetch('/api/rating/buyer/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state.ratingBuyer)
            }).then(res => {
                if (res.status === 200) {
                    res.json().then(json => {
                        console.log(json)
                    });
                    this.setState({ratingBuyerDone: true});
                } 
                else {
                    //console.log(res)
                }
            });
        }
    }

    render() {
        return <RatingStar
                    isVoteItem={this.props.isVoteItem}
                    isVoteSeller={this.props.isVoteSeller}
                    isVoteBuyer={this.props.isVoteBuyer}

                    isVoteDialogOpen={this.state.isVoteDialogOpen}
                    wantToVoteItemName={this.props.itemName}
                    wantToVoteBuyerName={this.props.buyerName}
                
                    onClickVote={this.handleDialogClickVote}

                    onVoteSubmit={this.handleDialogClickSubmitVote}
                    onVoteCancel={this.handleDialogClickCancelVote}

                    onStarClickItem={this.onStarClickItem}
                    onStarClickSeller={this.onStarClickSeller}
                    onStarClickBuyer={this.onStarClickBuyer}
                    
                    ratingSeller={this.state.ratingSeller.rating}
                    ratingItem={this.state.ratingItem.rating}
                    ratingBuyer={this.state.ratingBuyer.rating}

                />
                
    }
}
Rating.propTypes = {
     isVoteItem: PropTypes.bool,
     isVoteSeller: PropTypes.bool,
     isVoteBuyer: PropTypes.bool,
     buyerName: PropTypes.string,
     itemName: PropTypes.string,
     itemIndex: PropTypes.number.isRequired,
     setDialogVote: PropTypes.func.isRequired,
};