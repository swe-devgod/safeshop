import React, { Component } from 'react';
import { AuthService } from '../../components/auth-service';
import { withStyles, Typography } from '@material-ui/core';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import StarRatingComponent from 'react-star-rating-component';
import PropTypes from 'prop-types';

export default class RatingView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <Dialog
                    fullWidth={true}
                    maxWidth = {'md'}
                    open={this.props.isVoteDialogOpen}
                >
                    <DialogTitle>
                        <p>ให้คะแนน</p>
                    </DialogTitle>
                    <DialogContent>
                        {
                            <Grid>
                                {
                                    (this.props.isVoteItem) && 
                                    <Grid item>
                                        <Typography variant="h6">ให้คะแนนกระทู้สินค้า: {this.props.wantToVoteItemName}</Typography>
                                        <StarRatingComponent 
                                            name="rate1" 
                                            starCount={5}
                                            value={this.props.ratingItem}
                                            onStarClick={this.props.onStarClickItem}
                                        />
                                    </Grid>

                                }
                                {
                                    (this.props.isVoteSeller) && 
                                    <Grid item>
                                        <Typography variant="h6">ให้คะแนนผู้ขาย</Typography>
                                        <StarRatingComponent 
                                            name="rate1" 
                                            starCount={5}
                                            value={this.props.ratingSeller}
                                            onStarClick={this.props.onStarClickSeller}
                                        />
                                    </Grid>
                                }
                                {
                                    (this.props.isVoteBuyer) && 
                                    <Grid item>
                                        <Typography variant="h6">ให้คะแนนผู้ซื้อ: {this.props.wantToVoteBuyerName}</Typography>
                                        <StarRatingComponent 
                                            name="rate1" 
                                            starCount={5}
                                            value={this.props.ratingBuyer}
                                            onStarClick={this.props.onStarClickBuyer}
                                        />
                                    </Grid>
                                }
                                
                            </Grid>
                            
                        }
                    </DialogContent>
                    <DialogActions>
                        <Grid container xs={12} spacing={16}>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    onClick={this.props.onVoteSubmit}>ยืนยัน
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained' size='small'
                                    color="primary"
                                    onClick={this.props.onVoteCancel}>ยกเลิก
                                </Button>
                            </Grid>
                        </Grid>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        )    
    }
}
RatingView.propTypes = {
    isVoteItem: PropTypes.bool,
    isVoteSeller: PropTypes.bool,
    isVoteBuyer: PropTypes.bool,

    isVoteDialogOpen: PropTypes.bool,
    wantToVoteItemName: PropTypes.string,
    wantToVoteSellerName: PropTypes.string,
    wantToVoteBuyerName: PropTypes.string,

    onClickViewTopic: PropTypes.func.isRequired,
    onClickVote: PropTypes.func.isRequired,

    onVoteSubmit: PropTypes.func.isRequired,
    onVoteCancel: PropTypes.func.isRequired,

    onStarClickItem: PropTypes.func.isRequired,
    onStarClickSeller: PropTypes.func.isRequired,
    onStarClickBuyer: PropTypes.func.isRequired,
    
    ratingSeller: PropTypes.number.isRequired,
    ratingItem: PropTypes.number.isRequired,
    ratingBuyer: PropTypes.number.isRequired,
};