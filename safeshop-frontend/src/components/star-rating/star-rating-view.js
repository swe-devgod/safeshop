import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid'

export class StarRatingView extends Component {
    render() {
        return (
            <span className={this.props.className}>
                <Grid container spacing = {0} direction='row'>
                    {
                        this.props.starImgSrc.map((src, index) => (
                            <Grid key={index} item>
                                <img
                                    src={src}
                                    style={{marginTop: '3px', width: '15px', display: 'block'}}
                                    alt=''
                                />
                            </Grid>
                        ))
                    }
                    <Grid item>
                        <span className="color-grey" style={{width: '13px'}}>&nbsp;({this.props.ratingCount} ratings)</span>
                    </Grid>
                </Grid>
            </span>
        );
    }
}