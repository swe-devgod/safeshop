/**
 * Re-factoring by: Film
 * Created Date: 15/10/18
 * Why I'm not creating a whole new Component ?: I want to show consideration to him.
 */

import React, { Component } from 'react';

import imgStarFull from './res/img_star_full.png';
import imgStarQuarterFull from './res/img_star_quarter_full.png';
import imgStarHalf from './res/img_star_half.png';
import imgStarQuarterEmpty from './res/img_star_quarter_empty.png';
import imgStarEmpty from './res/img_star_empty.png';

import { StarRatingView } from './star-rating-view';

const STAR_SRC = [
    imgStarEmpty,
    imgStarQuarterEmpty,
    imgStarHalf,
    imgStarQuarterFull,
    imgStarFull
];

export class StarRating extends Component {
    constructor(props) {
        super(props);
    }

    calculateStarImageSrc() {
        const { ratings = 0 } = this.props;
        const newStarSrc = Array(5).fill(STAR_SRC[0]);
        for (let i = 0; i < 5; i++) {
            if (ratings < i + 0.06){
                newStarSrc[i] = STAR_SRC[0];
            } else if (ratings < i + 0.4){
                newStarSrc[i] = STAR_SRC[1];
            } else if (ratings < i + 0.7){
                newStarSrc[i] = STAR_SRC[2];
            } else if (ratings <= i + 0.99){
                newStarSrc[i] = STAR_SRC[3];
            } else {
                newStarSrc[i] = STAR_SRC[4];
            }
        }
        return newStarSrc;
    }

    render() {
        const starSrc = this.calculateStarImageSrc();
        const { ratingCount, noRatingMessage} = this.props;
        return (
            <StarRatingView
                className={this.props.className}
                ratingCount={ratingCount}
                starImgSrc={starSrc}                
                noRatingMessage={noRatingMessage} />
        );
    }
}