import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';

import NewFileUploadingView from '../new-file-uploading';

const styles = theme => ({

});

class DefaultSelectedFileUploading extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSelected: []
        };
    }
    
    handleClick(index) {
        const newIsSelected = this.props.items.map((selectable, i) => i === index);
        this.setState({
            isSelected: newIsSelected
        });
        this.props.onClick(index);
    }

    render() {
        return (
            <NewFileUploadingView
                selectable
                itemWidth={this.props.itemWidth}
                itemHeight={this.props.itemHeight}
                items={this.props.items}
                isSelected={this.state.isSelected}
                onClick={this.handleClick.bind(this)}
                onDelete={this.props.onDelete}
                onFileChanged={this.props.onFileChanged}
            />
        );
    }
}

DefaultSelectedFileUploading.propTypes = {
    items: PropTypes.array.isRequired,
    itemWidth: PropTypes.number.isRequired,
    itemHeight: PropTypes.number.isRequired,
    onClick: PropTypes.func,
    onDelete: PropTypes.func.isRequired,
    onFileChanged: PropTypes.func.isRequired,
};

export default withStyles(styles)(DefaultSelectedFileUploading);