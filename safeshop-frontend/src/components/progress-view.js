import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
    layout: {
        position: 'relative',
        height: '75vh',
        overflow: 'hidden',
    }
    ,container: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
        margin: '0 auto',
        display: 'table',
    },
    progress: {
        margin: theme.spacing.unit * 2,
    }
});

class ProgressView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <div className={classes.layout}>
                    <div className={classes.container}>
                        <CircularProgress className={classes.progress} color="secondary" size={100}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

ProgressView.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(ProgressView);