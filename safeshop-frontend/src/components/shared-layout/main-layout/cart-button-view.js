import React from 'react'

import { withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';

const styles = theme => ({
    badgeCart: {
        color: 'white',
    },
    cartIcon: {
        color: 'white',
    },
});

class CartIconButton extends React.Component {
    render() {
        const { classes, itemAmount } = this.props;

        return (
            <Button color="inherit">
                { itemAmount && itemAmount > 0 ? 
                    <Badge 
                        color="secondary"
                        badgeContent={itemAmount ? itemAmount : 0}
                        classes={{ badge: classes.badgeCart}}
                    >
                        <ShoppingCartIcon className={classes.cartIcon} />
                    </Badge>
                    :
                    <ShoppingCartOutlinedIcon  className={classes.cartIcon} />
                }
                &nbsp; Cart
            </Button>
        );
    }
}

export default withStyles(styles)(CartIconButton);