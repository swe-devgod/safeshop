import React, { Component } from 'react';
import { isAuthenticated } from '../../auth-service';
import { MainLayoutView } from './main-layout-view';

export class MainLayout extends Component {

    constructor(props) {
		super(props);
		this.state = {
			isOpenDrawer: false
		}
        this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
        this.handleResize = this.handleResize.bind(this);
	}

    componentDidMount() {
        window.addEventListener('resize', this.handleResize, false);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize, false);
    }

    handleDrawerOpen() {
        console.log('handleDrawerOpen', 'click');
		this.setState((state, props) => ({
				isOpenDrawer: !state.isOpenDrawer
			}
		));
	}

	handleResize(e) {
		if (e.target.innerWidth >= 960 && this.state.isOpenDrawer) {
			this.setState({ isOpenDrawer: false });
		}
    }

	render() {
		const isUserLoggedIn = isAuthenticated();
		return (
			<MainLayoutView 
				displayDispatcher={this.props.displayDispatcher}
				appBarHistory={this.props.appBarHistory}
				sideBarHistory={this.props.sideBarHistory}
				isUserLoggedIn={isUserLoggedIn} 
				isOpenDrawer={this.state.isOpenDrawer}
				handleDrawerOpen={this.handleDrawerOpen}>
				{this.props.children}
			</MainLayoutView>
		);
	}
}