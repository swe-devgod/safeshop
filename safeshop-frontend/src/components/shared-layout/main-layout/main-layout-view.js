import React, { Component } from "react";
import { Router, Link, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';

import classnames from 'classnames';

import { MainFooter } from '../main-footer/index';
import { MainAppBar } from './appbar-view';

const DRAWER_WIDTH = 240;

const styles = theme => ({
	root: {
		flexGrow: 1,
		zIndex: 1,
		overflow: 'hidden',
		position: 'relative',
		display: 'flex',
	},
	mainDrawer: {
		marginLeft: -DRAWER_WIDTH,
		position: 'absolute',
		height: '100%',
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		
		[theme.breakpoints.up('md')]: {
			position: 'relative',
			height: 'auto',
			marginLeft: 0
		}
	},
	showDrawer: {
		marginLeft: 0
	},
	drawerPaper: {
		position: 'relative',
		width: DRAWER_WIDTH,
	},
	content: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.default,
		//padding: `${theme.spacing.unit * 2}px 0 ${theme.spacing.unit * 2}px 0`,
		[theme.breakpoints.up('md')]: {
			//padding: theme.spacing.unit * 3,
		},
		minWidth: 0,
	},
	toolbar: { ...theme.mixins.toolbar, marginBottom: 12 },
});

class MainLayoutViewImpl extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		console.log('test: ', this.mainRef);
	}

	render() {
		const { classes, isUserLoggedIn, menuLists, handleDrawerOpen } = this.props;

		return (
            <React.Fragment>
			<div className={classes.root}>
				<MainAppBar
					displayDispatcher={this.props.displayDispatcher}
					appBarHistory={this.props.appBarHistory}
					sideBarHistory={this.props.sideBarHistory}
					isUserLoggedIn={isUserLoggedIn} 
					handleDrawerOpen={handleDrawerOpen} 
					menuLists={menuLists} />

				<Router history={this.props.sideBarHistory}>
					<Route exact path='/sidebar/main' render={props => (
						props.location.state != undefined && 
						<Drawer
							className={ classnames(classes.mainDrawer, {
								[classes.showDrawer]: this.props.isOpenDrawer
							})}
							variant="permanent"
							classes={{ paper: classes.drawerPaper }}>
								<div className={classes.toolbar} />
								<List onClick={handleDrawerOpen}>
									{ props.location.state }
								</List>
						</Drawer>
					)} />
				</Router>

                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    { this.props.children }
                </main>
            </div>
            <MainFooter />
            </React.Fragment>
        );
	}
}

MainLayoutViewImpl.propTypes = {
    classes: PropTypes.object.isRequired
};

export const MainLayoutView = withStyles(styles)(MainLayoutViewImpl);