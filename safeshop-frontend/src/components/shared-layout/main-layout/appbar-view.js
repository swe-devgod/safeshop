import React, { Component } from "react";
import { MemoryRouter, Router, Route, Link, Switch as RouterSwitch } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { IconButton, Switch } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import AccountCircle from '@material-ui/icons/AccountCircle';

import MessageIcon from '@material-ui/icons/Message'
import PeopleIcon from '@material-ui/icons/People';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ListAltIcon from '@material-ui/icons/ListAlt';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Displayer, StaticDisplayer } from "../../my-dispatcher";

import CartButtonView from './cart-button-view';

import { WithBrowserHistory } from '../../my-dispatcher';

const styles = theme => ({
	appBar: {
        zIndex: theme.zIndex.drawer + 1,
        //background: '#00acc1'
        background: theme.palette.primary
    },
    grow: {
		flexGrow: 1
    },
    menuButton: {
		[theme.breakpoints.up('md')]: {
            display: 'none'
		}
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
          display: 'flex',
          alignItems: 'center',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
    }
});

class MainAppBarImpl extends Component {

    constructor(props) {
        super(props);

        this.state = {
            achorElement: null,
            mobileAchorElement: null
        };
    }

    handleMobileMenuOpen = event => {
        this.setState({ mobileAchorElement: event.currentTarget });
    };
    
    handleMobileMenuClose = () => {
        this.setState({ mobileAchorElement: null });
    };

    render() {
        const { classes, isUserLoggedIn, menuLists, handleDrawerOpen } = this.props;
        const { achorElement, mobileAchorElement } = this.state;
        const isMobileMenuOpen = Boolean(mobileAchorElement);
        const menuForMobile = (
            <React.Fragment>
                <Menu
                    anchorEl={mobileAchorElement}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                    open={isMobileMenuOpen}
                    onClose={this.handleMobileMenuClose}
                >
                    {/* <Link to='/dev/upload'>
                        <MenuItem>
                            <p>Dev Upload</p>
                        </MenuItem>
                    </Link>
                    <Link to='/dev/comment'>
                        <MenuItem>Dev Comment</MenuItem>
                    </Link> */}
                    <Link to='/categories'>
                        <MenuItem onClick={this.handleMobileMenuClose}>
                            <ListAltIcon />
                            Categories
                        </MenuItem>
                    </Link>
                    { !!!isUserLoggedIn &&
                        <React.Fragment>
                            <Link to='/login' onClick={this.handleMobileMenuClose}>
                                <MenuItem color="inherit">Login</MenuItem>
                            </Link>
                            <Link to='/register' onClick={this.handleMobileMenuClose}>
                                <MenuItem color="inherit">Register</MenuItem>
                            </Link>
                        </React.Fragment>
                    }
                    { isUserLoggedIn &&
                        <React.Fragment>
                            <Link to='/messenger'>
                                <MenuItem onClick={this.handleMobileMenuClose}>
                                    <MessageIcon />
                                    Messenger
                                </MenuItem>
                            </Link>
                            <Link to='/add-topic'>
                                <MenuItem onClick={this.handleMobileMenuClose}>
                                    <AddBoxIcon />
                                    New Topic
                                </MenuItem>
                            </Link>
                            <Link to='/profile' onClick={this.handleMobileMenuClose}>
                                <MenuItem color="inherit">Profile</MenuItem>
                            </Link>
                            <Link to='/logout' onClick={this.handleMobileMenuClose}>
                                <MenuItem color="inherit">Logout</MenuItem>
                            </Link>
                        </React.Fragment>
                    }

                    {/* <Link to='/topic/1/'>
                        <MenuItem>Working Topic</MenuItem>
                    </Link> */}
                    {/* <MenuItem>
                        <IconButton color="inherit">
                            <Badge className={classes.margin} badgeContent={11} color="secondary">
                            <NotificationsIcon />
                            </Badge>
                        </IconButton>
                        <p>Notifications</p>
                    </MenuItem>
                    <MenuItem onClick={this.handleProfileMenuOpen}>
                        <IconButton color="inherit">
                            <AccountCircle />
                        </IconButton>
                        <p>Profile</p>
                    </MenuItem> */}
                </Menu>
            </React.Fragment>
        );
        
        return (
            <React.Fragment>
                <AppBar position="absolute" className={classes.appBar}>
                    <Toolbar>
                        <Router history={this.props.sideBarHistory}>
                            <Route exact path='/sidebar/main' render={props => (
                                <React.Fragment>
                                    {
                                        props.location.state != undefined && 
                                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={handleDrawerOpen} >
                                            <MenuIcon className={classes.menuButton}/>
                                        </IconButton>
                                    }
                                </React.Fragment>)} />
                        </Router>

                        {/* {   
                            menuLists != undefined &&
                            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={handleDrawerOpen} >
                                <MenuIcon className={classes.menuButton}/>
                            </IconButton>
                        } */}
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            <Link to='/'>SafeShop</Link>
                        </Typography>
                        { /*<div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <Input
                                placeholder="Search…"
                                disableUnderline
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}
                            />
                            </div>*/ }

                        <React.Fragment>
                            <StaticDisplayer endpoint='cart-button-appbar' dispatcher={this.props.displayDispatcher}
                                initProps={{ itemAmount: 0 }}
                                render={
                                    props => (
                                        <WithBrowserHistory>
                                            <React.Fragment>
                                                {
                                                    isUserLoggedIn &&
                                                    <Link to='/cart'>
                                                        <CartButtonView itemAmount={props.itemAmount} />
                                                    </Link>
                                                }
                                            </React.Fragment>
                                        </WithBrowserHistory>
                                    )
                                }
                            />
                        </React.Fragment>
                        
                        <div className={classes.sectionMobile}>
                            <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                            <MoreIcon />
                            </IconButton>
                        </div>
                        {menuForMobile}
                        
                        <div className={classes.sectionDesktop}>
                            {/* <Displayer endpoint='hello' dispatcher={this.props.displayDispatcher} />
                            <Displayer endpoint='world' dispatcher={this.props.displayDispatcher} /> */}

                            <Router history={this.props.appBarHistory}>
                                <React.Fragment>
                                    <Route path='/appbar' 
                                        render={props => {
                                            console.log('match ' + '/appbar');
                                            const View = props.location.state.view;
                                            return <View {...props.location.state.props } />
                                        }} />
                                    <Route exact path='/appbar/button' 
                                        render={props => {
                                            console.log('match ' + '/appbar/button');
                                            const View = props.location.state.view;
                                            return <View {...props.location.state.props } />
                                        }} />
                                </React.Fragment>
                            </Router>

                            {/* <Link to='/dev/upload'>
                                <Button color="inherit">Dev Upload</Button>
                            </Link>
                            <Link to='/dev/comment'>
                                <Button color="inherit">Dev Comment</Button>
                            </Link> */}
                            <Link to='/categories'>
                                <Button color="inherit">
                                    <ListAltIcon />
                                    &nbsp; Categories
                                </Button>
                            </Link>
                            {/* <Link to='/topic/1/'>
                                <Button color="inherit">Working Topic</Button>
                            </Link> */}
                            { !!!isUserLoggedIn &&
                                <React.Fragment>
                                    <Link to='/login'>
                                        <Button color="inherit">Login</Button>
                                    </Link>
                                    <Link to='/register'>
                                        <Button color="inherit">Register</Button>
                                    </Link>
                                </React.Fragment>
                            }
                            { isUserLoggedIn &&
                                <React.Fragment>
                                    {/* <Link to='/buyer-management'>
                                        <Button color="inherit">
                                            working buyer management
                                        </Button>
                                    </Link>
                                    <Link to='/seller-management'>
                                        <Button color="inherit">
                                            working seller management
                                        </Button>
                                    </Link> */}
                                    <Link to='/messenger'>
                                        <Button color="inherit">
                                            <PeopleIcon />
                                            &nbsp; Messenger
                                        </Button>
                                    </Link>
                                    <Link to='/add-topic'>
                                        <Button color="inherit">
                                            <AddBoxIcon />
                                            &nbsp; New Topic
                                        </Button>
                                    </Link>
                                    <Link to='/profile'>
                                        <Button color="inherit">Profile</Button>
                                    </Link>
                                    <Link to='/logout'>
                                        <Button color="inherit">Logout</Button>
                                    </Link>
                                </React.Fragment>
                            }
                        </div>
                    </Toolbar>
                </AppBar>
            </React.Fragment>
        );
    }
}

MainAppBarImpl.propTypes = {
    isUserLoggedIn: PropTypes.bool.isRequired,
    classes: PropTypes.object.isRequired
};

export const MainAppBar = withStyles(styles)(MainAppBarImpl);