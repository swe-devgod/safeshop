import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import { Divider, ListItem } from '@material-ui/core';
import IconPeople from '@material-ui/icons/People';
import IconEmail from '@material-ui/icons/Mail';

import LogoReact from './raw/image/logo_react.png'
import LogoMaterialUi from './raw/image/logo_material_ui.png'
import LogoSqlite from './raw/image/logo_sqlite.png'

import List from '@material-ui/core/List';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
    footer: {
		backgroundColor: theme.palette.background.paper,
	  	padding: theme.spacing.unit * 4,
	},
	divider: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 1}px 0`,
	},
	underline:{
		textDecoration: 'underline',
		opacity: '0.6',
		fontSize: '14px',
		'&:hover' : {
            opacity: '1'
        },
	}
});

export default class MainFooterImpl extends Component {
	render() {
		const { classes } = this.props;
		return (
			<footer className={classes.footer}>
				<Hidden smDown>
					<Grid container spacing={32} justify="center">
						<Grid item>
							<Typography variant="h6">
								About Us
							</Typography>
							<List dense>
								<ListItem>
									<ListItemIcon>
										<IconPeople/>
									</ListItemIcon>
									<ListItemText >
										<Typography variant="subtitle1" color="textSecondary">
											Developer: DevGod
										</Typography>
									</ListItemText>
								</ListItem>
								<ListItem align="center">
									<ListItemIcon>
										<IconEmail/>
									</ListItemIcon>
									<ListItemText>
										<Typography variant="subtitle1" color="textSecondary">
											Email: safeshop@devgod.com
										</Typography>
									</ListItemText>
								</ListItem>
							</List>
							<Grid container justify="center" spacing={24}>
								<Grid item>
									<span className={classes.underline}>
										นโยบายความเป็นส่วนตัว
									</span>
								</Grid>
								<Grid item>
									<span className={classes.underline}>
										ข้อตกลงในการใช้งาน
									</span>
								</Grid>
							</Grid>
						</Grid>
						<Grid item>
							<Typography variant="h6" align="center" gutterBottom>
								SafeShop 1.0
							</Typography>
							<Typography variant="subtitle1" align="center" color="textSecondary">
								SafeShop เป็นเว็บขายของมือสองที่ใช้ง่ายและปลอดภัย
							</Typography>
						</Grid>
						<Grid item>
							<img 
								src={LogoReact}
								width="170px"
								height="60px"
								align="center"
								alt="react-logo"
							/>
							<img 
								src={LogoMaterialUi}
								width="90px"
								height="80px"
								align="center"
								alt="materialUi-logo"
							/>
							<img 
								src={LogoSqlite}
								width="170px"
								height="60px"
								align="center"
								alt="sqlite-logo"
							/>
						</Grid>
					</Grid>
				</Hidden>
				<Hidden mdUp>
					<Grid container spacing={32} direction="column" alignItems="center">
						<Grid item>
							<Typography variant="h6" align="center" gutterBottom>
								SafeShop 1.0
							</Typography>
							<Typography variant="subtitle1" color="textSecondary" align="center">
								SafeShop เป็นเว็บขายของมือสองที่ใช้ง่ายและปลอดภัย
							</Typography>
						</Grid>
						<Grid item>
							<Typography variant="h6" align="center">
								About Us
							</Typography>
							<List dense>
								<ListItem>
									<ListItemIcon>
										<IconPeople/>
									</ListItemIcon>
									<ListItemText >
										<Typography variant="subtitle1" color="textSecondary">
											Developer: DevGod
										</Typography>
									</ListItemText>
								</ListItem>
								<ListItem align="center">
									<ListItemIcon>
										<IconEmail/>
									</ListItemIcon>
									<ListItemText>
										<Typography variant="subtitle1" color="textSecondary">
											safeshop@devgod.com
										</Typography>
									</ListItemText>
								</ListItem>
							</List>
							<Grid container justify="center" spacing={24}>
								<Grid item>
									<span className={classes.underline}>
										นโยบายความเป็นส่วนตัว
									</span>
								</Grid>
								<Grid item>
									<span className={classes.underline}>
										ข้อตกลงในการใช้งาน
									</span>
								</Grid>
							</Grid>
						</Grid> 
						<Grid item xs={12} align="center">
							<img
								src={LogoReact}
								width="180px"
								height="60px"
								align="center"
								alt="react-logo"
							/>
							<img 
								src={LogoMaterialUi}
								width="80px"
								height="75px"
								align="center"
								alt="materialUi-logo"
							/>
							<img 
								src={LogoSqlite}
								width="180px"
								height="60px"
								align="center"
								alt="sqlite-logo"
							/>
						</Grid>
					</Grid>
				</Hidden>
				<Divider className={classes.divider}/>
				<Typography style={{fontSize: '14px'}} variant="subtitle1" align="center" color="textSecondary" component="p">
					© 2018 DevGod All Rights Reserved
				</Typography>
			</footer>
		);
		/*return (
			<footer className={classes.footer}>
				<Hidden smDown>
					<Grid container alignItems="stretch">
						<Grid item xs={4}>
							<Typography variant="title" align="center" gutterBottom>
								About Us
							</Typography>
							<Grid container alignItems="center">
								<IconPeople/>
								<p>DevGod</p>
							</Grid>
							<Grid container alignItems="center">
								<IconEmail/>
								<p>safeshop@devgod.com</p>
							</Grid>
							<Grid container>
								<Grid item xs={6}>
									<span className={classes.underline}>
										นโยบายความเป็นส่วนตัว
									</span>
								</Grid>
								<Grid item xs={6}>
									<span className={classes.underline}>
										ข้อตกลงในการใช้งาน
									</span>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={4}>
							<Typography variant="title" align="center" gutterBottom>
								SafeShop 1.0
							</Typography>
							<Typography variant="subtitle1" align="center" color="textSecondary" component="p">
								SafeShop เป็นเว็บขายของมือสองที่ใช้ง่ายและปลอดภัย
							</Typography>
						</Grid>
						<Grid item xs={4} align="center">
							<img 
								src={LogoReact}
								width="210px"
								height="80px"
								align="center"
							/>
							<img 
								src={LogoMaterialUi}
								width="110px"
								height="90px"
								align="center"
							/>
							<img 
								src={LogoSqlite}
								width="210px"
								height="80px"
								align="center"
							/>
						</Grid>
					</Grid>
				</Hidden>
				<Hidden mdUp>
					<Grid container alignItems="center">
						<Grid item xs={12}>
							<Typography variant="title" align="center" gutterBottom>
								SafeShop 1.0
							</Typography>
							<Typography variant="subtitle1" align="center" color="textSecondary" component="p" gutterBottom>
								SafeShop เป็นเว็บขายของมือสองที่ใช้ง่ายและปลอดภัย
							</Typography>
						</Grid>
						<Grid item xs={12}>
							<Typography variant="title" align="center" gutterBottom>
								About Us
							</Typography>
							<Grid container alignItems="center">
								<Grid item xs={12} align="center">
									<IconPeople/>
									<span>Developed By : DevGod</span>
								</Grid>
							</Grid>
							<Grid container alignItems="center">
								<Grid item xs={12} align="center">
									<IconEmail/>
									<span>Email : safeshop@devgod.co.th</span>
								</Grid>
							</Grid>
							<Grid container spacing={2} alignItems="center">
								<Grid item xs={6} align="center">
									<span className={classes.underline}>
										นโยบายความเป็นส่วนตัว
									</span>
								</Grid>
								<Grid item xs={6} align="center">
									<span className={classes.underline}>
										ข้อตกลงในการใช้งาน
									</span>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={12} align="center">
							<img
								src={LogoReact}
								width="180px"
								height="60px"
								align="center"
							/>
							<img 
								src={LogoMaterialUi}
								width="80px"
								height="75px"
								align="center"
							/>
							<img 
								src={LogoSqlite}
								width="180px"
								height="60px"
								align="center"
							/>
						</Grid>
					</Grid>
				</Hidden>
				<Divider className={classes.divider}/>
				<Typography variant="subtitle1" align="center" color="textSecondary" component="p">
					© 2018 DevGod All Rights Reserved
				</Typography>
			</footer>
		);*/
	}
}

MainFooterImpl.propTypes = {
    classes: PropTypes.object.isRequired
};

export const MainFooter = withStyles(styles)(MainFooterImpl);