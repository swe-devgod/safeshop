import React, { Component } from 'react';
import { Route, Switch, Link } from 'react-router-dom';

import PropTypes from 'prop-types';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import { IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import { isAuthenticated } from '../../auth-service';

const styles = theme => ({
    root: {
        width: '100%',
        flexGrow: 1
    },
    grow: {
        flexGrow: 1
    },
    appBar: {
        background: 'linear-gradient(45deg, #0099ff 30%, #0099ff 90%)'
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit * 3,
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: 200,
        },
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    }
});

class MainNavigation extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { classes, } = this.props;
        const isUserLoggedIn = isAuthenticated();

        return (
            <div className={classes.root}>
                <AppBar position="static" className={classes.appBar}>
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                             <MenuIcon className={classes.menuIcon} />
                        </IconButton>
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            <Link to='/'>SafeShop</Link>
                        </Typography>
                        { /*<div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <Input
                                placeholder="Search…"
                                disableUnderline
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}
                            />
                            </div>*/ }
                        <Link to='/'>
                            <Button color="inherit">Working Index</Button>
                        </Link>
                        <Link to='/topic/1/'>
                            <Button color="inherit">Working Topic with ID:1</Button>
                        </Link>
                        { !!!isUserLoggedIn && 
                            <React.Fragment>
                                <Link to='/login'>
                                    <Button color="inherit">Login</Button>
                                </Link>
                                <Link to='/register'>
                                    <Button color="inherit">Register</Button>
                                </Link>
                            </React.Fragment>
                        }

                        { isUserLoggedIn && 
                            <React.Fragment>
                                <Link to='/profile'>
                                    <Button color="inherit">Profile</Button>
                                </Link>
                                <Link to='/logout'>
                                    <Button color="inherit">Logout</Button>
                                </Link>
                            </React.Fragment>
                        }
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

MainNavigation.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MainNavigation);