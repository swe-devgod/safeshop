import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, Grid } from '@material-ui/core';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import AddPhotoAltIcon from '@material-ui/icons/AddPhotoAlternate';

import NewFileUploadingItemView from './new-file-uploading-item-view';

const styles = theme => ({
    itemsRoot: {
        position: 'relative'
    },
    addFab: {
        position: 'absolute',
        right: 16,
        bottom: 4
    }
});

class NewFileUploadingView extends Component {
    constructor(props) {
        super(props);

        this.rootRef = React.createRef();

        this.handleResize = this.handleResize.bind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize, false);

        const styles = window.getComputedStyle(this.rootRef.current);
        const width = +styles.width.replace('px', '');
        this.props.onWidthChanged(width);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize, false);
    }

    handleResize(e) {
        const styles = window.getComputedStyle(this.rootRef.current);
        const width = +styles.width.replace('px', '');
        this.props.onWidthChanged(width);
    }

    getCurShowItem() {
        const index = this.props.curStep * (this.props.showItemCount);
        const end = index + this.props.showItemCount;
        return this.props.items.slice(index, Math.min(end, this.props.items.length));
    }

    render() {
        const { classes, theme, curStep, totalStep } = this.props;
        const actualIndex = this.props.curStep * (this.props.showItemCount);
        const showItems = this.getCurShowItem();
        return (
            <div ref={this.rootRef}>
                <div className={classes.itemsRoot}>
                    <Grid container justify='center' spacing={8}>
                        {
                            showItems.length <= 0 && (
                                <Grid item>
                                    <div style={{ width: this.props.itemWidth, height: this.props.itemHeight }} />
                                </Grid>
                            )
                        }
                        {
                            showItems.length > 0 && showItems.map((img, index) => (
                                <Grid key={index} item>
                                    <NewFileUploadingItemView 
                                        selectable={this.props.selectable}
                                        isSelect={this.props.isSelected != null && this.props.isSelected[actualIndex + index] != null ? this.props.isSelected[actualIndex + index] : false}
                                        itemWidth={this.props.itemWidth}
                                        itemHeight={this.props.itemHeight}
                                        onClick={e => this.props.onClick(index, e)}
                                        onDelete={e => this.props.onDelete(index, e)} 
                                        imageSrc={img} />
                                </Grid>
                            ))
                        }
                        {/* <Grid item>
                            <Button color='secondary' variant='contained' style={{ width: 150, height: 150 }}>
                                <AddPhotoAltIcon style={{ width: 64, height: 64 }} />
                            </Button>
                            <NewFileUploadingItemView imageSrc="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEXw8PABFicAAAD39/cACyD6+fpJUVgAABiLjpHS0tP8/PzOzs8AABUADyLs7e0AAAacnqAtOUMAABEcKjamp6qtsLMdJTBYX2V8gIQAABvk5eYAAA2zt7nV2NpASFEABhtpb3N0eX0AECAyPUYgLTliZ2zBw8UPGyohKDIYHyyRlJdUoWuXAAACX0lEQVR4nO3ceXOaQBiAcWAxIhUjRqN4NDaxOfz+H7BoeuzidooMe9nn9yfzkvjMKB4LRBEAAAAAAAAAAAAAAAAAAAAAAAAAAACAGybWX0yZCNdxZ6+JOSPXcSd5mcamVMuB67xa/lAYK0zvvCkc3vev8qlwPI8mfYuyyqPC2bz/g57IhhRaQmFXFNpDYVcU2nOLhSJXHOrCRd4LucdhoViVspf6E3JxKHuxl54K7grFKikUpy8BRT823/8UuSsc3Jn7Plg85r4UpuP+1X+2ePClMF1O+7evPCocHnPRt3w+86kwM/D+R6E1FHZFoT0UdkWhPdcVirz1ZJiF4li2Hg2yUCySNDm2nA2zcFXVn9LbPUoK7aFQRqEyS6E1FMooVGYptIZCGYXKLIXWUCj7LMz/PRgFUni5FvFZ2NysfdghFIrs41H1/hbH8dt7Y+vHXrt3AIXb5GJRN9YuECdTze4BFIrppuVS73AVZmE0ebrfNJwWxIvmxk2y0+wdQmE0WTRsl2lcfN02N69DfR1eHkz/cizV7xtEYdP/8o5P4c9ZCq2hUEahMkuhNRTKKFRmKbSGQhmFymyjcBBGYTaLq/01hbvfV6qPvrm6hvSqM4bWz4n2JwuNU2FcSFeqO7sO+Mrz2lrfNeBc2BBEYWu6wrH2l1XTjBYW6i0VynXv/6YFo2dfHtQ1Dje3xbB1fqk7FHZFoT0UduVXYZUZuJfJfOxRYVwZuGPSMPap0BBPCpeVscK09KEw2hl4hv7y6jruTExGpmgXhwEAAAAAAAAAAAAAAAAAAAAAAAAAAADcjB8HMnnfa0t+gwAAAABJRU5ErkJggg==" hideDelete />
                        </Grid> */}
                    </Grid>

                    <input
                        multiple
                        hidden
                        id="inp-file"
                        accept="image/*"
                        type="file"
                        onChange={e => { this.props.onFileChanged(e.target.files); e.target.value = ""; }} />

                    <label htmlFor="inp-file">
                        <Fab
                            component="span"
                            size="large" 
                            color="secondary" 
                            aria-label="Add" 
                            className={classes.addFab}>
                            <AddPhotoAltIcon />
                        </Fab>
                    </label>
                </div>

                <Paper style={{ marginTop: 16 }}>
                    <MobileStepper
                        steps={totalStep}
                        position="static"
                        activeStep={curStep}
                        className={classes.mobileStepper}
                        nextButton={
                            <Button size="small" onClick={this.props.onNext} disabled={curStep === totalStep - 1 || this.props.items.length <= 0}>
                            Next
                            {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                            </Button>
                        }
                        backButton={
                            <Button size="small" onClick={this.props.onBack} disabled={curStep === 0}>
                            {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                            Back
                            </Button>
                        }
                    />
                </Paper>
            </div>
        );
    }
}

NewFileUploadingView.defaultProps = {
    itemWidth: 150,
    itemHeight: 150
};

NewFileUploadingView.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    items: PropTypes.array.isRequired,
    curStep: PropTypes.number.isRequired,
    totalStep: PropTypes.number.isRequired,
    showItemCount: PropTypes.number.isRequired,
    onBack: PropTypes.func.isRequired,
    onNext: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    onWidthChanged: PropTypes.func.isRequired,
    onFileChanged: PropTypes.func.isRequired,
    itemWidth: PropTypes.number,
    itemHeight: PropTypes.number,
    isSelected: PropTypes.array,
    selectable: PropTypes.bool
};

export default withStyles(styles, { withTheme: true })(NewFileUploadingView);