import React, { Component } from 'react';
import PropTypes from 'prop-types';

import NewFileUploadingView from './new-file-uploading-view';
import { withStyles } from '@material-ui/core';

const styles = theme => ({

});

class NewFileUploading extends Component {
    constructor(props) {
        super(props);

        this.state = {
            totalStep: 1,
            curStep: 0,
            showItemCount: 1
        };
    }

    handleNext() {
        this.setState(prevState => ({
            curStep: prevState.curStep + 1,
        }));
    }

    handleBack() {
        this.setState(prevState => ({
            curStep: prevState.curStep - 1,
        }));
    }

    handleDelete(index, e) {
        this.props.onDelete(this.state.curStep * this.state.showItemCount + index);
    }

    handleClick(index, e) {
        this.props.onClick(this.state.curStep * this.state.showItemCount + index);
    }

    handleWidthChanged(width) {
        const contentWidth = width - (Math.trunc(width / 150) - 1) * 8;
        const curItemCount = Math.trunc(contentWidth / 150);
        const totalStep = Math.ceil(this.props.items.length / curItemCount);
        if (curItemCount !== this.state.showItemCount) {
            this.setState(prevState => ({
                showItemCount: curItemCount >= 1 ? curItemCount : 1,
                curStep: 0,
                totalStep: totalStep
            }));
        }
    }

    componentDidUpdate(prevProp, prevState) {
        if (prevProp.items.length !== this.props.items.length) {
            const totalStep = Math.ceil(this.props.items.length / prevState.showItemCount);
            this.setState({
                curStep: Math.max(0, Math.min(prevState.curStep, totalStep - 1)),
                totalStep: totalStep
            });
        }
    }

    render() {
        return <NewFileUploadingView
                    selectable={this.props.selectable}
                    isSelected={this.props.isSelected}
                    items={this.props.items}
                    curStep={this.state.curStep}
                    totalStep={this.state.totalStep}
                    showItemCount={this.state.showItemCount}
                    itemWidth={this.props.itemWidth}
                    itemHeight={this.props.itemHeight}
                    onBack={this.handleBack.bind(this)}
                    onNext={this.handleNext.bind(this)}
                    onDelete={this.handleDelete.bind(this)}
                    onClick={this.handleClick.bind(this)}
                    onWidthChanged={this.handleWidthChanged.bind(this)}
                    onFileChanged={this.props.onFileChanged}
                />
    }
}

NewFileUploading.defaultProps = {
    selectable: false
};

NewFileUploading.propTypes = {
    items: PropTypes.array.isRequired,
    itemWidth: PropTypes.number.isRequired,
    itemHeight: PropTypes.number.isRequired,
    selectable: PropTypes.bool,
    isSelected: PropTypes.array,
    onClick: PropTypes.func,
    onDelete: PropTypes.func.isRequired,
    onFileChanged: PropTypes.func.isRequired,
};

export default withStyles(styles)(NewFileUploading);