import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import classnames from 'classnames';

import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import DoneIcon from '@material-ui/icons/Done';
import ClearIcon from '@material-ui/icons/Clear';

const styles = theme => ({
    root: {
        display: 'block',
    },
    pointer: {
        cursor: 'pointer'
    },
    paper: {
        position: 'relative',
        width: '100%',
        height: '100%',
        backgroundSize: 'contain',
        background: 'no-repeat center center'
    },
    defaultFab: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)'
    },
    deleteFab: {
        position: 'absolute',
        top: 4,
        right: 4
    }
});

class NewFileUploadingItemView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            totalStep: 4,
            curStep: 0
        };
    }

    handleClick(e) {
        if (this.props.selectable) {
            this.props.onClick(e);
        }
    }

    handleDelete(e) {
        e.stopPropagation();
        this.props.onDelete();
    }
    
    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <div className={classes.root} style={{ width: this.props.itemWidth, height: this.props.itemHeight }}>
                    <Paper className={ classnames(classes.paper, { [classes.pointer]: this.props.selectable })} 
                        style={{ backgroundImage: `url(${this.props.imageSrc})`}}
                        onClick={this.handleClick.bind(this)}>
                        { !!!this.props.hideDelete &&
                            <Fab 
                                size="small" 
                                color="secondary" 
                                aria-label="Clear" 
                                className={classes.deleteFab}
                                onClick={this.handleDelete.bind(this)}>
                                <ClearIcon />
                            </Fab>
                        }
                        {
                            this.props.isSelect && <Fab
                                className={classes.defaultFab}
                                size="large" 
                                aria-label="Selected" 
                                color="primary">
                                <DoneIcon />
                            </Fab>
                        }
                    </Paper>
                </div>
            </React.Fragment>
        );
    }
}

NewFileUploadingItemView.defaultProps = {

}

NewFileUploadingItemView.propTypes = {
    classes: PropTypes.object.isRequired,
    imageSrc: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    onDelete: PropTypes.func.isRequired,
    itemWidth: PropTypes.number,
    itemHeight: PropTypes.number
};

export default withStyles(styles)(NewFileUploadingItemView);