import React, { Component } from 'react';

const USER_LOGGEDIN_STATUS_KEY = 'user-loggedin';
const USER_LOGGEDIN_ID_KEY = 'user-loggedin-id';

export class AuthService {
    static doAuthorization(response, tables) {
        if (!tables[response.status]) {
            localStorage.setItem(USER_LOGGEDIN_STATUS_KEY, false);
        } else {
            localStorage.setItem(USER_LOGGEDIN_STATUS_KEY, tables[response.status]);
        }
    }

    static isAuthenticated() {
        return localStorage.getItem(USER_LOGGEDIN_STATUS_KEY) == 'true';
    }

    static onUserLogin(response, callback) {
        if (response.status === 200) {
            response.json().then(json => {
                localStorage.setItem(USER_LOGGEDIN_STATUS_KEY, true);
                localStorage.setItem(USER_LOGGEDIN_ID_KEY, json.userId);
                callback(true);
            })
            .catch(err => {
                callback(false);
            })
        } else {
            callback(false);
        }
    }

    static onUserLoggedOut(response, callback) {
        if (response.status === 200) {
            localStorage.removeItem(USER_LOGGEDIN_STATUS_KEY);
            localStorage.removeItem(USER_LOGGEDIN_ID_KEY);
            callback(true);
        } else {
            callback(false);
        }
    }

    static getLoggedInUserId() {
        return +localStorage.getItem(USER_LOGGEDIN_ID_KEY);
    }
}

/**
 * @deprecated doAuthorization is deprecated, please use AuthService.doAuthorization instead.
 */
export function doAuthorization(response, tables) {
    if (!tables[response.status]) {
        localStorage.setItem(USER_LOGGEDIN_STATUS_KEY, false);
    } else {
        localStorage.setItem(USER_LOGGEDIN_STATUS_KEY, tables[response.status]);
    }
}

/**
 * @deprecated isAuthenticated is deprecated, please use AuthService.isAuthenticated instead.
 */
export function isAuthenticated() {
    return localStorage.getItem(USER_LOGGEDIN_STATUS_KEY) === 'true';
}