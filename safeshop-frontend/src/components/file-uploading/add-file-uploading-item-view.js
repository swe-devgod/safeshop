import React, { Component } from 'react';
import { Grid, Paper, LinearProgress, Button, Typography, Divider, Input } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

export default class AddFileUploadingItemView extends Component {
    render() {
        return (
            <Grid container spacing={8} alignItems='center'>
                <Grid item xs={12} style={{ textAlign: 'center' }}>
                    <input
                        accept={this.props.accept}
                        disabled={this.props.disabled}
                        multiple 
                        hidden
                        accept="image/*" 
                        id="raised-button-file" 
                        multiple 
                        type="file"
                        onChange={e => this.props.onFileChanged(e.target.files)} />
                    <label htmlFor="raised-button-file"> 
                        <Button disabled={this.props.disabled} variant='extendedFab' component='span'> 
                            <AddIcon />
                            &nbsp;Add New File
                        </Button> 
                    </label> 
                </Grid>
            </Grid>
        );
    }
}