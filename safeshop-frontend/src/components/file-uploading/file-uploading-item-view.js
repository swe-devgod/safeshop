import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Paper, LinearProgress, Button, Typography } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import DoneIcon from '@material-ui/icons/Done';

const fileUploadingItemViewStyles = theme => ({
    divImage: {
        width: 48,
        height: 48,
        //background: 'red',
        [theme.breakpoints.up('md')]: {
            width: 84,
            height: 84
        }
    }
});

class FileUploadingItemViewImpl extends Component {
    render() {
        const { classes } = this.props;
        return (
            <Grid container spacing={8} alignItems="center">
                <Grid item>
                    <div 
                        className={classes.divImage} 
                        style={{ backgroundImage: `url(${this.props.fileDataURL})`, backgroundSize: 'contain' }}>
                    </div>
                </Grid>
                <Grid item xs>
                    <Typography variant='subheading'>{this.props.fileName}</Typography>
                    <Typography variant='caption'>{`${this.props.fileSize} KByte${ +this.props.fileSize > 1 ? 's' : '' }`}</Typography>
                    <LinearProgress variant="determinate" value={this.props.percentUploading} />
                </Grid>
                <Grid item>
                    <Button disabled={this.props.isUploading} onClick={e => this.props.onDeleteClicked(this.props.index)}>
                        {
                            this.props.percentUploading < 100 ?
                                <ClearIcon />
                                :
                                <DoneIcon />
                        }
                    </Button>
                </Grid>
            </Grid>
        );
    }
}

FileUploadingItemViewImpl.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(fileUploadingItemViewStyles)(FileUploadingItemViewImpl);