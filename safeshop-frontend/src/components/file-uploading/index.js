import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FileUploadingView from './file-uploading-view';

export default class FileUploadingPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAddButtonDisabled: false,
            selectedFileList: []
        };

        this.itemHeightRef = React.createRef();
    }

    componentDidMount() {
        const height = +getComputedStyle(this.itemHeightRef.current).height.replace('px', '');
        this.setState({
            calculatedHeight: (height) * this.props.showItems + (8 * (this.props.showItems - 1))
        });
        this.handleWindowResized = this.handleWindowResized.bind(this);
        window.addEventListener('resize', this.handleWindowResized, false);
    }

    handleWindowResized(e) {
        const computedHeight = +getComputedStyle(this.itemHeightRef.current).height.replace('px', '');

        if (this.state.calculatedHeight !== computedHeight) {
            this.setState({
                calculatedHeight: (computedHeight) * this.props.showItems + (8 * (this.props.showItems - 1))
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResized);
    }

    handleFileChanged(files) {
        this.setState({
            isAddButtonDisabled: true
        });

        if (files.length > 0) {
            const newSelectedFileList = this.state.selectedFileList.slice();

            const loadImageFunc = (index) => {
                if (index < files.length) {
                    const reader = new FileReader();
                    reader.onload = e => {
                        newSelectedFileList.push({
                            file: files[index],
                            name: files[index].name,
                            size: files[index].size,
                            dataURL: e.target.result
                        });
                        loadImageFunc(index + 1);
                    }
                    reader.readAsDataURL(files[index]);
                } else {
                    this.setState({
                        isAddButtonDisabled: false,
                        selectedFileList: newSelectedFileList
                    }, () => {
                        this.props.onFileChanged(this.state.selectedFileList.map(f => ({ file: f.file, percent: 0 })));
                    });
                }
            };
            loadImageFunc(0);
        }
    }

    handleDeleteClicked(index) {
        if (index >= 0 && index < this.state.selectedFileList.length) {
            const newSelectedFileList = this.state.selectedFileList.slice();
            const newFileUploadingPercent = this.props.fileUploadingPercent.slice();
            newSelectedFileList.splice(index, 1);
            newFileUploadingPercent.splice(index, 1);

            this.setState({
                selectedFileList: newSelectedFileList
            }, () => {
                this.props.onFileChanged(this.state.selectedFileList.map((f, index) => ({ file: f.file, percent: newFileUploadingPercent[index] })));
            });

            // provide deleted callback
            // this.props.onDeleteClicked(index);
        }
    }

    render() {
        return <FileUploadingView
                    calculatedHeight={this.state.calculatedHeight}
                    isAddButtonDisabled={this.state.isAddButtonDisabled}
                    selectedFileList={this.state.selectedFileList}
                    itemHeightRef={this.itemHeightRef}
                    
                    onFileChanged={files => this.handleFileChanged(files)}
                    onDeleteClicked={index => this.handleDeleteClicked(index)} 
                    
                    isUploading={this.props.isUploading}
                    fileUploadingPercent={this.props.fileUploadingPercent}
                    onUploadButtonClicked={this.props.onUploadButtonClicked}
                    accept={this.props.accept}
                />
    }
}

FileUploadingPage.propTypes = {
    showItems: PropTypes.number.isRequired,
    accept: PropTypes.string.isRequired,
    onFileChanged: PropTypes.func.isRequired,
    fileUploadingPercent: PropTypes.array.isRequired,
    onUploadButtonClicked: PropTypes.func,
    isUploading: PropTypes.bool,
};