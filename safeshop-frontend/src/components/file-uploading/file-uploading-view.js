import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Paper, LinearProgress, Button, Typography, Divider, Input } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import AddIcon from '@material-ui/icons/Add';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import FileUploadingItemView from './file-uploading-item-view';
import AddFileUploadingItemView from './add-file-uploading-item-view';

import { styles } from './file-uploading-styles';

class FileUploadingViewImpl extends Component {
    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <div style={{ height: 0 }}>
                    <div ref={this.props.itemHeightRef}>
                        <Grid container direction="column" spacing={8}>
                            <Grid item style={{ visibility: 'hidden' }}>
                                <Grid item>
                                    <FileUploadingItemView
                                        percentUploading={0}
                                        index={0} 
                                        fileName={'ABCabc012'} 
                                        fileSize={`0123KBytes`}
                                        fileDataURL={''}
                                        onDeleteClicked={''} />
                                </Grid>
                                <Grid item>
                                    <Divider />
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </div>
                <Paper>
                    <div style={{ padding: 16, height: this.props.calculatedHeight, overflowY: 'scroll' }}>
                        <Grid container direction="column" spacing={8}>
                            {
                                this.props.selectedFileList &&
                                this.props.selectedFileList.map((file, index) => (
                                    <React.Fragment key={index}>
                                        <Grid item>
                                            <FileUploadingItemView 
                                                percentUploading={this.props.fileUploadingPercent[index]}
                                                isUploading={this.props.isUploading}
                                                index={index} 
                                                fileName={file.name} 
                                                fileSize={Math.trunc(Math.round(file.size / 1024))}
                                                fileDataURL={file.dataURL}
                                                onDeleteClicked={this.props.onDeleteClicked} />
                                        </Grid>
                                        <Grid item>
                                            <Divider />
                                        </Grid>
                                    </React.Fragment>
                                ))
                            }
                        </Grid>
                    </div>
                    <div style={{ padding: 16 }}>
                        <Grid container justify='space-between' direction='row' style={{ marginTop: 16 }}>
                            <Grid item>
                                <Grid container spacing={8} alignItems='center'>
                                    <Grid item xs={12} style={{ textAlign: 'center' }}>
                                        <input
                                            accept={this.props.accept}
                                            disabled={this.props.disabled}
                                            multiple 
                                            hidden
                                            accept="image/*" 
                                            id="raised-button-file" 
                                            multiple 
                                            type="file"
                                            onChange={e => this.props.onFileChanged(e.target.files)} />
                                        <label htmlFor="raised-button-file"> 
                                            <Button disabled={this.props.isUploading} variant='extendedFab' component='span'> 
                                                <AddIcon />
                                                &nbsp;Add New File
                                            </Button> 
                                        </label> 
                                    </Grid>
                                </Grid>
                            </Grid>
                            { this.props.showUploadButton &&
                                <Grid item>
                                    <Button variant='extendedFab' component='span' onClick={this.props.onUploadButtonClicked}> 
                                    { 
                                        this.props.isUploading ?
                                        <React.Fragment>
                                            <CloudUploadIcon />
                                            &nbsp;Stop
                                        </React.Fragment>
                                        :
                                        <React.Fragment>
                                            <CloudUploadIcon />
                                            &nbsp;Upload
                                        </React.Fragment>
                                    }
                                    </Button>
                                </Grid>
                            }
                        </Grid>
                    </div>
                </Paper>
            </React.Fragment>
        );
    }
}

FileUploadingViewImpl.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(FileUploadingViewImpl);