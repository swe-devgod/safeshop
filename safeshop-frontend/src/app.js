import React, { Component } from 'react';

import { MainLayout } from './components/shared-layout/main-layout/index';

import { createMemoryHistory } from 'history';
import { Route, Switch, Router } from 'react-router-dom';

import IndexPage from './pages/index';
import LoginPage from './pages/login';
import RegisterPage from './pages/register';
import ShowcasePage from './pages/showcase';
import ProfilePage from './pages/profile';
import TopicPage from './pages/showcase'
import LogoutPage from './pages/logout';    
import AddTopicPage from './pages/add-topic'
import EditTopicPage from './pages/add-topic/edit-topic-index'
import CategoryPage from './pages/category';
import MessengerPage from './pages/messenger';
import CartPage from './pages/cart';
import CheckoutPage from './pages/checkout'
import SellerManagementPage from './pages/seller-management'
import BuyerManagementPage from './pages/buyer-management'
import BillPage from './pages/bill'

import UploadDevPage from './pages/dev/upload';
import FileUploadingPage from './pages/dev/file-uploading';

import Button from '@material-ui/core/Button';

import './css/fonts.css';
import './css/material-ui.css';
import './css/style.css';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Redirect from 'react-router-dom/Redirect';
import { DisplayDispatcher, setBrowserHistory} from './components/my-dispatcher';
import { fetchCartAppbarAmount } from './lib/cart-appbar-helper';

const theme = createMuiTheme({
    typography: {
        useNextVariants: true
    },
    palette: {
        primary: {
            main: '#e91e63'
        },
        secondary: {
            main: '#ff9800'
        }
    }
});

export default class App extends Component {

    constructor(props) {
        super(props);

        this.appBarHistory = createMemoryHistory();
        this.sideBarHistory = createMemoryHistory();

        this.dySec = {
            setSideBarItems: this.setSideBarItems.bind(this)
        };

        this.dispatcher = new DisplayDispatcher();
    }

    componentDidMount() {
        console.log('[App] componentDidMount');

        /* Set CartButton item amount */
        fetchCartAppbarAmount(this.dispatcher);
    }

    setSideBarItems(view) {
        this.sideBarHistory.replace('/sidebar/main', view);
    }

    render() {
        return (
            <MuiThemeProvider theme={theme}>
                <Route path='/' render={props => { setBrowserHistory(props.history); return <React.Fragment /> }} />
                <MainLayout appBarHistory={this.appBarHistory} sideBarHistory={this.sideBarHistory} displayDispatcher={this.dispatcher}>
                    <Switch>
                        <Route exact path="/" render={props => (<IndexPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/login" render={props => (<LoginPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/register" render={props => (<RegisterPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route exact path="/topic/:id(\d+)" render={props => (<TopicPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        {/* <Route path="/showcase" render={props => (<ShowcasePage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/> */}
                        <Route path="/profile" render={props => (<ProfilePage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/logout" render={props => (<LogoutPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/add-topic" render={props => (<AddTopicPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/topic/:id(\d+)/edit" render={props => (<EditTopicPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/seller-management" render={props => (<SellerManagementPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/buyer-management" render={props => (<BuyerManagementPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/categories/:id(\d+)?" render={props => (<CategoryPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/dev/upload" render={props => (<UploadDevPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/dev/comment" render={props => (<FileUploadingPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/messenger/:id(\d+)?" render={props => (<MessengerPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/cart" render={props => (<CartPage dySec={this.dySec} dispatcher={this.dispatcher} {...props} />)}/>
                        <Route path="/checkout/bill/:id(\d+)" render={props => (<BillPage {...props} />)} />
                        <Route path="/checkout/:id(\d+)" render={props => (<CheckoutPage {...props} />)} />
                        <Redirect to="/" />
                    </Switch>
                </MainLayout>
            </MuiThemeProvider>
        );
    }
}