// Setup for enzyme
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

// Setup for jest-fetch-mock
global.fetch = require('jest-fetch-mock');
