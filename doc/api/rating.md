# Rating API 1.0 ระบบให้คะแนนกระทู้และผู้ใช้

## ให้คะแนนผู้ขาย
```
METHOD:     POST
ENDPOINT:   /api/rating/seller/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    itemId: number;         # ID ของ CheckoutItem
    rating: number;         # คะแนน
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    rating: number;         # คะแนนล่าสุดหลังจากอัพเดท
}
```

## ให้คะแนนผู้ซื้อ
```
METHOD:     POST
ENDPOINT:   /api/rating/buyer/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    itemId: number;         # ID ของ CheckoutItem
    rating: number;         # คะแนน
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    rating: number;         # คะแนนล่าสุดหลังจากอัพเดท
}
```

## ให้คะแนนกระทู้
```
METHOD:     POST
ENDPOINT:   /api/rating/topic/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    itemId: number;         # ID ของ CheckoutItem
    rating: number;         # คะแนน
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    rating: number;         # คะแนนล่าสุดหลังจากอัพเดท
}
```