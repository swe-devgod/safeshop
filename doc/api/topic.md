# Topic API 1.0
### ดึงข้อมูลกระทู้
```
METHOD:     GET
ENDPOINT:   /api/topic/:topicId/

DEFINE:
Shipping: JSON = {
    name: string,
    price: number
};

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    productName: string,
    productDescription: string,
    productPrice: real,
    createdDate: integer,
    topicRating: integer,
    topicRatingCount: integer,
    ownerUserId: number,
    ownerName: string,
    ownerPicture: string,
    ownerRating: real,
    ownerRatingCount: integer,
    remainingItems: integer,
    imgs: array<string>,
    shippings: array<Shipping>
}
```

### เพิ่มข้อมูลกระทู้
```
METHOD: POST
ENDPOINT: /api/topic/

DEFINE:
Shipping: JSON = {
    id: number
    name: string,
    price: number
};

ThumbnailInfo: JSON = {
    name: string,
    type: string,
    size: number,
    lastModified: number
};

## REQUEST ##
TYPE: multipart/form-data
CONTENT:
{
    topicName: string,
    topicDescription: string,
    topicPrice: number,
    topicRemainingItem: number,
    topicShipping: array<Shipping>
    topicCategory: array<number>        # List ของ CategoryID
    topicThumbnail: ThumbnailInfo       # ข้อมูลของไฟล์สำหรับเป็น Thumbnail
    topicPictures: array<File>          # File Handle ของแต่ละรูป
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    topicId: number
}
```

### อัพเดทข้อมูลกระทู้
```
METHOD: PATCH
ENDPOINT: /api/

DEFINE:
ShippingSchema:JSON = {
    id: number,
    price?: number;
}

ThumbnailSchema:JSON = {
    name: string;
    type: string;
    size: number;
}

## REQUEST ##
TYPE: multipart/form-data
CONTENT:
{
    topicName?: string;                             # เครื่องหมาย ? หมายความว่าถ้ามีการเปลี่ยนแปลงค่อยส่งมา
    topicDescription?: string;                      # เครื่องหมาย ? หมายความว่าถ้ามีการเปลี่ยนแปลงค่อยส่งมา
    topicPrice?: number;                            # เครื่องหมาย ? หมายความว่าถ้ามีการเปลี่ยนแปลงค่อยส่งมา
    topicRemainingItems?: number; 

    addCategories?: number[];                       # ส่งเป็น id ของ category ที่ต้องการจะเพิ่ม
    removeCategories?: number[];                    # ส่งเป็น id ของ category ที่ต้องการจะลบ

    addShippings?: ShippingSchema[];                # ส่งเป็น ShippingSchema ของ shipping ที่ต้องการจะเพิ่ม (แนบราคามาด้วย)
    updateShippings?: ShippingSchema[];             # ส่งเป็น ShippingSchema ของ shipping ที่ต้องการจะเปลี่ยนราคา
    removeShippings?: number[];                     # ส่งเป็น id ของ shipping ที่ต้องการจะลบ

    topicThumbnail?: ThumbnailSchema | number;      # ตอนดึงข้อมูล Server จะโยนข้อมูล ID ของรูปภาพมาด้วย กรณีที่เปลี่ยนเป็นรูปที่มีอยู่ใน Server อยู่แล้วให้ใช้ Picture ID
                                                    # ในกรณีที่ต้องการใช้รูปใหม่เป็น Thumbnail ให้แนบข้อมูลเป็น ThumbnailSchema

    addPictures?: array<file>;                      # ส่งเป็น array ของ files
    removePictures?: number[];                      # ตอนดึงข้อมูล Server จะโยนข้อมูล ID ของรูปภาพมาด้วย ให้แนบ id ของรูปที่ต้องการจะลบมา
                                                    # ในกรณีที่ลบรูปภาพที่เป็น Thumbnail จะต้องส่งส่วน topicThumbnail อันใหม่มาด้วย 
                                                    # (ซึ่งอาจจะเป็น ThumbnailSchema หรือ number)
}

## RESPONSE ##
TYPE: STATUS CODE
    200     OK                  (การแก้ไขสำเร็จ)
    400     BAD REQUEST         (การแก้ไขไม่ถูกต้อง มีการจัดการข้อมูลที่ผิดพลาด เช่นใช้ ID ผิดอันหรือเลือก ID มาใช้ที่ Server ตรวจสอบแล้วไม่ถูกต้อง)
    401     UNAUTHORIZATION     (ไม่ได้ LogIn)
    403     FORBIDDEN           (ไม่มีสิทธิในการแก้ไขกระทู้)
    404     NOT FOUND           (หมายเลขกระทู้ไม่ถูกต้อง)
```

### ดึงข้อมูล Shipping
```
METHOD: GET
ENDPOINT: /api/topic/shipping/list

DEFINE:
ShippingCarrier: JSON = {
    id: number,
    name: string
};

## RESPONSE ##
TYPE: JSON
CONTENT:
[ array of ShippingCarrier ]
```

### ดึงข้อมูล Category
```
METHOD: GET
ENDPOINT: /api/topic/category/list

DEFINE:
Category: JSON = {
    id: number,
    name: string
};

## RESPONSE ##
TYPE: JSON
CONTENT:
[ array of Category ]
```