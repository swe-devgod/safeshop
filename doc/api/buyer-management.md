# Buyer Management API 1.0 - สำหรับหน้าจัดการสินค้า

## ดึงข้อมูลกระทู้ที่ต้องทำการจ่ายเงิน
```
METHOD:     GET
ENDPOINT:   /api/management/buyer/checkout

DEFINE:
CheckoutInfo: JSON = {
    id: number,
    checkoutDate: number,
    statusId: number,
    addressId: number,
    checkoutItems: CheckoutItem[]
}

CheckoutItem: JSON = {
    id: number,
    pricePerItem: number,
    amount: number,
    addedDate: number,
    statusId: number
    topic: CheckoutTopic,
    shippings: CheckoutShipping[],
    user: CheckoutUser
}

CheckoutTopic: JSON = {
    id: number,
    topicCreatedDate: number,
    topicPrice: number,
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string
}

CheckoutShipping: JSON = {
    id: number,
    name: string
}

CheckoutUser: JSON = {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    email: string,
    tel: string,
    registerDate: number,
    rating: number,
    ratingVoteCount: number
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[
    array of CheckoutInfo
]
```

## ดึงข้อมูลกระทู้ที่ต้องจัดส่ง
```
METHOD:     GET
ENDPOINT:   /api/management/buyer/awaiting-shipment

DEFINE:
CheckoutInfo: JSON = {
    id: number,                         # ID ของ Checkout Item
    pricePerItem: number;               # ราคาของสินค้าต่อหนึ่งชิ้น
    amount: number,                     # จำนวนสินค้าที่ทำการสั่งซื้อ
    addedDate: number,                  # วันที่ทำการ Checkout จากตะกร้า
    statusId: number,
    topic: TopicInfo,                   # ข้อมูลกระทู้เพื่อแสดงผล
    user: UserInfo,
    address: AddressInfo
}

TopicInfo: JSON = {
    id: number,                         # ID ของ Topic
    userId: number,
    topicCreatedDate: number,
    topicPrice: number,                 # ราคาของสินค้าต่อหนึ่งชิ้น (จะมีค่าเท่ากับ pricePerItem)
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string                   # สำหรับแสดงรูป
}

UserInfo: JSON {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    email: string,
    tel: string,
    registerDate: number,
    rating: number,
    ratingVoteCount: number
}

AddressInfo: JSON {
    id: number,
    address: string
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[ 
    array of CheckoutInfo 
]
```

## ดึงข้อมูลกระทู้ที่กำลังจัดส่ง
```
METHOD:     GET
ENDPOINT:   /api/management/buyer/shipping

DEFINE:
CheckoutInfo: JSON = {
    id: number,                         # ID ของ Checkout Item
    pricePerItem: number;               # ราคาของสินค้าต่อหนึ่งชิ้น
    amount: number,                     # จำนวนสินค้าที่ทำการสั่งซื้อ
    addedDate: number,                  # วันที่ทำการ Checkout จากตะกร้า
    statusId: number,
    topic: TopicInfo;                   # ข้อมูลกระทู้เพื่อแสดงผล
    user: UserInfo;
    address: AddressInfo,
    shipping: ShippingInfo,
}

TopicInfo: JSON = {
    id: number,                         # ID ของ Topic
    userId: number,
    topicCreatedDate: number,
    topicPrice: number,                 # ราคาของสินค้าต่อหนึ่งชิ้น (จะมีค่าเท่ากับ pricePerItem)
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string                   # สำหรับแสดงรูป
}

UserInfo: JSON {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    email: string,
    tel: string,
    registerDate: number,
    rating: number,
    ratingVoteCount: number
}

AddressInfo: JSON {
    id: number,
    address: string
}

ShippingInfo: JSON {
    id: number,
    name: string                        # หมายเลข Track
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[ 
    array of CheckoutInfo 
]
```

## ดึงข้อมูลกระทู้ที่จัดส่งแล้ว
```
METHOD:     GET
ENDPOINT:   /api/management/buyer/success

DEFINE:
CheckoutInfo: JSON = {
    id: number,                         # ID ของ Checkout Item
    pricePerItem: number;               # ราคาของสินค้าต่อหนึ่งชิ้น
    amount: number,                     # จำนวนสินค้าที่ทำการสั่งซื้อ
    addedDate: number,                  # วันที่ทำการ Checkout จากตะกร้า
    statusId: number,
    topic: TopicInfo;                   # ข้อมูลกระทู้เพื่อแสดงผล
    user: UserInfo;
    address: AddressInfo,
    shipping: ShippingInfo,
}

TopicInfo: JSON = {
    id: number,                         # ID ของ Topic
    userId: number,
    topicCreatedDate: number,
    topicPrice: number,                 # ราคาของสินค้าต่อหนึ่งชิ้น (จะมีค่าเท่ากับ pricePerItem)
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string                   # สำหรับแสดงรูป
}

UserInfo: JSON {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    email: string,
    tel: string,
    registerDate: number,
    rating: number,
    ratingVoteCount: number
}

AddressInfo: JSON {
    id: number,
    address: string
}

ShippingInfo: JSON {
    id: number,
    name: string                        # หมายเลข Track
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[ 
    array of CheckoutInfo 
]
```

## ยืนยันการจัดส่งสินค้า
```
METHOD:     POST
ENDPOINT:   /api/management/buyer/confirm

## REQUEST ##
TYPE: JSON
CONTENT:
[ 
    itemId: number          # ID ของ CheckoutItem
]

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     OK
    401     UNAUTHORIZED
    403     FORBIDDEN
```