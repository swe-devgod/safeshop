# Messenger API 1.0

### ติดต่อผู้ขาย
```
METHOD:     POST
ENDPOINT:   /api/messenger/new

## REQUEST ##
TYPE: JSON
CONTENT: {
    destUserId: number;     # ID ของคนที่ต้องการคุยด้วย
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    roomId: number;         # ได้ RoomID มาใช้สำหรับ Redirect
                            # ไปที่ /messenger/:RoomID
}
```