# Cart API 0.1 - สำหรับตะกร้าสินค้า

### ดึงข้อมูลจำนวนสินค้าในตะกร้า
```
METHOD:     GET
ENDPOINT:   /api/cart/

## RESPONSE ##
TYPE: JSON
CONTENT:
[
    {
        count: number       # จำนวนสินค้าในตะกร้า
    }
]
```

### ดึงข้อมูลตะกร้า
```
METHOD:     GET
ENDPOINT:   /api/cart/list

## RESPONSE ##
TYPE: JSON
CONTENT:
[
    {
        id: number;                         # หมายเลข Item ในตะกร้าสินค้า         
        amount: number;                     # จำนวนสินค้าที่เพิ่มลงตะกร้า
        addedDate: number;                  # วันที่เพิ่มเข้าตะกร้า
        topic: {
            topicId: number;
            topicName: string;
            topicPrice: number;
            createdDate: number;            # วันที่กระทู้ถูกสร้าง
            topicRating: number;
            topicRatingCount: number;
            ownerUserId: number;
            ownerName: string;
            ownerPicture: string;
            ownerRating: number;
            ownerRatingCount: number;
            remainingItems: number;
            thumbnail: string;              # หน้าสำหรับแสดงรูปของกระทู้
        }      
    }
]
```

### เพิ่มสินค้าเข้าสู่ตะกร้า
```
METHOD:     PUT
ENDPOINT:   /api/cart/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    topicId: number         # แต่ละสินค้าจะมองเป็น TopicID,
    amount: number          # จำนวนสินค้า
}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     OK
    403     NOT AVAILABLE
    404     TOPIC NOT FOUND
```

### กำหนดจำนวนสินค้าในตะกร้า
```
METHOD:     PATCH
ENDPOINT:   /api/cart/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    topicId: number         # แต่ละสินค้าจะมองเป็น TopicID,
    amount: number          # จำนวนสินค้าที่จะกำหนด
}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     OK
    403     NOT AVAILABLE       # เพิ่มไม่ได้
    404     TOPIC NOT FOUND     
```

### ลบสินค้าออกจากตะกร้า
```
METHOD:     DELETE
ENDPOINT:   /api/cart/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    id: number         # หมายเลข Item ในตะกร้าสินค้า
}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     OK              # การลบสำเร็จ
    403     FORBIDDEN       # พยายามลบหมายเลขสินค้าที่ไม่สามารถลบได้
```