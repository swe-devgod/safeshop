# Comment API 1.0 - ระบบจัดการคอมเม้นต์

## ดึงข้อมูลคอมเม้นต์ โดยการระบุ Topic ID
```
METHOD:     GET
ENDPOINT:   /api/topic/:topicId/comments

DEFINE:
ImageSchema: JSON = {
    id: number;
    url: string;
};

Comemnt: JSON = {
    commentId: integer
    userId: integer,                //  ใช้สำหรับดึงข้อมูลที่เกี่ยวข้องสำหรับเจ้าของคอมเม้นต์
    userPicture: string,            //  ตำแหน่งสำหรับใช้ดึงรูปภาพของเจ้าของคอมเม้นต์
    displayName: string,            //  ชื่อสำหรับแสดงผลของผู้ที่คอมเม้นต์
    content: string,                //  ใช้สำหรับระบุเนื้อหาในคอมเม้นต์
    createdDate: Date               //  เวลาที่คอมเม้นต์ถูกสร้างขึ้น
    imgs: array<ImageSchema>        //  เป็น array ของ url ที่ใช้ดึงภาพตรงๆ ใช้กับ src ได้เลย
    subComments: array<Comment>?    //  (ถ้ามี) เป็นการระบุคอมเม้นต์ย่อย
};

## RESPONSE ##
TYPE: application/json
CONTENT:
{
    []: array<Comment>
}
```

## เพิ่มคอมเม้นต์ไปยัง Topic ID ที่กำหนด
```
METHOD:     POST
ENDPOINT:   /api/topic/:topicId/comments

## REQUEST ##
TYPE: multipart/form-data
CONTENT:
{
    content: string,
    imgs?: array<files>,
    parentId?: number,     // ในกรณีที่เป็น Sub-Comment ต้องระบุ Parent Comment ID ด้วย

}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     UPLOAD SUCCESSFUL
    400     BAD REQUEST
    401     AUTHORIZATION ERROR
    500     INTERNAL SERVER ERROR
```

## การ Edit Comemnt
```
METHOD:     PATCH
ENDPOINT:   /api/topic/comments/:commentId

## REQUEST ##
TYPE: multipart/form-data
CONTENT:
{
    content?: string,                   # ถ้ามีการเปลี่ยนแปลงเนื้อหา (Optional)
    appendImgs?: array<file>,           # กรณีเพิ่มรูปส่งข้อมูลนี้มาด้วย
    deletedImgs?: array<number>         # กรณีลบรูปส่งข้อมูลนี้มาด้วย (ส่ง ID ของรูป)
}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     UPLOAD SUCCESSFUL
    400     BAD REQUEST
    401     AUTHORIZATION ERROR
    500     INTERNAL SERVER ERROR
```

## การลบ Comment
```
METHOD:     DELETE
ENDPOINT:   /api/topic/comments/:commentId

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     UPLOAD SUCCESSFUL
    400     BAD REQUEST
    401     AUTHORIZATION ERROR
    500     INTERNAL SERVER ERROR
```