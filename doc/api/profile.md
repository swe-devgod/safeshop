# Profile API 2.0 - ดึงข้อมูลผู้ใช้

### ดึงข้อมูลส่วนตัวของผู้ใช้งาน
```
METHOD:     GET
ENDPOINT:   /api/user/profile/

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    username: string,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    address: string,
    tel: string,
    email: string,
    registerDate: Date,
    picture: string             // เป็น url ที่สามารถใส่ใน src ได้เลย
}
```

### สำหรับแก้ไขข้อมูลส่วนตัว
```
METHOD:     PUT
ENDPOINT:   /api/user/profile/

DEFINE:
Password:JSON = {
    newPassword: string,
    curPassword: string
};

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    firstName: string?,
    middleName: string?,
    lastName: string?,
    password: Password?,        // ในกรณีที่ต้องการเปลียนรหัสผ่าน
    displayName: string?,
    tel: string?,
    email: string?,
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    ...,
    [Field Name]: true | false, // เป็นชื่อของ field ที่ส่งเข้ามา โดยค่า true คือเปลี่ยนสำเร็จ
    ...
}
```

### สำหรับเปลี่ยนรูปภาพของผู้ใช้งาน
```
METHOD:     POST
ENDPOINT:   /api/user/profile/image

## RESPONSE ##
TYPE: multipart/form-data
CONTENT:
{
    picture: File           # File สำหรับใช้เป็นรูปภาพ (ใช้ FormData ในการอัพโหลด)
}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     UPLOAD SUCCESSFUL
    400     BAD REQUEST
    401     AUTHORIZATION ERROR
    500     INTERNAL SERVER ERROR
```