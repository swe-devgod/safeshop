# Checkout API 1.0 - สำหรับการซื้อสินค้า

## กำหนดสินค้าสำหรับ Checkout
```
METHOD:     POST
ENDPOINT:   /api/checkout/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    cardItemIds: number[]        # หมายเลขของสินค้าในตะกร้า
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    checkoutId: number      # ใช้สำหรับ Redirect ไปหน้า Checkout
}
```

## ดึงข้อมูลในหน้า Checkout
```
METHOD:     GET
ENDPOINT:   /api/checkout/:checkoutId

DEFINE:

UserSchema {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    email: string,
    tel: string,
    registerDate: number,
    rating: number,
    ratingVoteCount: number,
    picture: string;
}

AddressSchema {
    id: number;
    address: string;
}

CheckoutItemSchema {
    id: number;                                 # เป็น ID ใช้สำหรับส่งข้อมูลยืนยันการสั่งซื้อ
    amount: number;
    addedDate: number;
    topic: CheckoutItemTopicSchema;
}

CheckoutItemTopicSchema {
    id: number,
    topicCreatedDate: number,
    topicPrice: number,
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string;
    shippings: CheckoutItemShippingSchema[];
}

CheckoutItemShippingSchema {
    id: number;
    price: number;
    shipping: { id: number, name: string };     
}

PaymentSchema {
    id: number;
    name: string;
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    user: UserSchema;
    checkout: {
        checkoutDate: number,
        checkoutItems: CheckoutItemSchema[],
    },
    address: {
        defaultId: number,
        addresses: AddressSchema[];
    }
    paymentMethods: PaymentSchema[];
}
```

### ตัวอย่างการตอบกลับ สำหรับดึงข้อมูลในหน้า Checkout

```JSON
{
    "user": {
        "id": 3,
        "firstName": "film",
        "middleName": "film",
        "lastName": "film",
        "displayName": "Thanapawee",
        "email": "film@safeshop.com",
        "tel": "0000000000",
        "registerDate": 1540387757285,
        "rating": 0,
        "ratingVoteCount": 0,
        "picture": "http://localhost:3001/static/users/3.png"
    },
    "address": {
        "defaultId": 3,
        "addresses": [
            {
                "id": 3,
                "address": "Kasetsart"
            }
        ]
    },
    "checkout": {
        "checkoutDate": 1542791674782,
        "checkoutItems": [
            {
                "id": 10,
                "amount": 5,
                "addedDate": 1542791653160,
                "topic": {
                    "id": 1,
                    "topicCreatedDate": 1540387757293,
                    "topicPrice": 399,
                    "topicRemainingItems": 47,
                    "topicName": "Photo Set BNK48 รวมรูปภาพชุดใหม่ สำหรับปี 2018",
                    "topicDescription": "รูปภาพ BNK48 ขายแบบทั้ง set ของแท้ มีทั้งมิวนิค จูเน่และอีกหลายๆคน",
                    "topicVoteCount": 49,
                    "topicRating": 4.5,
                    "thumbnail": "http://localhost:3001/static/topics/10/m-1-img_bnk01.jpg",
                    "shippings": [
                        {
                            "id": 1,
                            "price": 45,
                            "shipping": {
                                "id": 1,
                                "name": "ThaiPost EMS"
                            }
                        },
                        {
                            "id": 2,
                            "price": 30,
                            "shipping": {
                                "id": 2,
                                "name": "ThaiPost Register"
                            }
                        },
                        {
                            "id": 3,
                            "price": 45,
                            "shipping": {
                                "id": 4,
                                "name": "Kerrry"
                            }
                        }
                    ]
                }
            },
            {
                "id": 11,
                "amount": 7,
                "addedDate": 1542791658025,
                "topic": {
                    "id": 2,
                    "topicCreatedDate": 1540387757294,
                    "topicPrice": 599,
                    "topicRemainingItems": 20,
                    "topicName": "หูฟัง One Odio สภาพใหม่",
                    "topicDescription": "หูฟัง One Odio สภาพใหม่ ใช้ไป 2 ชั่วโมง ของแท้ Pre-Order มาจากจีน",
                    "topicVoteCount": 0,
                    "topicRating": 0,
                    "thumbnail": "http://localhost:3001/static/topics/11/t-1.jpg",
                    "shippings": [
                        {
                            "id": 6,
                            "price": 75,
                            "shipping": {
                                "id": 1,
                                "name": "ThaiPost EMS"
                            }
                        },
                        {
                            "id": 7,
                            "price": 40,
                            "shipping": {
                                "id": 2,
                                "name": "ThaiPost Register"
                            }
                        }
                    ]
                }
            }
        ]
    },
    "paymentMethods": [
        {
            "id": 1,
            "name": "PromptPay"
        }
    ]
}
```

## ยืนยันการสั่งซื้อสินค้า
```
METHOD:     POST
ENDPOINT:   /api/checkout/:checkoutId

DEFINE:
CheckoutShipping: JSON = {
    itemId: number                  # ID ของ CheckoutItem
    topicShippingId: number         # ID ของ Shipping ที่อยู่ภายใน Topic
};

## REQUEST ##
TYPE: JSON
CONTENT:
{
    selectedAddressId: number,
    selectedPaymentId: number,
    checkoutShippings: CheckoutShipping[]
}

## RESPONSE ##

TASK RESULT: OK
TYPE: STATUS CODE
CONTENT:
    200     OK

TASK RESULT: ERROR
TYPE: JSON
CONTENT:
    403     FORBIDDEN
    [STRUCTURE]
    {
        errorCode: number,
        ...                     # Additional Data
    }
    [ERRORS]
        ERROR CODE: 0
        DESCRIPTION: INVALID_CHECKOUT_ID
        DATA: NONE

        ERROR CODE: 1
        DESCRIPTION: INVALID_ADDRESS_ID
        DATA: NONE

        ERROR CODE: 2
        DESCRIPTION: INVALID_CHECKOUT_ITEM_ID
        DATA: NONE

        ERROR CODE: 3
        DESCRIPTION: INVLAID_TOPIC_SHIPPING_ID
        DATA: NONE

        ERROR CODE: 4
        DESCRIPTION: INVALID_AMOUNT  
        DATA:
        {
            itemIds: array<number>         # ID ของ CheckoutItem ที่มีจำนวนของไม่พอ
        }

```

## เพิ่มข้อมูลหมายเลขการส่งสินค้า
```
METHOD:     POST
ENDPOINT:   /api/checkout/items/:checkoutItemId

## REQUEST ##
TYPE: JSON
CONTENT:
{
    shippingNo: string;
}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     OK
    403     FORBIDDEN
```

## สำหรับแสดงผล Bill
```
METHOD:         GET
ENDPOINT:       /api/checkout/:checkoutId/billing

DEFINE:
CheckoutItemShippingSchema: JSON = {
    id: number;
    price: number;
    name: string;
}

CheckoutItemTopicSchema: JSON = {
    id: number,
    topicCreatedDate: number,
    topicPrice: number,
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string;
}

CheckoutItemSchema: JSON = {
    id: number;
    amount: number;
    addedDate: number;
    topic: CheckoutItemTopicSchema;
    shipping: CheckoutItemShippingSchema;
}

UserSchema: JSON = {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    email: string,
    tel: string,
    registerDate: number,
    rating: number,
    ratingVoteCount: number
}

AddressSchema: JSON = {
    id: number;
    address: string;
}

PaymentSchema: JSON = {
    id: number;
    name: string;
}

## RESPONSE ##
TYPE: JSON
CONTENT:
{
    user: UserSchema;
    checkout: {
        id: number,                             # ID ของ Checkout ที่กำลังแสดง Bill
        checkoutDate: number,
        statusId: number,
        checkoutItems: CheckoutItemSchema[],    # ของที่อยู่ภายใน Checkout
    },
    address: AddressSchema,                     # ที่อยู่ที่เลือก
    paymentMethods: PaymentSchema;
}
```