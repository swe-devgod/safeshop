# Registration API 1.0 - ลงทะเบียน User

```
METHOD:     POST
ENDPOINT:   /api/register/

## REQUEST ##
TYPE: JSON
CONTENT:
{
    username: string,
    password: string,
    firstName: string,
    middleName: string,
    lastName: string,
    email: string,
    tel: string,
    address: string
}

## RESPONSE ##
TYPE: STATUS CODE
CONTENT:
    200     REGISTER SUCCESSFUL
    400     VALIDATION ERROR
```