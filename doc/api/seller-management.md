# Seller Management API 1.0 - สำหรับหน้าจัดการสินค้า 

### ดึงข้อมูลกระทู้ที่ตั้ง
```
METHOD:     GET
ENDPOINT:   /api/management/seller/selling

interface TopicInfo {
    id: number;
    userId: number;
    topicCreatedDate: number;
    topicPrice: number;
    topicRemainingItems: number;
    topicName: string;
    topicDescription: string;
    topicVoteCount: number;
    topicRating: number;
    thumbnail: string;
    shippings: TopicShippingInfo[];
}

interface TopicShippingInfo {
    id: number;
    name: string;
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[
    array of TopicInfo
]
```

### ดึงข้อมูลกระทู้ต้องจัดส่ง
```
METHOD:     GET
ENDPOINT:   /api/management/seller/awaiting-shipment

DEFINE:
CheckoutInfo: JSON = {
    id: number,                         # ID ของ Checkout Item
    pricePerItem: number;               # ราคาของสินค้าต่อหนึ่งชิ้น (ใช้ราคาจุดนี้เป็นหลัก)
    amount: number,                     # จำนวนสินค้าที่ทำการสั่งซื้อ
    addedDate: number,                  # วันที่ทำการ Checkout จากตะกร้า
    statusId: number,
    topic: TopicInfo;                   # ข้อมูลกระทู้เพื่อแสดงผล
}

TopicInfo: JSON = {
    id: number,                         # ID ของ Topic
    userId: number,
    topicCreatedDate: number,
    topicPrice: number,                 # ราคาปัจจุบันของกระทู้
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string                   # สำหรับแสดงรูป
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[ 
    array of CheckoutInfo 
]
```

### ดึงข้อมูลกระทู้ที่กำลังจัดส่ง
```
METHOD:     GET
ENDPOINT:   /api/management/seller/shipping

DEFINE:
CheckoutInfo: JSON = {
    id: number,                         # ID ของ Checkout Item
    pricePerItem: number;               # ราคาของสินค้าต่อหนึ่งชิ้น (ใช้ราคาจุดนี้เป็นหลัก)
    amount: number,                     # จำนวนสินค้าที่ทำการสั่งซื้อ
    addedDate: number,                  # วันที่ทำการ Checkout จากตะกร้า
    statusId: number,
    topic: TopicInfo;                   # ข้อมูลกระทู้เพื่อแสดงผล
    shipping: ShippingInfo;
}

ShippingInfo: JSON = {
    id: number,                         
    shippingNo: string                  # หมายเลขพัสดุ
};

TopicInfo: JSON = {
    id: number,                         # ID ของ Topic
    userId: number,
    topicCreatedDate: number,
    topicPrice: number,                 # ราคาปัจจุบันของกระทู้
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string                   # สำหรับแสดงรูป
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[ 
    array of CheckoutInfo 
]
```

### ดึงข้อมูลกระทู้จัดส่งสำเร็จแล้ว
```
METHOD:     GET
ENDPOINT:   /api/management/seller/success

DEFINE:
CheckoutInfo: JSON = {
    id: number,                         # ID ของ Checkout Item
    pricePerItem: number;               # ราคาของสินค้าต่อหนึ่งชิ้น (ใช้ราคาจุดนี้เป็นหลัก)
    amount: number,                     # จำนวนสินค้าที่ทำการสั่งซื้อ
    addedDate: number,                  # วันที่ทำการ Checkout จากตะกร้า
    statusId: number,
    topic: TopicInfo;                   # ข้อมูลกระทู้เพื่อแสดงผล
    shipping: ShippingInfo;
}

ShippingInfo: JSON = {
    id: number,                         
    shippingNo: string                  # หมายเลขพัสดุ
};

TopicInfo: JSON = {
    id: number,                         # ID ของ Topic
    userId: number,
    topicCreatedDate: number,
    topicPrice: number,                 # ราคาปัจจุบันของกระทู้
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string                   # สำหรับแสดงรูป
}

## RESPONSE ##
TYPE: JSON
CONTENT:
[ 
    array of CheckoutInfo 
]
```