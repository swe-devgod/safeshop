#!/bin/sh
set -e      # FOR EXIT ON ERROR

cd ~
cd safeshop
pm2 stop safeshop

git config --global user.email "thanapawee.film@gmail.com"
git config --global user.name "vokup"

git reset --hard
git remote set-url origin git@gitlab.com:swe-devgod/safeshop.git
git checkout -B master origin/master
git pull origin master

cd safeshop-frontend
npm install --save
npm run build
if [ $? -eq 0 ]
then
    cd ..
    cd safeshop-backend
    npm install --save
    rm -rf ./public/react/*
    cp -R ../safeshop-frontend/build/* ./public/react/
    pm2 start ecosystem.config.js --env production
else
    exit 1
fi