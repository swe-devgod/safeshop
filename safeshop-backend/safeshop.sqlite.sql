BEGIN TRANSACTION;
INSERT INTO `user` (id,firstName,middleName,lastName,displayName,email,tel,registerDate,rating,ratingVoteCount) VALUES (1,'wave','wave','wave','Pharanyu','wave@safeshop.com','0000000000',1540387757283,4.2,36);
INSERT INTO `user` (id,firstName,middleName,lastName,displayName,email,tel,registerDate,rating,ratingVoteCount) VALUES (2,'check','check','check','Rawit','check@safeshop.com','0000000000',1540387757284,0.0,0);
INSERT INTO `user` (id,firstName,middleName,lastName,displayName,email,tel,registerDate,rating,ratingVoteCount) VALUES (3,'film','film','film','Thanapawee','film@safeshop.com','0000000000',1540387757285,0.0,0);
INSERT INTO `user` (id,firstName,middleName,lastName,displayName,email,tel,registerDate,rating,ratingVoteCount) VALUES (4,'phan','phan','phan','Kunanon','phan@safeshop.com','0000000000',1540387757286,0.0,0);
INSERT INTO `user` (id,firstName,middleName,lastName,displayName,email,tel,registerDate,rating,ratingVoteCount) VALUES (5,'few','few','few','Phukla','few@safeshop.com','0000000000',1540387757287,0.0,0);
INSERT INTO `user` (id,firstName,middleName,lastName,displayName,email,tel,registerDate,rating,ratingVoteCount) VALUES (6,'Guest','Guest','Guest','Guest','guest@safeshop.com','0000000000',1541357217384,0.0,0);
INSERT INTO `user_picture` (id,fileName,userId) VALUES (1,'1.png',1);
INSERT INTO `user_picture` (id,fileName,userId) VALUES (2,'2.png',2);
INSERT INTO `user_picture` (id,fileName,userId) VALUES (3,'3.png',3);
INSERT INTO `user_picture` (id,fileName,userId) VALUES (4,'4.png',4);
INSERT INTO `user_picture` (id,fileName,userId) VALUES (5,'5.png',5);
INSERT INTO `session` (userId,username,password,lastActiveDate) VALUES (1,'devwave','$2a$10$WDGYAnsl4ajbhGSE43rZx..UCtks6IbrKNzJ/U1GfODkhBxchy4ta',1540387757288);
INSERT INTO `session` (userId,username,password,lastActiveDate) VALUES (2,'devcheck','$2a$10$j6zpeS8KtUO9x6vGvkIabe8k4SH8KZ/2EG7YKBBvabF8RN1cfsLrO',1540387757289);
INSERT INTO `session` (userId,username,password,lastActiveDate) VALUES (3,'devfilm','$2a$10$5QGEct6OezWKoPva0q8G3u0G8vhirTMmcPV/EYORctwKY1suhvLji',1540387757290);
INSERT INTO `session` (userId,username,password,lastActiveDate) VALUES (4,'devphan','$2a$10$UQEO6vbhgb5Ep3EXSrkEVuwI8N4TXTMYocEQCQuitmgYsZGA2tVD6',1540387757291);
INSERT INTO `session` (userId,username,password,lastActiveDate) VALUES (5,'devfew','$2a$10$x/HQGwaCoygCRkvRZuBhmOC1pz86dLvp1ybq6Nrg7FoGJTzDkn1ea',1540387757292);
INSERT INTO `session` (userId,username,password,lastActiveDate) VALUES (6,'guest','$2a$10$opHurY7XCTzefdve6Gy5zeRc7rsuGKnq82pNkUxI9XnQgwF/XO6Da',1541357217384);
INSERT INTO `address` (id,address,userId) VALUES (1,'Kasetsart',1);
INSERT INTO `address` (id,address,userId) VALUES (2,'Kasetsart',2);
INSERT INTO `address` (id,address,userId) VALUES (3,'Kasetsart',3);
INSERT INTO `address` (id,address,userId) VALUES (4,'Kasetsart',4);
INSERT INTO `address` (id,address,userId) VALUES (5,'Kasetsart',5);
INSERT INTO `address` (id,address,userId) VALUES (6,'Kasetsart',6);
INSERT INTO `default_address` (userId,addressId) VALUES (1,1);
INSERT INTO `default_address` (userId,addressId) VALUES (2,2);
INSERT INTO `default_address` (userId,addressId) VALUES (3,3);
INSERT INTO `default_address` (userId,addressId) VALUES (4,4);
INSERT INTO `default_address` (userId,addressId) VALUES (5,5);
INSERT INTO `default_address` (userId,addressId) VALUES (6,6);
INSERT INTO `shipping_carrier` (id,name,website) VALUES (1,'ThaiPost EMS', 'A');
INSERT INTO `shipping_carrier` (id,name,website) VALUES (2,'ThaiPost Register', 'B');
INSERT INTO `shipping_carrier` (id,name,website) VALUES (3,'Lalamove', 'C');
INSERT INTO `shipping_carrier` (id,name,website) VALUES (4,'Kerrry', 'D');
INSERT INTO `shipping_carrier` (id,name,website) VALUES (5,'Nim Express', 'E');
INSERT INTO `topic` (id,topicCreatedDate,topicPrice,topicRemainingItems,topicName,topicDescription,topicVoteCount,topicRating,userId) VALUES (1,1540387757293,399.0,47,'Photo Set BNK48 รวมรูปภาพชุดใหม่ สำหรับปี 2018','รูปภาพ BNK48 ขายแบบทั้ง set ของแท้ มีทั้งมิวนิค จูเน่และอีกหลายๆคน',49,4.5,1);
INSERT INTO `topic` (id,topicCreatedDate,topicPrice,topicRemainingItems,topicName,topicDescription,topicVoteCount,topicRating,userId) VALUES (2,1540387757294,599.0,20,'หูฟัง One Odio สภาพใหม่','หูฟัง One Odio สภาพใหม่ ใช้ไป 2 ชั่วโมง ของแท้ Pre-Order มาจากจีน',0,0.0,2);
INSERT INTO `topic_shipping` (id,price,topicId,shippingId) VALUES (1,45,1,1);
INSERT INTO `topic_shipping` (id,price,topicId,shippingId) VALUES (2,30,1,2);
INSERT INTO `topic_shipping` (id,price,topicId,shippingId) VALUES (3,45,1,4);
INSERT INTO `topic_shipping` (id,price,topicId,shippingId) VALUES (4,75,2,1);
INSERT INTO `topic_shipping` (id,price,topicId,shippingId) VALUES (5,40,2,2);
INSERT INTO `topic_pictures` (id,fileName,topicId) VALUES (1,'m-1-img_bnk01.jpg',1);
INSERT INTO `topic_pictures` (id,fileName,topicId) VALUES (2,'m-2-img_bnk02.jpg',1);
INSERT INTO `topic_pictures` (id,fileName,topicId) VALUES (3,'m-3-img_bnk03.jpg',1);
INSERT INTO `topic_pictures` (id,fileName,topicId) VALUES (4,'t-1.jpg',2);
INSERT INTO `topic_pictures` (id,fileName,topicId) VALUES (5,'t-2.jpg',2);
INSERT INTO `topic_pictures` (id,fileName,topicId) VALUES (6,'t-3.jpg',2);
INSERT INTO `topic_thumbnail` (topicId,topicPictureId) VALUES (1,1);
INSERT INTO `topic_thumbnail` (topicId,topicPictureId) VALUES (2,4);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (1,1,1,1540387757294,'สอบถามติดต่อเพิ่มเติมได้ที่ Post ได้เลยนะครับ',NULL,1);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (2,2,1,1540387757295,'ซื้อสามรูปลดได้ไหมครับ',NULL,0);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (3,3,1,1540387757296,'สวยจังเลยครับ',NULL,0);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (4,4,1,1540387757297,'เหมามิวนิคทั้งหมด เท่าไรครับ',NULL,1);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (5,5,1,1540387757298,'จูเน่สวยจังเลยครับ ขอดูรูปใกล้ๆได้ไหม',NULL,0);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (6,1,1,1540387757299,'ได้ครับตั้งแต่รูปที่สองเป็นต้นไปลดรูปละ 5% ครับ',2,1);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (7,1,1,1540387757300,'ขอโทษครับ พอดีขายปลีกอย่างเดียวครับ',4,0);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (8,1,1,1540387757301,'ได้ครับรูปมีตามนี้นะครับ',5,1);
INSERT INTO `topic_comment` (id,userId,topicId,createdDate,content,parentCommentId,haveImage) VALUES (9,2,2,1540387757296,'สอบถามได้เลยนะครับ',NULL,0);
INSERT INTO `category` (id,categoryName,parentCategoryId) VALUES (1,'สินค้าทั้งหมด',NULL);
INSERT INTO `topic_categories_category` (topicId,categoryId) VALUES (1,1);
INSERT INTO `topic_categories_category` (topicId,categoryId) VALUES (2,1);
INSERT INTO `comment_pictures` (id,fileName,commentId) VALUES (1,'m-1-img_bnk01.jpg',1);
INSERT INTO `comment_pictures` (id,fileName,commentId) VALUES (2,'m-2-img_bnk02.jpg',1);
INSERT INTO `comment_pictures` (id,fileName,commentId) VALUES (3,'m-3-img_bnk03.jpg',1);
INSERT INTO `comment_pictures` (id,fileName,commentId) VALUES (4,'m-1-img_bnk01.jpg',8);
INSERT INTO `comment_pictures` (id,fileName,commentId) VALUES (5,'m-2-img_bnk02.jpg',8);
INSERT INTO `checkout_status` (id,description) VALUES (1,'Awaiting Payment');
INSERT INTO `checkout_status` (id,description) VALUES (2,'Successful');
INSERT INTO `checkout_status` (id,description) VALUES (3,'Cancel');
INSERT INTO `checkout_item_status` (id,description) VALUES (1,'AwaitingPayment');
INSERT INTO `checkout_item_status` (id,description) VALUES (2,'AwaitingShipment');
INSERT INTO `checkout_item_status` (id,description) VALUES (3,'Shipping');
INSERT INTO `checkout_item_status` (id,description) VALUES (4,'Successful');
INSERT INTO `checkout_item_status` (id,description) VALUES (5,'Cancel');
INSERT INTO `messenger_room` (id,timestamp) VALUES (1,1540666890817);
INSERT INTO `messenger_room` (id,timestamp) VALUES (2,1540666890812);
INSERT INTO `messenger_room` (id,timestamp) VALUES (3,1540666890818);
INSERT INTO `messenger_room` (id,timestamp) VALUES (4,1540666890819);
INSERT INTO `messenger_room` (id,timestamp) VALUES (5,1540726870228);
INSERT INTO `messenger_room` (id,timestamp) VALUES (6,1540666890821);
INSERT INTO `messenger_room` (id,timestamp) VALUES (7,1540666890822);
INSERT INTO `messenger_room` (id,timestamp) VALUES (8,1540666890814);
INSERT INTO `messenger_room` (id,timestamp) VALUES (9,1540666890815);
INSERT INTO `messenger_room` (id,timestamp) VALUES (10,1540666890824);
INSERT INTO `messenger_room` (id,timestamp) VALUES (11,1540726901509);
INSERT INTO `messenger_room` (id,timestamp) VALUES (12,1540666890820);
INSERT INTO `messenger_room` (id,timestamp) VALUES (13,1540666890823);
INSERT INTO `messenger_room` (id,timestamp) VALUES (14,1540666890825);
INSERT INTO `messenger_room` (id,timestamp) VALUES (15,1540666890826);
INSERT INTO `room_users` (userId,roomId) VALUES (1,1);
INSERT INTO `room_users` (userId,roomId) VALUES (2,1);
INSERT INTO `room_users` (userId,roomId) VALUES (1,2);
INSERT INTO `room_users` (userId,roomId) VALUES (3,2);
INSERT INTO `room_users` (userId,roomId) VALUES (4,3);
INSERT INTO `room_users` (userId,roomId) VALUES (1,3);
INSERT INTO `room_users` (userId,roomId) VALUES (1,4);
INSERT INTO `room_users` (userId,roomId) VALUES (5,4);
INSERT INTO `room_users` (userId,roomId) VALUES (2,5);
INSERT INTO `room_users` (userId,roomId) VALUES (3,5);
INSERT INTO `room_users` (userId,roomId) VALUES (2,6);
INSERT INTO `room_users` (userId,roomId) VALUES (4,6);
INSERT INTO `room_users` (userId,roomId) VALUES (2,7);
INSERT INTO `room_users` (userId,roomId) VALUES (5,7);
INSERT INTO `room_users` (userId,roomId) VALUES (3,8);
INSERT INTO `room_users` (userId,roomId) VALUES (4,8);
INSERT INTO `room_users` (userId,roomId) VALUES (3,9);
INSERT INTO `room_users` (userId,roomId) VALUES (5,9);
INSERT INTO `room_users` (userId,roomId) VALUES (4,10);
INSERT INTO `room_users` (userId,roomId) VALUES (5,10);
INSERT INTO `room_users` (userId,roomId) VALUES (3,11);
INSERT INTO `room_users` (userId,roomId) VALUES (1,12);
INSERT INTO `room_users` (userId,roomId) VALUES (2,13);
INSERT INTO `room_users` (userId,roomId) VALUES (4,14);
INSERT INTO `room_users` (userId,roomId) VALUES (5,15);
COMMIT;