import * as express from 'express';

import { topicRoutes } from './topic/@/topic-routes';
import { userRoutes } from './user/@/user-routes';
import { messengerRoutes } from './messenger/@/messenger-routes';
import { categoryRoutes } from './category/@/category-routes';
import { cartRoutes } from './cart/cart-routes';
import { checkoutRoutes } from './checkout/checkout-routes';
import { managementRoutes } from './management/@/management-routes';
import { ratingRoute } from './ratings/@/rating-routes';

import { mapRoutes } from '../../helper/map-routes';


const router = express.Router();

mapRoutes(router, topicRoutes);
mapRoutes(router, userRoutes);
mapRoutes(router, messengerRoutes);
mapRoutes(router, categoryRoutes);
mapRoutes(router, cartRoutes);
mapRoutes(router, checkoutRoutes);
mapRoutes(router, managementRoutes);
mapRoutes(router, ratingRoute);

export const API_ROUTER = router;