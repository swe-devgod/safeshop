import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";

export async function GetUserLogin(req, res, next) {
    if (req.isAuthenticated()) {
        res.sendStatus(200);
    } else {
        return next(new UnauthorizedError());
    }
}