"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const no_session_found_error_1 = require("../../../../../../errors/no-session-found-error");
const bad_request_error_1 = require("../../../../../../errors/base/bad-request-error");
const validator_helper_1 = require("../../../../../../helper/validator-helper");
function PostUserLogin(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        // validation
        const { username, password } = req.body;
        const userResult = !!!validator_helper_1.ValidatorHelper.isNullOrUndefined(username) && validator_helper_1.ValidatorHelper.checkUsername(username);
        const pwdResult = !!!validator_helper_1.ValidatorHelper.isNullOrUndefined(password) && validator_helper_1.ValidatorHelper.checkPassword(password);
        const validationResult = userResult && pwdResult;
        if (validationResult) {
            //console.log(req.body);
            passport.authenticate('local', function (err, session, info) {
                if (err) {
                    return next(err);
                }
                if (!session) {
                    return next(new no_session_found_error_1.NoSessionFoundError());
                }
                req.logIn(session, function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json({
                        userId: session.userId
                    });
                });
            })(req, res, next);
        }
        else {
            return next(new bad_request_error_1.BadRequestError({
                success: false,
                result: {
                    username: userResult,
                    password: pwdResult
                }
            }));
        }
    });
}
exports.PostUserLogin = PostUserLogin;
