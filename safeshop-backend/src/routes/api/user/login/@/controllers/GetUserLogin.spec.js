"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("supertest");
const app = require('../../../../../../../app');
const VALID_USER_MOCK = {
    username: 'admin',
    password: 'safeshop',
    firstName: 'SafeShop',
    middleName: 'Dev',
    lastName: 'Admin',
    displayName: 'SafeShopAdmin',
    addr: 'Kasetsart',
    email: 'admin@safeshop.com',
    tel: '+66123456789'
};
beforeAll(done => {
    app.ready.then(() => __awaiter(this, void 0, void 0, function* () {
        const agent = request.agent(app);
        try {
            yield agent
                .post('/api/user/register')
                .send(VALID_USER_MOCK)
                .expect(200);
        }
        catch (err) {
            return done(err);
        }
        done();
    }));
});
test("Test Unauthorized User", done => {
    app.ready.then(() => {
        request(app)
            .get('/api/user/login')
            .expect(401)
            .end(function (err, res) {
            if (err) {
                return done(err);
            }
            done();
        });
    });
});
test("Test Authorized User", done => {
    app.ready.then(() => __awaiter(this, void 0, void 0, function* () {
        const agent = request.agent(app);
        yield agent
            .post('/api/user/login')
            .send({ username: VALID_USER_MOCK.username, password: VALID_USER_MOCK.password })
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        yield agent
            .get('/api/user/login')
            .expect(200);
        done();
    }));
});
