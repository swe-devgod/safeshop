import { GetUserLogin } from './GetUserLogin';
import * as request from 'supertest';
import { PostRegisterUserRequestSchema } from '../../../register/@/controllers/PostRegisterUser';

const app = require('../../../../../../../app');

const VALID_USER_MOCK = {
    username: 'admin',
    password: 'safeshop',
    firstName: 'SafeShop',
    middleName: 'Dev',
    lastName: 'Admin',
    displayName: 'SafeShopAdmin',
    addr: 'Kasetsart',
    email: 'admin@safeshop.com',
    tel: '+66123456789'
} as PostRegisterUserRequestSchema;

beforeAll(done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        try {
            await agent
            .post('/api/user/register')
            .send(VALID_USER_MOCK)
            .expect(200)
        } catch (err) {
            return done(err);
        }
        done();
    });
});

test("Test Unauthorized User", done => {
    app.ready.then(() => {
    request(app)
        .get('/api/user/login')
        .expect(401)
        .end(function(err, res) {
            if (err) {
                return done(err);
            }
            done();
        })
    });
});

test("Test Authorized User", done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        await agent
            .post('/api/user/login')
            .send({ username: VALID_USER_MOCK.username, password: VALID_USER_MOCK.password })
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);

        await agent
            .get('/api/user/login')
            .expect(200);

        done();
    });
});