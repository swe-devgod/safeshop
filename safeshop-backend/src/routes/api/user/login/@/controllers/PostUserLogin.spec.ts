import { GetUserLogin } from './GetUserLogin';
import * as request from 'supertest';
import { PostRegisterUserRequestSchema } from '../../../register/@/controllers/PostRegisterUser';

const app = require('../../../../../../../app');

const VALID_USER_MOCK = {
    username: 'admin',
    password: 'safeshop',
    firstName: 'SafeShop',
    middleName: 'Dev',
    lastName: 'Admin',
    displayName: 'SafeShopAdmin',
    addr: 'Kasetsart',
    email: 'admin@safeshop.com',
    tel: '+66123456789'
} as PostRegisterUserRequestSchema;

beforeAll(done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        try {
            await agent
            .post('/api/user/register')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .send(VALID_USER_MOCK)
            .expect(200)
        } catch (err) {
            return done(err);
        }
        done();
    });
});

test("Test Empty Request Body", done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        await agent
            .post('/api/user/login')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .send({ })
            .expect(400);
        done();
    });
});

test("Test Correct User and Password", done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        await agent
            .post('/api/user/login')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .send({ username: VALID_USER_MOCK.username, password: VALID_USER_MOCK.password })
            .expect(200);

        done();
    });
});

test("Test Incorrect Username", done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        await agent
            .post('/api/user/login')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .send({ username: VALID_USER_MOCK.username + 'something', password: VALID_USER_MOCK.password })
            .expect(401);
        done();
    });
});

test("Test Incorrect Password", done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        await agent
            .post('/api/user/login')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .send({ username: VALID_USER_MOCK.username, password: VALID_USER_MOCK.password + 'something' })
            .expect(401);
        done();
    });
});

test("Test Incorrect Username and Password", done => {
    app.ready.then(async() => {
        const agent = request.agent(app);
        await agent
            .post('/api/user/login')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .send({ username: VALID_USER_MOCK.username + 'something', password: VALID_USER_MOCK.password + 'something' })
            .expect(401);
        done();
    });
});