import * as passport from 'passport';
import * as validator from 'validator';
import { Request, Response, NextFunction } from "express";
import { NoSessionFoundError } from '../../../../../../errors/no-session-found-error';
import { BadRequestError } from '../../../../../../errors/base/bad-request-error';
import { ValidatorHelper } from '../../../../../../helper/validator-helper';

export async function PostUserLogin(req: Request, res: Response, next: NextFunction) {
    // validation
    const { username, password } = req.body;

    const userResult = !!!ValidatorHelper.isNullOrUndefined(username) && ValidatorHelper.checkUsername(username);
    const pwdResult = !!!ValidatorHelper.isNullOrUndefined(password) && ValidatorHelper.checkPassword(password);
    const validationResult = userResult && pwdResult;

    if (validationResult) {
        //console.log(req.body);
        passport.authenticate('local', function(err, session, info) {
            if (err) {
                return next(err);
            }
            
            if (!session) {
                return next(new NoSessionFoundError());
            }

            req.logIn(session, function(err) {
                if (err) {
                    return next(err);
                }
                res.status(200).json({
                    userId: session.userId
                })
            });
        })(req, res, next);
    } else {
        return next(new BadRequestError({
            success: false,
            result: {
                username: userResult,
                password: pwdResult
            }
        }));
    }
}