import { PostUserLogin } from './controllers/PostUserLogin';
import { GetUserLogin } from './controllers/GetUserLogin';

export const loginRoutes = {
    name: "login",
    routes: [
        {
            path: '',
            method: 'get',
            action: GetUserLogin
        },
        {
            path: '',
            method: 'post',
            action: PostUserLogin
        }
    ]
};