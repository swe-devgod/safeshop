"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PostUserLogin_1 = require("./controllers/PostUserLogin");
const GetUserLogin_1 = require("./controllers/GetUserLogin");
exports.loginRoutes = {
    name: "login",
    routes: [
        {
            path: '',
            method: 'get',
            action: GetUserLogin_1.GetUserLogin
        },
        {
            path: '',
            method: 'post',
            action: PostUserLogin_1.PostUserLogin
        }
    ]
};
