import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";

export async function PostUserLogout(req, res, next) {
    if (req.isAuthenticated()) {
        //req.session.destroy();
        req.logout();
        res.sendStatus(200);
    } else {
        next(new UnauthorizedError(null, 'The user is not logging in!'));
    }
}