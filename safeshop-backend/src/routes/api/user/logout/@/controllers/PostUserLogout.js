"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
function PostUserLogout(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            //req.session.destroy();
            req.logout();
            res.sendStatus(200);
        }
        else {
            next(new unauthorized_error_1.UnauthorizedError(null, 'The user is not logging in!'));
        }
    });
}
exports.PostUserLogout = PostUserLogout;
