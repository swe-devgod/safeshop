import { PostUserLogout } from './controllers/PostUserLogout';

export const logoutRoutes = {
    name: "logout",
    routes: [
        {
            path: '',
            method: 'post',
            action: PostUserLogout
        }
    ]
};