"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PostUserLogout_1 = require("./controllers/PostUserLogout");
exports.logoutRoutes = {
    name: "logout",
    routes: [
        {
            path: '',
            method: 'post',
            action: PostUserLogout_1.PostUserLogout
        }
    ]
};
