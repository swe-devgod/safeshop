import * as request from 'supertest';
import { PostRegisterUserRequestSchema } from './PostRegisterUser';

const VALID_USER_MOCK = {
    username: 'admin',
    password: 'safeshop',
    firstName: 'SafeShop',
    middleName: 'Dev',
    lastName: 'Admin',
    displayName: 'SafeShopAdmin',
    addr: 'Kasetsart',
    email: 'admin@safeshop.com',
    tel: '+66123456789'
} as PostRegisterUserRequestSchema;

test("Test valid registration", function(done) {
    const app = require('../../../../../../../app');
    app.ready.then(async() => {
        const agent = request.agent(app);
        try {
            await agent
            .post('/api/user/register')
            .send(VALID_USER_MOCK)
            .expect(200)
        } catch (err) {
            return done(err);
        }
        done();
    });
});

test("Test invalid username", function(done) {
    const app = require('../../../../../../../app');
    app.ready.then(async() => {
        const agent = request.agent(app);
        try {
            await agent
            .post('/api/user/register')
            .send(Object.assign({}, VALID_USER_MOCK, { username: 'a' }))
            .expect(400)
        } catch (err) {
            return done(err);
        }
        done();
    });
});