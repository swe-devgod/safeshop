import * as bcrypt from 'bcryptjs';
import * as validator from 'validator';
import { User } from '../../../../../../database/models/user';
import { Address } from '../../../../../../database/models/address';
import { Session } from '../../../../../../database/models/session';
import { UserDatabase } from '../../../../../../database/repositories/user-database';
import { SessionDatabase } from '../../../../../../database/repositories/session-database';
import { AddressDatabase } from '../../../../../../database/repositories/address-database';
import { ValidatorHelper } from '../../../../../../helper/validator-helper';
import { urlencoded } from 'body-parser';

const BCRYPT_SALT_ROUND = 10;

export interface PostRegisterUserRequestSchema {
    username: string;
    password: string;
    displayName: string;
    firstName: string;
    middleName: string;
    lastName: string;
    email: string;
    tel: string;
    addr: string;
};

export async function PostRegisterUser(req, res) {
    // console.log(req.body);

    const {
        username = '',
        password = '',
        displayName = '',
        firstName = '',
        middleName = '',
        lastName = '',
        email = '',
        tel = '',
        addr = ''
    }: PostRegisterUserRequestSchema = req.body;

    // validation
    const usernameResult = ValidatorHelper.checkUsername(username);
    const passwordResult = ValidatorHelper.checkPassword(password);
    const firstNameResult = ValidatorHelper.checkFirstName(firstName);
    const middleNameResult = (ValidatorHelper.checkEmpty(middleName) || ValidatorHelper.checkMiddleName(middleName));
    const lastNameResult = ValidatorHelper.checkLastName(lastName);
    const displayResult = ValidatorHelper.checkDisplayName(displayName);
    const telResult = ValidatorHelper.checkTel(tel);
    const addrResult = ValidatorHelper.checkNotEmpty(addr);
    const emailResult = ValidatorHelper.checkEmail(email);

    const validationResult =
        usernameResult &&
        passwordResult &&
        displayResult &&
        firstNameResult &&
        middleNameResult &&
        lastNameResult &&
        telResult &&
        addrResult &&
        emailResult;

    if (validationResult) {
        // validation successful
        const user = new User();
        user.firstName = firstName;
        user.middleName = middleName;
        user.lastName = lastName;
        user.displayName = displayName;
        user.email = email;
        user.tel = tel;
        user.registerDate = req.timestamp

        const salt = await bcrypt.genSalt(BCRYPT_SALT_ROUND);
        const hash = await bcrypt.hash(password, salt);

        const session = new Session();
        session.username = username;
        session.password = hash;
        session.user = user;
        session.lastActiveDate = req.timestamp;

        const address = new Address();
        address.address = addr;
        address.user = user;

        await UserDatabase.insert(user);
        await SessionDatabase.insert(session);
        await AddressDatabase.insert(address);
        await AddressDatabase.sefDefaultDatabase(user, address);
        
        res.sendStatus(200);
    } else {
        // validation error
        res.status(400).json({
            success: false,
            result: {
                username: usernameResult,
                password: passwordResult,
                displayName: displayResult,
                firstName: firstNameResult,
                middleName: middleNameResult,
                lastName: lastNameResult,
                email: emailResult,
                tel: telResult,
                addr: addrResult
            }
        });
    }
}