"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcryptjs");
const user_1 = require("../../../../../../database/models/user");
const address_1 = require("../../../../../../database/models/address");
const session_1 = require("../../../../../../database/models/session");
const user_database_1 = require("../../../../../../database/repositories/user-database");
const session_database_1 = require("../../../../../../database/repositories/session-database");
const address_database_1 = require("../../../../../../database/repositories/address-database");
const validator_helper_1 = require("../../../../../../helper/validator-helper");
const BCRYPT_SALT_ROUND = 10;
;
function PostRegisterUser(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        // console.log(req.body);
        const { username = '', password = '', displayName = '', firstName = '', middleName = '', lastName = '', email = '', tel = '', addr = '' } = req.body;
        // validation
        const usernameResult = validator_helper_1.ValidatorHelper.checkUsername(username);
        const passwordResult = validator_helper_1.ValidatorHelper.checkPassword(password);
        const firstNameResult = validator_helper_1.ValidatorHelper.checkFirstName(firstName);
        const middleNameResult = (validator_helper_1.ValidatorHelper.checkEmpty(middleName) || validator_helper_1.ValidatorHelper.checkMiddleName(middleName));
        const lastNameResult = validator_helper_1.ValidatorHelper.checkLastName(lastName);
        const displayResult = validator_helper_1.ValidatorHelper.checkDisplayName(displayName);
        const telResult = validator_helper_1.ValidatorHelper.checkTel(tel);
        const addrResult = validator_helper_1.ValidatorHelper.checkNotEmpty(addr);
        const emailResult = validator_helper_1.ValidatorHelper.checkEmail(email);
        const validationResult = usernameResult &&
            passwordResult &&
            displayResult &&
            firstNameResult &&
            middleNameResult &&
            lastNameResult &&
            telResult &&
            addrResult &&
            emailResult;
        if (validationResult) {
            // validation successful
            const user = new user_1.User();
            user.firstName = firstName;
            user.middleName = middleName;
            user.lastName = lastName;
            user.displayName = displayName;
            user.email = email;
            user.tel = tel;
            user.registerDate = req.timestamp;
            const salt = yield bcrypt.genSalt(BCRYPT_SALT_ROUND);
            const hash = yield bcrypt.hash(password, salt);
            const session = new session_1.Session();
            session.username = username;
            session.password = hash;
            session.user = user;
            session.lastActiveDate = req.timestamp;
            const address = new address_1.Address();
            address.address = addr;
            address.user = user;
            yield user_database_1.UserDatabase.insert(user);
            yield session_database_1.SessionDatabase.insert(session);
            yield address_database_1.AddressDatabase.insert(address);
            yield address_database_1.AddressDatabase.sefDefaultDatabase(user, address);
            res.sendStatus(200);
        }
        else {
            // validation error
            res.status(400).json({
                success: false,
                result: {
                    username: usernameResult,
                    password: passwordResult,
                    displayName: displayResult,
                    firstName: firstNameResult,
                    middleName: middleNameResult,
                    lastName: lastNameResult,
                    email: emailResult,
                    tel: telResult,
                    addr: addrResult
                }
            });
        }
    });
}
exports.PostRegisterUser = PostRegisterUser;
