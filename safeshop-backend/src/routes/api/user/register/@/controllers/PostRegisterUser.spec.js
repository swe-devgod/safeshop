"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("supertest");
const VALID_USER_MOCK = {
    username: 'admin',
    password: 'safeshop',
    firstName: 'SafeShop',
    middleName: 'Dev',
    lastName: 'Admin',
    displayName: 'SafeShopAdmin',
    addr: 'Kasetsart',
    email: 'admin@safeshop.com',
    tel: '+66123456789'
};
test("Test valid registration", function (done) {
    const app = require('../../../../../../../app');
    app.ready.then(() => __awaiter(this, void 0, void 0, function* () {
        const agent = request.agent(app);
        try {
            yield agent
                .post('/api/user/register')
                .send(VALID_USER_MOCK)
                .expect(200);
        }
        catch (err) {
            return done(err);
        }
        done();
    }));
});
test("Test invalid username", function (done) {
    const app = require('../../../../../../../app');
    app.ready.then(() => __awaiter(this, void 0, void 0, function* () {
        const agent = request.agent(app);
        try {
            yield agent
                .post('/api/user/register')
                .send(Object.assign({}, VALID_USER_MOCK, { username: 'a' }))
                .expect(400);
        }
        catch (err) {
            return done(err);
        }
        done();
    }));
});
