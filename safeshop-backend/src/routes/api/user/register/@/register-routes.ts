import { PostRegisterUser } from './controllers/PostRegisterUser';

export const registerRoutes = {
    name: "register",
    routes: [
        {
            path: '',
            method: 'post',
            action: PostRegisterUser
        }
    ]
};