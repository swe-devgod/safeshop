"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PostRegisterUser_1 = require("./controllers/PostRegisterUser");
exports.registerRoutes = {
    name: "register",
    routes: [
        {
            path: '',
            method: 'post',
            action: PostRegisterUser_1.PostRegisterUser
        }
    ]
};
