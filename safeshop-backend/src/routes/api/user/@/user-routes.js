"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const route_helper_1 = require("../../../../helper/route-helper");
const login_routes_1 = require("../login/@/login-routes");
const logout_routes_1 = require("../logout/@/logout-routes");
const profile_routes_1 = require("../profile/@/profile-routes");
const register_routes_1 = require("../register/@/register-routes");
exports.userRoutes = {
    name: "user",
    routes: []
};
route_helper_1.attachRoutesToParent(login_routes_1.loginRoutes, exports.userRoutes);
route_helper_1.attachRoutesToParent(logout_routes_1.logoutRoutes, exports.userRoutes);
route_helper_1.attachRoutesToParent(profile_routes_1.profileRoutes, exports.userRoutes);
route_helper_1.attachRoutesToParent(register_routes_1.registerRoutes, exports.userRoutes);
