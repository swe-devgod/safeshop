import { attachRoutesToParent } from '../../../../helper/route-helper';
import { loginRoutes } from '../login/@/login-routes';
import { logoutRoutes } from '../logout/@/logout-routes';
import { profileRoutes } from '../profile/@/profile-routes';
import { registerRoutes } from '../register/@/register-routes';

export const userRoutes = {
    name: "user",
    routes: []
};

attachRoutesToParent(loginRoutes, userRoutes);
attachRoutesToParent(logoutRoutes, userRoutes);
attachRoutesToParent(profileRoutes, userRoutes);
attachRoutesToParent(registerRoutes, userRoutes);