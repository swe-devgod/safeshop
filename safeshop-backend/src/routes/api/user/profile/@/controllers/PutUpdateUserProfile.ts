import { SessionDatabase } from "../../../../../../database/repositories/session-database";
import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { ValidatorHelper as vh } from "../../../../../../helper/validator-helper";
import { getManager } from "typeorm";
import { User } from "../../../../../../database/models/user";
import { Session } from "../../../../../../database/models/session";

import * as bcrypt from 'bcryptjs';
import { BadRequestError } from "../../../../../../errors/base/bad-request-error";
import { ForbiddenError } from "../../../../../../errors/base/forbidden-error";
import { DefaultAddress } from "../../../../../../database/models/default-address";
import { Address } from "../../../../../../database/models/address";

const BCRYPT_SALT_ROUND = 10;

interface UpdateUserProfileRequestSchema {
    firstName: string;
    middleName: string;
    lastName: string;
    displayName: string;
    curPassword: string;
    newPassword: string;
    addr: string,
    tel: string;
    email: string;
}

export async function PutUpdateUserProfile(req, res, next) {
    if (req.isAuthenticated()) {
        const {
            firstName = null,
            middleName = null,
            lastName = null,
            displayName = null,
            curPassword = null,
            newPassword = null,
            addr = null,
            tel = null,
            email = null
        }: UpdateUserProfileRequestSchema = req.body;
        
        const firstNameResult = vh.isNullOrUndefined(firstName) || vh.checkFirstName(firstName);
        const middleNameResult = vh.isNullOrUndefined(middleName) || vh.checkMiddleName(middleName);
        const lastNameResult = vh.isNullOrUndefined(lastName) || vh.checkLastName(lastName);
        const displayNameResult = vh.isNullOrUndefined(displayName) || vh.checkDisplayName(displayName);
        const curPasswordResult = vh.isNullOrUndefined(curPassword) || vh.checkEmpty(curPassword) || vh.checkPassword(curPassword);
        const newPasswordResult = vh.isNullOrUndefined(newPassword) || vh.checkEmpty(newPassword) || vh.checkPassword(newPassword);
        const addrResult = vh.isNullOrUndefined(addr) || vh.checkNotEmpty(addr);
        const telResult = vh.isNullOrUndefined(tel) || vh.checkTel(tel);
        const emailResult = vh.isNullOrUndefined(email) || vh.checkEmail(email);
        const passwordChangingResult = vh.isNullOrUndefined(curPassword) == vh.isNullOrUndefined(newPassword);

        const validationResult: boolean = 
            firstNameResult &&
            middleNameResult &&
            lastNameResult &&
            displayNameResult &&
            curPasswordResult &&
            newPasswordResult &&
            addrResult &&
            telResult &&
            emailResult &&
            passwordChangingResult;

        if (validationResult) {
            try {
                const session = await SessionDatabase.findByUserId(req.user.id);

                const isCurPasswordCorrect = await new Promise((resolve, reject) => {
                    if ((vh.isNullOrUndefined(curPassword) && vh.isNullOrUndefined(newPassword)) ||
                        (vh.checkEmpty(curPassword) && vh.checkEmpty(newPassword))) {
                        return resolve(true);
                    }
                    else {
                        bcrypt.compare(curPassword, session.password, function(err, success) {
                            if (err) {
                                return reject(err);
                            }
                            return resolve(success);
                        });
                    }
                });

                if (isCurPasswordCorrect) {
                    await getManager().transaction(async tem => {
                        await getManager()
                                .update(User, req.user.id, {
                                    firstName,
                                    middleName,
                                    lastName,
                                    displayName,
                                    tel,
                                    email
                                });
                        
                        await getManager()
                                .update(Address, { user: req.user.id }, <any>{
                                    address: addr
                                });

                        if (!!!vh.isNullOrUndefined(curPassword) && !!!vh.isNullOrUndefined(newPassword) &&
                            vh.checkNotEmpty(curPassword) && vh.checkNotEmpty(newPassword)) {
                            const salt = await bcrypt.genSalt(BCRYPT_SALT_ROUND);
                            const hash = await bcrypt.hash(newPassword, salt);
                            await getManager()
                                    .update(Session, req.user.id, {
                                        password: hash
                                    });
                        }
                    });
                    return res.sendStatus(200);
                } else {
                    return next(new ForbiddenError({
                        firstName: firstNameResult,
                        middleName: middleNameResult,
                        lastName: lastNameResult,
                        displayName: displayNameResult,
                        password: false,
                        addr: addrResult,
                        tel: telResult,
                        email: emailResult
                    }));
                }
            } catch (err) {
                return next(err);
            }
        } else {
            return next(new BadRequestError({
                firstName: firstNameResult,
                middleName: middleNameResult,
                lastName: lastNameResult,
                displayName: displayNameResult,
                password: (curPasswordResult && newPasswordResult) && (curPassword == newPassword),
                addr: addrResult,
                tel: telResult,
                email: emailResult
            }));
        }
    } else {
        return next(new UnauthorizedError());
    }
}