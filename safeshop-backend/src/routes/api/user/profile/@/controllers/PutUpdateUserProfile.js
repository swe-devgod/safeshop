"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const session_database_1 = require("../../../../../../database/repositories/session-database");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const validator_helper_1 = require("../../../../../../helper/validator-helper");
const typeorm_1 = require("typeorm");
const user_1 = require("../../../../../../database/models/user");
const session_1 = require("../../../../../../database/models/session");
const bcrypt = require("bcryptjs");
const bad_request_error_1 = require("../../../../../../errors/base/bad-request-error");
const forbidden_error_1 = require("../../../../../../errors/base/forbidden-error");
const address_1 = require("../../../../../../database/models/address");
const BCRYPT_SALT_ROUND = 10;
function PutUpdateUserProfile(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const { firstName = null, middleName = null, lastName = null, displayName = null, curPassword = null, newPassword = null, addr = null, tel = null, email = null } = req.body;
            const firstNameResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(firstName) || validator_helper_1.ValidatorHelper.checkFirstName(firstName);
            const middleNameResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(middleName) || validator_helper_1.ValidatorHelper.checkMiddleName(middleName);
            const lastNameResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(lastName) || validator_helper_1.ValidatorHelper.checkLastName(lastName);
            const displayNameResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(displayName) || validator_helper_1.ValidatorHelper.checkDisplayName(displayName);
            const curPasswordResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(curPassword) || validator_helper_1.ValidatorHelper.checkEmpty(curPassword) || validator_helper_1.ValidatorHelper.checkPassword(curPassword);
            const newPasswordResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(newPassword) || validator_helper_1.ValidatorHelper.checkEmpty(newPassword) || validator_helper_1.ValidatorHelper.checkPassword(newPassword);
            const addrResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(addr) || validator_helper_1.ValidatorHelper.checkNotEmpty(addr);
            const telResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(tel) || validator_helper_1.ValidatorHelper.checkTel(tel);
            const emailResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(email) || validator_helper_1.ValidatorHelper.checkEmail(email);
            const passwordChangingResult = validator_helper_1.ValidatorHelper.isNullOrUndefined(curPassword) == validator_helper_1.ValidatorHelper.isNullOrUndefined(newPassword);
            const validationResult = firstNameResult &&
                middleNameResult &&
                lastNameResult &&
                displayNameResult &&
                curPasswordResult &&
                newPasswordResult &&
                addrResult &&
                telResult &&
                emailResult &&
                passwordChangingResult;
            if (validationResult) {
                try {
                    const session = yield session_database_1.SessionDatabase.findByUserId(req.user.id);
                    const isCurPasswordCorrect = yield new Promise((resolve, reject) => {
                        if ((validator_helper_1.ValidatorHelper.isNullOrUndefined(curPassword) && validator_helper_1.ValidatorHelper.isNullOrUndefined(newPassword)) ||
                            (validator_helper_1.ValidatorHelper.checkEmpty(curPassword) && validator_helper_1.ValidatorHelper.checkEmpty(newPassword))) {
                            return resolve(true);
                        }
                        else {
                            bcrypt.compare(curPassword, session.password, function (err, success) {
                                if (err) {
                                    return reject(err);
                                }
                                return resolve(success);
                            });
                        }
                    });
                    if (isCurPasswordCorrect) {
                        yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                            yield typeorm_1.getManager()
                                .update(user_1.User, req.user.id, {
                                firstName,
                                middleName,
                                lastName,
                                displayName,
                                tel,
                                email
                            });
                            yield typeorm_1.getManager()
                                .update(address_1.Address, { user: req.user.id }, {
                                address: addr
                            });
                            if (!!!validator_helper_1.ValidatorHelper.isNullOrUndefined(curPassword) && !!!validator_helper_1.ValidatorHelper.isNullOrUndefined(newPassword) &&
                                validator_helper_1.ValidatorHelper.checkNotEmpty(curPassword) && validator_helper_1.ValidatorHelper.checkNotEmpty(newPassword)) {
                                const salt = yield bcrypt.genSalt(BCRYPT_SALT_ROUND);
                                const hash = yield bcrypt.hash(newPassword, salt);
                                yield typeorm_1.getManager()
                                    .update(session_1.Session, req.user.id, {
                                    password: hash
                                });
                            }
                        }));
                        return res.sendStatus(200);
                    }
                    else {
                        return next(new forbidden_error_1.ForbiddenError({
                            firstName: firstNameResult,
                            middleName: middleNameResult,
                            lastName: lastNameResult,
                            displayName: displayNameResult,
                            password: false,
                            addr: addrResult,
                            tel: telResult,
                            email: emailResult
                        }));
                    }
                }
                catch (err) {
                    return next(err);
                }
            }
            else {
                return next(new bad_request_error_1.BadRequestError({
                    firstName: firstNameResult,
                    middleName: middleNameResult,
                    lastName: lastNameResult,
                    displayName: displayNameResult,
                    password: (curPasswordResult && newPasswordResult) && (curPassword == newPassword),
                    addr: addrResult,
                    tel: telResult,
                    email: emailResult
                }));
            }
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PutUpdateUserProfile = PutUpdateUserProfile;
