import { crc32 } from 'crc';
import { IncomingForm } from 'formidable'; 

import * as validator from 'validator';
import { CommentDatabase } from '../../../../../../database/repositories/comment-database';
import { TopicComment } from '../../../../../../database/models/topic-comment';
import { UnauthorizedError } from '../../../../../../errors/base/unauthorized-error';
import { BadRequestError } from '../../../../../../errors/base/bad-request-error';
import { DateTimeHelper } from '../../../../../../helper/datetime-helper';
import { getRandomInt } from '../../../../../../helper/utils';
import { ValidatorHelper } from '../../../../../../helper/validator-helper';
import { getManager } from 'typeorm';
import { fromAssert } from '../../../../../../helper/saefshop-path-helper';
import { FileHelper } from '../../../../../../helper/file-helper';
import { CommentPictures } from '../../../../../../database/models/comment-pictures';
import { UserPicture } from '../../../../../../database/models/user-picture';
import { User } from '../../../../../../database/models/user';

interface PostUpdateUserProfilePictureUploadImageSchema {
    src: string,
    dest: string
}

function resolveFileName(fileName: string, timestamp: number): string {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${getRandomInt(1000, 9999)}-` + crc32(timestamp + fileName + getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}

export async function PostUpdateUserProfilePicture(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;

        const form = new IncomingForm();
        form.keepExtensions = true;
        form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
        form.multiples = false;

        let picture = { } as PostUpdateUserProfilePictureUploadImageSchema;

        form
        .on('field', function(name, value) {

        })
        .on('fileBegin', function(name, file) {

        })
        .on('file', function(name, file) {
            if (name == "picture") {
                picture.src = file.path;
                picture.dest = resolveFileName(file.name, req.timestamp);
            }
        })
        .on('abort', function() {

        })
        .on('error', function(err) {
            console.error(err);
            return next(err);
        })
        .on('end', async function() {
            await FileHelper.copy(picture.src, fromAssert(`static/users/${picture.dest}`));
            const userPicture = await getManager()
                .createQueryBuilder()
                .select('A')
                .from(UserPicture, 'A')
                .where("A.userId = :userId", { userId })
                .getOne();

            if (!!!userPicture) {
                await getManager()
                    .createQueryBuilder()
                    .insert()
                    .into(UserPicture)
                    .values([
                        { user: { id: userId } as User, fileName: picture.dest }
                    ])
                    .execute();
            } else {
                userPicture.fileName = picture.dest;
                await getManager().save(userPicture);
            }

            res.sendStatus(200);
        })
        .parse(req);
    } else {
        next(new UnauthorizedError());
    }
}