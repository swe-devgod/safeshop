"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crc_1 = require("crc");
const formidable_1 = require("formidable");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const utils_1 = require("../../../../../../helper/utils");
const typeorm_1 = require("typeorm");
const saefshop_path_helper_1 = require("../../../../../../helper/saefshop-path-helper");
const file_helper_1 = require("../../../../../../helper/file-helper");
const user_picture_1 = require("../../../../../../database/models/user-picture");
function resolveFileName(fileName, timestamp) {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${utils_1.getRandomInt(1000, 9999)}-` + crc_1.crc32(timestamp + fileName + utils_1.getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}
function PostUpdateUserProfilePicture(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const form = new formidable_1.IncomingForm();
            form.keepExtensions = true;
            form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
            form.multiples = false;
            let picture = {};
            form
                .on('field', function (name, value) {
            })
                .on('fileBegin', function (name, file) {
            })
                .on('file', function (name, file) {
                if (name == "picture") {
                    picture.src = file.path;
                    picture.dest = resolveFileName(file.name, req.timestamp);
                }
            })
                .on('abort', function () {
            })
                .on('error', function (err) {
                console.error(err);
                return next(err);
            })
                .on('end', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    yield file_helper_1.FileHelper.copy(picture.src, saefshop_path_helper_1.fromAssert(`static/users/${picture.dest}`));
                    const userPicture = yield typeorm_1.getManager()
                        .createQueryBuilder()
                        .select('A')
                        .from(user_picture_1.UserPicture, 'A')
                        .where("A.userId = :userId", { userId })
                        .getOne();
                    if (!!!userPicture) {
                        yield typeorm_1.getManager()
                            .createQueryBuilder()
                            .insert()
                            .into(user_picture_1.UserPicture)
                            .values([
                            { user: { id: userId }, fileName: picture.dest }
                        ])
                            .execute();
                    }
                    else {
                        userPicture.fileName = picture.dest;
                        yield typeorm_1.getManager().save(userPicture);
                    }
                    res.sendStatus(200);
                });
            })
                .parse(req);
        }
        else {
            next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostUpdateUserProfilePicture = PostUpdateUserProfilePicture;
