import { UserDatabase } from "../../../../../../database/repositories/user-database";
import { SessionDatabase } from "../../../../../../database/repositories/session-database";
import { AddressDatabase } from "../../../../../../database/repositories/address-database";
import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { GlobalVar } from "../../../../../../helper/global-var";
import { ValidatorHelper } from "../../../../../../helper/validator-helper";
import { ClientURLHelper } from "../../../../../../helper/client-url-helper";

export async function GetUserProfile(req, res, next) {
    if (req.isAuthenticated()) {
        const user = await UserDatabase.findByUserId(req.user.id);
        const session = await SessionDatabase.findByUserId(req.user.id);
        const defaultAddress = await AddressDatabase.findDefaultAddressByUserId(req.user.id);
        const address = await AddressDatabase.findAddressById(defaultAddress.addressId);
        const userPicture = await UserDatabase.fincUserPictureByUserId(req.user.id);

        res.json({
            username: session.username,
            firstName: user.firstName,
            middleName: user.middleName,
            lastName: user.lastName,
            displayName: user.displayName,
            address: address.address,
            tel: user.tel,
            email: user.email,
            registerDate: user.registerDate,
            picture: ClientURLHelper.resolve('/static/users/' + userPicture.fileName)
        });
    } else {
        return next(new UnauthorizedError());
    }
}