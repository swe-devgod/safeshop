"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_database_1 = require("../../../../../../database/repositories/user-database");
const session_database_1 = require("../../../../../../database/repositories/session-database");
const address_database_1 = require("../../../../../../database/repositories/address-database");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const client_url_helper_1 = require("../../../../../../helper/client-url-helper");
function GetUserProfile(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const user = yield user_database_1.UserDatabase.findByUserId(req.user.id);
            const session = yield session_database_1.SessionDatabase.findByUserId(req.user.id);
            const defaultAddress = yield address_database_1.AddressDatabase.findDefaultAddressByUserId(req.user.id);
            const address = yield address_database_1.AddressDatabase.findAddressById(defaultAddress.addressId);
            const userPicture = yield user_database_1.UserDatabase.fincUserPictureByUserId(req.user.id);
            res.json({
                username: session.username,
                firstName: user.firstName,
                middleName: user.middleName,
                lastName: user.lastName,
                displayName: user.displayName,
                address: address.address,
                tel: user.tel,
                email: user.email,
                registerDate: user.registerDate,
                picture: client_url_helper_1.ClientURLHelper.resolve('/static/users/' + userPicture.fileName)
            });
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.GetUserProfile = GetUserProfile;
