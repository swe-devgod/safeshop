"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetUserProfile_1 = require("./controllers/GetUserProfile");
const PutUpdateUserProfile_1 = require("./controllers/PutUpdateUserProfile");
const PostUpdateUserProfilePicture_1 = require("./controllers/PostUpdateUserProfilePicture");
exports.profileRoutes = {
    name: "profile",
    routes: [
        {
            path: '',
            method: 'get',
            action: GetUserProfile_1.GetUserProfile
        },
        {
            path: '',
            method: 'put',
            action: PutUpdateUserProfile_1.PutUpdateUserProfile
        },
        {
            path: '/image',
            method: 'post',
            action: PostUpdateUserProfilePicture_1.PostUpdateUserProfilePicture
        }
    ]
};
