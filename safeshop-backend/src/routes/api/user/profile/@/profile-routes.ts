import { GetUserProfile } from './controllers/GetUserProfile';
import { PutUpdateUserProfile } from './controllers/PutUpdateUserProfile';
import { PostUpdateUserProfilePicture } from './controllers/PostUpdateUserProfilePicture';

export const profileRoutes = {
    name: "profile",
    routes: [
        {
            path: '',
            method: 'get',
            action: GetUserProfile
        },
        {
            path: '',
            method: 'put',
            action: PutUpdateUserProfile
        },
        {
            path: '/image',
            method: 'post',
            action: PostUpdateUserProfilePicture
        }
    ]
};
