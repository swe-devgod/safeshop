"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const messenger_room_1 = require("../../../../../database/models/messenger-room");
const room_users_1 = require("../../../../../database/models/room-users");
function PostTryCreateNewRoom(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const curUserId = req.user.id;
            const { destUserId } = req.body;
            if (curUserId != destUserId) {
                var result = yield typeorm_1.getManager()
                    .createQueryBuilder()
                    .select('A.roomId')
                    .from(room_users_1.RoomUsers, 'A')
                    .innerJoinAndSelect(room_users_1.RoomUsers, 'B', 'A.roomId = B.roomId AND B.userId = :b', { b: destUserId })
                    .where('A.userId = :a', { a: curUserId })
                    .limit(1)
                    .getOne();
            }
            else {
                var result = yield typeorm_1.getManager()
                    .createQueryBuilder()
                    .select('A.roomId')
                    .from(room_users_1.RoomUsers, 'A')
                    .leftJoin(room_users_1.RoomUsers, 'B', 'A.roomId = B.roomId AND A.userId != B.userId')
                    .where('A.userId = :a AND B.userId IS null', { a: curUserId })
                    .limit(1)
                    .getOne();
            }
            if (!result) {
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    const newRoom = new messenger_room_1.MessengerRoom();
                    newRoom.timestamp = req.timestamp;
                    var newRoomResult = yield tem.save(newRoom);
                    result = new room_users_1.RoomUsers();
                    result.roomId = newRoomResult.id;
                    if (curUserId == destUserId) {
                        const roomUsers = new room_users_1.RoomUsers();
                        roomUsers.userId = curUserId;
                        roomUsers.roomId = newRoomResult.id;
                        yield tem.save(roomUsers);
                    }
                    else {
                        yield tem.createQueryBuilder()
                            .insert()
                            .into(room_users_1.RoomUsers)
                            .values([
                            { roomId: newRoomResult.id, userId: curUserId },
                            { roomId: newRoomResult.id, userId: destUserId }
                        ])
                            .execute();
                    }
                }));
            }
            res.json(result);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostTryCreateNewRoom = PostTryCreateNewRoom;
