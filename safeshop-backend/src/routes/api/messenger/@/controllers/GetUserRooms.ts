import { MessengerDatabase } from "../../../../../database/repositories/messenger-database";
import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { RoomUsers } from "../../../../../database/models/room-users";
import { User } from "../../../../../database/models/user";
import { MessengerRoom } from "../../../../../database/models/messenger-room";
import { Message } from "../../../../../database/models/message";
import { UserPicture } from "../../../../../database/models/user-picture";
import { GlobalVar } from "../../../../../helper/global-var";
import { ClientURLHelper } from "../../../../../helper/client-url-helper";

export async function GetUserRooms(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        // const result = await getManager().query(
        // `
        //     SELECT
        //         A.userId as userId, 
        //         A.messengerRoomId as roomId, 
        //         D.displayName as displayName, 
        //         E.timestamp as timestamp, 
        //         E.userId as ownerMsgId, 
        //         E.message as message 
        //     FROM messenger_room_users_user as A
        //     INNER JOIN messenger_room as C ON A.messengerRoomId = C.id
        //     INNER JOIN user as D ON A.userId = D.id
        //     INNER JOIN message as E ON C.timestamp = E.timestamp AND C.id = E.roomId
        //     WHERE A.userId != ? AND A.messengerRoomId IN (
        //         SELECT B.messengerRoomId FROM messenger_room_users_user as B
        //         WHERE B.userId = ?
        //     )
        //     ORDER BY C.timestamp
        //     LIMIT 10
        // `, [ userId, userId ]);

        let rooms = await getManager()
            .createQueryBuilder()
            .select('C.id as userId, C.displayName as displayName, D.id as roomId, D.timestamp as timestamp, E.userId as ownerMsgId, E.message as message, F.fileName as userPicture')
            .from(RoomUsers, 'A')
            .leftJoin(RoomUsers, 'B', 'A.roomId = B.roomId AND B.userId != A.userId')
            .innerJoin(User, 'C', 'C.id = coalesce(B.userId, A.userId)')
            .innerJoin(MessengerRoom, 'D', 'D.id = coalesce(B.roomId, A.roomId)')
            .leftJoin(Message, 'E', 'E.roomId = D.id AND E.timestamp = D.timestamp')
            .innerJoin(UserPicture, 'F', 'C.id = F.userId')
            .where('A.userId = :id', { id : userId })
            .orderBy('D.timestamp', 'DESC')
            .getRawMany();

        rooms.map(room => {
            room.userPicture = ClientURLHelper.resolve(`/static/users/${room.userPicture}`);
        })

        res.json(rooms);

    } else {
        return next(new UnauthorizedError());
    }
}