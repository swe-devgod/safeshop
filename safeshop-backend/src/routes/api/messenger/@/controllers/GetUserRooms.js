"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const room_users_1 = require("../../../../../database/models/room-users");
const user_1 = require("../../../../../database/models/user");
const messenger_room_1 = require("../../../../../database/models/messenger-room");
const message_1 = require("../../../../../database/models/message");
const user_picture_1 = require("../../../../../database/models/user-picture");
const client_url_helper_1 = require("../../../../../helper/client-url-helper");
function GetUserRooms(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            // const result = await getManager().query(
            // `
            //     SELECT
            //         A.userId as userId, 
            //         A.messengerRoomId as roomId, 
            //         D.displayName as displayName, 
            //         E.timestamp as timestamp, 
            //         E.userId as ownerMsgId, 
            //         E.message as message 
            //     FROM messenger_room_users_user as A
            //     INNER JOIN messenger_room as C ON A.messengerRoomId = C.id
            //     INNER JOIN user as D ON A.userId = D.id
            //     INNER JOIN message as E ON C.timestamp = E.timestamp AND C.id = E.roomId
            //     WHERE A.userId != ? AND A.messengerRoomId IN (
            //         SELECT B.messengerRoomId FROM messenger_room_users_user as B
            //         WHERE B.userId = ?
            //     )
            //     ORDER BY C.timestamp
            //     LIMIT 10
            // `, [ userId, userId ]);
            let rooms = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('C.id as userId, C.displayName as displayName, D.id as roomId, D.timestamp as timestamp, E.userId as ownerMsgId, E.message as message, F.fileName as userPicture')
                .from(room_users_1.RoomUsers, 'A')
                .leftJoin(room_users_1.RoomUsers, 'B', 'A.roomId = B.roomId AND B.userId != A.userId')
                .innerJoin(user_1.User, 'C', 'C.id = coalesce(B.userId, A.userId)')
                .innerJoin(messenger_room_1.MessengerRoom, 'D', 'D.id = coalesce(B.roomId, A.roomId)')
                .leftJoin(message_1.Message, 'E', 'E.roomId = D.id AND E.timestamp = D.timestamp')
                .innerJoin(user_picture_1.UserPicture, 'F', 'C.id = F.userId')
                .where('A.userId = :id', { id: userId })
                .orderBy('D.timestamp', 'DESC')
                .getRawMany();
            rooms.map(room => {
                room.userPicture = client_url_helper_1.ClientURLHelper.resolve(`/static/users/${room.userPicture}`);
            });
            res.json(rooms);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.GetUserRooms = GetUserRooms;
