"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
const message_1 = require("../../../../../database/models/message");
function PostFetchRoomMessages(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const { timestamp = null } = req.body;
            if (timestamp == null) {
                return next(new bad_request_error_1.BadRequestError());
            }
            const roomId = +req.params.id;
            // let lastestMessage = await getManager()
            //     .createQueryBuilder()
            //     .select(['msgRoom', 'msg.userId', 'msg.message', 'msg.timestamp'])
            //     .from(MessengerRoom, 'msgRoom')
            //     .innerJoin('msgRoom.messages', 'msg', 'msg.timestamp > :timestamp', { timestamp: req.body.timestamp })
            //     .where('msgRoom.id = :roomId', { roomId: +req.params.id })
            //     .orderBy('msg.timestamp', 'DESC')
            //     .limit(50)
            //     .getOne();
            // let raw = await getManager().query(
            //     `
            //         SELECT 
            //             userId,
            //             timestamp,
            //             message
            //         FROM message
            //         WHERE roomId = ? AND timestamp > ?
            //     `, [ +req.params.id, req.body.timestamp ]);
            const lastestMessage = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select(['msg.userId', 'msg.timestamp', 'msg.message'])
                .from(message_1.Message, 'msg')
                .where('msg.roomId = :id AND msg.timestamp > :timestamp', { id: roomId, timestamp: timestamp })
                .orderBy('msg.timestamp', 'DESC')
                .limit(50)
                .getMany();
            if (lastestMessage != undefined) {
                res.json(lastestMessage);
            }
            else {
                res.json([]);
            }
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostFetchRoomMessages = PostFetchRoomMessages;
