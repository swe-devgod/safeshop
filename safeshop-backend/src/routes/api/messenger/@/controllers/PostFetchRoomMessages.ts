import { MessengerDatabase } from "../../../../../database/repositories/messenger-database";
import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { MessengerRoom } from "../../../../../database/models/messenger-room";
import { BadRequestError } from "../../../../../errors/base/bad-request-error";
import { Message } from "../../../../../database/models/message";

export async function PostFetchRoomMessages(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const { timestamp = null }  = req.body;
        
        if (timestamp == null) {
            return next(new BadRequestError());
        }
        
        const roomId = +req.params.id;
        // let lastestMessage = await getManager()
        //     .createQueryBuilder()
        //     .select(['msgRoom', 'msg.userId', 'msg.message', 'msg.timestamp'])
        //     .from(MessengerRoom, 'msgRoom')
        //     .innerJoin('msgRoom.messages', 'msg', 'msg.timestamp > :timestamp', { timestamp: req.body.timestamp })
        //     .where('msgRoom.id = :roomId', { roomId: +req.params.id })
        //     .orderBy('msg.timestamp', 'DESC')
        //     .limit(50)
        //     .getOne();

        // let raw = await getManager().query(
        //     `
        //         SELECT 
        //             userId,
        //             timestamp,
        //             message
        //         FROM message
        //         WHERE roomId = ? AND timestamp > ?
        //     `, [ +req.params.id, req.body.timestamp ]);

        const lastestMessage = await getManager()
            .createQueryBuilder()
            .select([ 'msg.userId', 'msg.timestamp', 'msg.message'])
            .from(Message, 'msg')
            .where('msg.roomId = :id AND msg.timestamp > :timestamp', { id: roomId, timestamp: timestamp })
            .orderBy('msg.timestamp', 'DESC')
            .limit(50)
            .getMany();

        if (lastestMessage != undefined) {
            res.json(lastestMessage);
        } else {
            res.json([]);
        }
    } else {
        return next(new UnauthorizedError());
    }
}