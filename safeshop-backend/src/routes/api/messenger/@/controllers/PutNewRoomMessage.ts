import { MessengerDatabase } from "../../../../../database/repositories/messenger-database";
import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager, InsertQueryBuilder, Timestamp } from "typeorm";
import { MessengerRoom } from "../../../../../database/models/messenger-room";
import { BadRequestError } from "../../../../../errors/base/bad-request-error";
import { Message } from "../../../../../database/models/message";

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export async function PutNewRoomMessage(req, res, next) {
    if (req.isAuthenticated()) {
        const { message = '' } = req.body;
        const roomId = parseInt(req.params.id);
        if (message == '' ||  isNaN(roomId)) {
            return next(new BadRequestError());
        }
        const userId = req.user.id;

        await getManager().transaction(async tem => {
            const msg = new Message();
            msg.message = message;
            msg.roomId = roomId;
            msg.userId = userId;
            msg.timestamp = req.timestamp;
            await getManager().save(msg);

            await tem.createQueryBuilder()
                .update(MessengerRoom)
                .set(<MessengerRoom>{ timestamp: msg.timestamp })
                .where('id = :id', { id: roomId })
                .execute();

            res.json({
                timestamp: req.timestamp
            });
        });

        // if (qb == null) {
        //     qb = getManager()
        //         .createQueryBuilder()
        //         .insert()
        //         .into(Message);
        // }
        // await qb
        //     .values([
        //         { message: message, roomId: roomId , userId: userId, timestamp: req.timestamp }
        //     ])
        //     .execute();        
    } else {
        return next(new UnauthorizedError());
    }
}