"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const messenger_room_1 = require("../../../../../database/models/messenger-room");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
const message_1 = require("../../../../../database/models/message");
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function PutNewRoomMessage(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const { message = '' } = req.body;
            const roomId = parseInt(req.params.id);
            if (message == '' || isNaN(roomId)) {
                return next(new bad_request_error_1.BadRequestError());
            }
            const userId = req.user.id;
            yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                const msg = new message_1.Message();
                msg.message = message;
                msg.roomId = roomId;
                msg.userId = userId;
                msg.timestamp = req.timestamp;
                yield typeorm_1.getManager().save(msg);
                yield tem.createQueryBuilder()
                    .update(messenger_room_1.MessengerRoom)
                    .set({ timestamp: msg.timestamp })
                    .where('id = :id', { id: roomId })
                    .execute();
                res.json({
                    timestamp: req.timestamp
                });
            }));
            // if (qb == null) {
            //     qb = getManager()
            //         .createQueryBuilder()
            //         .insert()
            //         .into(Message);
            // }
            // await qb
            //     .values([
            //         { message: message, roomId: roomId , userId: userId, timestamp: req.timestamp }
            //     ])
            //     .execute();        
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PutNewRoomMessage = PutNewRoomMessage;
