import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { MessengerRoom } from "../../../../../database/models/messenger-room";
import { RoomUsers } from "../../../../../database/models/room-users";

export async function PostTryCreateNewRoom(req, res, next) {
    if (req.isAuthenticated()) {
        const curUserId = req.user.id;
        const { destUserId } = req.body;

        if (curUserId != destUserId) {
            var result = await getManager()
                .createQueryBuilder()
                .select('A.roomId')
                .from(RoomUsers, 'A')
                .innerJoinAndSelect(RoomUsers, 'B', 'A.roomId = B.roomId AND B.userId = :b', { b: destUserId })
                .where('A.userId = :a', { a: curUserId })
                .limit(1)
                .getOne();
        } else {
            var result = await getManager()
                .createQueryBuilder()
                .select('A.roomId')
                .from(RoomUsers, 'A')
                .leftJoin(RoomUsers, 'B', 'A.roomId = B.roomId AND A.userId != B.userId')
                .where('A.userId = :a AND B.userId IS null', { a: curUserId })
                .limit(1)
                .getOne();
        }

        if (!result) {
            await getManager().transaction(async tem => {
                const newRoom = new MessengerRoom();
                newRoom.timestamp = req.timestamp;
                var newRoomResult = await tem.save(newRoom);

                result = new RoomUsers();
                result.roomId = newRoomResult.id;
                
                if (curUserId == destUserId) {
                    const roomUsers = new RoomUsers();
                    roomUsers.userId = curUserId;
                    roomUsers.roomId = newRoomResult.id;
                    await tem.save(roomUsers);
                } else {
                    await tem.createQueryBuilder()
                        .insert()
                        .into(RoomUsers)
                        .values([
                            <RoomUsers>{ roomId: newRoomResult.id, userId: curUserId },
                            <RoomUsers>{ roomId: newRoomResult.id, userId: destUserId }
                        ])
                        .execute();
                }
            });
        }
        res.json(result);
    } else {
        return next(new UnauthorizedError());
    }
}