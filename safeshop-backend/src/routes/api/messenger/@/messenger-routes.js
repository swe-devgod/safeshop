"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetUserRooms_1 = require("./controllers/GetUserRooms");
const PostFetchRoomMessages_1 = require("./controllers/PostFetchRoomMessages");
const PutNewRoomMessage_1 = require("./controllers/PutNewRoomMessage");
const PostTryCreateNewRoom_1 = require("./controllers/PostTryCreateNewRoom");
exports.messengerRoutes = {
    name: "messenger",
    routes: [
        {
            path: '/lists',
            method: 'get',
            action: GetUserRooms_1.GetUserRooms
        },
        {
            path: '/new',
            method: 'post',
            action: PostTryCreateNewRoom_1.PostTryCreateNewRoom
        },
        {
            path: '/:id(\\d+?)',
            method: 'post',
            action: PostFetchRoomMessages_1.PostFetchRoomMessages
        },
        {
            path: '/:id(\\d+?)',
            method: 'put',
            action: PutNewRoomMessage_1.PutNewRoomMessage
        }
    ]
};
