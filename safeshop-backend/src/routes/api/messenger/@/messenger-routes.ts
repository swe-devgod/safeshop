import { GetUserRooms } from "./controllers/GetUserRooms";
import { PostFetchRoomMessages } from "./controllers/PostFetchRoomMessages";
import { PutNewRoomMessage } from "./controllers/PutNewRoomMessage";
import { PostTryCreateNewRoom } from "./controllers/PostTryCreateNewRoom";

export const messengerRoutes = {
    name: "messenger",
    routes: [
        {
            path: '/lists',
            method: 'get',
            action: GetUserRooms
        },
        {
            path: '/new',
            method: 'post',
            action: PostTryCreateNewRoom
        },
        {
            path: '/:id(\\d+?)',
            method: 'post',
            action: PostFetchRoomMessages
        },
        {
            path: '/:id(\\d+?)',
            method: 'put',
            action: PutNewRoomMessage
        }
    ]
};