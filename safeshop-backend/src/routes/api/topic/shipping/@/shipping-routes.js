"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetShoppingCarrierList_1 = require("./controllers/GetShoppingCarrierList");
exports.topicShippingRoutes = {
    name: "shipping",
    routes: [
        {
            path: '/list',
            method: 'get',
            action: GetShoppingCarrierList_1.GetShippingCarrierList
        }
    ]
};
