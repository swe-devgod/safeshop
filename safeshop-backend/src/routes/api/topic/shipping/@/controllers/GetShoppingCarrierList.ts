import { getManager } from "typeorm";
import { TopicShipping } from "../../../../../../database/models/topic-shipping";
import { ShippingCarrier } from "../../../../../../database/models/shipping-carrier";

export async function GetShippingCarrierList(req, res, next) {
    const shippings = await getManager()
        .find(ShippingCarrier);
    res.json(shippings);
}