import { GetShippingCarrierList } from "./controllers/GetShoppingCarrierList";

export const topicShippingRoutes = {
    name: "shipping",
    routes: [
        {
            path: '/list',
            method: 'get',
            action: GetShippingCarrierList
        }
    ]
};