"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetTopicContent_1 = require("./controllers/GetTopicContent");
const comment_routes_1 = require("../comment/@/comment-routes");
const route_helper_1 = require("../../../../helper/route-helper");
const PostAddNewTopic_1 = require("./controllers/PostAddNewTopic");
const shipping_routes_1 = require("../shipping/@/shipping-routes");
const PatchEditTopicContent_1 = require("./controllers/PatchEditTopicContent");
const DeleteTopic_1 = require("./controllers/DeleteTopic");
exports.topicRoutes = {
    name: "topic",
    routes: [
        {
            path: '/:id(\\d+?)',
            method: 'get',
            action: GetTopicContent_1.GetTopicContent
        },
        {
            path: '',
            method: 'post',
            action: PostAddNewTopic_1.PostAddNewTopic
        },
        {
            path: '/:id(\\d+?)',
            method: 'patch',
            action: PatchEditTopicContent_1.PatchEditTopicContent
        },
        {
            path: '/:id(\\d+?)',
            method: 'delete',
            action: DeleteTopic_1.DeleteTopic
        }
    ]
};
route_helper_1.appendRoutes(comment_routes_1.topicCommentRoutes, exports.topicRoutes);
route_helper_1.attachRoutesToParent(shipping_routes_1.topicShippingRoutes, exports.topicRoutes);
