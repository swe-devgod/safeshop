"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crc_1 = require("crc");
const formidable_1 = require("formidable");
const typeorm_1 = require("typeorm");
const topic_1 = require("../../../../../database/models/topic");
const topic_shipping_1 = require("../../../../../database/models/topic-shipping");
const topic_pictures_1 = require("../../../../../database/models/topic-pictures");
const datetime_helper_1 = require("../../../../../helper/datetime-helper");
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const file_helper_1 = require("../../../../../helper/file-helper");
const topic_thumbnail_1 = require("../../../../../database/models/topic-thumbnail");
const saefshop_path_helper_1 = require("../../../../../helper/saefshop-path-helper");
const utils_1 = require("../../../../../helper/utils");
function resolveFileName(fileName, timestamp) {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${utils_1.getRandomInt(1000, 9999)}-` + crc_1.crc32(timestamp + fileName + utils_1.getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}
function PostAddNewTopic(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const form = new formidable_1.IncomingForm();
            form.keepExtensions = true;
            form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
            form.multiples = false;
            const fromData = {};
            const uploadPictures = [];
            form
                .on('field', function (name, value) {
                console.log(name, value);
                switch (name) {
                    case 'topicShipping':
                    case 'topicCategory':
                    case 'topicThumbnail':
                        fromData[name] = JSON.parse(value);
                        break;
                    default:
                        fromData[name] = value;
                        break;
                }
            })
                .on('fileBegin', function (name, file) {
                // if (name == 'topicPictures') {
                //     uploadPictures.push({ 
                //         name: file.name,
                //         type: file.type,
                //         size: file.size,
                //         src: file.path, 
                //         dest: resolveFileName(file.name, req.timestamp) 
                //     });
                // }
            })
                .on('file', function (name, file) {
                if (name == 'topicPictures') {
                    uploadPictures.push({
                        name: file.name,
                        type: file.type,
                        size: file.size,
                        src: file.path,
                        dest: resolveFileName(file.name, req.timestamp)
                    });
                }
            })
                .on('abort', function () {
            })
                .on('error', function (err) {
                console.error(err);
                return next(err);
            })
                .on('end', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    const { topicName = '', topicDescription, topicPrice, topicRemainingItem, topicShipping, topicCategory } = fromData;
                    console.log(fromData);
                    //console.log(uploadPictures);
                    const id = yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                        const topic = new topic_1.Topic();
                        topic.userId = req.user.id;
                        topic.topicName = topicName;
                        topic.topicDescription = topicDescription;
                        topic.topicPrice = topicPrice;
                        topic.topicRemainingItems = topicRemainingItem,
                            topic.topicCreatedDate = datetime_helper_1.DateTimeHelper.getCurrentTimestamp();
                        topic.categories = topicCategory.map(c => ({ id: c.id }));
                        yield tem.save(topic);
                        const isDestFolderExists = yield file_helper_1.FileHelper.exists(saefshop_path_helper_1.fromAssert(`static/topics/${topic.id}/`));
                        if (!!!isDestFolderExists) {
                            yield file_helper_1.FileHelper.mkdir(saefshop_path_helper_1.fromAssert(`static/topics/${topic.id}/`));
                        }
                        for (const picture of uploadPictures) {
                            yield file_helper_1.FileHelper.copy(picture.src, saefshop_path_helper_1.fromAssert(`static/topics/${topic.id}/${picture.dest}`));
                        }
                        const topicPictures = yield tem.save(uploadPictures.map(picture => {
                            const pic = tem.create(topic_pictures_1.TopicPictures);
                            pic.fileName = picture.dest;
                            pic.topicId = topic.id;
                            return pic;
                        }));
                        const topicThumbnail = fromData.topicThumbnail;
                        let isThumbnailFound = false;
                        for (const tp of topicPictures) {
                            if (isThumbnailFound) {
                                break;
                            }
                            for (const up of uploadPictures) {
                                if (tp.fileName == up.dest) {
                                    if (up.name == topicThumbnail.name &&
                                        up.type == topicThumbnail.type &&
                                        up.size == topicThumbnail.size) {
                                        const thumbnail = tem.create(topic_thumbnail_1.TopicThumbnail);
                                        thumbnail.topicId = topic.id;
                                        thumbnail.topicPictureId = tp.id;
                                        yield tem.save(thumbnail);
                                        isThumbnailFound = true;
                                    }
                                    break;
                                }
                            }
                        }
                        // await tem.createQueryBuilder()
                        //     .insert()
                        //     .into(TopicPictures)
                        //     .values(uploadPictures.map(picture => (<TopicPictures>{
                        //         fileName: picture.dest,
                        //         topicId: topic.id
                        //     })))
                        //     .execute();
                        yield tem.createQueryBuilder()
                            .insert()
                            .into(topic_shipping_1.TopicShipping)
                            .values(topicShipping.map(s => ({
                            topicId: topic.id,
                            shippingId: s.id,
                            price: s.price
                        })))
                            .execute();
                        return topic.id;
                    }));
                    res.json({ topicId: id });
                });
            });
            form.parse(req);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostAddNewTopic = PostAddNewTopic;
