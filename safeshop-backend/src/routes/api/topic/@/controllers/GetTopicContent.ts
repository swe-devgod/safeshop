import { UserDatabase } from '../../../../../database/repositories/user-database';
import { TopicDatabase } from '../../../../../database/repositories/topic-database';
import { BadRequestError } from '../../../../../errors/base/bad-request-error';
import { TopicNotFoundError } from '../../../../../errors/topic-not-found-error';
import { getManager } from 'typeorm';
import { TopicShipping } from '../../../../../database/models/topic-shipping';
import { ShippingCarrier } from '../../../../../database/models/shipping-carrier';
import { ClientURLHelper } from '../../../../../helper/client-url-helper';

interface GetTopicContentRequestSchema {
    id: number;
}

interface GetTopicContentShippingSchema {
    id: number;
    price: number;
    name: string;
}

interface GetTopicContentResponseSchema {
    productName: string,
    productDescription: string,
    productPrice: number,
    createdDate: number,
    topicRating: number,
    topicRatingCount: number,
    ownerUserId: number,
    ownerName: string,
    ownerPicture: string,
    ownerRating: number,
    ownerRatingCount: number,
    remainingItems: number,
    imgs: any[],
    categories: any,
    shippings: GetTopicContentShippingSchema[]
}

export async function GetTopicContent(req, res, next) {
    const { id } : GetTopicContentRequestSchema = req.params;

    const topic = await TopicDatabase.findTopicById(id, { withCategories: true });
    if (!!!topic || topic.topicRemainingItems <= 0) {
        return next(new TopicNotFoundError());
    }

    const user = await UserDatabase.findByUserId(topic.userId);
    if (!!!user) {
        return next(new BadRequestError());
    }

    const userPicture = await UserDatabase.fincUserPictureByUserId(topic.userId);
    const topicPictures = await TopicDatabase.findPicturesByTopic(topic);

    const shippings = await getManager()
        .createQueryBuilder()
        .select(['B.id as id', 'A.price as price', 'B.name as name'])
        .from(TopicShipping, 'A')
        .innerJoin(ShippingCarrier, 'B', 'B.id = A.shippingId')
        .where('A.topicId = :id', { id })
        .getRawMany();

    const result: GetTopicContentResponseSchema = {
        productName: topic.topicName,
        productDescription: topic.topicDescription,
        productPrice: topic.topicPrice,
        createdDate: topic.topicCreatedDate,
        topicRating: topic.topicRating,
        topicRatingCount: topic.topicVoteCount,
        ownerUserId: user.id,
        ownerName: user.displayName,
        ownerRating: user.rating,
        ownerPicture: ClientURLHelper.resolve('/static/users/' + userPicture.fileName),
        ownerRatingCount: user.ratingVoteCount,
        remainingItems: topic.topicRemainingItems,
        imgs: topicPictures.map(tp => ({ 
            id: tp.id,
            url: ClientURLHelper.resolve(`/static/topics/${id}/` + tp.fileName) 
        })),
        categories: topic.categories,
        shippings
    };
    
    res.json(result);
}