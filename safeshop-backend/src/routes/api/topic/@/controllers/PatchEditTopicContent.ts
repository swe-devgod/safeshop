import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager, Timestamp } from "typeorm";
import { Topic } from "../../../../../database/models/topic";
import { crc32 } from 'crc';
import { IncomingForm } from 'formidable'; 
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { Category } from "../../../../../database/models/category";
import { TopicShipping } from "../../../../../database/models/topic-shipping";
import { ShippingCarrier } from "../../../../../database/models/shipping-carrier";
import { BadRequestError } from "../../../../../errors/base/bad-request-error";
import { FileHelper } from "../../../../../helper/file-helper";
import { TopicPictures } from "../../../../../database/models/topic-pictures";
import { TopicThumbnail } from "../../../../../database/models/topic-thumbnail";
import { fromAssert } from "../../../../../helper/saefshop-path-helper";

interface ShippingSchema {
    id: number,
    price?: number;
}

interface ThumbnailSchema {
    name: string;
    type: string;
    size: number;
}

interface UploadPictureSchema {
    name: string;
    type: string;
    size: number;
    src: string;
    dest: string;
}

interface PatchEditTopicContentRequestSchema {
    topicName?: string;
    topicDescription?: string;
    topicPrice?: number;
    topicRemainingItems?: number; 

    addCategories?: number[];
    removeCategories?: number[];

    addShippings?: ShippingSchema[];
    updateShippings?: ShippingSchema[];
    removeShippings?: number[];

    topicThumbnail?: ThumbnailSchema | number;
    addPictures?: UploadPictureSchema[];
    removePictures?: number[];
}

function resolveFileName(fileName: string, timestamp: number): string {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-` + crc32(timestamp + fileName).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}

function isSubset<U, V>(parent: U[], child: V[], getParent: (p: U) => any, getChild: (c: V) => any) {
    if (parent == null || child == null) {
        return false;
    }

    for (let i = 0; i < child.length; i++) {
        let isMatch = false;
        for (let j = 0; j < parent.length; j++) {
            if (getChild(child[i]) == getParent(parent[j])) {
                isMatch = true;
                break;
            }
        }
        if (!!!isMatch) {
            return false;
        }
    }
    return true;
}

function isPartialMatch<U, V>(A: U[], B: V[], getA: (a: U) => any, getB: (b : V) => any) {
    if (A == null || B == null) {
        return false;
    }
    
    for (let i = 0; i < A.length; i++) {
        for (let j = 0; j < B.length; j++) {
            if (getA(A[i]) == getB(B[j])) {
                return true;
            }
        }
    }
    return false;
}

export async function PatchEditTopicContent(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const topicId = +req.params.id

        const body = {} as PatchEditTopicContentRequestSchema;

        const form = new IncomingForm();
        form.keepExtensions = true;
        form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
        form.multiples = false;

        form
        .on('field', function(name, value) {
            console.log(name, value);
            switch(name) {
                case 'addCategories':
                case 'removeCategories':
                case 'addShippings':
                case 'updateShippings':
                case 'removeShippings':
                case 'removePictures':
                    body[name] = JSON.parse(value);
                    break;
                case 'topicThumbnail':
                    const thumbnail = parseInt(value);
                    if (isNaN(thumbnail)) {
                        body[name] = JSON.parse(value);
                    } else {
                        body[name] = thumbnail;
                    }
                    break;
                default:
                    body[name] = value;
                    break;
            }
        })
        .on('fileBegin', function(name, file) {

        })
        .on('file', function(name, file) {
            if (body.addPictures == null) {
                body.addPictures = [];
            }

            if (name == 'addPictures') {
                body.addPictures.push({ 
                    name: file.name,
                    type: file.type,
                    size: file.size,
                    src: file.path, 
                    dest: resolveFileName(file.name, req.timestamp) 
                });            
            }
        })
        .on('abort', function() {

        })
        .on('error', function(err) {
            console.error(err);
            return next(err);
        })
        .on('end', async function() {
            const topic = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(Topic, 'A')
            .innerJoinAndSelect('A.thumbnail', 'B')
            .innerJoinAndSelect('A.pictures', 'C')
            .innerJoinAndSelect('A.categories', 'D')
            .innerJoinAndSelect('A.shippings', 'E')
            .where('A.id = :topicId', { topicId })
            .andWhere('A.userId = :userId', { userId })
            .getOne();

            if (!!!topic) {
                return next(new ForbiddenError());
            }

            await getManager().transaction(async tem => {
                /**
                 * FOR TOPIC CONTENT
                 */
                const newTopic = Object.assign({ 
                        id: topic.id,
                        topicName: topic.topicName,
                        topicDescription: topic.topicDescription,
                        topicPrice: topic.topicPrice,
                        topicRemainingItems: topic.topicRemainingItems
                    }, 
                    { topicName: body.topicName },
                    { topicDescription: body.topicDescription },
                    { topicPrice: body.topicPrice },
                    { topicRemainingItems: body.topicRemainingItems }
                );
                await tem.save(Topic, newTopic);

                /**
                 * FOR CATEGORIES
                 */
                if (isPartialMatch(body.addCategories, body.removeCategories, ac => ac, rc => rc)) {
                    throw next(new BadRequestError());
                }

                let categories: Category[] = null;
                let categoryIds: number[] = null;
                if (body.addCategories != null) {
                     categories = await tem
                        .createQueryBuilder()
                        .relation(Topic, "categories")
                        .of(topicId)
                        .loadMany();
                    categoryIds = categories.map(cat => cat.id);

                    if (isPartialMatch(categoryIds, body.addCategories, ci => ci, ac => ac)) {
                        throw next(new BadRequestError());
                    }

                    await tem.createQueryBuilder()
                        .relation(Topic, "categories")
                        .of(topicId)
                        .add(body.addCategories);
                }

                if (body.removeCategories != null) {
                    if (categories == null) {
                        categories = await tem
                            .createQueryBuilder()
                            .relation(Topic, "categories")
                            .of(topicId)
                            .loadMany();
                        categoryIds = categories.map(cat => cat.id);
                    }

                    if (!!!isSubset(categoryIds, body.removeCategories, id => id, rc => rc)) {
                        throw next(new BadRequestError());
                    }

                    await tem.createQueryBuilder()
                        .relation(Topic, "categories")
                        .of(topicId)
                        .remove(body.removeCategories);
                }

                /**
                 * FOR SHIPPING
                 */
                if (isPartialMatch(body.addShippings, body.removeShippings, as => as.id, rs => rs) ||
                    isPartialMatch(body.addShippings, body.updateShippings, as => as.id, us => us.id) ||
                    isPartialMatch(body.updateShippings, body.removeShippings, us => us.id, rs => rs)) {
                        console.log('yes1');
                        throw next(new BadRequestError());
                }

                let shippings: TopicShipping[] = null;
                let shippingIds: number[] = null;
                if (body.addShippings != null) {
                    shippings = await tem.createQueryBuilder()
                        .createQueryBuilder()
                        .relation(Topic, "shippings")
                        .of(topicId)
                        .loadMany();
                    shippingIds = shippings.map(s => s.shippingId);

                    if (isPartialMatch(shippingIds, body.addShippings, si => si, as => as.id)) {
                        console.log('yes2');
                        throw next(new BadRequestError());
                    }

                    await tem.createQueryBuilder()
                        .insert()
                        .into(TopicShipping)
                        .values(body.addShippings.map(as => ({ topicId, shippingId: as.id, price: as.price } as TopicShipping)))
                        .execute();
                }

                if (body.removeShippings != null) {
                    if (shippings == null) {
                        shippings = await tem.createQueryBuilder()
                            .createQueryBuilder()
                            .relation(Topic, "shippings")
                            .of(topicId)
                            .loadMany();
                        shippingIds = shippings.map(s => s.shippingId);
                    }

                    if (!!!isPartialMatch(shippingIds, body.removeShippings, si => si, rs => rs)) {
                        throw next(new BadRequestError());
                    }

                    for (const rs of body.removeShippings) {
                        await tem.createQueryBuilder()
                            .delete()
                            .from(TopicShipping)
                            .where('shippingId = :rs', { rs })
                            .andWhere('topicId = :topicId', { topicId })
                            .execute();
                    }
                    // await tem.remove(TopicShipping, body.removeShippings.map(rs => ({
                    //     shippingId: rs,
                    //     topicId: topicId
                    // }) as TopicShipping));
                }

                if (body.updateShippings != null) {
                    if (shippings == null) {
                        shippings = await tem.createQueryBuilder()
                            .createQueryBuilder()
                            .relation(Topic, "shippings")
                            .of(topicId)
                            .loadMany();
                        shippingIds = shippings.map(s => s.shippingId);
                    }

                    if (!!!isPartialMatch(shippingIds, body.updateShippings, si => si, us => us.id)) {
                        throw next(new BadRequestError());
                    }

                    for (const us of body.updateShippings) {
                        for (const shipping of shippings) {
                            if (shipping.shippingId == us.id) {
                                await tem.createQueryBuilder()
                                    .update(TopicShipping)
                                    .set({ price: us.price } as TopicShipping)
                                    .where('id = :id', { id: shipping.id })
                                    .execute();
                                break;
                            }
                        }
                    }
                }

                /**
                 * CHECKING NEW THUMBNAIL FOR ADDED PICTURE
                 */
                if (body.topicThumbnail != null && typeof body.topicThumbnail != 'number') {
                    let isMatch = false;
                    const topicThumbnail: ThumbnailSchema = body.topicThumbnail;
                    for (const ap of body.addPictures) {
                        if (ap.name == topicThumbnail.name &&
                            ap.type == topicThumbnail.type &&
                            ap.size == topicThumbnail.size) {
                            isMatch = true;
                        }
                    }
                    if (!!!isMatch) {
                        return next(new BadRequestError());
                    }
                }

                /**
                 * FOR TOPIC PICTURE
                 */
                let addedPictures: TopicPictures[] = null;
                if (body.addPictures != null) {
                    console.log(body.addPictures);
                    for (const pic of body.addPictures) {
                        await FileHelper.copy(pic.src, fromAssert(`static/topics/${topicId}/${pic.dest}`));
                    }

                    addedPictures = await tem.save(body.addPictures.map(picture => {
                        const pic = tem.create(TopicPictures);
                        pic.fileName = picture.dest;
                        pic.topicId = topic.id;
                        return pic;
                    }));
                }
                
                if (body.topicThumbnail != null) {
                    if (typeof body.topicThumbnail == 'number') {
                        const pictures: TopicPictures[] = await tem
                            .createQueryBuilder()
                            .relation(Topic, "pictures")
                            .of(topicId)
                            .loadMany();
                        const pictureIds = pictures.map(p => p.id);
                        if (pictureIds.indexOf(body.topicThumbnail) != -1) {
                            await tem.createQueryBuilder()
                                .update(TopicThumbnail)
                                .set({ topicPictureId: body.topicThumbnail } as TopicThumbnail)
                                .where('topicId = :topicId', { topicId })
                                .execute();
                        } else {
                            throw next(new BadRequestError());
                        }
                    } else {
                        const topicThumbnail: ThumbnailSchema = body.topicThumbnail;
                        let isThumbnailFound = false;
                        for (const tp of addedPictures) {
                            if (isThumbnailFound) {
                                break;
                            }
                            for (const ap of body.addPictures) {
                                if (tp.fileName == ap.dest) {
                                    if (ap.name == topicThumbnail.name &&
                                        ap.type == topicThumbnail.type &&
                                        ap.size == topicThumbnail.size) {
                                        
                                        await tem.createQueryBuilder()
                                            .update(TopicThumbnail)
                                            .set({ topicPictureId: tp.id } as TopicThumbnail)
                                            .where('topicId = :topicId', { topicId })
                                            .execute();

                                        isThumbnailFound = true;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                if (body.removePictures != null) {
                    const pictures: TopicPictures[] = await tem
                        .createQueryBuilder()
                        .relation(Topic, "pictures")
                        .of(topicId)
                        .loadMany();

                    const pictureIds = pictures.map(p => p.id);
                    
                    if (!!!isSubset(pictureIds, body.removePictures, pi => pi, rp => rp)) {
                        throw new BadRequestError();
                    }

                    if (pictures.length - body.removePictures.length <= 0) {
                        throw new BadRequestError();
                    }

                    const thumbnail = await tem
                        .createQueryBuilder()
                        .select('A')
                        .from(TopicThumbnail, 'A')
                        .where('A.topicId = :topicId', { topicId })
                        .getOne();

                    if (body.removePictures.indexOf(thumbnail.topicPictureId) != -1) {
                        for (const rp of body.removePictures) {
                            const index = pictureIds.indexOf(rp);
                            if (index != -1) {
                                pictureIds.splice(index, 1);
                            }
                        }
                        await tem.createQueryBuilder()
                            .update(TopicThumbnail)
                            .set({ topicPictureId: pictureIds[0] } as TopicThumbnail)
                            .where('topicId = :topicId', { topicId })
                            .execute();
                    }

                    for (const rp of body.removePictures) {
                        await tem
                            .createQueryBuilder()
                            .delete()
                            .from(TopicPictures)
                            .where('id = :id', { id: rp })
                            .execute();
                    }
                }
            });

            return res.sendStatus(200);
        })
        .parse(req);
    } else {
        return next(new UnauthorizedError());
    }
}