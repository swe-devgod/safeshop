"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const topic_1 = require("../../../../../database/models/topic");
function DeleteTopic(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const topicId = req.params.id;
            try {
                yield typeorm_1.getManager()
                    .createQueryBuilder()
                    .update(topic_1.Topic)
                    .set({ topicRemainingItems: 0 })
                    .where('id = :id', { id: topicId })
                    .andWhere('userId = :userId', { userId })
                    .execute();
                return res.sendStatus(200);
            }
            catch (err) {
                return next(err);
            }
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.DeleteTopic = DeleteTopic;
