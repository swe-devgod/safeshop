import { crc32 } from 'crc';
import { IncomingForm } from 'formidable'; 
import { getManager } from "typeorm";

import { Topic } from "../../../../../database/models/topic";
import { TopicShipping } from "../../../../../database/models/topic-shipping";
import { TopicPictures } from "../../../../../database/models/topic-pictures";

import { DateTimeHelper } from "../../../../../helper/datetime-helper";
import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { FileHelper } from '../../../../../helper/file-helper';
import { TopicThumbnail } from '../../../../../database/models/topic-thumbnail';
import { fromAssert } from '../../../../../helper/saefshop-path-helper';
import { getRandomInt } from '../../../../../helper/utils';

interface ShippingSchema {
    id: number,
    name: string;
    price: number;
}

interface CategorySchema {
    id: number;
    name: string;
}

interface ThumbnailSchema {
    name: string;
    type: string;
    size: number;
}

interface AddTopicRequestSchema {
    topicName: string;
    topicDescription: string;
    topicPrice: number;
    topicRemainingItem: number;
    topicShipping: ShippingSchema[];
    topicCategory: CategorySchema[];
    topicThumbnail: ThumbnailSchema;
}

interface UploadPictureSchema {
    name: string;
    type: string;
    size: number;
    src: string;
    dest: string;
}

function resolveFileName(fileName: string, timestamp: number): string {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${getRandomInt(1000, 9999)}-` + crc32(timestamp + fileName + getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);}

export async function PostAddNewTopic(req, res, next) {
    if (req.isAuthenticated()) {
        const form = new IncomingForm();
        form.keepExtensions = true;
        form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
        form.multiples = false;

        const fromData = {} as AddTopicRequestSchema;
        const uploadPictures: UploadPictureSchema[] = [];

        form
        .on('field', function(name, value) {
            console.log(name, value);
            switch(name) {
                case 'topicShipping':
                case 'topicCategory':
                case 'topicThumbnail':
                    fromData[name] = JSON.parse(value);
                    break;
                default:
                    fromData[name] = value;
                    break;
            }
        })
        .on('fileBegin', function(name, file) {
            // if (name == 'topicPictures') {
            //     uploadPictures.push({ 
            //         name: file.name,
            //         type: file.type,
            //         size: file.size,
            //         src: file.path, 
            //         dest: resolveFileName(file.name, req.timestamp) 
            //     });
            // }
        })
        .on('file', function(name, file) {
            if (name == 'topicPictures') {
                uploadPictures.push({ 
                    name: file.name,
                    type: file.type,
                    size: file.size,
                    src: file.path, 
                    dest: resolveFileName(file.name, req.timestamp) 
                });
            }
        })
        .on('abort', function() {

        })
        .on('error', function(err) {
            console.error(err);
            return next(err);
        })
        .on('end', async function() {
            const {
                topicName = '',
                topicDescription,
                topicPrice,
                topicRemainingItem,
                topicShipping,
                topicCategory
            }: AddTopicRequestSchema = fromData;

            console.log(fromData);
            //console.log(uploadPictures);

            const id = await getManager().transaction(async tem => {
                const topic = new Topic();
                topic.userId = req.user.id;
                topic.topicName = topicName;
                topic.topicDescription = topicDescription;
                topic.topicPrice = topicPrice;
                topic.topicRemainingItems = topicRemainingItem,
                topic.topicCreatedDate = DateTimeHelper.getCurrentTimestamp();
                topic.categories = topicCategory.map(c => (<any>{ id: c.id }));
                await tem.save(topic);

                const isDestFolderExists = await FileHelper.exists(fromAssert(`static/topics/${topic.id}/`));
                if (!!!isDestFolderExists) {
                    await FileHelper.mkdir(fromAssert(`static/topics/${topic.id}/`));
                }

                for (const picture of uploadPictures) {
                    await FileHelper.copy(picture.src, fromAssert(`static/topics/${topic.id}/${picture.dest}`));
                }

                const topicPictures = await tem.save(uploadPictures.map(picture => {
                    const pic = tem.create(TopicPictures);
                    pic.fileName = picture.dest;
                    pic.topicId = topic.id;
                    return pic;
                }));

                const topicThumbnail: ThumbnailSchema = fromData.topicThumbnail;
                let isThumbnailFound = false;
                for (const tp of topicPictures) {
                    if (isThumbnailFound) {
                        break;
                    }
                    for (const up of uploadPictures) {
                        if (tp.fileName == up.dest) {
                            if (up.name == topicThumbnail.name &&
                                up.type == topicThumbnail.type &&
                                up.size == topicThumbnail.size) {
                                
                                const thumbnail = tem.create(TopicThumbnail);
                                thumbnail.topicId = topic.id;
                                thumbnail.topicPictureId = tp.id
                                await tem.save(thumbnail);

                                isThumbnailFound = true;
                            }
                            break;
                        }
                    }
                }

                // await tem.createQueryBuilder()
                //     .insert()
                //     .into(TopicPictures)
                //     .values(uploadPictures.map(picture => (<TopicPictures>{
                //         fileName: picture.dest,
                //         topicId: topic.id
                //     })))
                //     .execute();

                await tem.createQueryBuilder()
                    .insert()
                    .into(TopicShipping)
                    .values(topicShipping.map(s => (<TopicShipping>{
                        topicId: topic.id,
                        shippingId: s.id,
                        price: s.price
                    })))
                    .execute();

                return topic.id;
            });

            res.json({ topicId: id });
        });

        form.parse(req);
    } else {
        return next(new UnauthorizedError());
    }
}