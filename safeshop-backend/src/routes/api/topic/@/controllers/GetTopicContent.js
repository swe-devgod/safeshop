"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_database_1 = require("../../../../../database/repositories/user-database");
const topic_database_1 = require("../../../../../database/repositories/topic-database");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
const topic_not_found_error_1 = require("../../../../../errors/topic-not-found-error");
const typeorm_1 = require("typeorm");
const topic_shipping_1 = require("../../../../../database/models/topic-shipping");
const shipping_carrier_1 = require("../../../../../database/models/shipping-carrier");
const client_url_helper_1 = require("../../../../../helper/client-url-helper");
function GetTopicContent(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const { id } = req.params;
        const topic = yield topic_database_1.TopicDatabase.findTopicById(id, { withCategories: true });
        if (!!!topic || topic.topicRemainingItems <= 0) {
            return next(new topic_not_found_error_1.TopicNotFoundError());
        }
        const user = yield user_database_1.UserDatabase.findByUserId(topic.userId);
        if (!!!user) {
            return next(new bad_request_error_1.BadRequestError());
        }
        const userPicture = yield user_database_1.UserDatabase.fincUserPictureByUserId(topic.userId);
        const topicPictures = yield topic_database_1.TopicDatabase.findPicturesByTopic(topic);
        const shippings = yield typeorm_1.getManager()
            .createQueryBuilder()
            .select(['B.id as id', 'A.price as price', 'B.name as name'])
            .from(topic_shipping_1.TopicShipping, 'A')
            .innerJoin(shipping_carrier_1.ShippingCarrier, 'B', 'B.id = A.shippingId')
            .where('A.topicId = :id', { id })
            .getRawMany();
        const result = {
            productName: topic.topicName,
            productDescription: topic.topicDescription,
            productPrice: topic.topicPrice,
            createdDate: topic.topicCreatedDate,
            topicRating: topic.topicRating,
            topicRatingCount: topic.topicVoteCount,
            ownerUserId: user.id,
            ownerName: user.displayName,
            ownerRating: user.rating,
            ownerPicture: client_url_helper_1.ClientURLHelper.resolve('/static/users/' + userPicture.fileName),
            ownerRatingCount: user.ratingVoteCount,
            remainingItems: topic.topicRemainingItems,
            imgs: topicPictures.map(tp => ({
                id: tp.id,
                url: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${id}/` + tp.fileName)
            })),
            categories: topic.categories,
            shippings
        };
        res.json(result);
    });
}
exports.GetTopicContent = GetTopicContent;
