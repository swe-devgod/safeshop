"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const topic_1 = require("../../../../../database/models/topic");
const crc_1 = require("crc");
const formidable_1 = require("formidable");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const topic_shipping_1 = require("../../../../../database/models/topic-shipping");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
const file_helper_1 = require("../../../../../helper/file-helper");
const topic_pictures_1 = require("../../../../../database/models/topic-pictures");
const topic_thumbnail_1 = require("../../../../../database/models/topic-thumbnail");
const saefshop_path_helper_1 = require("../../../../../helper/saefshop-path-helper");
function resolveFileName(fileName, timestamp) {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-` + crc_1.crc32(timestamp + fileName).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}
function isSubset(parent, child, getParent, getChild) {
    if (parent == null || child == null) {
        return false;
    }
    for (let i = 0; i < child.length; i++) {
        let isMatch = false;
        for (let j = 0; j < parent.length; j++) {
            if (getChild(child[i]) == getParent(parent[j])) {
                isMatch = true;
                break;
            }
        }
        if (!!!isMatch) {
            return false;
        }
    }
    return true;
}
function isPartialMatch(A, B, getA, getB) {
    if (A == null || B == null) {
        return false;
    }
    for (let i = 0; i < A.length; i++) {
        for (let j = 0; j < B.length; j++) {
            if (getA(A[i]) == getB(B[j])) {
                return true;
            }
        }
    }
    return false;
}
function PatchEditTopicContent(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const topicId = +req.params.id;
            const body = {};
            const form = new formidable_1.IncomingForm();
            form.keepExtensions = true;
            form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
            form.multiples = false;
            form
                .on('field', function (name, value) {
                console.log(name, value);
                switch (name) {
                    case 'addCategories':
                    case 'removeCategories':
                    case 'addShippings':
                    case 'updateShippings':
                    case 'removeShippings':
                    case 'removePictures':
                        body[name] = JSON.parse(value);
                        break;
                    case 'topicThumbnail':
                        const thumbnail = parseInt(value);
                        if (isNaN(thumbnail)) {
                            body[name] = JSON.parse(value);
                        }
                        else {
                            body[name] = thumbnail;
                        }
                        break;
                    default:
                        body[name] = value;
                        break;
                }
            })
                .on('fileBegin', function (name, file) {
            })
                .on('file', function (name, file) {
                if (body.addPictures == null) {
                    body.addPictures = [];
                }
                if (name == 'addPictures') {
                    body.addPictures.push({
                        name: file.name,
                        type: file.type,
                        size: file.size,
                        src: file.path,
                        dest: resolveFileName(file.name, req.timestamp)
                    });
                }
            })
                .on('abort', function () {
            })
                .on('error', function (err) {
                console.error(err);
                return next(err);
            })
                .on('end', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    const topic = yield typeorm_1.getManager()
                        .createQueryBuilder()
                        .select('A')
                        .from(topic_1.Topic, 'A')
                        .innerJoinAndSelect('A.thumbnail', 'B')
                        .innerJoinAndSelect('A.pictures', 'C')
                        .innerJoinAndSelect('A.categories', 'D')
                        .innerJoinAndSelect('A.shippings', 'E')
                        .where('A.id = :topicId', { topicId })
                        .andWhere('A.userId = :userId', { userId })
                        .getOne();
                    if (!!!topic) {
                        return next(new forbidden_error_1.ForbiddenError());
                    }
                    yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                        /**
                         * FOR TOPIC CONTENT
                         */
                        const newTopic = Object.assign({
                            id: topic.id,
                            topicName: topic.topicName,
                            topicDescription: topic.topicDescription,
                            topicPrice: topic.topicPrice,
                            topicRemainingItems: topic.topicRemainingItems
                        }, { topicName: body.topicName }, { topicDescription: body.topicDescription }, { topicPrice: body.topicPrice }, { topicRemainingItems: body.topicRemainingItems });
                        yield tem.save(topic_1.Topic, newTopic);
                        /**
                         * FOR CATEGORIES
                         */
                        if (isPartialMatch(body.addCategories, body.removeCategories, ac => ac, rc => rc)) {
                            throw next(new bad_request_error_1.BadRequestError());
                        }
                        let categories = null;
                        let categoryIds = null;
                        if (body.addCategories != null) {
                            categories = yield tem
                                .createQueryBuilder()
                                .relation(topic_1.Topic, "categories")
                                .of(topicId)
                                .loadMany();
                            categoryIds = categories.map(cat => cat.id);
                            if (isPartialMatch(categoryIds, body.addCategories, ci => ci, ac => ac)) {
                                throw next(new bad_request_error_1.BadRequestError());
                            }
                            yield tem.createQueryBuilder()
                                .relation(topic_1.Topic, "categories")
                                .of(topicId)
                                .add(body.addCategories);
                        }
                        if (body.removeCategories != null) {
                            if (categories == null) {
                                categories = yield tem
                                    .createQueryBuilder()
                                    .relation(topic_1.Topic, "categories")
                                    .of(topicId)
                                    .loadMany();
                                categoryIds = categories.map(cat => cat.id);
                            }
                            if (!!!isSubset(categoryIds, body.removeCategories, id => id, rc => rc)) {
                                throw next(new bad_request_error_1.BadRequestError());
                            }
                            yield tem.createQueryBuilder()
                                .relation(topic_1.Topic, "categories")
                                .of(topicId)
                                .remove(body.removeCategories);
                        }
                        /**
                         * FOR SHIPPING
                         */
                        if (isPartialMatch(body.addShippings, body.removeShippings, as => as.id, rs => rs) ||
                            isPartialMatch(body.addShippings, body.updateShippings, as => as.id, us => us.id) ||
                            isPartialMatch(body.updateShippings, body.removeShippings, us => us.id, rs => rs)) {
                            console.log('yes1');
                            throw next(new bad_request_error_1.BadRequestError());
                        }
                        let shippings = null;
                        let shippingIds = null;
                        if (body.addShippings != null) {
                            shippings = yield tem.createQueryBuilder()
                                .createQueryBuilder()
                                .relation(topic_1.Topic, "shippings")
                                .of(topicId)
                                .loadMany();
                            shippingIds = shippings.map(s => s.shippingId);
                            if (isPartialMatch(shippingIds, body.addShippings, si => si, as => as.id)) {
                                console.log('yes2');
                                throw next(new bad_request_error_1.BadRequestError());
                            }
                            yield tem.createQueryBuilder()
                                .insert()
                                .into(topic_shipping_1.TopicShipping)
                                .values(body.addShippings.map(as => ({ topicId, shippingId: as.id, price: as.price })))
                                .execute();
                        }
                        if (body.removeShippings != null) {
                            if (shippings == null) {
                                shippings = yield tem.createQueryBuilder()
                                    .createQueryBuilder()
                                    .relation(topic_1.Topic, "shippings")
                                    .of(topicId)
                                    .loadMany();
                                shippingIds = shippings.map(s => s.shippingId);
                            }
                            if (!!!isPartialMatch(shippingIds, body.removeShippings, si => si, rs => rs)) {
                                throw next(new bad_request_error_1.BadRequestError());
                            }
                            for (const rs of body.removeShippings) {
                                yield tem.createQueryBuilder()
                                    .delete()
                                    .from(topic_shipping_1.TopicShipping)
                                    .where('shippingId = :rs', { rs })
                                    .andWhere('topicId = :topicId', { topicId })
                                    .execute();
                            }
                            // await tem.remove(TopicShipping, body.removeShippings.map(rs => ({
                            //     shippingId: rs,
                            //     topicId: topicId
                            // }) as TopicShipping));
                        }
                        if (body.updateShippings != null) {
                            if (shippings == null) {
                                shippings = yield tem.createQueryBuilder()
                                    .createQueryBuilder()
                                    .relation(topic_1.Topic, "shippings")
                                    .of(topicId)
                                    .loadMany();
                                shippingIds = shippings.map(s => s.shippingId);
                            }
                            if (!!!isPartialMatch(shippingIds, body.updateShippings, si => si, us => us.id)) {
                                throw next(new bad_request_error_1.BadRequestError());
                            }
                            for (const us of body.updateShippings) {
                                for (const shipping of shippings) {
                                    if (shipping.shippingId == us.id) {
                                        yield tem.createQueryBuilder()
                                            .update(topic_shipping_1.TopicShipping)
                                            .set({ price: us.price })
                                            .where('id = :id', { id: shipping.id })
                                            .execute();
                                        break;
                                    }
                                }
                            }
                        }
                        /**
                         * CHECKING NEW THUMBNAIL FOR ADDED PICTURE
                         */
                        if (body.topicThumbnail != null && typeof body.topicThumbnail != 'number') {
                            let isMatch = false;
                            const topicThumbnail = body.topicThumbnail;
                            for (const ap of body.addPictures) {
                                if (ap.name == topicThumbnail.name &&
                                    ap.type == topicThumbnail.type &&
                                    ap.size == topicThumbnail.size) {
                                    isMatch = true;
                                }
                            }
                            if (!!!isMatch) {
                                return next(new bad_request_error_1.BadRequestError());
                            }
                        }
                        /**
                         * FOR TOPIC PICTURE
                         */
                        let addedPictures = null;
                        if (body.addPictures != null) {
                            console.log(body.addPictures);
                            for (const pic of body.addPictures) {
                                yield file_helper_1.FileHelper.copy(pic.src, saefshop_path_helper_1.fromAssert(`static/topics/${topicId}/${pic.dest}`));
                            }
                            addedPictures = yield tem.save(body.addPictures.map(picture => {
                                const pic = tem.create(topic_pictures_1.TopicPictures);
                                pic.fileName = picture.dest;
                                pic.topicId = topic.id;
                                return pic;
                            }));
                        }
                        if (body.topicThumbnail != null) {
                            if (typeof body.topicThumbnail == 'number') {
                                const pictures = yield tem
                                    .createQueryBuilder()
                                    .relation(topic_1.Topic, "pictures")
                                    .of(topicId)
                                    .loadMany();
                                const pictureIds = pictures.map(p => p.id);
                                if (pictureIds.indexOf(body.topicThumbnail) != -1) {
                                    yield tem.createQueryBuilder()
                                        .update(topic_thumbnail_1.TopicThumbnail)
                                        .set({ topicPictureId: body.topicThumbnail })
                                        .where('topicId = :topicId', { topicId })
                                        .execute();
                                }
                                else {
                                    throw next(new bad_request_error_1.BadRequestError());
                                }
                            }
                            else {
                                const topicThumbnail = body.topicThumbnail;
                                let isThumbnailFound = false;
                                for (const tp of addedPictures) {
                                    if (isThumbnailFound) {
                                        break;
                                    }
                                    for (const ap of body.addPictures) {
                                        if (tp.fileName == ap.dest) {
                                            if (ap.name == topicThumbnail.name &&
                                                ap.type == topicThumbnail.type &&
                                                ap.size == topicThumbnail.size) {
                                                yield tem.createQueryBuilder()
                                                    .update(topic_thumbnail_1.TopicThumbnail)
                                                    .set({ topicPictureId: tp.id })
                                                    .where('topicId = :topicId', { topicId })
                                                    .execute();
                                                isThumbnailFound = true;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (body.removePictures != null) {
                            const pictures = yield tem
                                .createQueryBuilder()
                                .relation(topic_1.Topic, "pictures")
                                .of(topicId)
                                .loadMany();
                            const pictureIds = pictures.map(p => p.id);
                            if (!!!isSubset(pictureIds, body.removePictures, pi => pi, rp => rp)) {
                                throw new bad_request_error_1.BadRequestError();
                            }
                            if (pictures.length - body.removePictures.length <= 0) {
                                throw new bad_request_error_1.BadRequestError();
                            }
                            const thumbnail = yield tem
                                .createQueryBuilder()
                                .select('A')
                                .from(topic_thumbnail_1.TopicThumbnail, 'A')
                                .where('A.topicId = :topicId', { topicId })
                                .getOne();
                            if (body.removePictures.indexOf(thumbnail.topicPictureId) != -1) {
                                for (const rp of body.removePictures) {
                                    const index = pictureIds.indexOf(rp);
                                    if (index != -1) {
                                        pictureIds.splice(index, 1);
                                    }
                                }
                                yield tem.createQueryBuilder()
                                    .update(topic_thumbnail_1.TopicThumbnail)
                                    .set({ topicPictureId: pictureIds[0] })
                                    .where('topicId = :topicId', { topicId })
                                    .execute();
                            }
                            for (const rp of body.removePictures) {
                                yield tem
                                    .createQueryBuilder()
                                    .delete()
                                    .from(topic_pictures_1.TopicPictures)
                                    .where('id = :id', { id: rp })
                                    .execute();
                            }
                        }
                    }));
                    return res.sendStatus(200);
                });
            })
                .parse(req);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PatchEditTopicContent = PatchEditTopicContent;
