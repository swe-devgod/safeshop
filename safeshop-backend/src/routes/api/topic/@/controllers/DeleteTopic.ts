import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Topic } from "../../../../../database/models/topic";

export async function DeleteTopic(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const topicId = req.params.id;
        try {
            await getManager()
                .createQueryBuilder()
                .update(Topic)
                .set({ topicRemainingItems: 0 } as Topic)
                .where('id = :id', { id: topicId })
                .andWhere('userId = :userId', { userId })
                .execute();
            return res.sendStatus(200);
        } catch (err) {
            return next(err);
        }
    } else {
        return next(new UnauthorizedError());
    }
}