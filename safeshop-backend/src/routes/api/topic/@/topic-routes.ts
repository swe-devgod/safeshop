import { GetTopicContent } from './controllers/GetTopicContent';
import { topicCommentRoutes } from '../comment/@/comment-routes';
import { attachRoutesToParent, appendRoutes } from '../../../../helper/route-helper';
import { PostAddNewTopic } from './controllers/PostAddNewTopic';
import { topicShippingRoutes } from '../shipping/@/shipping-routes';
import { PatchEditTopicContent } from './controllers/PatchEditTopicContent';
import { DeleteTopic } from './controllers/DeleteTopic';

export const topicRoutes = {
    name: "topic",
    routes: [
        {
            path: '/:id(\\d+?)',
            method: 'get',
            action: GetTopicContent
        },
        {
            path: '',
            method: 'post',
            action: PostAddNewTopic
        },
        {
            path: '/:id(\\d+?)',
            method: 'patch',
            action: PatchEditTopicContent
        },
        {
            path: '/:id(\\d+?)',
            method: 'delete',
            action: DeleteTopic
        }
    ]
};

appendRoutes(topicCommentRoutes, topicRoutes);
attachRoutesToParent(topicShippingRoutes, topicRoutes);