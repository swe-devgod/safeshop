import { crc32 } from 'crc';
import { IncomingForm } from 'formidable'; 

import { TopicComment } from "../../../../../../database/models/topic-comment";
import { getManager } from "typeorm";
import { ValidatorHelper } from "../../../../../../helper/validator-helper";
import { ForbiddenError } from "../../../../../../errors/base/forbidden-error";
import { BadRequestError } from "../../../../../../errors/base/bad-request-error";
import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { getRandomInt, isSubset } from '../../../../../../helper/utils';
import { fromAssert } from '../../../../../../helper/saefshop-path-helper';
import { FileHelper } from '../../../../../../helper/file-helper';
import { CommentPictures } from '../../../../../../database/models/comment-pictures';

interface EditTopicCommentRequestSchema {
    content?: string;
    appendImgs?: PutEditTopicCommentUploadImageSchema[];
    deletedImgs: number[];
}

interface PutEditTopicCommentUploadImageSchema {
    src: string,
    dest: string
}

function resolveFileName(fileName: string, timestamp: number): string {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${getRandomInt(1000, 9999)}-` + crc32(timestamp + fileName + getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}

export async function PutEditTopicComment(req, res, next) {
    if (req.isAuthenticated()) {
        const commentId = req.params.id;

        const body = {} as EditTopicCommentRequestSchema;
        body.appendImgs = [];
        body.deletedImgs = [];

        const form = new IncomingForm();
        form.keepExtensions = true;
        form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
        form.multiples = false;

        form
        .on('field', function(name, value) {
            // console.log(name, value);
            switch (name) {
                case 'deletedImgs':
                    body[name] = JSON.parse(value);
                    break;
                default:
                    body[name] = value;
            }
        })
        .on('fileBegin', function(name, file) {

        })
        .on('file', function(name, file) {
            if (name == 'appendImgs') {
                body.appendImgs.push({
                    src: file.path,
                    dest: resolveFileName(file.name, req.timestamp)
                });
            }
        })
        .on('abort', function() {

        })
        .on('error', function(err) {
            console.error(err);
            return next(err);
        })
        .on('end', async function() {
            try {
                await getManager().transaction(async tem => {
                    let comment: TopicComment = null;
                    if (body.content != null) {
                        if (ValidatorHelper.checkEmpty(body.content)) {
                            return next(new BadRequestError());
                        }
                        comment = await tem.createQueryBuilder()
                            .select('A')
                            .from(TopicComment, 'A')
                            .where('A.id = :id AND A.userId = :userId', { id: commentId, userId: req.user.id })
                            .getOne();

                        if (!comment) {
                            return next(new ForbiddenError());
                        } else {
                            comment.content = body.content;
                            await tem.save(comment);
                        }
                    }

                    if (body.appendImgs.length > 0) {
                        if (!!!comment) {
                            comment = await tem.createQueryBuilder()
                            .select('A')
                            .from(TopicComment, 'A')
                            .where('A.id = :id AND A.userId = :userId', { id: commentId, userId: req.user.id })
                            .getOne();
                        }

                        const isDestFolderExists = await FileHelper.exists(fromAssert(`static/topics/${comment.topicId}/`));
                        if (!!!isDestFolderExists) {
                            await FileHelper.mkdir(fromAssert(`static/topics/${comment.topicId}/`));
                        }
        
                        for (const img of body.appendImgs) {
                            await FileHelper.copy(img.src, fromAssert(`static/topics/${comment.topicId}/${img.dest}`));
                        }    

                        await tem.save(body.appendImgs.map(picture => {
                            const pic = tem.create(CommentPictures);
                            pic.comment = { id: comment.id } as TopicComment;
                            pic.fileName = picture.dest;
                            return pic;
                        }));
                    }

                    if (body.deletedImgs.length > 0) {
                        const commentPictures = await tem.createQueryBuilder()
                            .select('A')
                            .from(CommentPictures, 'A')
                            .whereInIds(body.deletedImgs)
                            .andWhere('A.commentId = :commentId', { commentId })
                            .getMany();

                        if (!!!isSubset(commentPictures, body.deletedImgs, cp => cp.id, di => di)) {
                            // console.log(commentPictures, body.deletedImgs);
                            return next(new BadRequestError());
                        }

                        await tem.delete(CommentPictures, body.deletedImgs);

                        // for (const picture of commentPictures) {
                        //     await FileHelper.unlink(fromAssert(`static/topics/${comment.topicId}/${picture.fileName}`));
                        // }
                    }

                    res.sendStatus(200);
                });
            } catch (err) {
                return next(err);
            }
        })
        .parse(req);
    } else {
        return next(new UnauthorizedError());
    }
}