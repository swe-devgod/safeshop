"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const comment_database_1 = require("../../../../../../database/repositories/comment-database");
const user_database_1 = require("../../../../../../database/repositories/user-database");
const bad_request_error_1 = require("../../../../../../errors/base/bad-request-error");
const client_url_helper_1 = require("../../../../../../helper/client-url-helper");
function GetTopicComment(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const topicId = +req.params.id;
        if (isNaN(topicId) || topicId <= 0) {
            return next(new bad_request_error_1.BadRequestError());
        }
        const comments = yield comment_database_1.CommentDatabase.findCommentsByTopicId(topicId);
        const results = yield Promise.all(comments.map((comment) => __awaiter(this, void 0, void 0, function* () {
            const user = yield user_database_1.UserDatabase.findByUserId(comment.userId);
            const userPicture = yield user_database_1.UserDatabase.fincUserPictureByUserId(user.id);
            const commentPictures = (yield comment_database_1.CommentDatabase.findPicturesByComment(comment)) || [];
            const subComments = yield comment_database_1.CommentDatabase.findSubCommentsByComment(comment);
            return {
                commentId: comment.id,
                userId: comment.userId,
                userPicture: client_url_helper_1.ClientURLHelper.resolve('/static/users/' + userPicture.fileName),
                displayName: user.displayName,
                content: comment.content,
                createdDate: comment.createdDate,
                imgs: commentPictures.map(picture => ({
                    id: picture.id,
                    url: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${topicId}/` + picture.fileName)
                })),
                subComments: yield Promise.all(subComments.map((subComment) => __awaiter(this, void 0, void 0, function* () {
                    const subUser = yield user_database_1.UserDatabase.findByUserId(subComment.userId);
                    const subUserPicture = yield user_database_1.UserDatabase.fincUserPictureByUserId(subUser.id);
                    const subCommentPictures = (yield comment_database_1.CommentDatabase.findPicturesByComment(subComment)) || [];
                    return {
                        commentId: subComment.id,
                        userId: subComment.userId,
                        userPicture: client_url_helper_1.ClientURLHelper.resolve('/static/users/' + subUserPicture.fileName),
                        displayName: subUser.displayName,
                        content: subComment.content,
                        createdDate: subComment.createdDate,
                        imgs: subCommentPictures.map(picture => ({
                            id: picture.id,
                            url: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${topicId}/` + picture.fileName)
                        }))
                    };
                })))
            };
        })));
        res.json(results);
    });
}
exports.GetTopicComment = GetTopicComment;
