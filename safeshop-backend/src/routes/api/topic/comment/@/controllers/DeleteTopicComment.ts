import { ValidatorHelper } from "../../../../../../helper/validator-helper";
import { BadRequestError } from "../../../../../../errors/base/bad-request-error";
import { getManager } from "typeorm";
import { TopicComment } from "../../../../../../database/models/topic-comment";
import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { CommentPictures } from "../../../../../../database/models/comment-pictures";

export async function DeleteTopicComment(req, res, next) {
    if (req.isAuthenticated()) {
        const commentId = +req.params.id;

        if (commentId > 0) {
            await getManager().transaction(async tem => {
                const comment = await tem.createQueryBuilder()
                    .select('A.id as id')
                    .from(TopicComment, 'A')
                    .where('A.id = :id AND A.userId = :userId', { id: commentId, userId: req.user.id })
                    .getRawOne();

                if (!!!comment) {
                    return next(new BadRequestError());
                } else {
                    await tem.createQueryBuilder()
                        .delete()
                        .from(CommentPictures)
                        .where('commentId = :id', { id: comment.id })
                        .execute();

                    // find all childs
                    const childComments = await tem.createQueryBuilder()
                        .select('A')
                        .from(TopicComment, 'A')
                        .where('parentCommentId = :id', { id: comment.id })
                        .getMany();

                    if (childComments != null) {
                        for (const child of childComments) {
                            await tem.createQueryBuilder()
                                .delete()
                                .from(CommentPictures)
                                .where('commentId = :id', { id: child.id })
                                .execute();

                            await tem.delete(TopicComment, child.id);
                        }
                    }

                    await tem.createQueryBuilder()
                        .delete()
                        .from(TopicComment)
                        .where('id = :id', { id: comment.id })
                        .execute();

                    res.sendStatus(200);
                }
            });
        } else {
            return next(new BadRequestError());
        }
    } else {
        return next(new UnauthorizedError());
    }
}