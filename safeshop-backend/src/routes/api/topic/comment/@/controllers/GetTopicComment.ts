import { CommentDatabase } from "../../../../../../database/repositories/comment-database";
import { UserDatabase } from "../../../../../../database/repositories/user-database";
import { BadRequestError } from "../../../../../../errors/base/bad-request-error";
import { GlobalVar } from "../../../../../../helper/global-var";
import { ClientURLHelper } from "../../../../../../helper/client-url-helper";

export async function GetTopicComment(req, res, next) {
    const topicId = +req.params.id;

    if (isNaN(topicId) || topicId <= 0) {
        return next(new BadRequestError());
    }

    const comments = await CommentDatabase.findCommentsByTopicId(topicId);
    const results = await Promise.all(comments.map(async comment => {
        const user = await UserDatabase.findByUserId(comment.userId);
        const userPicture = await UserDatabase.fincUserPictureByUserId(user.id);
        const commentPictures = (await CommentDatabase.findPicturesByComment(comment)) || [];
        const subComments = await CommentDatabase.findSubCommentsByComment(comment);

        return {
            commentId: comment.id,
            userId: comment.userId,
            userPicture: ClientURLHelper.resolve('/static/users/' + userPicture.fileName),
            displayName: user.displayName,
            content: comment.content,
            createdDate: comment.createdDate,
            imgs: commentPictures.map(picture => ({
                id: picture.id,
                url: ClientURLHelper.resolve(`/static/topics/${topicId}/` + picture.fileName)
            })),
            subComments: await Promise.all(subComments.map(async subComment => {
                const subUser = await UserDatabase.findByUserId(subComment.userId);
                const subUserPicture = await UserDatabase.fincUserPictureByUserId(subUser.id);
                const subCommentPictures = (await CommentDatabase.findPicturesByComment(subComment)) || [];
                return {
                    commentId: subComment.id,
                    userId: subComment.userId,
                    userPicture: ClientURLHelper.resolve('/static/users/' + subUserPicture.fileName),
                    displayName: subUser.displayName,
                    content: subComment.content,
                    createdDate: subComment.createdDate,
                    imgs: subCommentPictures.map(picture => ({
                        id: picture.id,
                        url: ClientURLHelper.resolve(`/static/topics/${topicId}/` + picture.fileName)
                    }))
                }
            }))
        }
    }));
    res.json(results);
}