"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bad_request_error_1 = require("../../../../../../errors/base/bad-request-error");
const typeorm_1 = require("typeorm");
const topic_comment_1 = require("../../../../../../database/models/topic-comment");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const comment_pictures_1 = require("../../../../../../database/models/comment-pictures");
function DeleteTopicComment(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const commentId = +req.params.id;
            if (commentId > 0) {
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    const comment = yield tem.createQueryBuilder()
                        .select('A.id as id')
                        .from(topic_comment_1.TopicComment, 'A')
                        .where('A.id = :id AND A.userId = :userId', { id: commentId, userId: req.user.id })
                        .getRawOne();
                    if (!!!comment) {
                        return next(new bad_request_error_1.BadRequestError());
                    }
                    else {
                        yield tem.createQueryBuilder()
                            .delete()
                            .from(comment_pictures_1.CommentPictures)
                            .where('commentId = :id', { id: comment.id })
                            .execute();
                        // find all childs
                        const childComments = yield tem.createQueryBuilder()
                            .select('A')
                            .from(topic_comment_1.TopicComment, 'A')
                            .where('parentCommentId = :id', { id: comment.id })
                            .getMany();
                        if (childComments != null) {
                            for (const child of childComments) {
                                yield tem.createQueryBuilder()
                                    .delete()
                                    .from(comment_pictures_1.CommentPictures)
                                    .where('commentId = :id', { id: child.id })
                                    .execute();
                                yield tem.delete(topic_comment_1.TopicComment, child.id);
                            }
                        }
                        yield tem.createQueryBuilder()
                            .delete()
                            .from(topic_comment_1.TopicComment)
                            .where('id = :id', { id: comment.id })
                            .execute();
                        res.sendStatus(200);
                    }
                }));
            }
            else {
                return next(new bad_request_error_1.BadRequestError());
            }
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.DeleteTopicComment = DeleteTopicComment;
