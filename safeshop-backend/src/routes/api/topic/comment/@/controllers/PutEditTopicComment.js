"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crc_1 = require("crc");
const formidable_1 = require("formidable");
const topic_comment_1 = require("../../../../../../database/models/topic-comment");
const typeorm_1 = require("typeorm");
const validator_helper_1 = require("../../../../../../helper/validator-helper");
const forbidden_error_1 = require("../../../../../../errors/base/forbidden-error");
const bad_request_error_1 = require("../../../../../../errors/base/bad-request-error");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const utils_1 = require("../../../../../../helper/utils");
const saefshop_path_helper_1 = require("../../../../../../helper/saefshop-path-helper");
const file_helper_1 = require("../../../../../../helper/file-helper");
const comment_pictures_1 = require("../../../../../../database/models/comment-pictures");
function resolveFileName(fileName, timestamp) {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${utils_1.getRandomInt(1000, 9999)}-` + crc_1.crc32(timestamp + fileName + utils_1.getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}
function PutEditTopicComment(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const commentId = req.params.id;
            const body = {};
            body.appendImgs = [];
            body.deletedImgs = [];
            const form = new formidable_1.IncomingForm();
            form.keepExtensions = true;
            form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
            form.multiples = false;
            form
                .on('field', function (name, value) {
                // console.log(name, value);
                switch (name) {
                    case 'deletedImgs':
                        body[name] = JSON.parse(value);
                        break;
                    default:
                        body[name] = value;
                }
            })
                .on('fileBegin', function (name, file) {
            })
                .on('file', function (name, file) {
                if (name == 'appendImgs') {
                    body.appendImgs.push({
                        src: file.path,
                        dest: resolveFileName(file.name, req.timestamp)
                    });
                }
            })
                .on('abort', function () {
            })
                .on('error', function (err) {
                console.error(err);
                return next(err);
            })
                .on('end', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                            let comment = null;
                            if (body.content != null) {
                                if (validator_helper_1.ValidatorHelper.checkEmpty(body.content)) {
                                    return next(new bad_request_error_1.BadRequestError());
                                }
                                comment = yield tem.createQueryBuilder()
                                    .select('A')
                                    .from(topic_comment_1.TopicComment, 'A')
                                    .where('A.id = :id AND A.userId = :userId', { id: commentId, userId: req.user.id })
                                    .getOne();
                                if (!comment) {
                                    return next(new forbidden_error_1.ForbiddenError());
                                }
                                else {
                                    comment.content = body.content;
                                    yield tem.save(comment);
                                }
                            }
                            if (body.appendImgs.length > 0) {
                                if (!!!comment) {
                                    comment = yield tem.createQueryBuilder()
                                        .select('A')
                                        .from(topic_comment_1.TopicComment, 'A')
                                        .where('A.id = :id AND A.userId = :userId', { id: commentId, userId: req.user.id })
                                        .getOne();
                                }
                                const isDestFolderExists = yield file_helper_1.FileHelper.exists(saefshop_path_helper_1.fromAssert(`static/topics/${comment.topicId}/`));
                                if (!!!isDestFolderExists) {
                                    yield file_helper_1.FileHelper.mkdir(saefshop_path_helper_1.fromAssert(`static/topics/${comment.topicId}/`));
                                }
                                for (const img of body.appendImgs) {
                                    yield file_helper_1.FileHelper.copy(img.src, saefshop_path_helper_1.fromAssert(`static/topics/${comment.topicId}/${img.dest}`));
                                }
                                yield tem.save(body.appendImgs.map(picture => {
                                    const pic = tem.create(comment_pictures_1.CommentPictures);
                                    pic.comment = { id: comment.id };
                                    pic.fileName = picture.dest;
                                    return pic;
                                }));
                            }
                            if (body.deletedImgs.length > 0) {
                                const commentPictures = yield tem.createQueryBuilder()
                                    .select('A')
                                    .from(comment_pictures_1.CommentPictures, 'A')
                                    .whereInIds(body.deletedImgs)
                                    .andWhere('A.commentId = :commentId', { commentId })
                                    .getMany();
                                if (!!!utils_1.isSubset(commentPictures, body.deletedImgs, cp => cp.id, di => di)) {
                                    // console.log(commentPictures, body.deletedImgs);
                                    return next(new bad_request_error_1.BadRequestError());
                                }
                                yield tem.delete(comment_pictures_1.CommentPictures, body.deletedImgs);
                                // for (const picture of commentPictures) {
                                //     await FileHelper.unlink(fromAssert(`static/topics/${comment.topicId}/${picture.fileName}`));
                                // }
                            }
                            res.sendStatus(200);
                        }));
                    }
                    catch (err) {
                        return next(err);
                    }
                });
            })
                .parse(req);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PutEditTopicComment = PutEditTopicComment;
