import { crc32 } from 'crc';
import { IncomingForm } from 'formidable'; 

import * as validator from 'validator';
import { CommentDatabase } from '../../../../../../database/repositories/comment-database';
import { TopicComment } from '../../../../../../database/models/topic-comment';
import { UnauthorizedError } from '../../../../../../errors/base/unauthorized-error';
import { BadRequestError } from '../../../../../../errors/base/bad-request-error';
import { DateTimeHelper } from '../../../../../../helper/datetime-helper';
import { getRandomInt } from '../../../../../../helper/utils';
import { ValidatorHelper } from '../../../../../../helper/validator-helper';
import { getManager } from 'typeorm';
import { fromAssert } from '../../../../../../helper/saefshop-path-helper';
import { FileHelper } from '../../../../../../helper/file-helper';
import { CommentPictures } from '../../../../../../database/models/comment-pictures';

interface PostAddTopicCommentRequestSchema {
    content: string;
    parentId?: number;
    imgs?: PostAddTopicCommentUploadImageSchema[];
}

interface PostAddTopicCommentUploadImageSchema {
    src: string,
    dest: string
}

function resolveFileName(fileName: string, timestamp: number): string {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${getRandomInt(1000, 9999)}-` + crc32(timestamp + fileName + getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}

export async function PostAddTopicComment(req, res, next) {
    if (req.isAuthenticated()) {

        const userId = req.user.id;

        const body = {} as PostAddTopicCommentRequestSchema;
        body.imgs = [];

        const form = new IncomingForm();
        form.keepExtensions = true;
        form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
        form.multiples = false;

        form
        .on('field', function(name, value) {
            //console.log(name, value);
            body[name] = value;
        })
        .on('fileBegin', function(name, file) {

        })
        .on('file', function(name, file) {
            if (name == 'imgs') {
                body.imgs.push({
                    src: file.path,
                    dest: resolveFileName(file.name, req.timestamp)
                });
            }
        })
        .on('abort', function() {

        })
        .on('error', function(err) {
            console.error(err);
            return next(err);
        })
        .on('end', async function() {
            const topicId = +req.params.id;
            const content = body.content;
            const parentId = +body.parentId;

            const validationResult = 
                typeof topicId == 'number' &&
                !!!ValidatorHelper.checkEmpty(content) &&
                (isNaN(parentId) || (typeof parentId == 'number' && parentId > 0));
                
            if (!!!validationResult) {
                return next(new BadRequestError());
            }

            await getManager().transaction(async tem => {

                const isDestFolderExists = await FileHelper.exists(fromAssert(`static/topics/${topicId}/`));
                if (!!!isDestFolderExists) {
                    await FileHelper.mkdir(fromAssert(`static/topics/${topicId}/`));
                }

                for (const img of body.imgs) {
                    await FileHelper.copy(img.src, fromAssert(`static/topics/${topicId}/${img.dest}`));
                }

                const comment = new TopicComment();
                comment.parentCommentId = parentId;
                comment.userId = userId;
                comment.topicId = +topicId;
                comment.parentCommentId = !!!isNaN(parentId) ? parentId : null;
                comment.createdDate = req.timestamp;
                comment.content = content;

                try {
                    await tem.save(comment);
                } catch (err) {
                    return next(err);
                }

                await tem.save(body.imgs.map(picture => {
                    const pic = tem.create(CommentPictures);
                    pic.comment = { id: comment.id } as TopicComment;
                    pic.fileName = picture.dest;
                    return pic;
                }));

                res.sendStatus(200);
            });
        })
        .parse(req);
    } else {
        next(new UnauthorizedError());
    }
}