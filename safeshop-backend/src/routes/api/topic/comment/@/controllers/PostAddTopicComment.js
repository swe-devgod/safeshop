"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crc_1 = require("crc");
const formidable_1 = require("formidable");
const topic_comment_1 = require("../../../../../../database/models/topic-comment");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const bad_request_error_1 = require("../../../../../../errors/base/bad-request-error");
const utils_1 = require("../../../../../../helper/utils");
const validator_helper_1 = require("../../../../../../helper/validator-helper");
const typeorm_1 = require("typeorm");
const saefshop_path_helper_1 = require("../../../../../../helper/saefshop-path-helper");
const file_helper_1 = require("../../../../../../helper/file-helper");
const comment_pictures_1 = require("../../../../../../database/models/comment-pictures");
function resolveFileName(fileName, timestamp) {
    const lastDotIndex = fileName.lastIndexOf('.');
    if (lastDotIndex == -1) {
        return null;
    }
    const fileExt = fileName.substring(lastDotIndex, fileName.length);
    if (!fileExt.match('\.(jpg|JPG|png|PNG)$')) {
        return null;
    }
    return `${timestamp}-${utils_1.getRandomInt(1000, 9999)}-` + crc_1.crc32(timestamp + fileName + utils_1.getRandomInt(1 << 16, 1 << 32)).toString(16) + fileName.substring(lastDotIndex, fileName.length);
}
function PostAddTopicComment(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const body = {};
            body.imgs = [];
            const form = new formidable_1.IncomingForm();
            form.keepExtensions = true;
            form.maxFileSize = 10 * 1024 * 1024; // 8 MBytes
            form.multiples = false;
            form
                .on('field', function (name, value) {
                //console.log(name, value);
                body[name] = value;
            })
                .on('fileBegin', function (name, file) {
            })
                .on('file', function (name, file) {
                if (name == 'imgs') {
                    body.imgs.push({
                        src: file.path,
                        dest: resolveFileName(file.name, req.timestamp)
                    });
                }
            })
                .on('abort', function () {
            })
                .on('error', function (err) {
                console.error(err);
                return next(err);
            })
                .on('end', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    const topicId = +req.params.id;
                    const content = body.content;
                    const parentId = +body.parentId;
                    const validationResult = typeof topicId == 'number' &&
                        !!!validator_helper_1.ValidatorHelper.checkEmpty(content) &&
                        (isNaN(parentId) || (typeof parentId == 'number' && parentId > 0));
                    if (!!!validationResult) {
                        return next(new bad_request_error_1.BadRequestError());
                    }
                    yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                        const isDestFolderExists = yield file_helper_1.FileHelper.exists(saefshop_path_helper_1.fromAssert(`static/topics/${topicId}/`));
                        if (!!!isDestFolderExists) {
                            yield file_helper_1.FileHelper.mkdir(saefshop_path_helper_1.fromAssert(`static/topics/${topicId}/`));
                        }
                        for (const img of body.imgs) {
                            yield file_helper_1.FileHelper.copy(img.src, saefshop_path_helper_1.fromAssert(`static/topics/${topicId}/${img.dest}`));
                        }
                        const comment = new topic_comment_1.TopicComment();
                        comment.parentCommentId = parentId;
                        comment.userId = userId;
                        comment.topicId = +topicId;
                        comment.parentCommentId = !!!isNaN(parentId) ? parentId : null;
                        comment.createdDate = req.timestamp;
                        comment.content = content;
                        try {
                            yield tem.save(comment);
                        }
                        catch (err) {
                            return next(err);
                        }
                        yield tem.save(body.imgs.map(picture => {
                            const pic = tem.create(comment_pictures_1.CommentPictures);
                            pic.comment = { id: comment.id };
                            pic.fileName = picture.dest;
                            return pic;
                        }));
                        res.sendStatus(200);
                    }));
                });
            })
                .parse(req);
        }
        else {
            next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostAddTopicComment = PostAddTopicComment;
