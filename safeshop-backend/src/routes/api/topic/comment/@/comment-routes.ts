import { GetTopicComment } from "./controllers/GetTopicComment";
import { PostAddTopicComment } from "./controllers/PostAddTopicComment";
import { PutEditTopicComment } from "./controllers/PutEditTopicComment";
import { DeleteTopicComment } from "./controllers/DeleteTopicComment";

export const topicCommentRoutes = {
    name: "comment",
    routes: [
        {
            path: '/:id(\\d+?)/comments',
            method: 'get',
            action: GetTopicComment
        },
        {
            path: '/:id(\\d+?)/comments',
            method: 'post',
            action: PostAddTopicComment
        },
        {
            path: '/comments/:id(\\d+?)',
            method: 'put',
            action: PutEditTopicComment
        },
        {
            path: '/comments/:id(\\d+?)',
            method: 'delete',
            action: DeleteTopicComment
        }
    ]
};