"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetTopicComment_1 = require("./controllers/GetTopicComment");
const PostAddTopicComment_1 = require("./controllers/PostAddTopicComment");
const PutEditTopicComment_1 = require("./controllers/PutEditTopicComment");
const DeleteTopicComment_1 = require("./controllers/DeleteTopicComment");
exports.topicCommentRoutes = {
    name: "comment",
    routes: [
        {
            path: '/:id(\\d+?)/comments',
            method: 'get',
            action: GetTopicComment_1.GetTopicComment
        },
        {
            path: '/:id(\\d+?)/comments',
            method: 'post',
            action: PostAddTopicComment_1.PostAddTopicComment
        },
        {
            path: '/comments/:id(\\d+?)',
            method: 'put',
            action: PutEditTopicComment_1.PutEditTopicComment
        },
        {
            path: '/comments/:id(\\d+?)',
            method: 'delete',
            action: DeleteTopicComment_1.DeleteTopicComment
        }
    ]
};
