import { GetCartList } from "./@/controllers/GetCartList";
import { PutItemToCart } from "./@/controllers/PutItemToCart";
import { GetCartItemCount } from "./@/controllers/GetCartItemCount";
import { DeleteCartItem } from "./@/controllers/DeleteCartItem";
import { PatchUpdateCartItem } from "./@/controllers/PatchUpdateCartItem";

export const cartRoutes = {
    name: "cart",
    routes: [
        {
            path: '',
            method: 'get',
            action: GetCartItemCount
        },
        {
            path: '/list',
            method: 'get',
            action: GetCartList
        },
        {
            path: '',
            method: 'put',
            action: PutItemToCart
        },
        {
            path: '',
            method: 'delete',
            action: DeleteCartItem
        },
        {
            path: '',
            method: 'patch',
            action: PatchUpdateCartItem
        }
    ]
};