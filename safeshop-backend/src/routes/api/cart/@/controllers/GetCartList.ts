import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Cart } from "../../../../../database/models/cart";
import { GlobalVar } from "../../../../../helper/global-var";
import { CartDatabase } from "../../../../../database/repositories/cart-database";

interface GetCartListResponseSchema {
    id: number;
    amount: number;
    addedDate: number;                
    topic: {
        topicId: number;
        topicName: string;
        topicPrice: number;
        createdDate: number;
        topicRating: number;
        topicRatingCount: number;
        ownerUserId: number;
        ownerName: string;
        ownerPicture: string;
        ownerRating: number;
        ownerRatingCount: number;
        remainingItems: number;
        thumbnail: string;     
    }         
}

export async function GetCartList(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;

        const response = await CartDatabase.findCartItems({
            userId: userId
        });
        
        res.json(response);
    } else {
        return next(new UnauthorizedError());
    }
}