"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const topic_1 = require("../../../../../database/models/topic");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const cart_1 = require("../../../../../database/models/cart");
const topic_not_found_error_1 = require("../../../../../errors/topic-not-found-error");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
function PatchUpdateCartItem(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const { topicId, amount = 0 } = req.body;
            if (amount < 1 || isNaN(amount)) {
                return next(new bad_request_error_1.BadRequestError());
            }
            const topic = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(topic_1.Topic, 'A')
                .where('A.id = :topicId', { topicId })
                .getOne();
            if (!!!topic) {
                return next(new topic_not_found_error_1.TopicNotFoundError());
            }
            const cardItem = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(cart_1.Cart, 'A')
                .where('topicId = :topicId', { topicId })
                .andWhere('userId = :userId', { userId })
                .getOne();
            if (!!!cardItem) {
                return next(new forbidden_error_1.ForbiddenError());
            }
            if ((topic.topicRemainingItems < amount) || topic.userId == userId) {
                return next(new forbidden_error_1.ForbiddenError());
            }
            cardItem.amount = amount;
            yield typeorm_1.getManager().save(cardItem);
            res.sendStatus(200);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PatchUpdateCartItem = PatchUpdateCartItem;
