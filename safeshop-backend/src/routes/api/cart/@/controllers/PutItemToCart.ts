import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Topic } from "../../../../../database/models/topic";
import { NotFoundError } from "../../../../../errors/base/not-found-error";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { Cart } from "../../../../../database/models/cart";
import { TopicNotFoundError } from "../../../../../errors/topic-not-found-error";
import { BadRequestError } from "../../../../../errors/base/bad-request-error";

interface PutItemToCartRequestSchema {
    topicId: number;
    amount: number;
}

export async function PutItemToCart(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const {
            topicId,
            amount
        }: PutItemToCartRequestSchema = req.body;
        
        if (amount < 1) {
            return next(new BadRequestError());
        }

        const topic = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(Topic, 'A')
            .where('A.id = :topicId', { topicId })
            .getOne();

        if (!!!topic) {
            return next(new TopicNotFoundError());
        }

        const cardItem = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(Cart, 'A')
            .where('topicId = :topicId', { topicId })
            .andWhere('userId = :userId', { userId })
            .getOne();
        
        let inCardAmount = 0;
        if (cardItem != null) {
            inCardAmount = cardItem.amount;
        }

        if ((topic.topicRemainingItems < amount + inCardAmount) || topic.userId == userId) {
            return next(new ForbiddenError());
        }

        if (!!!cardItem) {
            await getManager()
            .createQueryBuilder()
            .insert()
            .into(Cart)
            .values([
                { amount, addedDate: req.timestamp, userId, topicId }
            ])
            .execute()
        } else {
            cardItem.amount = cardItem.amount + amount;
            await getManager().save(cardItem);
        }

        res.sendStatus(200);
    } else {
        return next(new UnauthorizedError());
    }
}