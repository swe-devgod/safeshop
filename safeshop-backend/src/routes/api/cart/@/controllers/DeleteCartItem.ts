import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Cart } from "../../../../../database/models/cart";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";

interface DeleteCartItemRequestSchema {
    id: number;
}

export async function DeleteCartItem(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const {
            id
        }: DeleteCartItemRequestSchema = req.body;

        await getManager().transaction(async tem => {
            const { count } = await tem.createQueryBuilder()
                .select('COUNT(*)', 'count')
                .from(Cart, 'A')
                .where('A.id = :id', { id })
                .andWhere('A.userId = :userId', { userId })
                .getRawOne();
            
            if (count == 0) {
                return next(new ForbiddenError());
            }
            
            await tem.createQueryBuilder()
                .delete()
                .from(Cart)
                .where('id = :id', { id })
                .andWhere('userId = :userId', { userId })
                .execute();
            
            return res.sendStatus(200);
        });
    } else {
        return next(new UnauthorizedError());
    }
}