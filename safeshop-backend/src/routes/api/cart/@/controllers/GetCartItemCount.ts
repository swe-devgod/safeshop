import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Cart } from "../../../../../database/models/cart";

export async function GetCartItemCount(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;

        const result = await getManager()
            .createQueryBuilder()
            .select('COALESCE(SUM(A.amount), 0) as count')
            .from(Cart, 'A')
            .where('A.userId = :userId', { userId })
            .getRawOne();

        res.json(result);


    } else {
        return next(new UnauthorizedError());
    }
}