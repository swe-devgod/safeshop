"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const cart_1 = require("../../../../../database/models/cart");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
function DeleteCartItem(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const { id } = req.body;
            yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                const { count } = yield tem.createQueryBuilder()
                    .select('COUNT(*)', 'count')
                    .from(cart_1.Cart, 'A')
                    .where('A.id = :id', { id })
                    .andWhere('A.userId = :userId', { userId })
                    .getRawOne();
                if (count == 0) {
                    return next(new forbidden_error_1.ForbiddenError());
                }
                yield tem.createQueryBuilder()
                    .delete()
                    .from(cart_1.Cart)
                    .where('id = :id', { id })
                    .andWhere('userId = :userId', { userId })
                    .execute();
                return res.sendStatus(200);
            }));
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.DeleteCartItem = DeleteCartItem;
