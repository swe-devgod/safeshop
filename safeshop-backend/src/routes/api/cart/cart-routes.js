"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetCartList_1 = require("./@/controllers/GetCartList");
const PutItemToCart_1 = require("./@/controllers/PutItemToCart");
const GetCartItemCount_1 = require("./@/controllers/GetCartItemCount");
const DeleteCartItem_1 = require("./@/controllers/DeleteCartItem");
const PatchUpdateCartItem_1 = require("./@/controllers/PatchUpdateCartItem");
exports.cartRoutes = {
    name: "cart",
    routes: [
        {
            path: '',
            method: 'get',
            action: GetCartItemCount_1.GetCartItemCount
        },
        {
            path: '/list',
            method: 'get',
            action: GetCartList_1.GetCartList
        },
        {
            path: '',
            method: 'put',
            action: PutItemToCart_1.PutItemToCart
        },
        {
            path: '',
            method: 'delete',
            action: DeleteCartItem_1.DeleteCartItem
        },
        {
            path: '',
            method: 'patch',
            action: PatchUpdateCartItem_1.PatchUpdateCartItem
        }
    ]
};
