"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PostAddItemsToCheckout_1 = require("./@/controllers/PostAddItemsToCheckout");
const GetCheckoutInformation_1 = require("./@/controllers/GetCheckoutInformation");
const PostConfirmCheckout_1 = require("./@/controllers/PostConfirmCheckout");
const PostAddCheckoutItemShippingNo_1 = require("./@/controllers/PostAddCheckoutItemShippingNo");
const GetCheckoutBilling_1 = require("./@/controllers/GetCheckoutBilling");
exports.checkoutRoutes = {
    name: "checkout",
    routes: [
        {
            path: '/:id(\\d+?)',
            method: 'get',
            action: GetCheckoutInformation_1.GetCheckoutInformation
        },
        {
            path: '/:id(\\d+?)/billing',
            method: 'get',
            action: GetCheckoutBilling_1.GetCheckoutBilling
        },
        {
            path: '',
            method: 'post',
            action: PostAddItemsToCheckout_1.PostAddItemsToCheckout
        },
        {
            path: '/:id(\\d+?)',
            method: 'post',
            action: PostConfirmCheckout_1.PostConfirmCheckout
        },
        {
            path: '/items/:id(\\d+?)',
            method: 'post',
            action: PostAddCheckoutItemShippingNo_1.PostAddCheckoutItemShippingNo
        }
    ]
};
