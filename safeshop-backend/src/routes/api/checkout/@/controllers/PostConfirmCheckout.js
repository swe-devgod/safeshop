"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const address_1 = require("../../../../../database/models/address");
const topic_shipping_1 = require("../../../../../database/models/topic-shipping");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const checkout_item_1 = require("../../../../../database/models/checkout-item");
const checkout_1 = require("../../../../../database/models/checkout");
const checkout_shipping_1 = require("../../../../../database/models/checkout-shipping");
const topic_1 = require("../../../../../database/models/topic");
const checkout_item_status_1 = require("../../../../../database/enums/checkout-item-status");
const checkout_status_1 = require("../../../../../database/enums/checkout-status");
var PostConfitmCheckoutError;
(function (PostConfitmCheckoutError) {
    PostConfitmCheckoutError[PostConfitmCheckoutError["INVALID_CHECKOUT_ID"] = 0] = "INVALID_CHECKOUT_ID";
    PostConfitmCheckoutError[PostConfitmCheckoutError["INVALID_ADDRESS_ID"] = 1] = "INVALID_ADDRESS_ID";
    PostConfitmCheckoutError[PostConfitmCheckoutError["INVALID_CHECKOUT_ITEM_ID"] = 2] = "INVALID_CHECKOUT_ITEM_ID";
    PostConfitmCheckoutError[PostConfitmCheckoutError["INVLAID_TOPIC_SHIPPING_ID"] = 3] = "INVLAID_TOPIC_SHIPPING_ID";
    PostConfitmCheckoutError[PostConfitmCheckoutError["INVALID_AMOUNT"] = 4] = "INVALID_AMOUNT";
})(PostConfitmCheckoutError || (PostConfitmCheckoutError = {}));
function PostConfirmCheckout(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const checkoutId = +req.params.id;
            const { checkoutIdCount } = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('COUNT(*)', 'checkoutIdCount')
                .from(checkout_1.Checkout, 'A')
                .where('A.id = :id', { id: checkoutId })
                .andWhere('A.userId = :userId', { userId })
                .getRawOne();
            if (checkoutIdCount == 0) {
                return next(new forbidden_error_1.ForbiddenError('Invalid Checkout ID', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ID }));
            }
            const body = req.body;
            const { addrCount } = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('COUNT(*)', 'addrCount')
                .from(address_1.Address, 'A')
                .where('A.id = :id', { id: body.selectedAddressId })
                .andWhere('A.userId = :userId', { userId })
                .getRawOne();
            if (addrCount == 0) {
                return next(new forbidden_error_1.ForbiddenError('AddressID', { errorCode: PostConfitmCheckoutError.INVALID_ADDRESS_ID }));
            }
            // FIND ALL CHECKOUT ITEM WITHIN CHECKOUT
            const checkouteItems = yield typeorm_1.getManager().createQueryBuilder()
                .select('A.id', 'id')
                .from(checkout_item_1.CheckoutItem, 'A')
                .where('A.checkoutId = :checkoutId', { checkoutId })
                .getRawMany();
            const checkoutItemIds = new Set(checkouteItems.map(item => item.id));
            try {
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    let throwError = false;
                    let errorObj = null;
                    let invalidAmountCheckoutItemIds = null;
                    for (const shipping of body.checkoutShippings) {
                        if (!!!checkoutItemIds.has(shipping.itemId)) {
                            throw new forbidden_error_1.ForbiddenError('Invalid CheckoutItem ID', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ITEM_ID });
                        }
                        else {
                            checkoutItemIds.delete(shipping.itemId);
                        }
                        const forCheckingTopicResult = yield tem.createQueryBuilder()
                            .select('A.topicId', 'topicId')
                            .addSelect('A.amount', 'amount')
                            .addSelect('B.topicPrice', 'price')
                            .addSelect('B.topicRemainingItems', 'remainingItems')
                            .from(checkout_item_1.CheckoutItem, 'A')
                            .innerJoin(topic_1.Topic, 'B', 'B.id = A.topicId')
                            .where('A.id = :id', { id: shipping.itemId })
                            .andWhere('A.checkoutId = :checkoutId', { checkoutId })
                            .getRawOne();
                        if (!!!forCheckingTopicResult) {
                            throw new forbidden_error_1.ForbiddenError('Invalid CheckoutItem ID', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ITEM_ID });
                        }
                        if (forCheckingTopicResult.amount > forCheckingTopicResult.remainingItems) {
                            if (invalidAmountCheckoutItemIds == null) {
                                throwError = true;
                                invalidAmountCheckoutItemIds = [forCheckingTopicResult.topicId];
                            }
                            else {
                                invalidAmountCheckoutItemIds.push(forCheckingTopicResult.topicId);
                            }
                            errorObj = new forbidden_error_1.ForbiddenError(null, {
                                errorCode: PostConfitmCheckoutError.INVALID_AMOUNT,
                                itemIds: invalidAmountCheckoutItemIds
                            });
                        }
                        if (!!!throwError) {
                            const { shippingCount } = yield tem.createQueryBuilder()
                                .select('COUNT(*)', 'shippingCount')
                                .from(topic_shipping_1.TopicShipping, 'A')
                                .where('A.id = :id', { id: shipping.topicShippingId })
                                .andWhere('A.topicId = :topicId', { topicId: forCheckingTopicResult.topicId })
                                .getRawOne();
                            if (shippingCount == 0) {
                                throw new forbidden_error_1.ForbiddenError('Invalid TopicShipping ID', { errorCode: PostConfitmCheckoutError.INVLAID_TOPIC_SHIPPING_ID });
                            }
                            // EVERYTHING IS OK!!!
                            yield tem.createQueryBuilder()
                                .insert()
                                .into(checkout_shipping_1.CheckoutShipping)
                                .values({
                                checkoutItem: { id: shipping.itemId },
                                shipping: { id: shipping.topicShippingId }
                            })
                                .execute();
                            yield tem.createQueryBuilder()
                                .update(checkout_item_1.CheckoutItem)
                                .set({ pricePerItem: forCheckingTopicResult.price, statusId: checkout_item_status_1.CheckoutItemStatusEnum.AwaitingShipment })
                                .where('id = :id', { id: shipping.itemId })
                                .execute();
                        }
                    }
                    if (throwError) {
                        throw errorObj;
                    }
                    else if (checkoutItemIds.size > 0) {
                        throw new forbidden_error_1.ForbiddenError('Invalid CheckoutItem ID #2', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ITEM_ID });
                    }
                    else {
                        // EVERYTHING IS OK!!!
                        yield tem.createQueryBuilder()
                            .update(checkout_1.Checkout)
                            .set({ statusId: checkout_status_1.CheckoutStatusEnum.AwaitingSuccess, addressId: body.selectedAddressId })
                            .where('id = :id', { id: checkoutId })
                            .execute();
                    }
                }));
            }
            catch (err) {
                return next(err);
            }
            res.sendStatus(200);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostConfirmCheckout = PostConfirmCheckout;
