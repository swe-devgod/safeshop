import { getManager } from "typeorm";
import { Checkout } from "../../../../../database/models/checkout";
import { Cart } from "../../../../../database/models/cart";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { CheckoutStatus } from "../../../../../database/models/checkout-status";
import { CheckoutStatusEnum } from "../../../../../database/enums/checkout-status";
import { User } from "../../../../../database/models/user";
import { CheckoutItem } from "../../../../../database/models/checkout-item";
import { Topic } from "../../../../../database/models/topic";
import { CheckoutItemStatus } from "../../../../../database/models/checkout-item-status";
import { CheckoutItemStatusEnum } from "../../../../../database/enums/checkout-item-status";


interface PostAddItemsToCheckoutRequestSchema {
    cardItemIds: number[];
}

export async function PostAddItemsToCheckout(req, res, next) {
    if (req.isAuthenticated()) {
        await getManager().transaction(async tem => {
            const userId = req.user.id;

            const { 
                cardItemIds 
            }: PostAddItemsToCheckoutRequestSchema = req.body;

            const result = await tem.createQueryBuilder()
                .select('A')
                .from(Cart, 'A')
                .innerJoinAndSelect('A.topic', 'B')
                .whereInIds(cardItemIds)
                .andWhere('A.userId = :userId', { userId })
                .getMany();

            if (result.length != cardItemIds.length) {
                return next(new ForbiddenError());
            }

            const checkout = tem.create(Checkout);        
            checkout.checkoutDate = req.timestamp;
            checkout.user = <User>{ id: userId };
            checkout.status = <CheckoutStatus>{ id: CheckoutStatusEnum.AwaitingPayment };
            await tem.save(checkout);

            const checkoutItems = result.map(cardItem => {
                const checkoutItem = tem.create(CheckoutItem);
                checkoutItem.checkout = <Checkout>{ id: checkout.id };
                checkoutItem.pricePerItem = null;
                checkoutItem.amount = cardItem.amount;
                checkoutItem.addedDate = cardItem.addedDate;
                checkoutItem.topic = <Topic>{ id: cardItem.topicId };
                checkoutItem.status = <CheckoutItemStatus>{ id: CheckoutItemStatusEnum.AwaitingPayment };
                return checkoutItem;
            });
            await tem.save(checkoutItems);

            await tem.createQueryBuilder()
                .delete()
                .from(Cart)
                .whereInIds(cardItemIds)
                .andWhere('userId = :userId', { userId })
                .execute();

            res.json({
                checkoutId: checkout.id
            });
        });
    } else {
        return next(new UnauthorizedError());
    }
}