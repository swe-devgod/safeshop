import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { CheckoutDatabase } from "../../../../../database/repositories/checkout-database";
import { getManager } from "typeorm";
import { CheckoutItem } from "../../../../../database/models/checkout-item";
import { CheckoutShipping } from "../../../../../database/models/checkout-shipping";
import { Checkout } from "../../../../../database/models/checkout";
import { User } from "../../../../../database/models/user";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { CheckoutItemStatus } from "../../../../../database/models/checkout-item-status";
import { CheckoutItemStatusEnum } from "../../../../../database/enums/checkout-item-status";
import { ValidatorHelper } from "../../../../../helper/validator-helper";
import { BadRequestError } from "../../../../../errors/base/bad-request-error";

interface PostAddCheckoutItemShippingNoRequestSchema {
    shippingNo: string;
}

export async function PostAddCheckoutItemShippingNo(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const checkoutItemId = +req.params.id;

        const { 
            shippingNo
        }: PostAddCheckoutItemShippingNoRequestSchema = req.body;

        if (typeof shippingNo == 'string') {
            if (ValidatorHelper.checkEmpty(shippingNo)) {
                return next(new BadRequestError());
            }
        } else {
            return next(new BadRequestError());
        }

        const checkoutItem =await getManager().createQueryBuilder()
            .select('A')
            .from(CheckoutItem, 'A')
            .innerJoin('A.topic', 'B', 'B.userId = :userId', { userId })
            .where('A.id = :checkoutItemId', { checkoutItemId })
            .getOne();

        if (!!!checkoutItem || (checkoutItem.statusId != CheckoutItemStatusEnum.AwaitingShipment && checkoutItem.statusId != CheckoutItemStatusEnum.Shipping)) {
            return next(new ForbiddenError());
        }

        try {
            await getManager().transaction(async tem => {
                const checkoutShipping = await tem.createQueryBuilder()
                    .select('A')
                    .from(CheckoutShipping, 'A')
                    .where('A.checkoutItemId = :checkoutItemId', { checkoutItemId })
                    .getOne();
                checkoutShipping.shippingNo = shippingNo;
                await tem.save(checkoutShipping);

                checkoutItem.status = { id: CheckoutItemStatusEnum.Shipping } as CheckoutItemStatus;
                await tem.save(checkoutItem);
            });
        } catch (err) {
            return next(err);
        }
        res.sendStatus(200);
    } else {
        return next(new UnauthorizedError());
    }
}

// export async function PostAddCheckoutItemShippingNo(req, res, next) {
//     if (req.isAuthenticated()) {
//         const userId = req.user.id;
//         const checkoutItemId = +req.params.id;
//         console.log({ userId, checkoutItemId });
//         const { 
//             shippingNo 
//         }: PostAddCheckoutItemShippingNoRequestSchema = req.body;

//         const checkoutOwnerResult = await getManager().createQueryBuilder()
//             .select(['B.userId'])
//             .from(CheckoutItem, 'A')
//             .innerJoin(Checkout, 'B', 'B.id = A.checkoutId AND B.userId = :userId', { userId })
//             //.innerJoin(Checkout, 'B', 'A.checkoutId = B.id')
//             .where('A.id = :checkoutItemId', { checkoutItemId })
//             .getRawOne();

//         if (!!!checkoutOwnerResult) {
//             return next(new ForbiddenError());
//         }

//         const checkoutShipping = await getManager().createQueryBuilder()
//             .select('A')
//             .from(CheckoutShipping, 'A')
//             .where('A.checkoutItemId = :checkoutItemId', { checkoutItemId })
//             .getOne();
//         checkoutShipping.shippingNo = shippingNo;
//         await getManager().save(checkoutShipping);

//         res.sendStatus(200);
//     } else {
//         return next(new UnauthorizedError());
//     }
// }