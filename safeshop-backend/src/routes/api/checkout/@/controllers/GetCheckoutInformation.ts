import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { CheckoutDatabase } from "../../../../../database/repositories/checkout-database";

export async function GetCheckoutInformation(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const checkoutId = +req.params.id;

        const response = await CheckoutDatabase.getCheckoutInformation({ 
            userId: userId, 
            checkoutId: checkoutId 
        });

        res.json(response);
    } else {
        return next(new UnauthorizedError());
    }
}