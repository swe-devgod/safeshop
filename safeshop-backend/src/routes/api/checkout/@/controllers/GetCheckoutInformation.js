"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const checkout_database_1 = require("../../../../../database/repositories/checkout-database");
function GetCheckoutInformation(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const checkoutId = +req.params.id;
            const response = yield checkout_database_1.CheckoutDatabase.getCheckoutInformation({
                userId: userId,
                checkoutId: checkoutId
            });
            res.json(response);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.GetCheckoutInformation = GetCheckoutInformation;
