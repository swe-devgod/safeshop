"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const checkout_1 = require("../../../../../database/models/checkout");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const client_url_helper_1 = require("../../../../../helper/client-url-helper");
function GetCheckoutBilling(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const checkoutId = +req.params.id;
            const checkout = yield typeorm_1.getManager().createQueryBuilder()
                .select('A')
                .from(checkout_1.Checkout, 'A')
                .innerJoinAndSelect('A.checkoutItems', 'B')
                .innerJoinAndSelect('A.address', 'F')
                .innerJoinAndSelect('A.user', 'G')
                .innerJoinAndSelect('B.topic', 'C')
                .innerJoinAndSelect('C.thumbnail', 'D')
                .innerJoinAndSelect('D.topicPicture', 'E')
                .innerJoinAndSelect('B.shipping', 'H')
                .innerJoinAndSelect('H.shipping', 'I')
                .innerJoinAndSelect('I.shipping', 'J')
                .where('A.id = :checkoutId', { checkoutId })
                .andWhere('A.userId = :userId', { userId })
                .getOne();
            if (!!!checkout) {
                return next(new forbidden_error_1.ForbiddenError());
            }
            const response = {
                user: checkout.user,
                address: {
                    id: checkout.address.id,
                    address: checkout.address.address
                },
                paymentMethods: {
                    id: 1,
                    name: 'PromptPay'
                },
                checkout: {
                    id: checkout.id,
                    checkoutDate: checkout.checkoutDate,
                    statusId: checkout.statusId,
                    checkoutItems: checkout.checkoutItems.map(item => ({
                        id: item.id,
                        pricePerItem: item.pricePerItem,
                        amount: item.amount,
                        addedDate: item.addedDate,
                        topic: Object.assign({}, item.topic, { thumbnail: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${item.topic.id}/${item.topic.thumbnail.topicPicture.fileName}`) }),
                        shipping: {
                            id: item.shipping.shipping.id,
                            price: item.shipping.shipping.price,
                            name: item.shipping.shipping.shipping.name
                        }
                    }))
                }
            };
            res.json(response);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.GetCheckoutBilling = GetCheckoutBilling;
