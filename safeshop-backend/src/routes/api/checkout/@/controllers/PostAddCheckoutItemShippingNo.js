"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const checkout_item_1 = require("../../../../../database/models/checkout-item");
const checkout_shipping_1 = require("../../../../../database/models/checkout-shipping");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const checkout_item_status_1 = require("../../../../../database/enums/checkout-item-status");
const validator_helper_1 = require("../../../../../helper/validator-helper");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
function PostAddCheckoutItemShippingNo(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const checkoutItemId = +req.params.id;
            const { shippingNo } = req.body;
            if (typeof shippingNo == 'string') {
                if (validator_helper_1.ValidatorHelper.checkEmpty(shippingNo)) {
                    return next(new bad_request_error_1.BadRequestError());
                }
            }
            else {
                return next(new bad_request_error_1.BadRequestError());
            }
            const checkoutItem = yield typeorm_1.getManager().createQueryBuilder()
                .select('A')
                .from(checkout_item_1.CheckoutItem, 'A')
                .innerJoin('A.topic', 'B', 'B.userId = :userId', { userId })
                .where('A.id = :checkoutItemId', { checkoutItemId })
                .getOne();
            if (!!!checkoutItem || (checkoutItem.statusId != checkout_item_status_1.CheckoutItemStatusEnum.AwaitingShipment && checkoutItem.statusId != checkout_item_status_1.CheckoutItemStatusEnum.Shipping)) {
                return next(new forbidden_error_1.ForbiddenError());
            }
            try {
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    const checkoutShipping = yield tem.createQueryBuilder()
                        .select('A')
                        .from(checkout_shipping_1.CheckoutShipping, 'A')
                        .where('A.checkoutItemId = :checkoutItemId', { checkoutItemId })
                        .getOne();
                    checkoutShipping.shippingNo = shippingNo;
                    yield tem.save(checkoutShipping);
                    checkoutItem.status = { id: checkout_item_status_1.CheckoutItemStatusEnum.Shipping };
                    yield tem.save(checkoutItem);
                }));
            }
            catch (err) {
                return next(err);
            }
            res.sendStatus(200);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostAddCheckoutItemShippingNo = PostAddCheckoutItemShippingNo;
// export async function PostAddCheckoutItemShippingNo(req, res, next) {
//     if (req.isAuthenticated()) {
//         const userId = req.user.id;
//         const checkoutItemId = +req.params.id;
//         console.log({ userId, checkoutItemId });
//         const { 
//             shippingNo 
//         }: PostAddCheckoutItemShippingNoRequestSchema = req.body;
//         const checkoutOwnerResult = await getManager().createQueryBuilder()
//             .select(['B.userId'])
//             .from(CheckoutItem, 'A')
//             .innerJoin(Checkout, 'B', 'B.id = A.checkoutId AND B.userId = :userId', { userId })
//             //.innerJoin(Checkout, 'B', 'A.checkoutId = B.id')
//             .where('A.id = :checkoutItemId', { checkoutItemId })
//             .getRawOne();
//         if (!!!checkoutOwnerResult) {
//             return next(new ForbiddenError());
//         }
//         const checkoutShipping = await getManager().createQueryBuilder()
//             .select('A')
//             .from(CheckoutShipping, 'A')
//             .where('A.checkoutItemId = :checkoutItemId', { checkoutItemId })
//             .getOne();
//         checkoutShipping.shippingNo = shippingNo;
//         await getManager().save(checkoutShipping);
//         res.sendStatus(200);
//     } else {
//         return next(new UnauthorizedError());
//     }
// }
