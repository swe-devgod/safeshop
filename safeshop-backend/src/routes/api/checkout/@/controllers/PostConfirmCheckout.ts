import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Address } from "../../../../../database/models/address";
import { TopicShipping } from "../../../../../database/models/topic-shipping";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { CheckoutItem } from "../../../../../database/models/checkout-item";
import { Checkout } from "../../../../../database/models/checkout";
import { CheckoutShipping } from "../../../../../database/models/checkout-shipping";
import { Topic } from "../../../../../database/models/topic";
import { CheckoutItemStatusEnum } from "../../../../../database/enums/checkout-item-status";
import { CheckoutStatusEnum } from "../../../../../database/enums/checkout-status";

interface CheckoutShippingRequestSchema {
    itemId: number;
    topicShippingId: number;
}

interface PostConfirmCheckoutRequestSchema {
    selectedAddressId: number;
    selectedPaymentId: number;
    checkoutShippings: CheckoutShippingRequestSchema[]
}

enum PostConfitmCheckoutError {
    INVALID_CHECKOUT_ID = 0,
    INVALID_ADDRESS_ID = 1,
    INVALID_CHECKOUT_ITEM_ID = 2,
    INVLAID_TOPIC_SHIPPING_ID = 3,
    INVALID_AMOUNT = 4,
}

export async function PostConfirmCheckout(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const checkoutId = +req.params.id;

        const { checkoutIdCount } = await getManager()
            .createQueryBuilder()
            .select('COUNT(*)', 'checkoutIdCount')
            .from(Checkout, 'A')
            .where('A.id = :id', { id: checkoutId })
            .andWhere('A.userId = :userId', { userId })
            .getRawOne();

        if (checkoutIdCount == 0) {
            return next(new ForbiddenError('Invalid Checkout ID', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ID }));
        }

        const body = req.body as PostConfirmCheckoutRequestSchema;

        const { addrCount } = await getManager()
            .createQueryBuilder()
            .select('COUNT(*)', 'addrCount')
            .from(Address, 'A')
            .where('A.id = :id', { id: body.selectedAddressId })
            .andWhere('A.userId = :userId', { userId })
            .getRawOne();

        if (addrCount == 0) {
            return next(new ForbiddenError('AddressID', { errorCode: PostConfitmCheckoutError.INVALID_ADDRESS_ID }));
        }

        // FIND ALL CHECKOUT ITEM WITHIN CHECKOUT
        const checkouteItems = await getManager().createQueryBuilder()
            .select('A.id', 'id')
            .from(CheckoutItem, 'A')
            .where('A.checkoutId = :checkoutId', { checkoutId })
            .getRawMany();
        
        const checkoutItemIds = new Set(checkouteItems.map(item => item.id));

        try {
            await getManager().transaction(async tem => {

                let throwError = false;
                let errorObj = null;
                let invalidAmountCheckoutItemIds = null;

                for (const shipping of body.checkoutShippings) {         
                    if (!!!checkoutItemIds.has(shipping.itemId)) {
                        throw new ForbiddenError('Invalid CheckoutItem ID', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ITEM_ID });
                    } else {
                        checkoutItemIds.delete(shipping.itemId);
                    }

                    const forCheckingTopicResult = await tem.createQueryBuilder()
                    .select('A.topicId', 'topicId')
                    .addSelect('A.amount', 'amount')
                    .addSelect('B.topicPrice', 'price')
                    .addSelect('B.topicRemainingItems', 'remainingItems')
                    .from(CheckoutItem, 'A')
                    .innerJoin(Topic, 'B', 'B.id = A.topicId')
                    .where('A.id = :id', { id: shipping.itemId })
                    .andWhere('A.checkoutId = :checkoutId', { checkoutId })
                    .getRawOne();

                    if (!!!forCheckingTopicResult) {
                        throw new ForbiddenError('Invalid CheckoutItem ID', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ITEM_ID });
                    }

                    if (forCheckingTopicResult.amount > forCheckingTopicResult.remainingItems) {
                        if (invalidAmountCheckoutItemIds == null) {
                            throwError = true;
                            invalidAmountCheckoutItemIds = [ forCheckingTopicResult.topicId ];
                        } else {
                            invalidAmountCheckoutItemIds.push(forCheckingTopicResult.topicId);
                        }
                        
                        errorObj = new ForbiddenError(null, { 
                            errorCode: PostConfitmCheckoutError.INVALID_AMOUNT,
                            itemIds: invalidAmountCheckoutItemIds 
                        });
                    }

                    if (!!!throwError) {
                        const { shippingCount } = await tem.createQueryBuilder()
                        .select('COUNT(*)', 'shippingCount')
                        .from(TopicShipping, 'A')
                        .where('A.id = :id', { id: shipping.topicShippingId })
                        .andWhere('A.topicId = :topicId', { topicId: forCheckingTopicResult.topicId })
                        .getRawOne();

                        if (shippingCount == 0) {
                            throw new ForbiddenError('Invalid TopicShipping ID', { errorCode: PostConfitmCheckoutError.INVLAID_TOPIC_SHIPPING_ID });
                        }

                        // EVERYTHING IS OK!!!
                        await tem.createQueryBuilder()
                            .insert()
                            .into(CheckoutShipping)
                            .values({
                                checkoutItem: { id: shipping.itemId } as CheckoutItem,
                                shipping: { id: shipping.topicShippingId } as TopicShipping
                            })
                            .execute();

                        await tem.createQueryBuilder()
                            .update(CheckoutItem)
                            .set({ pricePerItem: forCheckingTopicResult.price, statusId: CheckoutItemStatusEnum.AwaitingShipment } as CheckoutItem)
                            .where('id = :id', { id: shipping.itemId })
                            .execute();
                    }
                }

                if (throwError) {
                    throw errorObj;
                } else if (checkoutItemIds.size > 0) {
                    throw new ForbiddenError('Invalid CheckoutItem ID #2', { errorCode: PostConfitmCheckoutError.INVALID_CHECKOUT_ITEM_ID });
                } else {
                    // EVERYTHING IS OK!!!
                    await tem.createQueryBuilder()
                    .update(Checkout)
                    .set({ statusId: CheckoutStatusEnum.AwaitingSuccess, addressId: body.selectedAddressId } as Checkout)
                    .where('id = :id', { id: checkoutId })
                    .execute();
                }
            });
        } catch(err) {
            return next(err);
        }
        
        res.sendStatus(200);
    } else {
        return next(new UnauthorizedError());
    }
}