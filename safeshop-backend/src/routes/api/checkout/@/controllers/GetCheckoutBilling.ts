import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { CheckoutDatabase } from "../../../../../database/repositories/checkout-database";
import { getManager } from "typeorm";
import { Checkout } from "../../../../../database/models/checkout";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { GlobalVar } from "../../../../../helper/global-var";
import { ClientURLHelper } from "../../../../../helper/client-url-helper";

interface CheckoutItemShippingSchema {
    id: number;
    price: number;
    name: string;
}

interface CheckoutItemTopicSchema {
    id: number,
    topicCreatedDate: number,
    topicPrice: number,
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string;
}

interface CheckoutItemSchema {
    id: number;
    pricePerItem: number;
    amount: number;
    addedDate: number;
    topic: CheckoutItemTopicSchema;
    shipping: CheckoutItemShippingSchema;
}

interface UserSchema {
    id: number,
    firstName: string,
    middleName: string,
    lastName: string,
    displayName: string,
    email: string,
    tel: string,
    registerDate: number,
    rating: number,
    ratingVoteCount: number,
}

interface AddressSchema {
    id: number;
    address: string;
}

interface PaymentSchema {
    id: number;
    name: string;
}

interface GetCheckoutBillingResponseSchema {
    user: UserSchema;
    checkout: {
        checkoutDate: number,
        id: number,
        statusId: number,
        checkoutItems: CheckoutItemSchema[],
    },
    address: AddressSchema,
    paymentMethods: PaymentSchema;
}

export async function GetCheckoutBilling(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;
        const checkoutId = +req.params.id;

        const checkout = await getManager().createQueryBuilder()
            .select('A')
            .from(Checkout, 'A')
            .innerJoinAndSelect('A.checkoutItems', 'B')
            .innerJoinAndSelect('A.address', 'F')
            .innerJoinAndSelect('A.user', 'G')
            .innerJoinAndSelect('B.topic', 'C')
            .innerJoinAndSelect('C.thumbnail', 'D')
            .innerJoinAndSelect('D.topicPicture', 'E')
            .innerJoinAndSelect('B.shipping', 'H')
            .innerJoinAndSelect('H.shipping', 'I')
            .innerJoinAndSelect('I.shipping', 'J')
            .where('A.id = :checkoutId', { checkoutId })
            .andWhere('A.userId = :userId', { userId })
            .getOne();

        if (!!!checkout) {
            return next(new ForbiddenError());
        }

        const response = {
            user: checkout.user,
            address: {
                id: checkout.address.id,
                address: checkout.address.address
            },
            paymentMethods: {
                id: 1,
                name: 'PromptPay'
            },
            checkout: {
                id: checkout.id,
                checkoutDate: checkout.checkoutDate,
                statusId: checkout.statusId,
                checkoutItems: checkout.checkoutItems.map(item => ({
                    id: item.id,
                    pricePerItem: item.pricePerItem,
                    amount: item.amount,
                    addedDate: item.addedDate,
                    topic: {
                        ...item.topic,
                        thumbnail: ClientURLHelper.resolve(`/static/topics/${item.topic.id}/${item.topic.thumbnail.topicPicture.fileName}`)
                    } as CheckoutItemTopicSchema,
                    shipping: { 
                        id: item.shipping.shipping.id,
                        price: item.shipping.shipping.price,
                        name: item.shipping.shipping.shipping.name
                    }
                }) as CheckoutItemSchema)
            }
        } as GetCheckoutBillingResponseSchema;

        res.json(response);
    } else {
        return next(new UnauthorizedError());
    }
}