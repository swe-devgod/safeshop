"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const checkout_1 = require("../../../../../database/models/checkout");
const cart_1 = require("../../../../../database/models/cart");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const checkout_status_1 = require("../../../../../database/enums/checkout-status");
const checkout_item_1 = require("../../../../../database/models/checkout-item");
const checkout_item_status_1 = require("../../../../../database/enums/checkout-item-status");
function PostAddItemsToCheckout(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                const userId = req.user.id;
                const { cardItemIds } = req.body;
                const result = yield tem.createQueryBuilder()
                    .select('A')
                    .from(cart_1.Cart, 'A')
                    .innerJoinAndSelect('A.topic', 'B')
                    .whereInIds(cardItemIds)
                    .andWhere('A.userId = :userId', { userId })
                    .getMany();
                if (result.length != cardItemIds.length) {
                    return next(new forbidden_error_1.ForbiddenError());
                }
                const checkout = tem.create(checkout_1.Checkout);
                checkout.checkoutDate = req.timestamp;
                checkout.user = { id: userId };
                checkout.status = { id: checkout_status_1.CheckoutStatusEnum.AwaitingPayment };
                yield tem.save(checkout);
                const checkoutItems = result.map(cardItem => {
                    const checkoutItem = tem.create(checkout_item_1.CheckoutItem);
                    checkoutItem.checkout = { id: checkout.id };
                    checkoutItem.pricePerItem = null;
                    checkoutItem.amount = cardItem.amount;
                    checkoutItem.addedDate = cardItem.addedDate;
                    checkoutItem.topic = { id: cardItem.topicId };
                    checkoutItem.status = { id: checkout_item_status_1.CheckoutItemStatusEnum.AwaitingPayment };
                    return checkoutItem;
                });
                yield tem.save(checkoutItems);
                yield tem.createQueryBuilder()
                    .delete()
                    .from(cart_1.Cart)
                    .whereInIds(cardItemIds)
                    .andWhere('userId = :userId', { userId })
                    .execute();
                res.json({
                    checkoutId: checkout.id
                });
            }));
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostAddItemsToCheckout = PostAddItemsToCheckout;
