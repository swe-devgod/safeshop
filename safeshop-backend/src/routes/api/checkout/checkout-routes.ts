import { PostAddItemsToCheckout } from "./@/controllers/PostAddItemsToCheckout";
import { GetCheckoutInformation } from "./@/controllers/GetCheckoutInformation";
import { PostConfirmCheckout } from "./@/controllers/PostConfirmCheckout";
import { PostAddCheckoutItemShippingNo } from './@/controllers/PostAddCheckoutItemShippingNo';
import { GetCheckoutBilling } from "./@/controllers/GetCheckoutBilling";

export const checkoutRoutes = {
    name: "checkout",
    routes: [
        {
            path: '/:id(\\d+?)',
            method: 'get',
            action: GetCheckoutInformation
        },
        {
            path: '/:id(\\d+?)/billing',
            method: 'get',
            action: GetCheckoutBilling
        },
        {
            path: '',
            method: 'post',
            action: PostAddItemsToCheckout
        },
        {
            path: '/:id(\\d+?)',
            method: 'post',
            action: PostConfirmCheckout
        },
        {
            path: '/items/:id(\\d+?)',
            method: 'post',
            action: PostAddCheckoutItemShippingNo
        }
    ]
};