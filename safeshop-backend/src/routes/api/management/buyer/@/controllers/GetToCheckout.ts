import { getManager } from "typeorm";
import { format } from "path";
import { CheckoutItem } from "../../../../../../database/models/checkout-item";
import { CheckoutItemStatusEnum } from "../../../../../../database/enums/checkout-item-status";
import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { GlobalVar } from "../../../../../../helper/global-var";
import { Checkout } from "../../../../../../database/models/checkout";
import { CheckoutStatusEnum } from "../../../../../../database/enums/checkout-status";
import { ClientURLHelper } from "../../../../../../helper/client-url-helper";

interface GetToCheckoutResponseSchema {
    id: number;
    checkoutDate: number;
    statusId: number;
    addressId: number;
    checkoutItems: GetToCheckoutItemSchema[];
}

interface GetToCheckoutItemSchema {
    id: number;
    pricePerItem: number;
    amount: number;
    addedDate: number;
    statusId: number
    topic: GetToCheckoutTopicSchema;
    shippings: GetToCheckoutShippingSchema[];
    user: GetToCheckoutUserSchema;
}

interface GetToCheckoutTopicSchema {
    id: number;
    topicCreatedDate: number;
    topicPrice: number;
    topicRemainingItems: number;
    topicName: string;
    topicDescription: string;
    topicVoteCount: number;
    topicRating: number;
    thumbnail: string;
}

interface GetToCheckoutShippingSchema {
    id: number;
    name: string;
}

interface GetToCheckoutUserSchema {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    displayName: string;
    email: string;
    tel: string;
    registerDate: number;
    rating: number;
    ratingVoteCount: number;
}

export async function GetToCheckout(req, res, next) {
    if (req.isAuthenticated()) {
    const userId=  req.user.id;

        const result = await getManager().createQueryBuilder()
            .select('A')
            .from(Checkout, 'A')
            .innerJoinAndSelect('A.checkoutItems', 'B')
            .innerJoinAndSelect('B.topic', 'C')
            .innerJoinAndSelect('C.thumbnail', 'D')
            .innerJoinAndSelect('D.topicPicture', 'E')
            .innerJoinAndSelect('C.shippings', 'F')
            .innerJoinAndSelect('F.shipping', 'G')
            .innerJoinAndSelect('C.user', 'H')
            .where('A.statusId = :statusId', { statusId: CheckoutStatusEnum.AwaitingPayment })
            .andWhere('A.userId = :userId', { userId })
            .getMany();

        const response = result.map(r => {
            const res = {
                id: r.id,
                checkoutDate: r.checkoutDate,
                statusId: r.statusId,
                addressId: r.addressId,
                checkoutItems: r.checkoutItems.map(item => ({
                    id: item.id,
                    pricePerItem: item.pricePerItem,
                    amount: item.amount,
                    addedDate: item.addedDate,
                    statusId: item.statusId,
                    topic: {
                        ...item.topic,
                        thumbnail: ClientURLHelper.resolve(`/static/topics/${item.topic.id}/${item.topic.thumbnail.topicPicture.fileName}`),
                    } as GetToCheckoutTopicSchema,
                    shippings: item.topic.shippings.map(shipping => ({
                        id: shipping.id,
                        name: shipping.shipping.name
                    }) as GetToCheckoutShippingSchema),
                    user: item.topic.user
                }) as GetToCheckoutItemSchema)
            } as GetToCheckoutResponseSchema;

            return res;
        });

        res.json(response);
    } else {
        return next(new UnauthorizedError());
    }
}