"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const checkout_1 = require("../../../../../../database/models/checkout");
const checkout_status_1 = require("../../../../../../database/enums/checkout-status");
const client_url_helper_1 = require("../../../../../../helper/client-url-helper");
function GetToCheckout(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const result = yield typeorm_1.getManager().createQueryBuilder()
                .select('A')
                .from(checkout_1.Checkout, 'A')
                .innerJoinAndSelect('A.checkoutItems', 'B')
                .innerJoinAndSelect('B.topic', 'C')
                .innerJoinAndSelect('C.thumbnail', 'D')
                .innerJoinAndSelect('D.topicPicture', 'E')
                .innerJoinAndSelect('C.shippings', 'F')
                .innerJoinAndSelect('F.shipping', 'G')
                .innerJoinAndSelect('C.user', 'H')
                .where('A.statusId = :statusId', { statusId: checkout_status_1.CheckoutStatusEnum.AwaitingPayment })
                .andWhere('A.userId = :userId', { userId })
                .getMany();
            const response = result.map(r => {
                const res = {
                    id: r.id,
                    checkoutDate: r.checkoutDate,
                    statusId: r.statusId,
                    addressId: r.addressId,
                    checkoutItems: r.checkoutItems.map(item => ({
                        id: item.id,
                        pricePerItem: item.pricePerItem,
                        amount: item.amount,
                        addedDate: item.addedDate,
                        statusId: item.statusId,
                        topic: Object.assign({}, item.topic, { thumbnail: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${item.topic.id}/${item.topic.thumbnail.topicPicture.fileName}`) }),
                        shippings: item.topic.shippings.map(shipping => ({
                            id: shipping.id,
                            name: shipping.shipping.name
                        })),
                        user: item.topic.user
                    }))
                };
                return res;
            });
            res.json(response);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.GetToCheckout = GetToCheckout;
