import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { CheckoutItem } from "../../../../../../database/models/checkout-item";
import { ForbiddenError } from "../../../../../../errors/base/forbidden-error";
import { CheckoutItemStatusEnum } from "../../../../../../database/enums/checkout-item-status";
import { CheckoutItemStatus } from "../../../../../../database/models/checkout-item-status";

interface PostConfirmShipmentSchema {
    itemId: number;
}

export async function PostConfirmShipment(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;

        const { itemId }: PostConfirmShipmentSchema = req.body;

        const checkoutItem = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(CheckoutItem, 'A')
            .innerJoin('A.checkout', 'B', 'B.userId = :userId', { userId })
            .where('A.id = :itemId', { itemId })
            .andWhere('A.statusId = :statusId', { statusId: CheckoutItemStatusEnum.Shipping })
            .getOne();
        console.log(checkoutItem);
        if (!!!checkoutItem) {
            return next(new ForbiddenError());
        }

        checkoutItem.status = { id: CheckoutItemStatusEnum.Successful } as CheckoutItemStatus
        await getManager().save(checkoutItem);

        res.sendStatus(200);
    } else {
        return next(new UnauthorizedError());
    }
}