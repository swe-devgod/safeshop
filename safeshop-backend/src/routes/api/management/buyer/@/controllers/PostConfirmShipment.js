"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const checkout_item_1 = require("../../../../../../database/models/checkout-item");
const forbidden_error_1 = require("../../../../../../errors/base/forbidden-error");
const checkout_item_status_1 = require("../../../../../../database/enums/checkout-item-status");
function PostConfirmShipment(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const { itemId } = req.body;
            const checkoutItem = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(checkout_item_1.CheckoutItem, 'A')
                .innerJoin('A.checkout', 'B', 'B.userId = :userId', { userId })
                .where('A.id = :itemId', { itemId })
                .andWhere('A.statusId = :statusId', { statusId: checkout_item_status_1.CheckoutItemStatusEnum.Shipping })
                .getOne();
            console.log(checkoutItem);
            if (!!!checkoutItem) {
                return next(new forbidden_error_1.ForbiddenError());
            }
            checkoutItem.status = { id: checkout_item_status_1.CheckoutItemStatusEnum.Successful };
            yield typeorm_1.getManager().save(checkoutItem);
            res.sendStatus(200);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostConfirmShipment = PostConfirmShipment;
