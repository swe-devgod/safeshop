"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const checkout_item_1 = require("../../../../../../database/models/checkout-item");
const checkout_item_status_1 = require("../../../../../../database/enums/checkout-item-status");
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const client_url_helper_1 = require("../../../../../../helper/client-url-helper");
function GetShipping(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const result = yield typeorm_1.getManager().createQueryBuilder()
                .select('A')
                .from(checkout_item_1.CheckoutItem, 'A')
                .innerJoinAndSelect('A.checkout', 'B', 'B.userId = :userId', { userId })
                .innerJoinAndSelect('A.topic', 'C')
                .innerJoinAndSelect('A.shipping', 'H')
                .innerJoinAndSelect('H.shipping', 'I')
                .innerJoinAndSelect('I.shipping', 'J')
                .innerJoinAndSelect('B.address', 'D')
                .innerJoinAndSelect('B.user', 'E')
                .innerJoinAndSelect('C.thumbnail', 'F')
                .innerJoinAndSelect('F.topicPicture', 'G')
                .where('A.statusId = :statusId', { statusId: checkout_item_status_1.CheckoutItemStatusEnum.Shipping })
                .getMany();
            const response = result.map(r => {
                const { checkout, topic, shipping } = r;
                const res = {
                    id: r.id,
                    pricePerItem: r.pricePerItem,
                    amount: r.amount,
                    addedDate: r.addedDate,
                    statusId: r.statusId,
                    topic: Object.assign({}, topic, { thumbnail: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail.topicPicture.fileName}`) }),
                    user: checkout.user,
                    address: {
                        id: checkout.address.id,
                        address: checkout.address.address
                    },
                    shipping: {
                        id: shipping.id,
                        name: shipping.shipping.shipping.name,
                        shippingNo: shipping.shippingNo
                    }
                };
                return res;
            });
            res.json(response);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.GetShipping = GetShipping;
