import { GetToCheckout } from "./controllers/GetToCheckout";
import { GetAwaitingShipment } from "./controllers/GetAwaitingShipment";
import { GetShipping } from "./controllers/GetShipping";
import { GetSuccess } from "./controllers/GetSuccess";
import { PostConfirmShipment } from "./controllers/PostConfirmShipment";

export const buyerRoutes = {
    name: "buyer",
    routes: [
        {
            path: '/checkout',
            method: 'get',
            action: GetToCheckout
        },
        {
            path: '/awaiting-shipment',
            method: 'get',
            action: GetAwaitingShipment
        },
        {
            path: '/shipping',
            method: 'get',
            action: GetShipping
        },
        {
            path: '/success',
            method: 'get',
            action: GetSuccess
        },
        {
            path: '/confirm',
            method: 'post',
            action: PostConfirmShipment
        }
    ]
};