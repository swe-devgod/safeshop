"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetToCheckout_1 = require("./controllers/GetToCheckout");
const GetAwaitingShipment_1 = require("./controllers/GetAwaitingShipment");
const GetShipping_1 = require("./controllers/GetShipping");
const GetSuccess_1 = require("./controllers/GetSuccess");
const PostConfirmShipment_1 = require("./controllers/PostConfirmShipment");
exports.buyerRoutes = {
    name: "buyer",
    routes: [
        {
            path: '/checkout',
            method: 'get',
            action: GetToCheckout_1.GetToCheckout
        },
        {
            path: '/awaiting-shipment',
            method: 'get',
            action: GetAwaitingShipment_1.GetAwaitingShipment
        },
        {
            path: '/shipping',
            method: 'get',
            action: GetShipping_1.GetShipping
        },
        {
            path: '/success',
            method: 'get',
            action: GetSuccess_1.GetSuccess
        },
        {
            path: '/confirm',
            method: 'post',
            action: PostConfirmShipment_1.PostConfirmShipment
        }
    ]
};
