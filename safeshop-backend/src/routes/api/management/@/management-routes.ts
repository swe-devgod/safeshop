import { mapRoutes } from "../../../../helper/map-routes";
import { attachRoutesToParent } from "../../../../helper/route-helper";
import { buyerRoutes } from "../buyer/@/buyer-routes";
import { sellerRoutes } from "../seller/@/seller-routes";

export const managementRoutes = {
    name: "management",
    routes: []
};

attachRoutesToParent(buyerRoutes, managementRoutes);
attachRoutesToParent(sellerRoutes, managementRoutes);