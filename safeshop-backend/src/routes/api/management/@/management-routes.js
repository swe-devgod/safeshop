"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const route_helper_1 = require("../../../../helper/route-helper");
const buyer_routes_1 = require("../buyer/@/buyer-routes");
const seller_routes_1 = require("../seller/@/seller-routes");
exports.managementRoutes = {
    name: "management",
    routes: []
};
route_helper_1.attachRoutesToParent(buyer_routes_1.buyerRoutes, exports.managementRoutes);
route_helper_1.attachRoutesToParent(seller_routes_1.sellerRoutes, exports.managementRoutes);
