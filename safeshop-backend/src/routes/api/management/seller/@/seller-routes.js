"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetAwaitingShipment_1 = require("./controllers/GetAwaitingShipment");
const GetSelling_1 = require("./controllers/GetSelling");
const GetShipping_1 = require("../@/controllers/GetShipping");
const GetSuccess_1 = require("./controllers/GetSuccess");
exports.sellerRoutes = {
    name: "seller",
    routes: [
        {
            path: '/selling',
            method: 'get',
            action: GetSelling_1.GetSelling
        },
        {
            path: '/awaiting-shipment',
            method: 'get',
            action: GetAwaitingShipment_1.GetAwaitingShipment
        },
        {
            path: '/shipping',
            method: 'get',
            action: GetShipping_1.GetShipping
        },
        {
            path: '/success',
            method: 'get',
            action: GetSuccess_1.GetSuccess
        }
    ]
};
