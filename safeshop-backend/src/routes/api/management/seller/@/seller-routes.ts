import { GetAwaitingShipment } from "./controllers/GetAwaitingShipment";
import { GetSelling } from "./controllers/GetSelling";
import { GetShipping } from "../@/controllers/GetShipping";
import { GetSuccess } from "./controllers/GetSuccess";

export const sellerRoutes = {
    name: "seller",
    routes: [
        {
            path: '/selling',
            method: 'get',
            action: GetSelling
        },
        {
            path: '/awaiting-shipment',
            method: 'get',
            action: GetAwaitingShipment
        },
        {
            path: '/shipping',
            method: 'get',
            action: GetShipping
        },
        {
            path: '/success',
            method: 'get',
            action: GetSuccess
        }
    ]
};