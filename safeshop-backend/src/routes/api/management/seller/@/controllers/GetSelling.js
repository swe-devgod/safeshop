"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const topic_1 = require("../../../../../../database/models/topic");
const client_url_helper_1 = require("../../../../../../helper/client-url-helper");
function GetSelling(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const topics = yield typeorm_1.getManager().createQueryBuilder()
                .select('A')
                .from(topic_1.Topic, 'A')
                .innerJoinAndSelect('A.shippings', 'D')
                .innerJoinAndSelect('D.shipping', 'E')
                .leftJoinAndSelect('A.thumbnail', 'B')
                .leftJoinAndSelect('B.topicPicture', 'C')
                .where('A.userId = :userId', { userId })
                .getMany();
            const response = topics.map(topic => (Object.assign({}, topic, { thumbnail: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail.topicPicture.fileName}`), shippings: topic.shippings.map(shipping => ({
                    id: shipping.id,
                    name: shipping.shipping.name
                })) })));
            res.json(response);
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.GetSelling = GetSelling;
