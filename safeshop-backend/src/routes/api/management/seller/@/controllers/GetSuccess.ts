import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { CheckoutItem } from "../../../../../../database/models/checkout-item";
import { CheckoutItemStatusEnum } from "../../../../../../database/enums/checkout-item-status";
import { GlobalVar } from "../../../../../../helper/global-var";
import { ClientURLHelper } from "../../../../../../helper/client-url-helper";

interface GetSuccessResponseSchema {
    id: number;
    pricePerItem: number;       
    amount: number;
    addedDate: number;
    statusId: number;
    topic: GetSuccessTopicSchema;
    user: GetSuccessUserSchema
    address: GetSuccessAddressSchema;
    shipping: GetSuccessShippingSchema;
}

interface GetSuccessTopicSchema {
    id: number;
    userId: number;
    topicCreatedDate: number;
    topicPrice: number;
    topicRemainingItems: number;
    topicName: string;
    topicDescription: string;
    topicVoteCount: number;
    topicRating: number;
    thumbnail: string;
}

interface GetSuccessUserSchema {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    displayName: string;
    email: string;
    tel: string;
    registerDate: number;
    rating: number;
    ratingVoteCount: number
}

interface GetSuccessAddressSchema {
    id: number;
    address: string;
}

interface GetSuccessShippingSchema {
    id: number;
    name: string;
    shippingNo: string;
}

export async function GetSuccess(req, res, next) {
    if (req.isAuthenticated()) {
        const userId=  req.user.id;
        const result = await getManager().createQueryBuilder()
            .select('A')
            .from(CheckoutItem, 'A')
            .innerJoinAndSelect('A.checkout', 'B')
            .innerJoinAndSelect('A.topic', 'C', 'C.userId = :userId', { userId })
            .innerJoinAndSelect('A.shipping', 'H')
            .innerJoinAndSelect('H.shipping', 'I')
            .innerJoinAndSelect('I.shipping', 'J')
            .innerJoinAndSelect('B.address', 'D')
            .innerJoinAndSelect('B.user', 'E')
            .innerJoinAndSelect('C.thumbnail', 'F')
            .innerJoinAndSelect('F.topicPicture', 'G')
            .where('A.statusId = :statusId', { statusId: CheckoutItemStatusEnum.Successful })
            .getMany();

        const response = result.map(r => {
            const { checkout, topic, shipping } = r;
            const res = {
                id: r.id,
                pricePerItem: r.pricePerItem,
                amount: r.amount,
                addedDate: r.addedDate,
                statusId: r.statusId,
                topic: {
                    ...topic,
                    thumbnail: ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail.topicPicture.fileName}`)
                },
                user: checkout.user as GetSuccessUserSchema,
                address: {
                    id: checkout.address.id,
                    address: checkout.address.address
                },
                shipping: {
                    id: shipping.id,
                    name: shipping.shipping.shipping.name,
                    shippingNo: shipping.shippingNo
                }
            } as GetSuccessResponseSchema;

            return res;
        });

        res.json(response);
    } else {
        return next(new UnauthorizedError());
    }
}