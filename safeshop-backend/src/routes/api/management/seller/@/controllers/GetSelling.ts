import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Topic } from "../../../../../../database/models/topic";
import { GlobalVar } from "../../../../../../helper/global-var";
import { ClientURLHelper } from "../../../../../../helper/client-url-helper";

interface GetSellingTopicSchema {
    id: number;
    userId: number;
    topicCreatedDate: number;
    topicPrice: number;
    topicRemainingItems: number;
    topicName: string;
    topicDescription: string;
    topicVoteCount: number;
    topicRating: number;
    thumbnail: string;
    shippings: GetSellingShippingSchema[];
}

interface GetSellingShippingSchema {
    id: number;
    name: string;
}

export async function GetSelling(req, res, next) {
    if (req.isAuthenticated()) {
        const userId=  req.user.id;

        const topics = await getManager().createQueryBuilder()
            .select('A')
            .from(Topic, 'A')
            .innerJoinAndSelect('A.shippings', 'D')
            .innerJoinAndSelect('D.shipping', 'E')
            .leftJoinAndSelect('A.thumbnail', 'B')
            .leftJoinAndSelect('B.topicPicture', 'C')
            .where('A.userId = :userId', { userId })
            .getMany();

        const response = topics.map(topic => ({
            ...topic,
            thumbnail: ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail.topicPicture.fileName}`) as any,
            shippings: topic.shippings.map(shipping => ({
                id: shipping.id,
                name: shipping.shipping.name
            }) as GetSellingShippingSchema)
        }) as GetSellingTopicSchema);

        res.json(response);
    } else {
        return next(new UnauthorizedError());
    }
}