import { UnauthorizedError } from "../../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { CheckoutItem } from "../../../../../../database/models/checkout-item";
import { CheckoutItemStatusEnum } from "../../../../../../database/enums/checkout-item-status";
import { GlobalVar } from "../../../../../../helper/global-var";
import { ClientURLHelper } from "../../../../../../helper/client-url-helper";

interface GetAwaitingShipmentResponseSchema {
    id: number;
    pricePerItem: number;       
    amount: number;
    addedDate: number;
    statusId: number;
    topic: GetAwaitingShipmentTopicSchema;
    user: GetAwaitingShipmentUserSchema
    address: GetAwaitingShipmentAddressSchema;
    shipping: GetAwaitingShipmentShippingSchema;
}

interface GetAwaitingShipmentTopicSchema {
    id: number;
    userId: number;
    topicCreatedDate: number;
    topicPrice: number;
    topicRemainingItems: number;
    topicName: string;
    topicDescription: string;
    topicVoteCount: number;
    topicRating: number;
    thumbnail: string;
}

interface GetAwaitingShipmentUserSchema {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    displayName: string;
    email: string;
    tel: string;
    registerDate: number;
    rating: number;
    ratingVoteCount: number
}

interface GetAwaitingShipmentAddressSchema {
    id: number;
    address: string;
}

interface GetAwaitingShipmentShippingSchema {
    id: number;
    name: string;
}

export async function GetAwaitingShipment(req, res, next) {
    if (req.isAuthenticated()) {
        const userId=  req.user.id;
        const result = await getManager().createQueryBuilder()
            .select('A')
            .from(CheckoutItem, 'A')
            .innerJoinAndSelect('A.checkout', 'B')
            .innerJoinAndSelect('A.topic', 'C', 'C.userId = :userId', { userId })
            .innerJoinAndSelect('A.shipping', 'H')
            .innerJoinAndSelect('H.shipping', 'I')
            .innerJoinAndSelect('I.shipping', 'J')
            .innerJoinAndSelect('B.address', 'D')
            .innerJoinAndSelect('B.user', 'E')
            .innerJoinAndSelect('C.thumbnail', 'F')
            .innerJoinAndSelect('F.topicPicture', 'G')
            .where('A.statusId = :statusId', { statusId: CheckoutItemStatusEnum.AwaitingShipment })
            .getMany();

        const response = result.map(r => {
            const { checkout, topic, shipping } = r;
            const res = {
                id: r.id,
                pricePerItem: r.pricePerItem,
                amount: r.amount,
                addedDate: r.addedDate,
                statusId: r.statusId,
                topic: {
                    ...topic,
                    thumbnail: ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail.topicPicture.fileName}`)
                },
                user: checkout.user as GetAwaitingShipmentUserSchema,
                address: {
                    id: checkout.address.id,
                    address: checkout.address.address
                },
                shipping: {
                    id: shipping.id,
                    name: shipping.shipping.shipping.name
                }
            } as GetAwaitingShipmentResponseSchema;

            return res;
        });

        res.json(response);
    } else {
        return next(new UnauthorizedError());
    }
}