import { GetProductInCategory } from "./controllers/GetProductInCategory";
import { GetListCategory } from "./controllers/GetListCategory";

export const categoryRoutes = {
    name: "category",
    routes: [
        {
            path: '/list',
            method: 'get',
            action: GetListCategory
        },
        {
            path: '/:id(\\d+?)',
            method: 'get',
            action: GetProductInCategory
        },
    ]
};