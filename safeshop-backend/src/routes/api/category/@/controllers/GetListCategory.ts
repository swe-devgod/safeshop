import { getManager } from "typeorm";
import { Category } from "../../../../../database/models/category";

const CAT_LEVEL = 2;

export async function GetListCategory(req, res, next) {
    const categories = await getManager()
    .createQueryBuilder()
    .select('A')
    .from(Category, 'A')
    //.leftJoinAndSelect('A.childCategories', 'B')
    //.leftJoinAndSelect('B.childCategories', 'C')
    .where('A.parentCategoryId = :id', { id: 1 })
    .orderBy('A.id', 'ASC')
    //.addOrderBy('B.id', 'ASC')
    //.addOrderBy('C.id', 'ASC')
    .getMany();
    
    // ROOT ALWAY HAS 1 AS ID
    const root = await getManager()
        .createQueryBuilder()
        .select('A')
        .from(Category, 'A')
        .where('A.id = :id', { id: 1 })
        .getOne();

    res.json([root].concat(categories));
}