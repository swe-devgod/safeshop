import { getManager } from "typeorm";
import { Category } from "../../../../../database/models/category";
import { TopicThumbnail } from "../../../../../database/models/topic-thumbnail";
import { TopicPictures } from "../../../../../database/models/topic-pictures";
import { GlobalVar } from "../../../../../helper/global-var";
import { Topic } from "../../../../../database/models/topic";
import { ClientURLHelper } from "../../../../../helper/client-url-helper";

export async function GetProductInCategory(req, res, next) {
    const categoryId  = req.params.id || 1;

    // GET ALL PRODUCT
    if (categoryId == 1) {
        var topics = await getManager()
            .createQueryBuilder()
            .select([ 
                'A.id as id',
                'A.topicPrice as price',
                'A.topicName as name',
                'A.topicDescription as Description',
                'A.topicVoteCount as ratingCount',
                'A.topicRating as rating',
                'C.fileName as thumbnail'
            ])
            .from(Topic, 'A')
            .leftJoin(TopicThumbnail, 'B', 'A.id = B.topicId')
            .leftJoin(TopicPictures, 'C', 'B.topicPictureId = C.id')
            .limit(100)
            .where('A.topicRemainingItems > 0')
            .orderBy('A.topicCreatedDate', 'DESC')
            .getRawMany();
    } else {
        var topics = await getManager()
            .createQueryBuilder()
            .select([ 
                'B.id as id',
                'B.topicPrice as price',
                'B.topicName as name',
                'B.topicDescription as Description',
                'B.topicVoteCount as ratingCount',
                'B.topicRating as rating',
                'D.fileName as thumbnail'
            ])
            .from(Category, 'A')
            .innerJoin('A.topics', 'B', 'B.topicRemainingItems > 0')
            .leftJoin(TopicThumbnail, 'C', 'B.id = C.topicId')
            .leftJoin(TopicPictures, 'D', 'C.topicPictureId = D.id')
            .where('A.id = :id', { id: categoryId })
            .getRawMany();
    }
    
    topics.map(topic => {
        topic.thumbnail = ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail}`)
    });

    res.json(topics);
}