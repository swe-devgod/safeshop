"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const category_1 = require("../../../../../database/models/category");
const topic_thumbnail_1 = require("../../../../../database/models/topic-thumbnail");
const topic_pictures_1 = require("../../../../../database/models/topic-pictures");
const topic_1 = require("../../../../../database/models/topic");
const client_url_helper_1 = require("../../../../../helper/client-url-helper");
function GetProductInCategory(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const categoryId = req.params.id || 1;
        // GET ALL PRODUCT
        if (categoryId == 1) {
            var topics = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select([
                'A.id as id',
                'A.topicPrice as price',
                'A.topicName as name',
                'A.topicDescription as Description',
                'A.topicVoteCount as ratingCount',
                'A.topicRating as rating',
                'C.fileName as thumbnail'
            ])
                .from(topic_1.Topic, 'A')
                .leftJoin(topic_thumbnail_1.TopicThumbnail, 'B', 'A.id = B.topicId')
                .leftJoin(topic_pictures_1.TopicPictures, 'C', 'B.topicPictureId = C.id')
                .limit(100)
                .where('A.topicRemainingItems > 0')
                .orderBy('A.topicCreatedDate', 'DESC')
                .getRawMany();
        }
        else {
            var topics = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select([
                'B.id as id',
                'B.topicPrice as price',
                'B.topicName as name',
                'B.topicDescription as Description',
                'B.topicVoteCount as ratingCount',
                'B.topicRating as rating',
                'D.fileName as thumbnail'
            ])
                .from(category_1.Category, 'A')
                .innerJoin('A.topics', 'B', 'B.topicRemainingItems > 0')
                .leftJoin(topic_thumbnail_1.TopicThumbnail, 'C', 'B.id = C.topicId')
                .leftJoin(topic_pictures_1.TopicPictures, 'D', 'C.topicPictureId = D.id')
                .where('A.id = :id', { id: categoryId })
                .getRawMany();
        }
        topics.map(topic => {
            topic.thumbnail = client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail}`);
        });
        res.json(topics);
    });
}
exports.GetProductInCategory = GetProductInCategory;
