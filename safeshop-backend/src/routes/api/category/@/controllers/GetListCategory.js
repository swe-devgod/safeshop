"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const category_1 = require("../../../../../database/models/category");
const CAT_LEVEL = 2;
function GetListCategory(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const categories = yield typeorm_1.getManager()
            .createQueryBuilder()
            .select('A')
            .from(category_1.Category, 'A')
            //.leftJoinAndSelect('A.childCategories', 'B')
            //.leftJoinAndSelect('B.childCategories', 'C')
            .where('A.parentCategoryId = :id', { id: 1 })
            .orderBy('A.id', 'ASC')
            //.addOrderBy('B.id', 'ASC')
            //.addOrderBy('C.id', 'ASC')
            .getMany();
        // ROOT ALWAY HAS 1 AS ID
        const root = yield typeorm_1.getManager()
            .createQueryBuilder()
            .select('A')
            .from(category_1.Category, 'A')
            .where('A.id = :id', { id: 1 })
            .getOne();
        res.json([root].concat(categories));
    });
}
exports.GetListCategory = GetListCategory;
