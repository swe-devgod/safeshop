"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetProductInCategory_1 = require("./controllers/GetProductInCategory");
const GetListCategory_1 = require("./controllers/GetListCategory");
exports.categoryRoutes = {
    name: "category",
    routes: [
        {
            path: '/list',
            method: 'get',
            action: GetListCategory_1.GetListCategory
        },
        {
            path: '/:id(\\d+?)',
            method: 'get',
            action: GetProductInCategory_1.GetProductInCategory
        },
    ]
};
