"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PostAddTopicRating_1 = require("./controllers/PostAddTopicRating");
const PostAddSellerRating_1 = require("./controllers/PostAddSellerRating");
const PostAddBuyerRating_1 = require("./controllers/PostAddBuyerRating");
exports.ratingRoute = {
    name: "rating",
    routes: [
        {
            path: '/topic',
            method: 'post',
            action: PostAddTopicRating_1.PostAddTopicRating
        },
        {
            path: '/seller',
            method: 'post',
            action: PostAddSellerRating_1.PostAddSellerRating
        },
        {
            path: '/buyer',
            method: 'post',
            action: PostAddBuyerRating_1.PostAddBuyerRating
        }
    ]
};
