import { PostAddTopicRating } from "./controllers/PostAddTopicRating";
import { PostAddSellerRating } from "./controllers/PostAddSellerRating";
import { PostAddBuyerRating } from "./controllers/PostAddBuyerRating";

export const ratingRoute = {
    name: "rating",
    routes: [
        {
            path: '/topic',
            method: 'post',
            action: PostAddTopicRating
        },
        {
            path: '/seller',
            method: 'post',
            action: PostAddSellerRating
        },
        {
            path: '/buyer',
            method: 'post',
            action: PostAddBuyerRating
        }
    ]
};