"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const topic_1 = require("../../../../../database/models/topic");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
const rating_validator_1 = require("../../../../../helper/rating-validator");
const user_topic_voting_1 = require("../../../../../database/models/user-topic-voting");
const checkout_item_1 = require("../../../../../database/models/checkout-item");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const checkout_item_status_1 = require("../../../../../database/enums/checkout-item-status");
function PostAddTopicRating(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const { itemId, rating } = req.body;
            if (!!!rating_validator_1.RatingValidator.isValidValue(rating)) {
                return next(new bad_request_error_1.BadRequestError());
            }
            // ดูว่าเป็นคนซื้อจริงหรือไม่ (เช็คจาก Checkout)
            const checkoutItem = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(checkout_item_1.CheckoutItem, 'A')
                .innerJoinAndSelect('A.checkout', 'C', 'C.userId = :userId', { userId })
                .innerJoinAndSelect('A.topic', 'B')
                .where('A.id = :itemId', { itemId })
                .andWhere('A.statusId = :statusId', { statusId: checkout_item_status_1.CheckoutItemStatusEnum.Successful })
                .getOne();
            if (!!!checkoutItem) {
                return next(new forbidden_error_1.ForbiddenError());
            }
            const checkoutTopicId = checkoutItem.topic.id;
            const userToTopicVoiting = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(user_topic_voting_1.UserToTopicVoting, 'A')
                .where('A.userId = :userId', { userId })
                .andWhere('A.topicId = :topicId', { topicId: checkoutTopicId })
                .getOne();
            let recRating = 0;
            if (!!!userToTopicVoiting) {
                const { id, topicRating, topicVoteCount } = checkoutItem.topic;
                const newRating = ((topicRating * topicVoteCount) + rating) / (topicVoteCount + 1);
                recRating = newRating;
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    yield tem
                        .createQueryBuilder()
                        .insert()
                        .into(user_topic_voting_1.UserToTopicVoting)
                        .values([
                        { rating: rating, topic: { id: checkoutTopicId }, user: { id: userId } }
                    ])
                        .execute();
                    yield tem
                        .createQueryBuilder()
                        .update(topic_1.Topic)
                        .set({ topicRating: newRating, topicVoteCount: topicVoteCount + 1 })
                        .where('id = :topicId', { topicId: id })
                        .execute();
                }));
            }
            else {
                const { id, topicRating, topicVoteCount } = checkoutItem.topic;
                const newRating = ((topicRating * topicVoteCount) - userToTopicVoiting.rating + rating) / topicVoteCount;
                recRating = newRating;
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    yield tem
                        .createQueryBuilder()
                        .update(user_topic_voting_1.UserToTopicVoting)
                        .set({ rating: rating })
                        .where('id = :id', { id: userToTopicVoiting.id })
                        .execute();
                    yield tem
                        .createQueryBuilder()
                        .update(topic_1.Topic)
                        .set({ topicRating: newRating })
                        .where('id = :topicId', { topicId: id })
                        .execute();
                }));
            }
            return res.json({
                rating: recRating
            });
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostAddTopicRating = PostAddTopicRating;
