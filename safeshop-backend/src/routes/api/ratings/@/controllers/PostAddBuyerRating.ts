import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { CheckoutItem } from "../../../../../database/models/checkout-item";
import { CheckoutItemStatusEnum } from "../../../../../database/enums/checkout-item-status";
import { BadRequestError } from "../../../../../errors/base/bad-request-error";
import { RatingValidator } from "../../../../../helper/rating-validator";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { UserToUserVoting } from "../../../../../database/models/user-user-voting";
import { User } from "../../../../../database/models/user";

interface PostAddBuyerRatingRequestSchema {
    itemId: number;
    rating: number;
}

interface PostAddBuyerRatingResponseSchema {
    rating: number;
}

export async function PostAddBuyerRating(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;

        const { itemId, rating }: PostAddBuyerRatingRequestSchema = req.body;

        if (!!!RatingValidator.isValidValue(rating)) {
            return next(new BadRequestError());
        }

        // ดูว่าเป็นเจ้าของ CheckoutItem จริงหรือไม่ (ดูจากกระทู้ที่ตั้ง)
        const checkoutItem = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(CheckoutItem, 'A')
            .innerJoinAndSelect('A.topic', 'B', 'B.userId = :userId', { userId: userId })
            .innerJoinAndSelect('A.checkout', 'C')
            .innerJoinAndSelect('C.user', 'D')
            .where('A.id = :itemId', { itemId })
            .andWhere('A.statusId = :statusId', { statusId: CheckoutItemStatusEnum.Successful })
            .getOne();

        if (!!!checkoutItem) {
            return next(new ForbiddenError());
        }

        // ดูว่าโหวตให้ใคร
        const votedUserId = checkoutItem.checkout.user.id;
        // ใช้ดูว่าเคยโหวตแล้วหรือยัง
        const userToUserVoting = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(UserToUserVoting, 'A')
            .where('A.checkoutItemId = :checkoutItemId', { checkoutItemId: itemId })
            .andWhere('A.srcUserId = :userId', { userId })
            .getOne();

        let resRating = 0;
        if (!!!userToUserVoting) {
            // ยังไม่เคยโหวต
            const votedUser = await getManager()
                .createQueryBuilder()
                .select('A')
                .from(User, 'A')
                .where('A.id = :id', { id: votedUserId })
                .getOne();

            const votedRating = votedUser.rating;
            const votedRatingVoteCount = votedUser.ratingVoteCount;

            const newRating = ((votedRating * votedRatingVoteCount) + rating) / (votedRatingVoteCount + 1);

            resRating = newRating;

            await getManager().transaction(async tem => {
                await tem
                    .createQueryBuilder()
                    .insert()
                    .into(UserToUserVoting)
                    .values([
                        {
                            srcUser: { id: userId } as User,
                            destUser: { id: votedUserId } as User,
                            checkoutItem: { id: itemId } as CheckoutItem,
                            rating: rating
                        } as UserToUserVoting
                    ])
                    .execute();

                await tem
                    .createQueryBuilder()
                    .update(User)
                    .set({ rating: newRating, ratingVoteCount: votedRatingVoteCount + 1 } as User)
                    .where("id = :id", { id: votedUserId })
                    .execute();
            });
        } else {
            // เคยโหวตแล้ว
            const votedUser = await getManager()
                .createQueryBuilder()
                .select('A')
                .from(User, 'A')
                .where('A.id = :id', { id: votedUserId })
                .getOne();

            const votedRating = votedUser.rating;
            const votedRatingVoteCount = votedUser.ratingVoteCount;

            const newRating = ((votedRating * votedRatingVoteCount) - userToUserVoting.rating + rating) / votedRatingVoteCount;

            resRating = newRating;

            await getManager().transaction(async tem => {
                await tem
                    .createQueryBuilder()
                    .update(UserToUserVoting)
                    .set({ rating: newRating } as UserToUserVoting)
                    .where("id = :id", { id: userToUserVoting.id })
                    .execute();

                await tem
                    .createQueryBuilder()
                    .update(User)
                    .set({ rating: newRating } as User)
                    .where("id = :id", { id: votedUserId })
                    .execute();
            });
        }

        return res.json({
            rating: resRating
        } as PostAddBuyerRatingResponseSchema)
    } else {
        return next(new UnauthorizedError());
    }
}