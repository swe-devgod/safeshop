"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("../../../../../errors/base/unauthorized-error");
const typeorm_1 = require("typeorm");
const checkout_item_1 = require("../../../../../database/models/checkout-item");
const checkout_item_status_1 = require("../../../../../database/enums/checkout-item-status");
const bad_request_error_1 = require("../../../../../errors/base/bad-request-error");
const rating_validator_1 = require("../../../../../helper/rating-validator");
const forbidden_error_1 = require("../../../../../errors/base/forbidden-error");
const user_user_voting_1 = require("../../../../../database/models/user-user-voting");
const user_1 = require("../../../../../database/models/user");
function PostAddBuyerRating(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        if (req.isAuthenticated()) {
            const userId = req.user.id;
            const { itemId, rating } = req.body;
            if (!!!rating_validator_1.RatingValidator.isValidValue(rating)) {
                return next(new bad_request_error_1.BadRequestError());
            }
            // ดูว่าเป็นเจ้าของ CheckoutItem จริงหรือไม่ (ดูจากกระทู้ที่ตั้ง)
            const checkoutItem = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(checkout_item_1.CheckoutItem, 'A')
                .innerJoinAndSelect('A.topic', 'B', 'B.userId = :userId', { userId: userId })
                .innerJoinAndSelect('A.checkout', 'C')
                .innerJoinAndSelect('C.user', 'D')
                .where('A.id = :itemId', { itemId })
                .andWhere('A.statusId = :statusId', { statusId: checkout_item_status_1.CheckoutItemStatusEnum.Successful })
                .getOne();
            if (!!!checkoutItem) {
                return next(new forbidden_error_1.ForbiddenError());
            }
            // ดูว่าโหวตให้ใคร
            const votedUserId = checkoutItem.checkout.user.id;
            // ใช้ดูว่าเคยโหวตแล้วหรือยัง
            const userToUserVoting = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(user_user_voting_1.UserToUserVoting, 'A')
                .where('A.checkoutItemId = :checkoutItemId', { checkoutItemId: itemId })
                .andWhere('A.srcUserId = :userId', { userId })
                .getOne();
            let resRating = 0;
            if (!!!userToUserVoting) {
                // ยังไม่เคยโหวต
                const votedUser = yield typeorm_1.getManager()
                    .createQueryBuilder()
                    .select('A')
                    .from(user_1.User, 'A')
                    .where('A.id = :id', { id: votedUserId })
                    .getOne();
                const votedRating = votedUser.rating;
                const votedRatingVoteCount = votedUser.ratingVoteCount;
                const newRating = ((votedRating * votedRatingVoteCount) + rating) / (votedRatingVoteCount + 1);
                resRating = newRating;
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    yield tem
                        .createQueryBuilder()
                        .insert()
                        .into(user_user_voting_1.UserToUserVoting)
                        .values([
                        {
                            srcUser: { id: userId },
                            destUser: { id: votedUserId },
                            checkoutItem: { id: itemId },
                            rating: rating
                        }
                    ])
                        .execute();
                    yield tem
                        .createQueryBuilder()
                        .update(user_1.User)
                        .set({ rating: newRating, ratingVoteCount: votedRatingVoteCount + 1 })
                        .where("id = :id", { id: votedUserId })
                        .execute();
                }));
            }
            else {
                // เคยโหวตแล้ว
                const votedUser = yield typeorm_1.getManager()
                    .createQueryBuilder()
                    .select('A')
                    .from(user_1.User, 'A')
                    .where('A.id = :id', { id: votedUserId })
                    .getOne();
                const votedRating = votedUser.rating;
                const votedRatingVoteCount = votedUser.ratingVoteCount;
                const newRating = ((votedRating * votedRatingVoteCount) - userToUserVoting.rating + rating) / votedRatingVoteCount;
                resRating = newRating;
                yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                    yield tem
                        .createQueryBuilder()
                        .update(user_user_voting_1.UserToUserVoting)
                        .set({ rating: newRating })
                        .where("id = :id", { id: userToUserVoting.id })
                        .execute();
                    yield tem
                        .createQueryBuilder()
                        .update(user_1.User)
                        .set({ rating: newRating })
                        .where("id = :id", { id: votedUserId })
                        .execute();
                }));
            }
            return res.json({
                rating: resRating
            });
        }
        else {
            return next(new unauthorized_error_1.UnauthorizedError());
        }
    });
}
exports.PostAddBuyerRating = PostAddBuyerRating;
