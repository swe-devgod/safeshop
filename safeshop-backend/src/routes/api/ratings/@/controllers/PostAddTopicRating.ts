import { UnauthorizedError } from "../../../../../errors/base/unauthorized-error";
import { getManager } from "typeorm";
import { Topic } from "../../../../../database/models/topic";
import { BadRequestError } from "../../../../../errors/base/bad-request-error";
import { RatingValidator } from "../../../../../helper/rating-validator";
import { UserToTopicVoting } from "../../../../../database/models/user-topic-voting";
import { CheckoutItem } from "../../../../../database/models/checkout-item";
import { ForbiddenError } from "../../../../../errors/base/forbidden-error";
import { User } from "../../../../../database/models/user";
import { CheckoutItemStatusEnum } from "../../../../../database/enums/checkout-item-status";

interface PostAddTopicRatingRequestSchema {
    itemId: number;
    rating: number;
}

interface PostAddTopicRatingResponseSchema {
    rating: number;
}

export async function PostAddTopicRating(req, res, next) {
    if (req.isAuthenticated()) {
        const userId = req.user.id;

        const { itemId, rating }: PostAddTopicRatingRequestSchema = req.body;

        if (!!!RatingValidator.isValidValue(rating)) {
            return next(new BadRequestError());
        }

        // ดูว่าเป็นคนซื้อจริงหรือไม่ (เช็คจาก Checkout)
        const checkoutItem = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(CheckoutItem, 'A')
            .innerJoinAndSelect('A.checkout', 'C', 'C.userId = :userId', { userId })
            .innerJoinAndSelect('A.topic', 'B')
            .where('A.id = :itemId', { itemId })
            .andWhere('A.statusId = :statusId', { statusId: CheckoutItemStatusEnum.Successful })
            .getOne();

        if (!!!checkoutItem) {
            return next(new ForbiddenError());
        }
        
        const checkoutTopicId = checkoutItem.topic.id;

        const userToTopicVoiting = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(UserToTopicVoting, 'A')
            .where('A.userId = :userId', { userId })
            .andWhere('A.topicId = :topicId', { topicId: checkoutTopicId })
            .getOne();

        let recRating = 0;
        if (!!!userToTopicVoiting) {
            const { id, topicRating, topicVoteCount } = checkoutItem.topic;
            const newRating = ((topicRating * topicVoteCount) + rating) / (topicVoteCount + 1);

            recRating = newRating;

            await getManager().transaction(async tem => {
                await tem
                .createQueryBuilder()
                .insert()
                .into(UserToTopicVoting)
                .values([
                    { rating: rating, topic: { id: checkoutTopicId } as Topic, user: { id: userId } as User } as UserToTopicVoting
                ])
                .execute();

                await tem
                    .createQueryBuilder()
                    .update(Topic)
                    .set({ topicRating: newRating, topicVoteCount: topicVoteCount + 1 } as Topic)
                    .where('id = :topicId', { topicId: id })
                    .execute();

            });
        } else {
            const { id, topicRating, topicVoteCount } = checkoutItem.topic;
            const newRating = ((topicRating * topicVoteCount) - userToTopicVoiting.rating + rating) / topicVoteCount;

            recRating = newRating

            await getManager().transaction(async tem => {
                await tem
                    .createQueryBuilder()
                    .update(UserToTopicVoting)
                    .set({ rating: rating } as UserToTopicVoting)
                    .where('id = :id', { id: userToTopicVoiting.id })
                    .execute();

                await tem
                    .createQueryBuilder()
                    .update(Topic)
                    .set({ topicRating: newRating } as Topic)
                    .where('id = :topicId', { topicId: id })
                    .execute();
            });
        }

        return res.json({
            rating: recRating
        } as PostAddTopicRatingResponseSchema);

    } else {
        return next(new UnauthorizedError());
    }
}