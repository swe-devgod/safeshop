"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const topic_1 = require("./topic");
const topic_pictures_1 = require("./topic-pictures");
let TopicThumbnail = class TopicThumbnail {
};
__decorate([
    typeorm_1.Column({ type: 'int', primary: true }),
    __metadata("design:type", Number)
], TopicThumbnail.prototype, "topicId", void 0);
__decorate([
    typeorm_1.OneToOne(type => topic_1.Topic, topic => topic.thumbnail),
    typeorm_1.JoinColumn(),
    __metadata("design:type", topic_1.Topic)
], TopicThumbnail.prototype, "topic", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', primary: true }),
    __metadata("design:type", Number)
], TopicThumbnail.prototype, "topicPictureId", void 0);
__decorate([
    typeorm_1.OneToOne(type => topic_pictures_1.TopicPictures),
    typeorm_1.JoinColumn(),
    __metadata("design:type", topic_pictures_1.TopicPictures)
], TopicThumbnail.prototype, "topicPicture", void 0);
TopicThumbnail = __decorate([
    typeorm_1.Entity()
], TopicThumbnail);
exports.TopicThumbnail = TopicThumbnail;
