"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Category_1;
const typeorm_1 = require("typeorm");
const topic_1 = require("./topic");
let Category = Category_1 = class Category {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Category.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ nullable: false, unique: true }),
    __metadata("design:type", String)
], Category.prototype, "categoryName", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Category_1, category => category.childCategories),
    __metadata("design:type", Category)
], Category.prototype, "parentCategory", void 0);
__decorate([
    typeorm_1.OneToMany(type => Category_1, category => category.parentCategory),
    __metadata("design:type", Array)
], Category.prototype, "childCategories", void 0);
__decorate([
    typeorm_1.ManyToMany(type => topic_1.Topic, topic => topic.categories),
    __metadata("design:type", Array)
], Category.prototype, "topics", void 0);
Category = Category_1 = __decorate([
    typeorm_1.Entity()
], Category);
exports.Category = Category;
