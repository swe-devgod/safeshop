"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const checkout_item_1 = require("./checkout-item");
const topic_shipping_1 = require("./topic-shipping");
let CheckoutShipping = class CheckoutShipping {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CheckoutShipping.prototype, "id", void 0);
__decorate([
    typeorm_1.OneToOne(type => checkout_item_1.CheckoutItem, item => item.shipping),
    typeorm_1.JoinColumn(),
    __metadata("design:type", checkout_item_1.CheckoutItem)
], CheckoutShipping.prototype, "checkoutItem", void 0);
__decorate([
    typeorm_1.ManyToOne(type => topic_shipping_1.TopicShipping, ts => ts.checkoutShipping),
    __metadata("design:type", topic_shipping_1.TopicShipping)
], CheckoutShipping.prototype, "shipping", void 0);
__decorate([
    typeorm_1.Column({ type: 'text', nullable: true }),
    __metadata("design:type", String)
], CheckoutShipping.prototype, "shippingNo", void 0);
CheckoutShipping = __decorate([
    typeorm_1.Entity()
], CheckoutShipping);
exports.CheckoutShipping = CheckoutShipping;
