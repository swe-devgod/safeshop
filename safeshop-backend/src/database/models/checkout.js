"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("./user");
const checkout_status_1 = require("./checkout-status");
const checkout_item_1 = require("./checkout-item");
const address_1 = require("./address");
let Checkout = class Checkout {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Checkout.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: false }),
    __metadata("design:type", Number)
], Checkout.prototype, "checkoutDate", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_1.User, user => user.checkouts),
    __metadata("design:type", user_1.User)
], Checkout.prototype, "user", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], Checkout.prototype, "statusId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => checkout_status_1.CheckoutStatus, cs => cs.id),
    __metadata("design:type", checkout_status_1.CheckoutStatus)
], Checkout.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], Checkout.prototype, "addressId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => address_1.Address, address => address.checkouts),
    __metadata("design:type", address_1.Address)
], Checkout.prototype, "address", void 0);
__decorate([
    typeorm_1.OneToMany(type => checkout_item_1.CheckoutItem, items => items.checkout),
    __metadata("design:type", Array)
], Checkout.prototype, "checkoutItems", void 0);
Checkout = __decorate([
    typeorm_1.Entity()
], Checkout);
exports.Checkout = Checkout;
