import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, AfterRemove, OneToOne, JoinColumn } from "typeorm";
import { CheckoutItem } from "./checkout-item";
import { TopicShipping } from "./topic-shipping";

@Entity()
export class CheckoutShipping {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => CheckoutItem, item => item.shipping)
    @JoinColumn()
    checkoutItem: CheckoutItem;

    @ManyToOne(type => TopicShipping, ts => ts.checkoutShipping)
    shipping: TopicShipping;

    @Column({ type: 'text', nullable: true })
    shippingNo: string;
}