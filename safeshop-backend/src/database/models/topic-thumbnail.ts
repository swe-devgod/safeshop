import { OneToOne, Column, JoinColumn, PrimaryGeneratedColumn, Entity } from "typeorm";
import { Topic } from "./topic";
import { TopicPictures } from "./topic-pictures";

@Entity()
export class TopicThumbnail {
    @Column({ type: 'int', primary: true })
    topicId: number;

    @OneToOne(type => Topic, topic => topic.thumbnail)
    @JoinColumn()
    topic: Topic;

    @Column({ type: 'int', primary: true })
    topicPictureId: number;

    @OneToOne(type => TopicPictures)
    @JoinColumn()
    topicPicture: TopicPictures;
}