"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("./user");
let Session = class Session {
};
__decorate([
    typeorm_1.Column({ type: 'int', nullable: false, primary: true }),
    __metadata("design:type", Number)
], Session.prototype, "userId", void 0);
__decorate([
    typeorm_1.OneToOne(type => user_1.User),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_1.User)
], Session.prototype, "user", void 0);
__decorate([
    typeorm_1.Column({ nullable: false, unique: true }),
    __metadata("design:type", String)
], Session.prototype, "username", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Session.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({ type: "bigint", nullable: false }),
    __metadata("design:type", Number)
], Session.prototype, "lastActiveDate", void 0);
Session = __decorate([
    typeorm_1.Entity()
], Session);
exports.Session = Session;
