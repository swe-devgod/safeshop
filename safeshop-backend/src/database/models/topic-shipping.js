"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const topic_1 = require("./topic");
const shipping_carrier_1 = require("./shipping-carrier");
const checkout_item_1 = require("./checkout-item");
const checkout_shipping_1 = require("./checkout-shipping");
let TopicShipping = class TopicShipping {
};
__decorate([
    typeorm_1.PrimaryColumn(),
    __metadata("design:type", Number)
], TopicShipping.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], TopicShipping.prototype, "topicId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => topic_1.Topic, topic => topic.shippings),
    __metadata("design:type", topic_1.Topic)
], TopicShipping.prototype, "topic", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], TopicShipping.prototype, "shippingId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => shipping_carrier_1.ShippingCarrier, carrier => carrier.topics),
    __metadata("design:type", shipping_carrier_1.ShippingCarrier)
], TopicShipping.prototype, "shipping", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: false }),
    __metadata("design:type", Number)
], TopicShipping.prototype, "price", void 0);
__decorate([
    typeorm_1.OneToMany(type => checkout_item_1.CheckoutItem, item => item.shipping),
    __metadata("design:type", Array)
], TopicShipping.prototype, "checkoutItem", void 0);
__decorate([
    typeorm_1.OneToMany(type => checkout_shipping_1.CheckoutShipping, cs => cs.shipping),
    __metadata("design:type", Array)
], TopicShipping.prototype, "checkoutShipping", void 0);
TopicShipping = __decorate([
    typeorm_1.Entity()
], TopicShipping);
exports.TopicShipping = TopicShipping;
