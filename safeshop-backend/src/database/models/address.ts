import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, OneToMany} from "typeorm";
import { User } from "./user";
import { Checkout } from "./checkout";

@Entity()
export class Address {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.addresses)
    user: User;

    @Column({ type: 'text', nullable: true })
    address: string;

    @OneToMany(type => Checkout, checkout => checkout.address)
    checkouts: Checkout[];
}