import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./user";
import { Topic } from "./topic";
import { Checkout } from "./checkout";

@Entity()   
export class Cart {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'int', nullable: true })
    userId: number;

    @ManyToOne(type => User, user => user.carts)
    user: User;

    @Column({ type: 'int', nullable: true })
    topicId: number;

    @ManyToOne(type => Topic, topic => topic.carts)
    topic: Topic;

    @Column({ type: 'int', nullable: false })
    amount: number;

    @Column({ type: 'bigint', nullable: false })
    addedDate: number;
}
