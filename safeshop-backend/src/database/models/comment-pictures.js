"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const topic_comment_1 = require("./topic-comment");
let CommentPictures = class CommentPictures {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CommentPictures.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => topic_comment_1.TopicComment, comment => comment.pictures),
    __metadata("design:type", topic_comment_1.TopicComment)
], CommentPictures.prototype, "comment", void 0);
__decorate([
    typeorm_1.RelationId((cp) => cp.comment),
    __metadata("design:type", Number)
], CommentPictures.prototype, "commentId", void 0);
__decorate([
    typeorm_1.Column({ unique: false, nullable: false }),
    __metadata("design:type", String)
], CommentPictures.prototype, "fileName", void 0);
CommentPictures = __decorate([
    typeorm_1.Entity()
], CommentPictures);
exports.CommentPictures = CommentPictures;
