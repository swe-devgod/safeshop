import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Topic } from "./topic";

@Entity()
export class TopicPictures {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'int', nullable: true })
    topicId: number;

    @ManyToOne(type => Topic, topic => topic.pictures)
    topic: Topic;

    @Column({ unique: false, nullable: false })
    fileName: string;
}