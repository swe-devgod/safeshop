"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const topic_1 = require("./topic");
const checkout_1 = require("./checkout");
const checkout_item_status_1 = require("./checkout-item-status");
const checkout_shipping_1 = require("./checkout-shipping");
const user_user_voting_1 = require("./user-user-voting");
let CheckoutItem = class CheckoutItem {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CheckoutItem.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'double', nullable: true }),
    __metadata("design:type", Number)
], CheckoutItem.prototype, "pricePerItem", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: false }),
    __metadata("design:type", Number)
], CheckoutItem.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column({ type: 'bigint', nullable: false }),
    __metadata("design:type", Number)
], CheckoutItem.prototype, "addedDate", void 0);
__decorate([
    typeorm_1.ManyToOne(type => checkout_1.Checkout, checkout => checkout),
    __metadata("design:type", checkout_1.Checkout)
], CheckoutItem.prototype, "checkout", void 0);
__decorate([
    typeorm_1.ManyToOne(type => topic_1.Topic, topic => topic.carts),
    __metadata("design:type", topic_1.Topic)
], CheckoutItem.prototype, "topic", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], CheckoutItem.prototype, "statusId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => checkout_item_status_1.CheckoutItemStatus, cis => cis.id),
    __metadata("design:type", checkout_item_status_1.CheckoutItemStatus)
], CheckoutItem.prototype, "status", void 0);
__decorate([
    typeorm_1.OneToOne(type => checkout_shipping_1.CheckoutShipping, cs => cs.checkoutItem),
    __metadata("design:type", checkout_shipping_1.CheckoutShipping)
], CheckoutItem.prototype, "shipping", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_user_voting_1.UserToUserVoting, utu => utu.checkoutItem),
    __metadata("design:type", Array)
], CheckoutItem.prototype, "userToUserVotings", void 0);
CheckoutItem = __decorate([
    typeorm_1.Entity()
], CheckoutItem);
exports.CheckoutItem = CheckoutItem;
