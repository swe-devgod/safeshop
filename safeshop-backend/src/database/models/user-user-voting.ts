import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./user";
import { CheckoutItem } from "./checkout-item";

@Entity()
export class UserToUserVoting {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.srcVotingUsers)
    srcUser: User;

    @ManyToOne(type => User, user => user.destVotingUsers)
    destUser: User;

    @ManyToOne(type => CheckoutItem, item => item.userToUserVotings)
    checkoutItem: CheckoutItem;

    @Column({ type: 'double', nullable: false })
    rating: number;
}