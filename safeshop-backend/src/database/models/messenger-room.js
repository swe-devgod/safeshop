"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const message_1 = require("./message");
const room_users_1 = require("./room-users");
let MessengerRoom = class MessengerRoom {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], MessengerRoom.prototype, "id", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column({ type: 'int', nullable: false }),
    __metadata("design:type", Number)
], MessengerRoom.prototype, "timestamp", void 0);
__decorate([
    typeorm_1.OneToMany(type => message_1.Message, message => message.room),
    __metadata("design:type", Array)
], MessengerRoom.prototype, "messages", void 0);
__decorate([
    typeorm_1.OneToMany(type => room_users_1.RoomUsers, roomUser => roomUser.room),
    __metadata("design:type", Array)
], MessengerRoom.prototype, "userRooms", void 0);
MessengerRoom = __decorate([
    typeorm_1.Entity()
], MessengerRoom);
exports.MessengerRoom = MessengerRoom;
