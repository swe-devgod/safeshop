"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("./user");
const checkout_item_1 = require("./checkout-item");
let UserToUserVoting = class UserToUserVoting {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], UserToUserVoting.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_1.User, user => user.srcVotingUsers),
    __metadata("design:type", user_1.User)
], UserToUserVoting.prototype, "srcUser", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_1.User, user => user.destVotingUsers),
    __metadata("design:type", user_1.User)
], UserToUserVoting.prototype, "destUser", void 0);
__decorate([
    typeorm_1.ManyToOne(type => checkout_item_1.CheckoutItem, item => item.userToUserVotings),
    __metadata("design:type", checkout_item_1.CheckoutItem)
], UserToUserVoting.prototype, "checkoutItem", void 0);
__decorate([
    typeorm_1.Column({ type: 'double', nullable: false }),
    __metadata("design:type", Number)
], UserToUserVoting.prototype, "rating", void 0);
UserToUserVoting = __decorate([
    typeorm_1.Entity()
], UserToUserVoting);
exports.UserToUserVoting = UserToUserVoting;
