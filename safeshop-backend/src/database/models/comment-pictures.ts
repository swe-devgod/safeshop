import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, RelationId} from "typeorm";
import { TopicComment } from "./topic-comment";

@Entity()
export class CommentPictures {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => TopicComment, comment => comment.pictures)
    comment: TopicComment;

    @RelationId((cp: CommentPictures) => cp.comment)
    commentId: number;

    @Column({ unique: false, nullable: false })
    fileName: string;
}