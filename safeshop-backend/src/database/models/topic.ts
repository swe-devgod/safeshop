import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable, RelationId, Index, OneToOne} from "typeorm";
import { User } from "./user";
import { TopicComment } from "./topic-comment";
import { Category } from "./category";
import { TopicPictures } from "./topic-pictures";
import { Cart } from "./cart";
import { TopicShipping } from "./topic-shipping";
import { TopicThumbnail } from "./topic-thumbnail";
import { UserToUserVoting } from "./user-user-voting";
import { UserToTopicVoting } from "./user-topic-voting";

@Entity()
export class Topic {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'int', nullable: true })
    userId: number;

    @ManyToOne(type => User, user => user.topics)
    user: User

    @Index()
    @Column({ type: 'bigint', nullable: false })
    topicCreatedDate: any

    @Column({ type: 'double', nullable: false, default: '0' })
    topicPrice: number

    @Column({ type: 'int', nullable: false, default: '0' })
    topicRemainingItems: number

    @Column({ nullable: false })
    topicName: string

    @Column({ nullable: false })
    topicDescription: string

    @Column({ type: "int", nullable: false, default: '0' })
    topicVoteCount: number;

    @Column({ type: "double", nullable: false, default: '0' })
    topicRating: number

    @OneToMany(type => TopicComment, comment => comment.topic)
    comments: TopicComment[];

    @OneToOne(type => TopicThumbnail, thumbnail => thumbnail.topic)
    thumbnail: TopicThumbnail;

    @OneToMany(type => TopicPictures, pictures => pictures.topic)
    pictures: TopicPictures[];

    @ManyToMany(type => Category, category => category.topics)
    @JoinTable()
    categories: Category[];

    @OneToMany(type => Cart, cart => cart.user)
    carts: Cart[];

    @OneToMany(type => TopicShipping, ts => ts.topic)
    shippings: TopicShipping[];

    @OneToMany(type => UserToTopicVoting, utt => utt.topic)
    votingUsers: UserToUserVoting[];
}