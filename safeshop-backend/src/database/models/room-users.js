"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("./user");
const messenger_room_1 = require("./messenger-room");
let RoomUsers = class RoomUsers {
};
__decorate([
    typeorm_1.Column({ type: 'int', primary: true }),
    __metadata("design:type", Number)
], RoomUsers.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_1.User, user => user.userRooms),
    __metadata("design:type", user_1.User)
], RoomUsers.prototype, "user", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', primary: true }),
    __metadata("design:type", Number)
], RoomUsers.prototype, "roomId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => messenger_room_1.MessengerRoom, room => room.userRooms),
    __metadata("design:type", messenger_room_1.MessengerRoom)
], RoomUsers.prototype, "room", void 0);
RoomUsers = __decorate([
    typeorm_1.Entity()
], RoomUsers);
exports.RoomUsers = RoomUsers;
