import {Entity, Column, OneToOne, JoinColumn, PrimaryColumn, UpdateDateColumn} from "typeorm";
import { User } from "./user";

@Entity()
export class Session {
    @Column({ type: 'int', nullable: false, primary: true })
    userId: number;

    @OneToOne(type => User)
    @JoinColumn()
    user: User;

    @Column({ nullable: false, unique: true })
    username: string;

    @Column({ nullable: false })
    password: string;

    @Column({ type: "bigint", nullable: false })
    lastActiveDate: number;
}