import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { Checkout } from "./checkout";

@Entity()
export class CheckoutStatus {
    @PrimaryGeneratedColumn({ type: 'int' })
    id: number;

    @Column({ type: 'text', nullable: false })
    description: string

    @OneToMany(type => Checkout, checkout => checkout.status)
    checkouts: Checkout[];
}