"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const address_1 = require("./address");
const topic_1 = require("./topic");
const topic_comment_1 = require("./topic-comment");
const message_1 = require("./message");
const room_users_1 = require("./room-users");
const cart_1 = require("./cart");
const checkout_1 = require("./checkout");
const user_picture_1 = require("./user-picture");
const default_address_1 = require("./default-address");
const user_user_voting_1 = require("./user-user-voting");
const user_topic_voting_1 = require("./user-topic-voting");
let User = class User {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], User.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], User.prototype, "firstName", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], User.prototype, "middleName", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], User.prototype, "lastName", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], User.prototype, "displayName", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], User.prototype, "tel", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column({ type: 'bigint', nullable: false }),
    __metadata("design:type", Object)
], User.prototype, "registerDate", void 0);
__decorate([
    typeorm_1.Column({ type: "double", nullable: false, default: '0' }),
    __metadata("design:type", Number)
], User.prototype, "rating", void 0);
__decorate([
    typeorm_1.Column({ type: "int", nullable: false, default: '0' }),
    __metadata("design:type", Number)
], User.prototype, "ratingVoteCount", void 0);
__decorate([
    typeorm_1.OneToOne(type => user_picture_1.UserPicture, picture => picture.user),
    __metadata("design:type", user_picture_1.UserPicture)
], User.prototype, "picture", void 0);
__decorate([
    typeorm_1.OneToOne(type => default_address_1.DefaultAddress, defaultAddr => defaultAddr.address),
    __metadata("design:type", default_address_1.DefaultAddress)
], User.prototype, "defaultAddress", void 0);
__decorate([
    typeorm_1.OneToMany(type => address_1.Address, address => address.user),
    __metadata("design:type", Array)
], User.prototype, "addresses", void 0);
__decorate([
    typeorm_1.OneToMany(type => topic_1.Topic, topic => topic.user),
    __metadata("design:type", Array)
], User.prototype, "topics", void 0);
__decorate([
    typeorm_1.OneToMany(type => topic_comment_1.TopicComment, comment => comment.user),
    __metadata("design:type", Array)
], User.prototype, "comments", void 0);
__decorate([
    typeorm_1.OneToMany(type => message_1.Message, message => message.user),
    __metadata("design:type", Array)
], User.prototype, "messages", void 0);
__decorate([
    typeorm_1.OneToMany(type => room_users_1.RoomUsers, roomUser => roomUser.user),
    __metadata("design:type", Array)
], User.prototype, "userRooms", void 0);
__decorate([
    typeorm_1.OneToMany(type => cart_1.Cart, cart => cart.user),
    __metadata("design:type", Array)
], User.prototype, "carts", void 0);
__decorate([
    typeorm_1.OneToMany(type => checkout_1.Checkout, checkout => checkout.user),
    __metadata("design:type", Array)
], User.prototype, "checkouts", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_user_voting_1.UserToUserVoting, utu => utu.srcUser),
    __metadata("design:type", Array)
], User.prototype, "destVotingUsers", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_user_voting_1.UserToUserVoting, utu => utu.destUser),
    __metadata("design:type", Array)
], User.prototype, "srcVotingUsers", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_topic_voting_1.UserToTopicVoting, utt => utt.user),
    __metadata("design:type", Array)
], User.prototype, "votingTopics", void 0);
User = __decorate([
    typeorm_1.Entity()
], User);
exports.User = User;
