import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, Index } from "typeorm";
import { MessengerRoom } from "./messenger-room";
import { User } from "./user";

@Entity()
export class Message {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'int', nullable: true })
    roomId: number;

    @ManyToOne(type => MessengerRoom, room => room.messages)
    room: MessengerRoom;

    @Column({ type: 'int', nullable: true })
    userId: number;
    
    @ManyToOne(type => User, user => user.messages)
    user: User;

    @Index()
    @Column({ type: 'bigint', nullable: false })
    timestamp: number;

    @Column({ nullable: false })
    message: string;
}