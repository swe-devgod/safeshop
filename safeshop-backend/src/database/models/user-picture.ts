import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn} from "typeorm";
import { User } from "./user";

@Entity()
export class UserPicture {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => User, user => user.picture)
    @JoinColumn()
    user: User;

    @Column({ unique: false, nullable: false })
    fileName: string;
}