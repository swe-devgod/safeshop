import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { Checkout } from "./checkout";
import { CheckoutItem } from "./checkout-item";

@Entity()
export class CheckoutItemStatus {
    @PrimaryGeneratedColumn({ type: 'int' })
    id: number;

    @Column({ type: 'text', nullable: false })
    description: string

    @OneToMany(type => CheckoutItem, items => items.status)
    checkouts: CheckoutItemStatus[];
}