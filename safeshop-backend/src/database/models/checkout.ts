import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, AfterRemove } from "typeorm";
import { User } from "./user";
import { CheckoutStatus } from "./checkout-status";
import { CheckoutItem } from "./checkout-item";
import { Address } from "./address";

@Entity()
export class Checkout {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'bigint', nullable: false })
    checkoutDate: number;

    @ManyToOne(type => User, user => user.checkouts)
    user: User;

    @Column({ type: 'int', nullable: true })
    statusId: number;

    @ManyToOne(type => CheckoutStatus, cs => cs.id)
    status: CheckoutStatus;

    @Column({ type: 'int', nullable: true })
    addressId: number;

    @ManyToOne(type => Address, address => address.checkouts)
    address: Address;

    @OneToMany(type => CheckoutItem, items => items.checkout)
    checkoutItems: CheckoutItem[];
}