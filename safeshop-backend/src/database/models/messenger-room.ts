import { Entity, PrimaryGeneratedColumn, Index, Column, ManyToMany, JoinTable, OneToMany } from "typeorm";
import { User } from "./user";
import { Message } from "./message";
import { RoomUsers } from "./room-users";

@Entity()
export class MessengerRoom { 
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column({ type: 'int', nullable: false })
    timestamp: number;

    @OneToMany(type => Message, message => message.room)
    messages: Message[];

    // @ManyToMany(type => User, user => user.messengerRooms)
    // @JoinTable()
    // users: User[];

    @OneToMany(type => RoomUsers, roomUser => roomUser.room)
    userRooms: RoomUsers[];
}