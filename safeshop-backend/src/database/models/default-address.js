"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("./user");
const address_1 = require("./address");
let DefaultAddress = class DefaultAddress {
};
__decorate([
    typeorm_1.OneToOne(type => user_1.User, user => user.defaultAddress),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_1.User)
], DefaultAddress.prototype, "user", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true, primary: true }),
    __metadata("design:type", Number)
], DefaultAddress.prototype, "userId", void 0);
__decorate([
    typeorm_1.OneToOne(type => address_1.Address),
    typeorm_1.JoinColumn(),
    __metadata("design:type", address_1.Address)
], DefaultAddress.prototype, "address", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true, primary: true }),
    __metadata("design:type", Number)
], DefaultAddress.prototype, "addressId", void 0);
DefaultAddress = __decorate([
    typeorm_1.Entity()
], DefaultAddress);
exports.DefaultAddress = DefaultAddress;
