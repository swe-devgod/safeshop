import {Entity, OneToOne, JoinColumn, RelationId, Column} from "typeorm";
import { User } from "./user";
import { Address } from "./address";

@Entity()
export class DefaultAddress {
    @OneToOne(type => User, user => user.defaultAddress)
    @JoinColumn()
    user: User;

    @Column({ type: 'int', nullable: true, primary: true })
    userId: number;

    @OneToOne(type => Address)
    @JoinColumn()
    address: Address;

    @Column({ type: 'int', nullable: true, primary: true })
    addressId: number;
}