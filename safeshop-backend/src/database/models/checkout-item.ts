import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, AfterRemove, OneToOne, JoinColumn } from "typeorm";
import { Topic } from "./topic";
import { Checkout } from "./checkout";
import { CheckoutItemStatus } from "./checkout-item-status";
import { TopicShipping } from "./topic-shipping";
import { CheckoutShipping } from "./checkout-shipping";
import { UserToUserVoting } from "./user-user-voting";

@Entity()
export class CheckoutItem {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'double', nullable: true })
    pricePerItem: number;

    @Column({ type: 'int', nullable: false })
    amount: number;

    @Column({ type: 'bigint', nullable: false })
    addedDate: number;

    @ManyToOne(type => Checkout, checkout => checkout)
    checkout: Checkout;

    @ManyToOne(type => Topic, topic => topic.carts)
    topic: Topic;
    
    @Column({ type: 'int', nullable: true })
    statusId: number;

    @ManyToOne(type => CheckoutItemStatus, cis => cis.id)
    status: CheckoutItemStatus;

    @OneToOne(type => CheckoutShipping, cs => cs.checkoutItem)
    shipping: CheckoutShipping

    @OneToMany(type => UserToUserVoting, utu => utu.checkoutItem)
    userToUserVotings: UserToUserVoting[];
}