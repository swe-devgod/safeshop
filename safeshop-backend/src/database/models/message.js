"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const messenger_room_1 = require("./messenger-room");
const user_1 = require("./user");
let Message = class Message {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Message.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], Message.prototype, "roomId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => messenger_room_1.MessengerRoom, room => room.messages),
    __metadata("design:type", messenger_room_1.MessengerRoom)
], Message.prototype, "room", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], Message.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_1.User, user => user.messages),
    __metadata("design:type", user_1.User)
], Message.prototype, "user", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column({ type: 'bigint', nullable: false }),
    __metadata("design:type", Number)
], Message.prototype, "timestamp", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Message.prototype, "message", void 0);
Message = __decorate([
    typeorm_1.Entity()
], Message);
exports.Message = Message;
