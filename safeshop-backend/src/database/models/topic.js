"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("./user");
const topic_comment_1 = require("./topic-comment");
const category_1 = require("./category");
const topic_pictures_1 = require("./topic-pictures");
const cart_1 = require("./cart");
const topic_shipping_1 = require("./topic-shipping");
const topic_thumbnail_1 = require("./topic-thumbnail");
const user_topic_voting_1 = require("./user-topic-voting");
let Topic = class Topic {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Topic.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: true }),
    __metadata("design:type", Number)
], Topic.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_1.User, user => user.topics),
    __metadata("design:type", user_1.User)
], Topic.prototype, "user", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column({ type: 'bigint', nullable: false }),
    __metadata("design:type", Object)
], Topic.prototype, "topicCreatedDate", void 0);
__decorate([
    typeorm_1.Column({ type: 'double', nullable: false, default: '0' }),
    __metadata("design:type", Number)
], Topic.prototype, "topicPrice", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', nullable: false, default: '0' }),
    __metadata("design:type", Number)
], Topic.prototype, "topicRemainingItems", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Topic.prototype, "topicName", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Topic.prototype, "topicDescription", void 0);
__decorate([
    typeorm_1.Column({ type: "int", nullable: false, default: '0' }),
    __metadata("design:type", Number)
], Topic.prototype, "topicVoteCount", void 0);
__decorate([
    typeorm_1.Column({ type: "double", nullable: false, default: '0' }),
    __metadata("design:type", Number)
], Topic.prototype, "topicRating", void 0);
__decorate([
    typeorm_1.OneToMany(type => topic_comment_1.TopicComment, comment => comment.topic),
    __metadata("design:type", Array)
], Topic.prototype, "comments", void 0);
__decorate([
    typeorm_1.OneToOne(type => topic_thumbnail_1.TopicThumbnail, thumbnail => thumbnail.topic),
    __metadata("design:type", topic_thumbnail_1.TopicThumbnail)
], Topic.prototype, "thumbnail", void 0);
__decorate([
    typeorm_1.OneToMany(type => topic_pictures_1.TopicPictures, pictures => pictures.topic),
    __metadata("design:type", Array)
], Topic.prototype, "pictures", void 0);
__decorate([
    typeorm_1.ManyToMany(type => category_1.Category, category => category.topics),
    typeorm_1.JoinTable(),
    __metadata("design:type", Array)
], Topic.prototype, "categories", void 0);
__decorate([
    typeorm_1.OneToMany(type => cart_1.Cart, cart => cart.user),
    __metadata("design:type", Array)
], Topic.prototype, "carts", void 0);
__decorate([
    typeorm_1.OneToMany(type => topic_shipping_1.TopicShipping, ts => ts.topic),
    __metadata("design:type", Array)
], Topic.prototype, "shippings", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_topic_voting_1.UserToTopicVoting, utt => utt.topic),
    __metadata("design:type", Array)
], Topic.prototype, "votingUsers", void 0);
Topic = __decorate([
    typeorm_1.Entity()
], Topic);
exports.Topic = Topic;
