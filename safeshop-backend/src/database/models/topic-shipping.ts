import { Entity, PrimaryColumn, ManyToOne, Column, OneToMany } from "typeorm";
import { Topic } from "./topic";
import { ShippingCarrier } from "./shipping-carrier";
import { CheckoutItem } from "./checkout-item";
import { CheckoutShipping } from "./checkout-shipping";

@Entity()
export class TopicShipping {
    @PrimaryColumn()
    id: number;

    @Column({ type: 'int', nullable: true })
    topicId: number;

    @ManyToOne(type => Topic, topic => topic.shippings)
    topic: Topic;

    @Column({ type: 'int', nullable: true })
    shippingId: number;

    @ManyToOne(type => ShippingCarrier, carrier => carrier.topics)
    shipping: ShippingCarrier;

    @Column({ type: 'int', nullable: false })
    price: number; 

    @OneToMany(type => CheckoutItem, item => item.shipping)
    checkoutItem: CheckoutItem[];

    @OneToMany(type => CheckoutShipping, cs => cs.shipping)
    checkoutShipping: CheckoutShipping[];
}