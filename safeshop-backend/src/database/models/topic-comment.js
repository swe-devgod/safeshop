"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var TopicComment_1;
const typeorm_1 = require("typeorm");
const topic_1 = require("./topic");
const user_1 = require("./user");
const comment_pictures_1 = require("./comment-pictures");
let TopicComment = TopicComment_1 = class TopicComment {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], TopicComment.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: "int", nullable: true }),
    __metadata("design:type", Number)
], TopicComment.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_1.User, user => user.comments),
    __metadata("design:type", user_1.User)
], TopicComment.prototype, "user", void 0);
__decorate([
    typeorm_1.Column({ type: "int", nullable: true }),
    __metadata("design:type", Number)
], TopicComment.prototype, "topicId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => topic_1.Topic, topic => topic.comments),
    __metadata("design:type", topic_1.Topic)
], TopicComment.prototype, "topic", void 0);
__decorate([
    typeorm_1.Index(),
    typeorm_1.Column({ type: 'bigint', nullable: false }),
    __metadata("design:type", Number)
], TopicComment.prototype, "createdDate", void 0);
__decorate([
    typeorm_1.Column({ nullable: false }),
    __metadata("design:type", String)
], TopicComment.prototype, "content", void 0);
__decorate([
    typeorm_1.OneToMany(type => comment_pictures_1.CommentPictures, pictures => pictures.comment),
    __metadata("design:type", Array)
], TopicComment.prototype, "pictures", void 0);
__decorate([
    typeorm_1.Column({ type: "int", nullable: true }),
    __metadata("design:type", Number)
], TopicComment.prototype, "parentCommentId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => TopicComment_1, comment => comment.childComments, { nullable: true }),
    __metadata("design:type", TopicComment)
], TopicComment.prototype, "parentComment", void 0);
__decorate([
    typeorm_1.OneToMany(type => TopicComment_1, category => category.parentComment, { nullable: true }),
    __metadata("design:type", Array)
], TopicComment.prototype, "childComments", void 0);
__decorate([
    typeorm_1.Column({ nullable: false, default: false }),
    __metadata("design:type", Boolean)
], TopicComment.prototype, "haveImage", void 0);
TopicComment = TopicComment_1 = __decorate([
    typeorm_1.Entity()
], TopicComment);
exports.TopicComment = TopicComment;
