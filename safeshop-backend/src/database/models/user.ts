import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, Index, OneToOne} from "typeorm";
import { Address } from "./address";
import { Topic } from "./topic";
import { TopicComment } from "./topic-comment";
import { MessengerRoom } from "./messenger-room";
import { Message } from "./message";
import { RoomUsers } from "./room-users";
import { Cart } from "./cart";
import { Checkout } from "./checkout";
import { UserPicture } from "./user-picture";
import { DefaultAddress } from "./default-address";
import { UserToUserVoting } from "./user-user-voting";
import { UserToTopicVoting } from "./user-topic-voting";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    firstName: string;

    @Column({ nullable: true })
    middleName: string;

    @Column({ nullable: false })
    lastName: string;

    @Column({ nullable: false })
    displayName: string

    @Column({ nullable: false })
    email: string;

    @Column({ nullable: false })
    tel: string;

    @Index()
    @Column({ type: 'bigint', nullable: false })
    registerDate: any;

    @Column({ type: "double", nullable: false, default: '0' })
    rating: number;

    @Column({ type: "int", nullable: false, default: '0' })
    ratingVoteCount: number;

    @OneToOne(type => UserPicture, picture => picture.user)
    picture: UserPicture

    @OneToOne(type => DefaultAddress, defaultAddr => defaultAddr.address)
    defaultAddress: DefaultAddress;

    @OneToMany(type => Address, address => address.user)
    addresses: Address[];

    @OneToMany(type => Topic, topic => topic.user)
    topics: Topic[];

    @OneToMany(type => TopicComment, comment => comment.user)
    comments: TopicComment[];

    @OneToMany(type => Message, message => message.user)
    messages: Message[];

    // @ManyToMany(type => MessengerRoom, room => room.users)
    // messengerRooms: MessengerRoom[];

    @OneToMany(type => RoomUsers, roomUser => roomUser.user)
    userRooms: RoomUsers[];

    @OneToMany(type => Cart, cart => cart.user)
    carts: Cart[];

    @OneToMany(type => Checkout, checkout => checkout.user)
    checkouts: Checkout[];

    @OneToMany(type => UserToUserVoting, utu => utu.srcUser)
    destVotingUsers: UserToUserVoting[];

    @OneToMany(type => UserToUserVoting, utu => utu.destUser)
    srcVotingUsers: UserToUserVoting[];
    
    @OneToMany(type => UserToTopicVoting, utt => utt.user)
    votingTopics: UserToUserVoting[];
}