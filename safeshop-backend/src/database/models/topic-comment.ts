import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, Index} from "typeorm";
import { Topic } from "./topic";
import { User } from "./user";
import { CommentPictures } from "./comment-pictures";

@Entity()
export class TopicComment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "int", nullable: true })
    userId: number;

    @ManyToOne(type => User, user => user.comments)
    user: User;

    @Column({ type: "int", nullable: true })
    topicId: number;

    @ManyToOne(type => Topic, topic => topic.comments)
    topic: Topic;

    // @RelationId((topicComment: TopicComment) => topicComment.topic)
    // topicId: number;

    @Index()
    @Column({ type: 'bigint', nullable: false })
    createdDate: number;

    @Column({ nullable: false })
    content: string;

    @OneToMany(type => CommentPictures, pictures => pictures.comment)
    pictures: CommentPictures[];

    @Column({ type: "int", nullable: true })
    parentCommentId: number;

    @ManyToOne(type => TopicComment, comment => comment.childComments, { nullable: true })
    parentComment: TopicComment;

    @OneToMany(type => TopicComment, category => category.parentComment, { nullable: true })
    childComments: TopicComment[];

    @Column({ nullable: false, default: false })
    haveImage: boolean;
}