import { Entity, ManyToOne, PrimaryGeneratedColumn, Column, PrimaryColumn } from "typeorm";
import { User } from "./user";
import { MessengerRoom } from "./messenger-room";

@Entity()
export class RoomUsers {
    // @PrimaryGeneratedColumn()
    // id: number;

    @Column({ type: 'int', primary: true })
    userId: number;

    @ManyToOne(type => User, user => user.userRooms)
    user: User;

    @Column({ type: 'int',  primary: true })
    roomId: number;

    @ManyToOne(type => MessengerRoom, room => room.userRooms)
    room: MessengerRoom;
}