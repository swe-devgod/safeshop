import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./user";
import { Topic } from "./topic";

@Entity()
export class UserToTopicVoting {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User)
    user: User;

    @ManyToOne(type => Topic)
    topic: Topic;

    @Column({ type: 'double', nullable: false })
    rating: number;
}