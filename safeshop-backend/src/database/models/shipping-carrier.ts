import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { TopicShipping } from "./topic-shipping";

@Entity()
export class ShippingCarrier {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text', nullable: false })
    name: string;

    @Column({ type: 'text', nullable: false })
    website: string;

    @OneToMany(type => TopicShipping, topic => topic.shipping)
    topics: TopicShipping[];
}