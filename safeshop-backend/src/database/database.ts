import "reflect-metadata";
import { createConnection, getConnection, getManager } from 'typeorm';
import { User } from './models/user';
import { Session } from "./models/session";
import { Address } from "./models/address";
import { Topic } from "./models/topic";
import { TopicComment } from "./models/topic-comment";
import { DefaultAddress } from "./models/default-address";
import { Category } from "./models/category";
import { CommentPictures } from "./models/comment-pictures";
import { TopicPictures } from "./models/topic-pictures";
import { UserPicture } from "./models/user-picture";
import { MessengerRoom } from "./models/messenger-room";
import { Message } from "./models/message";
import { RoomUsers } from "./models/room-users";
import { TopicThumbnail } from "./models/topic-thumbnail";
import { Cart } from "./models/cart";
import { Checkout } from "./models/checkout";
import { TopicShipping } from "./models/topic-shipping";
import { ShippingCarrier } from "./models/shipping-carrier";
import { CheckoutStatus } from "./models/checkout-status";
import { CheckoutItem } from "./models/checkout-item";
import { CheckoutItemStatus } from "./models/checkout-item-status";
import { CheckoutShipping } from "./models/checkout-shipping";
import { UserToUserVoting } from "./models/user-user-voting";
import { UserToTopicVoting } from "./models/user-topic-voting";

const entities = [
    User,
    Session,
    Address,
    Topic,
    TopicComment,
    DefaultAddress,
    Category,
    CommentPictures,
    UserPicture,
    TopicPictures,
    MessengerRoom,
    Message,
    RoomUsers,
    TopicThumbnail,
    Cart,
    Checkout,
    CheckoutStatus,
    CheckoutItem,
    CheckoutItemStatus,
    TopicShipping,
    ShippingCarrier,
    CheckoutShipping,
    UserToUserVoting,
    UserToTopicVoting
];

export class Database {
    static setupSQLiteDatabase() {
        return createConnection({
            type: 'sqlite',
            database: 'safeshop.sqlite',
            entities,
            synchronize: true
        });
    }

    static setupInMemorySQLiteDatabase() {
        return createConnection({
            type: 'sqlite',
            database: ':memory:',
            entities,
            synchronize: true
        });
    }
};