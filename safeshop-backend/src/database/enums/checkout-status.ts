export enum CheckoutStatusEnum {
    AwaitingPayment = 1,
    AwaitingSuccess = 2,
    Successful = 3,
    Cancel = 4
} 