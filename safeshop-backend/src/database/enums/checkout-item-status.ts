export enum CheckoutItemStatusEnum {
    AwaitingPayment = 1,
    AwaitingShipment = 2,
    Shipping = 3,
    Successful = 4,
    Cancel = 5
}