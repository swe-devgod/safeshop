"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CheckoutItemStatusEnum;
(function (CheckoutItemStatusEnum) {
    CheckoutItemStatusEnum[CheckoutItemStatusEnum["AwaitingPayment"] = 1] = "AwaitingPayment";
    CheckoutItemStatusEnum[CheckoutItemStatusEnum["AwaitingShipment"] = 2] = "AwaitingShipment";
    CheckoutItemStatusEnum[CheckoutItemStatusEnum["Shipping"] = 3] = "Shipping";
    CheckoutItemStatusEnum[CheckoutItemStatusEnum["Successful"] = 4] = "Successful";
    CheckoutItemStatusEnum[CheckoutItemStatusEnum["Cancel"] = 5] = "Cancel";
})(CheckoutItemStatusEnum = exports.CheckoutItemStatusEnum || (exports.CheckoutItemStatusEnum = {}));
