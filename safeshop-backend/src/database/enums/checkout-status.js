"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CheckoutStatusEnum;
(function (CheckoutStatusEnum) {
    CheckoutStatusEnum[CheckoutStatusEnum["AwaitingPayment"] = 1] = "AwaitingPayment";
    CheckoutStatusEnum[CheckoutStatusEnum["AwaitingSuccess"] = 2] = "AwaitingSuccess";
    CheckoutStatusEnum[CheckoutStatusEnum["Successful"] = 3] = "Successful";
    CheckoutStatusEnum[CheckoutStatusEnum["Cancel"] = 4] = "Cancel";
})(CheckoutStatusEnum = exports.CheckoutStatusEnum || (exports.CheckoutStatusEnum = {}));
