import { getManager, EntityManager } from "typeorm";
import { Checkout } from "../models/checkout";
import { GlobalVar } from "../../helper/global-var";
import { CheckoutItem } from "../models/checkout-item";
import { User } from "../models/user";
import { GetCheckoutInformation } from "../../routes/api/checkout/@/controllers/GetCheckoutInformation";
import { ClientURLHelper } from "../../helper/client-url-helper";

interface CheckoutItemShippingSchema {
    id: number;
    price: number;
    shipping: { id: number, name: string };
}

interface CheckoutItemTopicSchema {
    id: number,
    topicCreatedDate: number,
    topicPrice: number,
    topicRemainingItems: number,
    topicName: string,
    topicDescription: string,
    topicVoteCount: number,
    topicRating: number,
    thumbnail: string;
    shippings: CheckoutItemShippingSchema[];
}

interface CheckoutItemSchema {
    id: number;
    amount: number;
    addedDate: number;
    topic: CheckoutItemTopicSchema;
}

interface UserSchema {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    displayName: string;
    email: string;
    tel: string;
    registerDate: number;
    rating: number;
    ratingVoteCount: number;
    picture: string;
}

interface AddressSchema {
    id: number;
    address: string;
}

interface PaymentSchema {
    id: number;
    name: string;
}

interface GetCheckoutInformationResponseSchema {
    user: UserSchema;
    checkout: {
        checkoutDate: number,
        checkoutItems: CheckoutItemSchema[],
    },
    address: {
        defaultId: number,
        addresses: AddressSchema[];
    }
    paymentMethods: PaymentSchema[];
}

interface GetCheckoutInformationArgs {
    userId: number;
    checkoutId: number;
}

interface ICheckoutDatabase {
    getCheckoutInformation(args: GetCheckoutInformationArgs): Promise<GetCheckoutInformationResponseSchema>
}

export class CheckoutDatabase {
    static async getCheckoutInformation(args: GetCheckoutInformationArgs): Promise<GetCheckoutInformationResponseSchema> {
        const result = await getManager()
            .createQueryBuilder()
            .select([
                'A.id', 'A.checkoutDate',
                'B.id', 'B.amount', 'B.addedDate',
                'C.id', 'C.topicCreatedDate', 'C.topicPrice', 'C.topicRemainingItems',
                'C.topicName', 'C.topicDescription', 'C.topicVoteCount', 'C.topicRating',
                'D.topicPictureId', 'E.fileName',
                'F.id', 'F.price',
                'G.id', 'G.name',
                'I.addressId',
            ])
            .from(Checkout, 'A')
            .innerJoin('A.checkoutItems', 'B')
            .innerJoin('B.topic', 'C')
            .innerJoin('C.thumbnail', 'D')
            .innerJoin('D.topicPicture', 'E')
            .innerJoin('C.shippings', 'F')
            .innerJoin('F.shipping', 'G')
            .innerJoinAndSelect('A.user', 'H')
            .innerJoin('H.defaultAddress', 'I')
            .innerJoinAndSelect('H.addresses', 'J')
            .innerJoinAndSelect('H.picture', 'K')
            .where('A.id = :checkoutId', { checkoutId: args.checkoutId })
            .andWhere('A.userId = :userId', { userId: args.userId })
            .getOne();

        const { defaultAddress, addresses, picture, ...forUser } = result.user;
        result.checkoutItems = result.checkoutItems.map(item => {
            item.topic.thumbnail = ClientURLHelper.resolve(`/static/topics/${item.topic.id}/${item.topic.thumbnail.topicPicture.fileName}`) as any;
            return item;
        });

        return {
            user: { 
                ...forUser, 
                picture: ClientURLHelper.resolve(`/static/users/${picture.fileName}`)
            },
            address : {
                defaultId: defaultAddress.addressId,
                addresses: addresses
            },
            checkout: {
                checkoutDate: result.checkoutDate,
                checkoutItems: result.checkoutItems as any[]
            },
            paymentMethods: [
                { id: 1, name: 'PromptPay' }
            ]
        } as GetCheckoutInformationResponseSchema;
    }

    
    static async getCheckoutInformationNew(args: GetCheckoutInformationArgs): Promise<GetCheckoutInformationResponseSchema> {
        const result = await getManager().transaction(async tem => {

            const user = await tem.createQueryBuilder()
                .select('A')
                .from(User, 'A')
                .where('A.id = :id', { id: args.userId })
                .getOne();

            const checkout = await tem.createQueryBuilder()
                .select('A')
                .from(Checkout, 'A')
                .where('A.id = :checkoutId', { checkoutId: args.checkoutId })
                .andWhere('A.userId = :userId', { userId: args.userId })
                .getOne();

            const checkoutItems = await tem.createQueryBuilder()
                .select('A')
                .from(CheckoutItem, 'A')
                .where('A.checkoutId = :id', { id: args.checkoutId })
                .getMany();

            return {
                user: user as any,
                address: <any>{},
                checkout: <any>{},
                paymentMethods: <any>{}
            } as GetCheckoutInformationResponseSchema;
        });

        return result;
    }
}