import { getManager, Not } from "typeorm";
import { TopicComment } from "../models/topic-comment";
import { CommentPictures } from "../models/comment-pictures";
import { Topic } from "../models/topic";

export class CommentDatabase {

    static insertComment(comment: TopicComment) {
        return getManager().save(comment);
    }

    static findCommentsByTopicId(topicId: number): Promise<TopicComment[]> {
        return getManager().find(TopicComment, { 
            order: {
                createdDate: "ASC"
            },
            where: {
                topicId,
                parentCommentId: null
            }
        });
    }

    static findSubCommentsByComment(comment: TopicComment): Promise<TopicComment[]> {
        return getManager().find(TopicComment, { parentComment: comment });
    }

    static findPicturesByComment(comment: TopicComment): Promise<CommentPictures[]> {
        return getManager().find(CommentPictures, { comment });
    }
}