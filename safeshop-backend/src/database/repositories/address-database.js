"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const address_1 = require("../models/address");
const default_address_1 = require("../models/default-address");
class AddressDatabase {
    static insert(address) {
        return typeorm_1.getManager().save(address);
    }
    static sefDefaultDatabase(user, address) {
        const defaultAddress = new default_address_1.DefaultAddress();
        defaultAddress.user = user;
        defaultAddress.address = address;
        return typeorm_1.getManager().save(defaultAddress);
    }
    static findDefaultAddressByUserId(userId) {
        return typeorm_1.getManager().findOne(default_address_1.DefaultAddress, { userId });
    }
    static findAddressById(addressId) {
        return typeorm_1.getManager().findOne(address_1.Address, { id: addressId });
    }
}
exports.AddressDatabase = AddressDatabase;
