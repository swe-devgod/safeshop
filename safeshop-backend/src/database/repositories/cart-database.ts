import { getManager } from 'typeorm';
import { Cart } from '../models/cart';
import { GlobalVar } from '../../helper/global-var';
import { Checkout } from '../models/checkout';
import { ClientURLHelper } from '../../helper/client-url-helper';

interface GetCartListResponseSchema {
    id: number;
    amount: number;
    addedDate: number;                
    topic: {
        topicId: number;
        topicName: string;
        topicPrice: number;
        createdDate: number;
        topicRating: number;
        topicRatingCount: number;
        ownerUserId: number;
        ownerName: string;
        //ownerPicture: string;
        ownerRating: number;
        ownerRatingCount: number;
        remainingItems: number;
        thumbnail: string;     
    }         
}

interface FindCartItemsArgs {
    userId: number;
} 

export class CartDatabase {
    static async findCartItems(args: FindCartItemsArgs): Promise<GetCartListResponseSchema[]> {
        const cartItems = await getManager()
            .createQueryBuilder()
            .select('A')
            .from(Cart, 'A')
            //.innerJoinAndSelect('A.user', 'E')
            //.innerJoinAndSelect('E.picture', 'F')
            .innerJoinAndSelect('A.topic', 'B')
            .innerJoinAndSelect('B.user', 'E')
            .innerJoinAndSelect('B.thumbnail', 'C')
            .innerJoinAndSelect('C.topicPicture', 'D')
            .where('A.userId = :userId', { userId: args.userId })
            .orderBy('A.addedDate', 'DESC')
            .getMany();
        console.log(cartItems);
        return cartItems.map(item => {
            const topic = item.topic;
            //const user = item.user;

            return ({
                id: item.id,
                amount: item.amount,
                addedDate: item.addedDate,
                topic: {
                    topicId: topic.id,
                    topicName: topic.topicName,
                    topicPrice: topic.topicPrice,
                    createdDate: topic.topicCreatedDate,
                    topicRating: topic.topicRating,
                    topicRatingCount: topic.topicRating,
                    ownerUserId: topic.user.id,
                    ownerName: topic.user.displayName,
                    //ownerPicture: ClientURLHelper.resolve('/static/users/' + user.picture.fileName)
                    ownerRating: topic.user.rating,
                    ownerRatingCount: topic.user.ratingVoteCount,
                    remainingItems: topic.topicRemainingItems,
                    thumbnail: ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail.topicPicture.fileName}`)
                }
            });
        }) as GetCartListResponseSchema[];
    }
}