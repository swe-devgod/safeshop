import { getManager } from 'typeorm';
import { User } from '../models/user';
import { UserPicture } from '../models/user-picture';

export class UserDatabase {
    static insert(user: User) {
        return getManager().save(user);
    }

    static findByUserId(id) {
        return getManager().findOne(User, id);
    }

    static async fincUserPictureByUserId(id: number): Promise<UserPicture> {
        const picture = await getManager().findOne(UserPicture, { id });
        if (!picture) {
            return <UserPicture>{ fileName: 'default.png' };
        }
        return picture;
    }
};