"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const topic_1 = require("../models/topic");
const topic_pictures_1 = require("../models/topic-pictures");
;
class TopicDatabase {
    static findTopicById(topicId, options) {
        const innerOptions = {};
        if (options.withCategories) {
            innerOptions.relations = ["categories"];
        }
        return typeorm_1.getManager().findOne(topic_1.Topic, topicId, innerOptions);
    }
    static findPicturesByTopic(topic) {
        return typeorm_1.getManager().find(topic_pictures_1.TopicPictures, { topic });
    }
}
exports.TopicDatabase = TopicDatabase;
