"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("../models/user");
const user_picture_1 = require("../models/user-picture");
class UserDatabase {
    static insert(user) {
        return typeorm_1.getManager().save(user);
    }
    static findByUserId(id) {
        return typeorm_1.getManager().findOne(user_1.User, id);
    }
    static fincUserPictureByUserId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const picture = yield typeorm_1.getManager().findOne(user_picture_1.UserPicture, { id });
            if (!picture) {
                return { fileName: 'default.png' };
            }
            return picture;
        });
    }
}
exports.UserDatabase = UserDatabase;
;
