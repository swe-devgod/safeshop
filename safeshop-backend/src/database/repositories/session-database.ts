import { getManager } from 'typeorm';
import { Session } from '../models/session';

export class SessionDatabase {
    static insert(session: Session) {
        return getManager().save(session);
    }

    static findByUsername(username: string) {
        return getManager().findOne(Session, { username: username });
    }

    static findByUserId(userId) {
        return getManager().findOne(Session, { userId: userId });
    }
};