"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const cart_1 = require("../models/cart");
const client_url_helper_1 = require("../../helper/client-url-helper");
class CartDatabase {
    static findCartItems(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const cartItems = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select('A')
                .from(cart_1.Cart, 'A')
                //.innerJoinAndSelect('A.user', 'E')
                //.innerJoinAndSelect('E.picture', 'F')
                .innerJoinAndSelect('A.topic', 'B')
                .innerJoinAndSelect('B.user', 'E')
                .innerJoinAndSelect('B.thumbnail', 'C')
                .innerJoinAndSelect('C.topicPicture', 'D')
                .where('A.userId = :userId', { userId: args.userId })
                .orderBy('A.addedDate', 'DESC')
                .getMany();
            console.log(cartItems);
            return cartItems.map(item => {
                const topic = item.topic;
                //const user = item.user;
                return ({
                    id: item.id,
                    amount: item.amount,
                    addedDate: item.addedDate,
                    topic: {
                        topicId: topic.id,
                        topicName: topic.topicName,
                        topicPrice: topic.topicPrice,
                        createdDate: topic.topicCreatedDate,
                        topicRating: topic.topicRating,
                        topicRatingCount: topic.topicRating,
                        ownerUserId: topic.user.id,
                        ownerName: topic.user.displayName,
                        //ownerPicture: ClientURLHelper.resolve('/static/users/' + user.picture.fileName)
                        ownerRating: topic.user.rating,
                        ownerRatingCount: topic.user.ratingVoteCount,
                        remainingItems: topic.topicRemainingItems,
                        thumbnail: client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${topic.id}/${topic.thumbnail.topicPicture.fileName}`)
                    }
                });
            });
        });
    }
}
exports.CartDatabase = CartDatabase;
