"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const topic_comment_1 = require("../models/topic-comment");
const comment_pictures_1 = require("../models/comment-pictures");
class CommentDatabase {
    static insertComment(comment) {
        return typeorm_1.getManager().save(comment);
    }
    static findCommentsByTopicId(topicId) {
        return typeorm_1.getManager().find(topic_comment_1.TopicComment, {
            order: {
                createdDate: "ASC"
            },
            where: {
                topicId,
                parentCommentId: null
            }
        });
    }
    static findSubCommentsByComment(comment) {
        return typeorm_1.getManager().find(topic_comment_1.TopicComment, { parentComment: comment });
    }
    static findPicturesByComment(comment) {
        return typeorm_1.getManager().find(comment_pictures_1.CommentPictures, { comment });
    }
}
exports.CommentDatabase = CommentDatabase;
