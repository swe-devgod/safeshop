import { getManager } from "typeorm";
import { Topic } from "../models/topic";
import { TopicPictures } from "../models/topic-pictures";

interface FindByTopicIdOptions {
    withCategories: boolean
};

export class TopicDatabase {
    static findTopicById(topicId: number, options?: FindByTopicIdOptions): Promise<Topic> {
        const innerOptions = {} as any;
        if (options.withCategories) {
            innerOptions.relations = [ "categories" ]
        }
        return getManager().findOne(Topic, topicId, innerOptions);
    }

    static findPicturesByTopic(topic: Topic): Promise<TopicPictures[]> {
        return getManager().find(TopicPictures, { topic })
    }
}