"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const session_1 = require("../models/session");
class SessionDatabase {
    static insert(session) {
        return typeorm_1.getManager().save(session);
    }
    static findByUsername(username) {
        return typeorm_1.getManager().findOne(session_1.Session, { username: username });
    }
    static findByUserId(userId) {
        return typeorm_1.getManager().findOne(session_1.Session, { userId: userId });
    }
}
exports.SessionDatabase = SessionDatabase;
;
