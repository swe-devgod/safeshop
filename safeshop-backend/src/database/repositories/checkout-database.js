"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const checkout_1 = require("../models/checkout");
const checkout_item_1 = require("../models/checkout-item");
const user_1 = require("../models/user");
const client_url_helper_1 = require("../../helper/client-url-helper");
class CheckoutDatabase {
    static getCheckoutInformation(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield typeorm_1.getManager()
                .createQueryBuilder()
                .select([
                'A.id', 'A.checkoutDate',
                'B.id', 'B.amount', 'B.addedDate',
                'C.id', 'C.topicCreatedDate', 'C.topicPrice', 'C.topicRemainingItems',
                'C.topicName', 'C.topicDescription', 'C.topicVoteCount', 'C.topicRating',
                'D.topicPictureId', 'E.fileName',
                'F.id', 'F.price',
                'G.id', 'G.name',
                'I.addressId',
            ])
                .from(checkout_1.Checkout, 'A')
                .innerJoin('A.checkoutItems', 'B')
                .innerJoin('B.topic', 'C')
                .innerJoin('C.thumbnail', 'D')
                .innerJoin('D.topicPicture', 'E')
                .innerJoin('C.shippings', 'F')
                .innerJoin('F.shipping', 'G')
                .innerJoinAndSelect('A.user', 'H')
                .innerJoin('H.defaultAddress', 'I')
                .innerJoinAndSelect('H.addresses', 'J')
                .innerJoinAndSelect('H.picture', 'K')
                .where('A.id = :checkoutId', { checkoutId: args.checkoutId })
                .andWhere('A.userId = :userId', { userId: args.userId })
                .getOne();
            const _a = result.user, { defaultAddress, addresses, picture } = _a, forUser = __rest(_a, ["defaultAddress", "addresses", "picture"]);
            result.checkoutItems = result.checkoutItems.map(item => {
                item.topic.thumbnail = client_url_helper_1.ClientURLHelper.resolve(`/static/topics/${item.topic.id}/${item.topic.thumbnail.topicPicture.fileName}`);
                return item;
            });
            return {
                user: Object.assign({}, forUser, { picture: client_url_helper_1.ClientURLHelper.resolve(`/static/users/${picture.fileName}`) }),
                address: {
                    defaultId: defaultAddress.addressId,
                    addresses: addresses
                },
                checkout: {
                    checkoutDate: result.checkoutDate,
                    checkoutItems: result.checkoutItems
                },
                paymentMethods: [
                    { id: 1, name: 'PromptPay' }
                ]
            };
        });
    }
    static getCheckoutInformationNew(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield typeorm_1.getManager().transaction((tem) => __awaiter(this, void 0, void 0, function* () {
                const user = yield tem.createQueryBuilder()
                    .select('A')
                    .from(user_1.User, 'A')
                    .where('A.id = :id', { id: args.userId })
                    .getOne();
                const checkout = yield tem.createQueryBuilder()
                    .select('A')
                    .from(checkout_1.Checkout, 'A')
                    .where('A.id = :checkoutId', { checkoutId: args.checkoutId })
                    .andWhere('A.userId = :userId', { userId: args.userId })
                    .getOne();
                const checkoutItems = yield tem.createQueryBuilder()
                    .select('A')
                    .from(checkout_item_1.CheckoutItem, 'A')
                    .where('A.checkoutId = :id', { id: args.checkoutId })
                    .getMany();
                return {
                    user: user,
                    address: {},
                    checkout: {},
                    paymentMethods: {}
                };
            }));
            return result;
        });
    }
}
exports.CheckoutDatabase = CheckoutDatabase;
