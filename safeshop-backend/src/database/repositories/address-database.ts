import { getManager } from 'typeorm';
import { Address } from "../models/address";
import { DefaultAddress } from "../models/default-address";
import { User } from '../models/user';

export class AddressDatabase {
    static insert(address: Address) {
        return getManager().save(address);
    }    
    
    static sefDefaultDatabase(user: User, address: Address) {
        const defaultAddress = new DefaultAddress();
        defaultAddress.user = user;
        defaultAddress.address = address;
        return getManager().save(defaultAddress);
    }

    static findDefaultAddressByUserId(userId: number) {
        return getManager().findOne(DefaultAddress, { userId });
    }

    static findAddressById(addressId: number) {
        return getManager().findOne(Address, { id: addressId });
    }
}