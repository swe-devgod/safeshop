"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const user_1 = require("./models/user");
const session_1 = require("./models/session");
const address_1 = require("./models/address");
const topic_1 = require("./models/topic");
const topic_comment_1 = require("./models/topic-comment");
const default_address_1 = require("./models/default-address");
const category_1 = require("./models/category");
const comment_pictures_1 = require("./models/comment-pictures");
const topic_pictures_1 = require("./models/topic-pictures");
const user_picture_1 = require("./models/user-picture");
const messenger_room_1 = require("./models/messenger-room");
const message_1 = require("./models/message");
const room_users_1 = require("./models/room-users");
const topic_thumbnail_1 = require("./models/topic-thumbnail");
const cart_1 = require("./models/cart");
const checkout_1 = require("./models/checkout");
const topic_shipping_1 = require("./models/topic-shipping");
const shipping_carrier_1 = require("./models/shipping-carrier");
const checkout_status_1 = require("./models/checkout-status");
const checkout_item_1 = require("./models/checkout-item");
const checkout_item_status_1 = require("./models/checkout-item-status");
const checkout_shipping_1 = require("./models/checkout-shipping");
const user_user_voting_1 = require("./models/user-user-voting");
const user_topic_voting_1 = require("./models/user-topic-voting");
const entities = [
    user_1.User,
    session_1.Session,
    address_1.Address,
    topic_1.Topic,
    topic_comment_1.TopicComment,
    default_address_1.DefaultAddress,
    category_1.Category,
    comment_pictures_1.CommentPictures,
    user_picture_1.UserPicture,
    topic_pictures_1.TopicPictures,
    messenger_room_1.MessengerRoom,
    message_1.Message,
    room_users_1.RoomUsers,
    topic_thumbnail_1.TopicThumbnail,
    cart_1.Cart,
    checkout_1.Checkout,
    checkout_status_1.CheckoutStatus,
    checkout_item_1.CheckoutItem,
    checkout_item_status_1.CheckoutItemStatus,
    topic_shipping_1.TopicShipping,
    shipping_carrier_1.ShippingCarrier,
    checkout_shipping_1.CheckoutShipping,
    user_user_voting_1.UserToUserVoting,
    user_topic_voting_1.UserToTopicVoting
];
class Database {
    static setupSQLiteDatabase() {
        return typeorm_1.createConnection({
            type: 'sqlite',
            database: 'safeshop.sqlite',
            entities,
            synchronize: true
        });
    }
    static setupInMemorySQLiteDatabase() {
        return typeorm_1.createConnection({
            type: 'sqlite',
            database: ':memory:',
            entities,
            synchronize: true
        });
    }
}
exports.Database = Database;
;
