import { GlobalVar } from "./global-var";

export class ClientURLHelper {
    static resolve(path: string): string {
        return (GlobalVar.isDevelopment()) ? "http://localhost:3001" + path : path;
    }
}