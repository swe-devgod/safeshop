"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RatingValidator {
    static isValidValue(rating) {
        return rating >= 0 && rating <= 5;
    }
}
exports.RatingValidator = RatingValidator;
