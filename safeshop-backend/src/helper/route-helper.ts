export function attachRoutesToParent(from, to) {
    from.routes.forEach(route => {
        to.routes.push({
            path: `/${from.name}${route.path}`,
            method: route.method,
            action: route.action
        });
    });
}

export function appendRoutes(from, to) {
    from.routes.forEach(route => {
        to.routes.push({
            path: `${route.path}`,
            method: route.method,
            action: route.action
        });
    });
}

