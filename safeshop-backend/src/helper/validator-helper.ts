import * as validator from 'validator';

export class  ValidatorHelper {

    private static readonly USERNAME_LENGTH = 5;
    private static readonly PASSWORD_LENGTH = 8;
    private static readonly DISPLAYNAME_LENGTH = 5;

    static checkEmpty(str: string): boolean {
        return validator.isEmpty(str);
    }

    static checkNotEmpty(str: string) {
        return !!!this.checkEmpty(str);
    }

    static checkUsername(username: string): boolean {
        return validator.isLength(username, this.USERNAME_LENGTH) && validator.isAlphanumeric(username);
    }

    static checkPassword(password: string): boolean {
        return validator.isLength(password, this.PASSWORD_LENGTH) && validator.isAlphanumeric(password);
    }

    static checkFirstName(firstName: string): boolean {
        return validator.isAlpha(firstName);
    }

    static checkMiddleName(middleName: string): boolean {
        return validator.isAlpha(middleName);
    }

    static checkLastName(lastName: string): boolean {
        return validator.isAlpha(lastName);
    }

    static checkDisplayName(displayName: string): boolean {
        return validator.isLength(displayName, this.DISPLAYNAME_LENGTH) && validator.isAlphanumeric(displayName);
    }

    static checkEmail(email: string): boolean {
        return validator.isEmail(email);
    }

    static checkTel(tel: string): boolean {
        const plusIndex = tel.lastIndexOf('+');
        if (plusIndex == -1 || plusIndex == 0) {
            return validator.isWhitelisted(tel, "0123456789+");
        } else {
            return false;
        }
    }

    static isNumeric(numeric: any) {
        return !!!this.isNullOrUndefined(numeric) && ((typeof numeric == 'number') || validator.isNumeric(numeric));
    }

    static isNullOrUndefined(obj): boolean {
        return obj == null || obj == undefined;
    }

    static default(obj: any, value: any): any {
        return (obj == null || obj == undefined) ? value : obj;
    }

    static checkCommentId(commentId: string | number): boolean {
        if (typeof commentId == 'number') {
            return true;
        }
        return validator.isNumeric(commentId);
    }
}