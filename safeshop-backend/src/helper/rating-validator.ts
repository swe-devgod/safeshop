export class RatingValidator {
    static isValidValue(rating: number): boolean {
        return rating >= 0 && rating <= 5;
    }
}