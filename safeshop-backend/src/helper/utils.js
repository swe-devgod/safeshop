"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
exports.getRandomInt = getRandomInt;
function isSubset(parent, child, getParent, getChild) {
    if (parent == null || child == null) {
        return false;
    }
    for (let i = 0; i < child.length; i++) {
        let isMatch = false;
        for (let j = 0; j < parent.length; j++) {
            if (getChild(child[i]) == getParent(parent[j])) {
                isMatch = true;
                break;
            }
        }
        if (!!!isMatch) {
            return false;
        }
    }
    return true;
}
exports.isSubset = isSubset;
