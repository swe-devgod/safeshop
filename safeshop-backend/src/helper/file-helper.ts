import * as fs from 'fs';

export class FileHelper {
    static move(src: fs.PathLike, dest: fs.PathLike): Promise<void> {
        return new Promise(function(resolve, reject) {
            fs.rename(src, dest, function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        });
    }
    
    static copy(src: fs.PathLike, dest: fs.PathLike): Promise<void> {
        return new Promise(function(resolve, reject) {
            fs.copyFile(src, dest, function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        });
    }
    
    static exists(path: fs.PathLike): Promise<boolean> {
        return new Promise(function(resolve, reject) {
            fs.exists(path, function(isExists) {
                resolve(isExists);
            });
        });
    }
    
    static mkdir(path: fs.PathLike): Promise<void> {
        return new Promise(function(resolve, reject) {
            fs.mkdir(path, function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    static unlink(path: fs.PathLike): Promise<void> {
        return new Promise(function(resolve, reject) {
            fs.unlink(path, function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }
}