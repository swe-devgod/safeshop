"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DateTimeHelper {
    static getCurrentTimestamp() {
        return new Date().getTime();
    }
}
exports.DateTimeHelper = DateTimeHelper;
