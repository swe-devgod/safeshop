"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const global_var_1 = require("./global-var");
class ClientURLHelper {
    static resolve(path) {
        return (global_var_1.GlobalVar.isDevelopment()) ? "http://localhost:3001" + path : path;
    }
}
exports.ClientURLHelper = ClientURLHelper;
