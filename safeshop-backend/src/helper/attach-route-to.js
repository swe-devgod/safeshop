"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function attachRoutesToParent(from, to) {
    from.routes.forEach(route => {
        to.routes.push({
            path: `/${from.name}${route.path}`,
            method: route.method,
            action: route.action
        });
    });
}
exports.attachRoutesToParent = attachRoutesToParent;
function attachRoutes(from, to) {
    from.routes.forEach(route => {
        to.routes.push({
            path: `${route.path}`,
            method: route.method,
            action: route.action
        });
    });
}
exports.attachRoutes = attachRoutes;
