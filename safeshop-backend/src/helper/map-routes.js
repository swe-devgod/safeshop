"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function mapRoutes(router, routes) {
    routes.routes.forEach(route => {
        router[route.method](`/${routes.name}${route.path}`, (req, res, next) => {
            route.action(req, res, next).then(() => next).catch(err => next(err));
        });
    });
}
exports.mapRoutes = mapRoutes;
