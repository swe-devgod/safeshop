"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
class FileHelper {
    static move(src, dest) {
        return new Promise(function (resolve, reject) {
            fs.rename(src, dest, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    }
    static copy(src, dest) {
        return new Promise(function (resolve, reject) {
            fs.copyFile(src, dest, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    }
    static exists(path) {
        return new Promise(function (resolve, reject) {
            fs.exists(path, function (isExists) {
                resolve(isExists);
            });
        });
    }
    static mkdir(path) {
        return new Promise(function (resolve, reject) {
            fs.mkdir(path, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    }
    static unlink(path) {
        return new Promise(function (resolve, reject) {
            fs.unlink(path, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    }
}
exports.FileHelper = FileHelper;
