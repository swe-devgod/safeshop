"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GlobalVar {
    static isProduction() {
        return process.env.NODE_ENV == 'production';
    }
    static isDevelopment() {
        return !!!this.isProduction();
    }
}
exports.GlobalVar = GlobalVar;
