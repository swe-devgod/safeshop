export class DateTimeHelper {
    static getCurrentTimestamp(): number {
        return new Date().getTime();
    }
}