export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function isSubset<U, V>(parent: U[], child: V[], getParent: (p: U) => any, getChild: (c: V) => any) {
    if (parent == null || child == null) {
        return false;
    }

    for (let i = 0; i < child.length; i++) {
        let isMatch = false;
        for (let j = 0; j < parent.length; j++) {
            if (getChild(child[i]) == getParent(parent[j])) {
                isMatch = true;
                break;
            }
        }
        if (!!!isMatch) {
            return false;
        }
    }
    return true;
}