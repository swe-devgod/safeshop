"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validator = require("validator");
class ValidatorHelper {
    static checkEmpty(str) {
        return validator.isEmpty(str);
    }
    static checkNotEmpty(str) {
        return !!!this.checkEmpty(str);
    }
    static checkUsername(username) {
        return validator.isLength(username, this.USERNAME_LENGTH) && validator.isAlphanumeric(username);
    }
    static checkPassword(password) {
        return validator.isLength(password, this.PASSWORD_LENGTH) && validator.isAlphanumeric(password);
    }
    static checkFirstName(firstName) {
        return validator.isAlpha(firstName);
    }
    static checkMiddleName(middleName) {
        return validator.isAlpha(middleName);
    }
    static checkLastName(lastName) {
        return validator.isAlpha(lastName);
    }
    static checkDisplayName(displayName) {
        return validator.isLength(displayName, this.DISPLAYNAME_LENGTH) && validator.isAlphanumeric(displayName);
    }
    static checkEmail(email) {
        return validator.isEmail(email);
    }
    static checkTel(tel) {
        const plusIndex = tel.lastIndexOf('+');
        if (plusIndex == -1 || plusIndex == 0) {
            return validator.isWhitelisted(tel, "0123456789+");
        }
        else {
            return false;
        }
    }
    static isNumeric(numeric) {
        return !!!this.isNullOrUndefined(numeric) && ((typeof numeric == 'number') || validator.isNumeric(numeric));
    }
    static isNullOrUndefined(obj) {
        return obj == null || obj == undefined;
    }
    static default(obj, value) {
        return (obj == null || obj == undefined) ? value : obj;
    }
    static checkCommentId(commentId) {
        if (typeof commentId == 'number') {
            return true;
        }
        return validator.isNumeric(commentId);
    }
}
ValidatorHelper.USERNAME_LENGTH = 5;
ValidatorHelper.PASSWORD_LENGTH = 8;
ValidatorHelper.DISPLAYNAME_LENGTH = 5;
exports.ValidatorHelper = ValidatorHelper;
