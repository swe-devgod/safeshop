"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcryptjs");
const session_database_1 = require("../database/repositories/session-database");
const LocalStrategy = require('passport-local').Strategy;
class PassportConfig {
    static config(passport) {
        passport.use(new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        }, function (username, password, done) {
            //console.log(`passport: ${username} ${password}`)
            // query session from database
            // const session = await SessionDatabase.findByUsername(username);
            // if (session != undefined) {
            //     try {
            //         const hash = await bcrypt.compare(password, session.password);
            //     } catch {
            //     }
            // }
            session_database_1.SessionDatabase.findByUsername(username).then(session => {
                if (session) {
                    bcrypt.compare(password, session.password, function (err, res) {
                        if (err) {
                            return done(err);
                        }
                        else {
                            if (res) {
                                return done(null, session);
                            }
                            else {
                                return done(null, false, { message: 'Incorrect password.' });
                            }
                        }
                    });
                }
                else {
                    return done(null, false, { message: 'Incorrect username.' });
                }
            });
        }));
        passport.serializeUser(function (session, done) {
            //console.log('serialize: ' + session.userId);
            done(null, session.userId);
        });
        passport.deserializeUser(function (id, done) {
            //console.log('deserialize: ' + id);
            done(null, { id });
        });
    }
}
exports.PassportConfig = PassportConfig;
