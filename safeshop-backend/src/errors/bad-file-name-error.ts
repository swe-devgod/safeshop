import { BadRequestError } from "./base/bad-request-error";

export class BadFileNameError extends BadRequestError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(message, data, description);
    }
}