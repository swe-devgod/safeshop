import { UnauthorizedError } from "./base/unauthorized-error";

export class NoSessionFoundError extends UnauthorizedError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(message, data, description);
    }
}