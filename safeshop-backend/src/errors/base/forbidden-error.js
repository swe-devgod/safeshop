"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_error_1 = require("./app-error");
class ForbiddenError extends app_error_1.AppError {
    constructor(message, data, description) {
        super(403, message || 'Forbidden', data, description);
    }
}
exports.ForbiddenError = ForbiddenError;
