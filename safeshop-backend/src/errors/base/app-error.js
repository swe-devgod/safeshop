"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppError extends Error {
    constructor(status, message, data, description) {
        super((typeof message == 'string' ? message : JSON.stringify(message)) || 'Internal Server Error');
        Error.captureStackTrace(this, this.constructor);
        this.name = this.constructor.name;
        this.status = status || 500;
        this.data = data;
        this.description = description;
    }
}
exports.AppError = AppError;
