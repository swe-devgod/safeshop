import { AppError } from "./app-error";

export class NotImplementedError extends AppError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(501, message || 'Not Implemented', data, description);
    }
}