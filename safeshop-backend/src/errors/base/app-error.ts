export class AppError extends Error {
    status: number;
    data: any;
    description: string;

    constructor(status?: number, message?: string | object, data?: any, description?: string) {
        super((typeof message == 'string' ? message : JSON.stringify(message)) || 'Internal Server Error');
        Error.captureStackTrace(this, this.constructor);
        this.name = this.constructor.name;
        this.status = status || 500;
        this.data = data;
        this.description = description;
    }
}