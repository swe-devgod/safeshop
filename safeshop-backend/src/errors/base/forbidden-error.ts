import { AppError } from "./app-error";

export class ForbiddenError extends AppError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(403, message || 'Forbidden', data, description);
    }
}