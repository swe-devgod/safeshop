"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_error_1 = require("./app-error");
class NotImplementedError extends app_error_1.AppError {
    constructor(message, data, description) {
        super(501, message || 'Not Implemented', data, description);
    }
}
exports.NotImplementedError = NotImplementedError;
