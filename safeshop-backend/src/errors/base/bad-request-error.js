"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_error_1 = require("./app-error");
class BadRequestError extends app_error_1.AppError {
    constructor(message, data, description) {
        super(400, message || 'Bad Request', data, description);
    }
}
exports.BadRequestError = BadRequestError;
