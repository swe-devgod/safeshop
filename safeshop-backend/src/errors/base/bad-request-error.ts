import { AppError } from "./app-error";

export class BadRequestError extends AppError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(400, message || 'Bad Request', data, description);
    }
}