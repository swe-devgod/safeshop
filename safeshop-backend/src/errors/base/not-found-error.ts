import { AppError } from "./app-error";

export class NotFoundError extends AppError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(404, message || 'Not Found', data, description);
    }
}