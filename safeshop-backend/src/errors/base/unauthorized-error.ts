import { AppError } from "./app-error";

export class UnauthorizedError extends AppError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(401, message || 'Unauthorized', data, description);
    }
}