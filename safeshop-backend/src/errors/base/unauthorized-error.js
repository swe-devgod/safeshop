"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_error_1 = require("./app-error");
class UnauthorizedError extends app_error_1.AppError {
    constructor(message, data, description) {
        super(401, message || 'Unauthorized', data, description);
    }
}
exports.UnauthorizedError = UnauthorizedError;
