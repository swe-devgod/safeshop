"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_error_1 = require("./app-error");
class NotFoundError extends app_error_1.AppError {
    constructor(message, data, description) {
        super(404, message || 'Not Found', data, description);
    }
}
exports.NotFoundError = NotFoundError;
