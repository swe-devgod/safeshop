"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const not_found_error_1 = require("./base/not-found-error");
class TopicNotFoundError extends not_found_error_1.NotFoundError {
    constructor(message, data, description) {
        super(message, data, description);
    }
}
exports.TopicNotFoundError = TopicNotFoundError;
