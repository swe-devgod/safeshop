"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bad_request_error_1 = require("./base/bad-request-error");
class BadFileNameError extends bad_request_error_1.BadRequestError {
    constructor(message, data, description) {
        super(message, data, description);
    }
}
exports.BadFileNameError = BadFileNameError;
