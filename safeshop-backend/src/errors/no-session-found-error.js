"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const unauthorized_error_1 = require("./base/unauthorized-error");
class NoSessionFoundError extends unauthorized_error_1.UnauthorizedError {
    constructor(message, data, description) {
        super(message, data, description);
    }
}
exports.NoSessionFoundError = NoSessionFoundError;
