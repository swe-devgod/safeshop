import { NotFoundError } from "./base/not-found-error";

export class TopicNotFoundError extends NotFoundError {
    constructor(message?: string | object, data?: any, description?: string) {
        super(message, data, description);
    }
}