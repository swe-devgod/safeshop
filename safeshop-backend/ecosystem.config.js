module.exports = {
    apps: [
        {
            name: "safeshop",
            script: "./bin/www",
            watch: false,
            env_production: {
                "PORT": "3002",
                "NODE_ENV": "production"
            }
        }
    ]
};