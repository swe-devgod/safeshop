"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compression = require('compression');
// for Authentication
const session = require('express-session');
const passport = require('passport');
// for Session Storage
const SQLiteStore = require('connect-sqlite3')(session);
const api_routes_1 = require("./src/routes/api/api-routes");
const database_1 = require("./src/database/database");
const passport_config_1 = require("./src/authenticate/passport-config");
const not_found_error_1 = require("./src/errors/base/not-found-error");
const app = express();
module.exports = app;
const dbSetupFunc = typeof global["it"] == 'function' ? database_1.Database.setupInMemorySQLiteDatabase : database_1.Database.setupSQLiteDatabase;
module.exports.ready = dbSetupFunc().then(function () {
    const sessionOptions = {
        name: 'ssid',
        secret: 'thisissafeshopsecret',
        resave: false,
        saveUninitialized: false,
        cookie: {
            path: '/',
            httpOnly: true,
            secure: false,
            maxAge: null,
        },
        store: new SQLiteStore({
            table: 'sessions',
            db: 'safeshop.session.sqlite',
            dir: '.'
        })
    };
    app.use(logger('dev'));
    //app.use(require('express-status-monitor')());
    app.use(compression());
    app.use(express.static(path.join(__dirname, 'public/react')));
    app.use(express.static(path.join(__dirname, 'public/asserts')));
    app.use((req, res, next) => {
        req.timestamp = new Date().getTime();
        next();
    });
    app.use(bodyParser.raw({
        type: 'application/octet-stream',
        limit: 256 * 1024
    }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser('thisissafeshopsecret'));
    app.use(session(sessionOptions));
    app.use(passport.initialize());
    app.use(passport.session());
    // config passport
    passport_config_1.PassportConfig.config(passport);
    // Backend
    app.use('/api', api_routes_1.API_ROUTER, function (req, res, next) {
        return next(new not_found_error_1.NotFoundError());
    });
    // For React Application (Front-End)
    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname, 'public/react/index.html'));
    });
    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        next(createError(404));
    });
    // error handler
    app.use(function (err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') == 'development' ? err : {};
        console.log(`${err.status} ${err.message} [${err.name}]: ${err.description ? err.description : 'Default Error Handling'}`);
        if (!!!err.data) {
            res.status(err.status || 500).end(err.message);
        }
        else {
            res.status(err.status || 500).json(err.data);
        }
    });
    console.log('Database Connection Ready !');
});
